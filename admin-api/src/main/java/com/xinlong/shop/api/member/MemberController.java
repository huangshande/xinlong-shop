package com.xinlong.shop.api.member;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.address.service.IMemberAddressService;
import com.xinlong.shop.core.member.entity.dto.MemberDTO;
import com.xinlong.shop.core.member.entity.dto.MemberListDTO;
import com.xinlong.shop.core.member.entity.dto.MemberSearchDTO;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;

/**
 * <p>
 * 会员 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@RestController
@Api(tags = "会员管理")
@RequestMapping("/member/member")
public class MemberController {

    private final IMemberService memberService;
    private final IMemberAddressService memberAddressService;

    public MemberController(IMemberService memberService, IMemberAddressService memberAddressService) {
        this.memberService = memberService;
        this.memberAddressService = memberAddressService;
    }

    @ApiOperation(value = "分页查询会员")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = MemberListDTO.class))
    @RequestMapping(value = "/page", method = RequestMethod.GET, produces = "application/json")
    public R page(MemberSearchDTO memberSearchDTO, Integer current, Integer size){
        IPage<Member> page = new Page<>(current, size);
        page = this.memberService.page(memberSearchDTO, page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody MemberDTO memberDTO){
        String password = memberDTO.getPassword();
        if (password != null) {
            if (password.length() < 6 || password.length() > 20) {
                return R.error("密码长度在6~20字符之间");
            }
        } else {
            return R.error("密码不能为空");
        }
        this.memberService.save(memberDTO);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody MemberDTO memberDTO, @PathVariable("id") Integer id){
        memberDTO.setId(id);
        String password = memberDTO.getPassword();
        if (password != null) {
            if (password.length() > 6 || password.length() > 20) {
                return R.error("密码长度在6~20字符之间");
            }
        }
        this.memberService.update(memberDTO, id);
        return R.success("操作成功", memberDTO);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.memberService.delete(id);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/def-address/{id}", method = RequestMethod.GET)
    public R getDefAddress(@PathVariable("id") Integer memberId) {
        return R.success("操作成功", this.memberAddressService.getDefByMemberId(memberId));
    }

    @ApiOperation(value = "修改会员状态")
    @RequestMapping(value = "/status", method = RequestMethod.PUT, produces = "application/json")
    public R updateStatus(Integer id, Integer status){
        this.memberService.updateStatus(id, status);
        return R.success("操作成功");
    }

    @ApiOperation(value = "修改会员余额")
    @RequestMapping(value = "/balance/{member_id}", method = RequestMethod.PUT, produces = "application/json")
    public R updateBalance(@PathVariable("member_id")Integer memberId, BigDecimal balance){
        this.memberService.updateBalance(memberId, balance);
        return R.success();
    }



}
