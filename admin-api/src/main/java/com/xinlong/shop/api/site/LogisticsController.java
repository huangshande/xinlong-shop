package com.xinlong.shop.api.site;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.core.site.entity.Logistics;
import com.xinlong.shop.core.site.service.ILogisticsService;
import javax.validation.Valid;

/**
 * <p>
 * 物流公司 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
@RestController
@RequestMapping("/site/logistics")
public class LogisticsController {

    private final ILogisticsService logisticsService;

    public LogisticsController(ILogisticsService logisticsService) {
        this.logisticsService = logisticsService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<Logistics> page = new Page<>(current, size);
        page.addOrder(new OrderItem("sort", true));
        page = this.logisticsService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody Logistics logistics ){
        this.logisticsService.save(logistics);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody Logistics logistics, @PathVariable("id") Integer id){
        this.logisticsService.update(logistics, id);
        return R.success("操作成功",logistics);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.logisticsService.delete(id);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/status", method = RequestMethod.PUT)
    public R updateStatus(Integer id, Integer status){
        this.logisticsService.updateStatus(id, status);
        return R.success("操作成功");
    }

}
