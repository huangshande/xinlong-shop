package com.xinlong.shop.api.member;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.member.entity.MemberBalanceDetail;
import com.xinlong.shop.core.member.entity.vo.MemberBalanceDetailVO;
import com.xinlong.shop.core.member.service.IMemberBalanceDetailService;
import com.xinlong.shop.framework.common.R;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 用户余额明细 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-04-06
 */
@RestController
@RequestMapping("/member/balance-detail")
public class MemberBalanceDetailController {

    private final IMemberBalanceDetailService memberBalanceDetailService;

    public MemberBalanceDetailController(IMemberBalanceDetailService memberBalanceDetailService) {
        this.memberBalanceDetailService = memberBalanceDetailService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size, String mobile){
        IPage<MemberBalanceDetailVO> page = new Page<>(current, size);
        page = this.memberBalanceDetailService.pageByVo(page, mobile);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody MemberBalanceDetail memberBalanceDetail ){
        this.memberBalanceDetailService.save(memberBalanceDetail);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody MemberBalanceDetail memberBalanceDetail, @PathVariable("id") Integer id){
        this.memberBalanceDetailService.update(memberBalanceDetail, id);
        return R.success("操作成功",memberBalanceDetail);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.memberBalanceDetailService.delete(id);
        return R.success("删除成功");
    }

}
