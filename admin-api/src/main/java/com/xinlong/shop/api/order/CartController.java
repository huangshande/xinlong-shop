package com.xinlong.shop.api.order;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.core.order.entity.Cart;
import com.xinlong.shop.core.order.service.ICartService;
import javax.validation.Valid;

/**
 * <p>
 * 购物车表 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-04-21
 */
@RestController
@RequestMapping("/order/cart")
public class CartController {

    private final ICartService cartService;

    public CartController(ICartService cartService) {
        this.cartService = cartService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<Cart> page = new Page<>(current, size);
        page = this.cartService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody Cart cart ){
        this.cartService.save(cart);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody Cart cart, @PathVariable("id") Integer id){
        this.cartService.update(cart, id);
        return R.success("操作成功",cart);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.cartService.delete(id);
        return R.success("删除成功");
    }

}
