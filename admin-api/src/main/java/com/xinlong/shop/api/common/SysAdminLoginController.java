package com.xinlong.shop.api.common;


import com.xinlong.shop.framework.core.model.AdminInfoDTO;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.SysRole;
import com.xinlong.shop.framework.core.entity.SysAdmin;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import com.xinlong.shop.framework.service.ISysRoleService;
import com.xinlong.shop.framework.service.ISysAdminService;
import com.xinlong.shop.framework.util.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 01:10 2022/5/25
 */
@RestController
@RequestMapping("/sys/admin")
public class SysAdminLoginController {

    private final ISysAdminService sysAdminService;

    private final ISysRoleService sysRoleService;

    private final TokenUtil tokenUtil;

    public SysAdminLoginController(ISysAdminService sysAdminService, ISysRoleService sysRoleService, TokenUtil tokenUtil) {
        this.sysAdminService = sysAdminService;
        this.sysRoleService = sysRoleService;
        this.tokenUtil = tokenUtil;
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public R login(String userName,  String password){
        String token = this.sysAdminService.login(userName, password);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("token", token);
        if (token == null || "".equals(token)) {
            return R.error("登录失败,错误的用户名或密码", map);
        } else {
            // 记住我功能后期再开发，现在默认就是记住
            String refreshToken = this.sysAdminService.generateRefreshToken(userName, true);
            map.put("refreshToken", refreshToken);
        }
        return R.success("登录成功",map);
    }
    @RequestMapping(value = "/token", method = RequestMethod.GET)
    public R refreshToken(String refreshToken, String userName){
        if (StringUtil.notEmpty(refreshToken) && StringUtil.notEmpty(userName)) {
            refreshToken = refreshToken.replace(SecurityConstants.TOKEN_PREFIX, "");
            String tokenUserName = tokenUtil.getUserNameFromToken(refreshToken);
            // 判断用户名与token是否匹配
            if (!userName.equals(tokenUserName)) {
                return R.success("用户名与refreshToken不匹配");
            }
            String token = this.sysAdminService.generateRefreshToken(userName, false);
            return R.success("操作成功", token);
        } else {
            return R.error("token和用户名不能为空");
        }
    }

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public R info(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){

        if (StringUtil.notEmpty(token)) {
            token = token.replace(SecurityConstants.TOKEN_PREFIX, "");
            String userName = tokenUtil.getUserNameFromToken(token);
            SysAdmin sysAdmin = sysAdminService.findByUserName(userName);
            AdminInfoDTO adminInfo = new AdminInfoDTO();

            // copy属性
            BeanUtils.copyProperties(sysAdmin, adminInfo);
            // 密码就不返回了
            adminInfo.setPassword("");
            // 得到所有角色开始拼凑权限数组
            List<SysRole> roles = sysRoleService.findRolesByAdminId(adminInfo.getId());
            List<String> permissions = new ArrayList<>();
            for(SysRole role : roles) {
                if(StringUtil.notEmpty(role.getPermissions())) {
                    String permission = role.getPermissions();
                    List<String> tempPer = Arrays.asList(permission.split(","));
                    permissions.addAll(tempPer);
                }
            }
            adminInfo.setPermissions(permissions);

            return R.success("操作成功",adminInfo);
        } else {
            return R.error("token不能为空");
        }
    }
}
