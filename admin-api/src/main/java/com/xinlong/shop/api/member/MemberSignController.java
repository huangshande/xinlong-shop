package com.xinlong.shop.api.member;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.member.entity.MemberSign;
import com.xinlong.shop.core.member.service.IMemberSignService;
import com.xinlong.shop.framework.common.R;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 会员签名记录表 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-04-14
 */
@RestController
@RequestMapping("/member/member-sign")
public class MemberSignController {

    private final IMemberSignService memberSignService;

    public MemberSignController(IMemberSignService memberSignService) {
        this.memberSignService = memberSignService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size, Integer status){
        if (status == null) {
            status = 0;
        }
        IPage<MemberSign> page = new Page<>(current, size);
        page = this.memberSignService.page(page, status);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody MemberSign memberSign ){
        this.memberSignService.save(memberSign);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody MemberSign memberSign, @PathVariable("id") Integer id){
        this.memberSignService.update(memberSign, id);
        return R.success("操作成功",memberSign);
    }

    @RequestMapping(value = "/pass/{id}", method = RequestMethod.PUT)
    public R pass(@PathVariable("id") Integer id){
        this.memberSignService.pass(id);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.memberSignService.delete(id);
        return R.success("删除成功");
    }

}
