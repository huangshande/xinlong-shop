package com.xinlong.shop.api.member;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.member.MemberWithdrawalQueryParam;
import com.xinlong.shop.core.member.MemberWithdrawalStatus;
import com.xinlong.shop.core.member.entity.MemberWithdrawal;
import com.xinlong.shop.core.member.entity.dto.MemberWithdrawalDTO;
import com.xinlong.shop.core.member.service.IMemberWithdrawalService;
import com.xinlong.shop.framework.common.R;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 用户提现记录 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-04-06
 */
@RestController
@RequestMapping("/member/member/withdrawal")
public class MemberWithdrawalController {

    private final IMemberWithdrawalService memberWithdrawalService;

    public MemberWithdrawalController(IMemberWithdrawalService memberWithdrawalService) {
        this.memberWithdrawalService = memberWithdrawalService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size, MemberWithdrawalQueryParam queryParam){
        Page<MemberWithdrawalDTO> page = new Page<>(current, size);
        page = this.memberWithdrawalService.page(page, queryParam);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody MemberWithdrawal memberWithdrawal ){
        this.memberWithdrawalService.save(memberWithdrawal);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody MemberWithdrawal memberWithdrawal, @PathVariable("id") Integer id){
        this.memberWithdrawalService.update(memberWithdrawal, id);
        return R.success("操作成功",memberWithdrawal);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.memberWithdrawalService.delete(id);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/pass/{id}", method = RequestMethod.PUT)
    public R pass(@PathVariable("id") Integer id){
        //this.memberWithdrawalService.update(memberWithdrawal, id);

        this.memberWithdrawalService.updateStatus(id, MemberWithdrawalStatus.PASS.getCode());

        return R.success("操作成功");
    }

    @RequestMapping(value = "/pay/{id}", method = RequestMethod.PUT)
    public R pay(@PathVariable("id") Integer id){
        //this.memberWithdrawalService.update(memberWithdrawal, id);
        this.memberWithdrawalService.updateStatus(id, MemberWithdrawalStatus.PAY.getCode());
        return R.success("操作成功");
    }

    @RequestMapping(value = "/refuse/{id}", method = RequestMethod.PUT)
    public R refuse(@PathVariable("id") Integer id){
        //this.memberWithdrawalService.update(memberWithdrawal, id);
        this.memberWithdrawalService.refuse(id);
        return R.success("操作成功");
    }



}
