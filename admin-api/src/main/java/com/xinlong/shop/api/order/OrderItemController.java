package com.xinlong.shop.api.order;

import com.xinlong.shop.core.order.entity.OrderItem;
import com.xinlong.shop.core.order.service.IOrderItemService;
import com.xinlong.shop.framework.common.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * <p>
 * 订单子项表 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-07-04
 */
@RestController
@Validated      // get请求验证参数时需要
@Api(tags = "订单子项管理")
@RequestMapping("/order/order/item")
public class OrderItemController {

    private final IOrderItemService orderItemService;

    public OrderItemController(IOrderItemService orderItemService) {
        this.orderItemService = orderItemService;
    }

    @ApiOperation(value = "获取订单项列表")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = OrderItem.class))
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public R list(@NotNull(message = "订单id不能为空") Integer orderId){
//        Page<OrderItem> page = new Page<>(current, size);
//        page = this.orderItemService.page(page);
        List<OrderItem> list = this.orderItemService.listByOrderId(orderId);
        return R.success("操作成功", list);
    }

//    @RequestMapping(value = "", method = RequestMethod.POST)
//    public R save(@Valid @RequestBody OrderItem orderItem ){
//        this.orderItemService.save(orderItem);
//        return R.success("操作成功");
//    }
//
//    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//    public R update(@Valid @RequestBody OrderItem orderItem, @PathVariable("id") Integer id){
//        this.orderItemService.update(orderItem, id);
//        return R.success("操作成功",orderItem);
//    }
//
//    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//    public R delete(@PathVariable("id") Integer id) {
//        this.orderItemService.delete(id);
//        return R.success("删除成功");
//    }

}
