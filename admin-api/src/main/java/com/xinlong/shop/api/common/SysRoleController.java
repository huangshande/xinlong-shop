package com.xinlong.shop.api.common;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.SysRole;
import com.xinlong.shop.framework.service.ISysRoleService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * @Author Sylow
 * @Description
 * @Date: Created in 02:25 2022/8/12
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController {

    private final ISysRoleService sysRoleService;

    public SysRoleController(ISysRoleService sysRoleService) {
        this.sysRoleService = sysRoleService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        IPage<SysRole> rolesPage = new Page<>(current, size);
        rolesPage = sysRoleService.page(rolesPage);
        return R.success("操作成功", rolesPage);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody SysRole sysRole ){
        sysRoleService.addSysRole(sysRole);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody SysRole sysRole, @PathVariable("id") Integer id){
        sysRoleService.update(sysRole, id);
        return R.success("操作成功",sysRole);
    }

    @RequestMapping(value = "/save-permissions", method = RequestMethod.GET)
    public R savePermissions(Integer id, String permissions){

        sysRoleService.updatePermission(id, permissions);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {

        sysRoleService.delete(id);
        return R.success("删除成功");
    }

}
