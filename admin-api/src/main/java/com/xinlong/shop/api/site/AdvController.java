package com.xinlong.shop.api.site;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.site.entity.Adv;
import com.xinlong.shop.core.site.entity.dto.AdvListDTO;
import com.xinlong.shop.core.site.service.IAdvService;
import com.xinlong.shop.framework.common.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 广告表 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
@RestController
@Api(tags = "广告管理")
@RequestMapping("/site/adv")
public class AdvController {

    private final IAdvService advService;

    public AdvController(IAdvService advService) {
        this.advService = advService;
    }

    @ApiOperation(value = "获取广告列表分页")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = AdvListDTO.class))
    @RequestMapping(value = "/page", method = RequestMethod.GET, produces = "application/json")
    public R page(Integer current, Integer size, Integer promotionId){
        IPage<AdvListDTO> page = new Page<>(current, size);
        page = this.advService.pageByPromotionId(page, promotionId);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody Adv adv ){
        adv.setCreateTime(DateUtil.currentSeconds());
        this.advService.save(adv);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody Adv adv, @PathVariable("id") Integer id){
        this.advService.update(adv, id);
        return R.success("操作成功",adv);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.advService.delete(id);
        return R.success("删除成功");
    }

}
