package com.xinlong.shop.api.promotion;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.promotion.entity.RushPurchaseCart;
import com.xinlong.shop.core.promotion.entity.dto.RushPurchaseCartDTO;
import com.xinlong.shop.core.promotion.service.IRushPurchaseCartService;
import com.xinlong.shop.framework.common.R;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 抢购购物车 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-05-15
 */
@RestController
@RequestMapping("/promotion/rush-purchase/cart")
public class RushPurchaseCartController {

    private final IRushPurchaseCartService rushPurchaseCartService;

    public RushPurchaseCartController(IRushPurchaseCartService rushPurchaseCartService) {
        this.rushPurchaseCartService = rushPurchaseCartService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size, String goodsName){
        IPage<RushPurchaseCartDTO> page = new Page<>(current, size);
        page = this.rushPurchaseCartService.dtoPage(page, goodsName);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody RushPurchaseCart rushPurchaseCart ){
        this.rushPurchaseCartService.save(rushPurchaseCart);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody RushPurchaseCart rushPurchaseCart, @PathVariable("id") Integer id){
        this.rushPurchaseCartService.update(rushPurchaseCart, id);
        return R.success("操作成功",rushPurchaseCart);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.rushPurchaseCartService.delete(id);
        return R.success("删除成功");
    }

}
