/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50743
 Source Host           : localhost:3306
 Source Schema         : xinlong-shop

 Target Server Type    : MySQL
 Target Server Version : 50743
 File Encoding         : 65001

 Date: 11/10/2023 11:26:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for s_activity_income
-- ----------------------------
DROP TABLE IF EXISTS `s_activity_income`;
CREATE TABLE `s_activity_income` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(8) NOT NULL COMMENT '会员id',
  `buy_price` decimal(10,2) NOT NULL COMMENT '买入价格',
  `sell_price` decimal(10,2) NOT NULL COMMENT '卖出价格',
  `service_price` decimal(10,2) NOT NULL COMMENT '服务费',
  `profit` decimal(10,2) NOT NULL COMMENT '利润=卖出价格减去买入和服务费',
  `rush_purchase_detail_id` int(8) NOT NULL COMMENT '活动明细id',
  `activity_id` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT ' 活动期数',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='会员收益表';

-- ----------------------------
-- Records of s_activity_income
-- ----------------------------
BEGIN;
INSERT INTO `s_activity_income` VALUES (1, 3, 10.00, 10.40, 0.26, 0.14, 44, '2023033001', 1680167719);
COMMIT;

-- ----------------------------
-- Table structure for s_blind_box
-- ----------------------------
DROP TABLE IF EXISTS `s_blind_box`;
CREATE TABLE `s_blind_box` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `blind_box_name` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '盲盒名称',
  `blind_box_img` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '盲盒标题图',
  `back_img` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '背景图',
  `price` decimal(10,2) DEFAULT NULL COMMENT '单价',
  `price_rules` text COLLATE utf8mb4_bin COMMENT '价格规则',
  `produce_rules` text COLLATE utf8mb4_bin COMMENT '产出规则',
  `show_produce_rules` text COLLATE utf8mb4_bin COMMENT '前端显示产出规则',
  `status` int(1) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='盲盒';

-- ----------------------------
-- Records of s_blind_box
-- ----------------------------
BEGIN;
INSERT INTO `s_blind_box` VALUES (1, 'POP MART泡泡玛特 Minico兔宝儿的回忆手办公仔潮流玩具礼物娃娃生日礼物 Minico兔宝儿的回忆手办', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/eed5aef1-3166-4f71-961d-c3a759794d79.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/9ab4b8a0-f4ca-4075-a092-db03f16fd7cf.png', 50.00, '[{\"id\":\"one\",\"name\":\"一发入魂\",\"buyNum\":1.0,\"price\":0.01},{\"id\":\"two\",\"name\":\"欧气三连\",\"buyNum\":3.0,\"price\":0.02},{\"id\":\"five\",\"name\":\"霸气五连\",\"buyNum\":5.0,\"price\":0.03}]', '[{\"id\":\"bronze\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade1.png\",\"name\":\"青铜\",\"odds\":50.0},{\"id\":\"silver\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade2.png\",\"name\":\"白银\",\"odds\":20.0},{\"id\":\"gold\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade3.png\",\"name\":\"黄金\",\"odds\":15.0},{\"id\":\"platinum\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade4.png\",\"name\":\"铂金\",\"odds\":10.0},{\"id\":\"diamond\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade5.png\",\"name\":\"钻石\",\"odds\":5.0}]', '[{\"id\":\"bronze\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade1.png\",\"name\":\"青铜\",\"odds\":50.0},{\"id\":\"silver\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade2.png\",\"name\":\"白银\",\"odds\":20.0},{\"id\":\"gold\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade3.png\",\"name\":\"黄金\",\"odds\":15.0},{\"id\":\"platinum\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade4.png\",\"name\":\"铂金\",\"odds\":10.0},{\"id\":\"diamond\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade5.png\",\"name\":\"钻石\",\"odds\":5.0}]', 1, 1696922321);
INSERT INTO `s_blind_box` VALUES (2, 'POP MART泡泡玛特 INSTINCTOY慕奇盛大庆祝手办大号公仔潮玩玩具生日礼物 慕奇盛大庆祝手办', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/c3abcc6b-f74a-47d6-831c-af979784d747.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/798d8b35-6b7a-410f-8fd1-38a745af1840.png', 100.00, '[{\"id\":\"one\",\"name\":\"一发入魂\",\"buyNum\":1.0,\"price\":0.01},{\"id\":\"two\",\"name\":\"欧气三连\",\"buyNum\":3.0,\"price\":0.02},{\"id\":\"five\",\"name\":\"霸气五连\",\"buyNum\":5.0,\"price\":0.03}]', '[{\"id\":\"bronze\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade1.png\",\"name\":\"青铜\",\"odds\":50.0},{\"id\":\"silver\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade2.png\",\"name\":\"白银\",\"odds\":20.0},{\"id\":\"gold\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade3.png\",\"name\":\"黄金\",\"odds\":15.0},{\"id\":\"platinum\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade4.png\",\"name\":\"铂金\",\"odds\":10.0},{\"id\":\"diamond\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade5.png\",\"name\":\"钻石\",\"odds\":5.0}]', '[{\"id\":\"bronze\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade1.png\",\"name\":\"青铜\",\"odds\":50.0},{\"id\":\"silver\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade2.png\",\"name\":\"白银\",\"odds\":20.0},{\"id\":\"gold\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade3.png\",\"name\":\"黄金\",\"odds\":15.0},{\"id\":\"platinum\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade4.png\",\"name\":\"铂金\",\"odds\":10.0},{\"id\":\"diamond\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade5.png\",\"name\":\"钻石\",\"odds\":5.0}]', 1, 1696922398);
INSERT INTO `s_blind_box` VALUES (3, 'POP MART泡泡玛特 迪士尼100周年公主小时候系列手办盲盒玩具生日礼物', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/4d39ce46-4eae-42ee-a87f-c2fc3f8c5f20.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/a7849aff-93dd-4f35-b1d1-adc2662c610a.png', 200.00, '[{\"id\":\"one\",\"name\":\"一发入魂\",\"buyNum\":1.0,\"price\":0.01},{\"id\":\"two\",\"name\":\"欧气三连\",\"buyNum\":3.0,\"price\":0.02},{\"id\":\"five\",\"name\":\"霸气五连\",\"buyNum\":5.0,\"price\":0.03}]', '[{\"id\":\"bronze\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade1.png\",\"name\":\"青铜\",\"odds\":50.0},{\"id\":\"silver\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade2.png\",\"name\":\"白银\",\"odds\":20.0},{\"id\":\"gold\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade3.png\",\"name\":\"黄金\",\"odds\":15.0},{\"id\":\"platinum\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade4.png\",\"name\":\"铂金\",\"odds\":10.0},{\"id\":\"diamond\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade5.png\",\"name\":\"钻石\",\"odds\":5.0}]', '[{\"id\":\"bronze\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade1.png\",\"name\":\"青铜\",\"odds\":50.0},{\"id\":\"silver\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade2.png\",\"name\":\"白银\",\"odds\":20.0},{\"id\":\"gold\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade3.png\",\"name\":\"黄金\",\"odds\":15.0},{\"id\":\"platinum\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade4.png\",\"name\":\"铂金\",\"odds\":10.0},{\"id\":\"diamond\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade5.png\",\"name\":\"钻石\",\"odds\":5.0}]', 1, 1696922467);
INSERT INTO `s_blind_box` VALUES (4, 'POP MART泡泡玛特 BOBO COCO×ONE PIECE绵绵世界手办大号潮玩生日礼物 BOBO&COCO×ONE PIEC绵绵世界手办', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/82fee902-7506-4ab6-aed4-51c704d77703.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/730270bd-f95a-406b-9d0c-f441d5ecf026.png', 99.00, '[{\"id\":\"one\",\"name\":\"一发入魂\",\"buyNum\":1.0,\"price\":0.01},{\"id\":\"two\",\"name\":\"欧气三连\",\"buyNum\":3.0,\"price\":0.02},{\"id\":\"five\",\"name\":\"霸气五连\",\"buyNum\":5.0,\"price\":0.03}]', '[{\"id\":\"bronze\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade1.png\",\"name\":\"青铜\",\"odds\":50.0},{\"id\":\"silver\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade2.png\",\"name\":\"白银\",\"odds\":20.0},{\"id\":\"gold\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade3.png\",\"name\":\"黄金\",\"odds\":15.0},{\"id\":\"platinum\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade4.png\",\"name\":\"铂金\",\"odds\":10.0},{\"id\":\"diamond\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade5.png\",\"name\":\"钻石\",\"odds\":5.0}]', '[{\"id\":\"bronze\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade1.png\",\"name\":\"青铜\",\"odds\":50.0},{\"id\":\"silver\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade2.png\",\"name\":\"白银\",\"odds\":20.0},{\"id\":\"gold\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade3.png\",\"name\":\"黄金\",\"odds\":15.0},{\"id\":\"platinum\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade4.png\",\"name\":\"铂金\",\"odds\":10.0},{\"id\":\"diamond\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade5.png\",\"name\":\"钻石\",\"odds\":5.0}]', 1, 1696922554);
INSERT INTO `s_blind_box` VALUES (5, 'POP MART泡泡玛特 INSTINCTOY毛怪的欢乐生活系列手办盲盒玩具生日礼物', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/90fd6d81-6d1d-4217-9db3-97c2cbe50d1e.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/16823d4e-27ca-4b8b-bba1-2abb1d1404ae.png', 39.00, '[{\"id\":\"one\",\"name\":\"一发入魂\",\"buyNum\":1.0,\"price\":0.01},{\"id\":\"two\",\"name\":\"欧气三连\",\"buyNum\":3.0,\"price\":0.02},{\"id\":\"five\",\"name\":\"霸气五连\",\"buyNum\":5.0,\"price\":0.03}]', '[{\"id\":\"bronze\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade1.png\",\"name\":\"青铜\",\"odds\":50.0},{\"id\":\"silver\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade2.png\",\"name\":\"白银\",\"odds\":20.0},{\"id\":\"gold\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade3.png\",\"name\":\"黄金\",\"odds\":15.0},{\"id\":\"platinum\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade4.png\",\"name\":\"铂金\",\"odds\":10.0},{\"id\":\"diamond\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade5.png\",\"name\":\"钻石\",\"odds\":5.0}]', '[{\"id\":\"bronze\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade1.png\",\"name\":\"青铜\",\"odds\":50.0},{\"id\":\"silver\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade2.png\",\"name\":\"白银\",\"odds\":20.0},{\"id\":\"gold\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade3.png\",\"name\":\"黄金\",\"odds\":15.0},{\"id\":\"platinum\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade4.png\",\"name\":\"铂金\",\"odds\":10.0},{\"id\":\"diamond\",\"img\":\"https://yashangxuan.oss-cn-beijing.aliyuncs.com/blind/grade5.png\",\"name\":\"钻石\",\"odds\":5.0}]', 1, 1696922645);
COMMIT;

-- ----------------------------
-- Table structure for s_blind_box_goods
-- ----------------------------
DROP TABLE IF EXISTS `s_blind_box_goods`;
CREATE TABLE `s_blind_box_goods` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `blind_box_id` int(8) DEFAULT NULL COMMENT '盲盒id',
  `goods_id` int(8) DEFAULT NULL COMMENT '商品id',
  `tag` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品标签',
  `odds` decimal(10,2) DEFAULT NULL COMMENT '商品概率',
  `goods_img` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品图片',
  `goods_name` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品名称',
  `goods_price` decimal(10,2) DEFAULT NULL COMMENT '商品价格',
  `stock` int(8) DEFAULT NULL COMMENT '库存量',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='盲盒关联商品表';

-- ----------------------------
-- Records of s_blind_box_goods
-- ----------------------------
BEGIN;
INSERT INTO `s_blind_box_goods` VALUES (1, 1, 1, 'bronze', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/8c920db2-703e-4910-a50f-4ba3fbb9ad78.jpg', 'POP MART泡泡玛特 BOBO COCO×ONE PIECE绵绵世界手办大号潮玩生日礼物 BOBO&COCO×ONE PIEC绵绵世界手办', 599.00, 999, 1696922321);
INSERT INTO `s_blind_box_goods` VALUES (2, 1, 2, 'silver', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/3b6bb48c-d849-4aaf-bdaa-f64dda5a6ee2.jpg', 'POP MART泡泡玛特 Minico兔宝儿的回忆手办公仔潮流玩具礼物娃娃生日礼物 Minico兔宝儿的回忆手办', 699.00, 999, 1696922321);
INSERT INTO `s_blind_box_goods` VALUES (3, 1, 3, 'gold', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/e9d30c61-845f-462b-b0db-0d9de6f20688.jpg', 'POP MART泡泡玛特 迪士尼100周年公主小时候系列手办盲盒玩具生日礼物 整盒', 828.00, 999, 1696922321);
INSERT INTO `s_blind_box_goods` VALUES (4, 1, 4, 'platinum', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/82b0a345-81f7-4684-8734-2a30254ec2e1.jpg', 'POP MART泡泡玛特 INSTINCTOY慕奇盛大庆祝手办大号公仔潮玩玩具生日礼物 慕奇盛大庆祝手办', 999.00, 999, 1696922321);
INSERT INTO `s_blind_box_goods` VALUES (5, 1, 5, 'diamond', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/b41e54bb-869a-47c6-ba66-35a3b1bed690.jpg', 'Apple iPhone 15 Pro Max (A3108) 256GB 蓝色钛金属 支持移动联通电信5G 双卡双待手机', 9999.00, 999, 1696922321);
INSERT INTO `s_blind_box_goods` VALUES (6, 2, 1, 'bronze', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/8c920db2-703e-4910-a50f-4ba3fbb9ad78.jpg', 'POP MART泡泡玛特 BOBO COCO×ONE PIECE绵绵世界手办大号潮玩生日礼物 BOBO&COCO×ONE PIEC绵绵世界手办', 599.00, 999, 1696922681);
INSERT INTO `s_blind_box_goods` VALUES (7, 2, 2, 'silver', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/3b6bb48c-d849-4aaf-bdaa-f64dda5a6ee2.jpg', 'POP MART泡泡玛特 Minico兔宝儿的回忆手办公仔潮流玩具礼物娃娃生日礼物 Minico兔宝儿的回忆手办', 699.00, 999, 1696922681);
INSERT INTO `s_blind_box_goods` VALUES (8, 2, 3, 'gold', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/e9d30c61-845f-462b-b0db-0d9de6f20688.jpg', 'POP MART泡泡玛特 迪士尼100周年公主小时候系列手办盲盒玩具生日礼物 整盒', 828.00, 999, 1696922681);
INSERT INTO `s_blind_box_goods` VALUES (9, 2, 4, 'platinum', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/82b0a345-81f7-4684-8734-2a30254ec2e1.jpg', 'POP MART泡泡玛特 INSTINCTOY慕奇盛大庆祝手办大号公仔潮玩玩具生日礼物 慕奇盛大庆祝手办', 999.00, 999, 1696922681);
INSERT INTO `s_blind_box_goods` VALUES (10, 2, 5, 'diamond', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/b41e54bb-869a-47c6-ba66-35a3b1bed690.jpg', 'Apple iPhone 15 Pro Max (A3108) 256GB 蓝色钛金属 支持移动联通电信5G 双卡双待手机', 9999.00, 999, 1696922681);
INSERT INTO `s_blind_box_goods` VALUES (11, 3, 1, 'bronze', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/8c920db2-703e-4910-a50f-4ba3fbb9ad78.jpg', 'POP MART泡泡玛特 BOBO COCO×ONE PIECE绵绵世界手办大号潮玩生日礼物 BOBO&COCO×ONE PIEC绵绵世界手办', 599.00, 999, 1696922467);
INSERT INTO `s_blind_box_goods` VALUES (12, 3, 2, 'silver', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/3b6bb48c-d849-4aaf-bdaa-f64dda5a6ee2.jpg', 'POP MART泡泡玛特 Minico兔宝儿的回忆手办公仔潮流玩具礼物娃娃生日礼物 Minico兔宝儿的回忆手办', 699.00, 998, 1696922467);
INSERT INTO `s_blind_box_goods` VALUES (13, 3, 3, 'gold', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/e9d30c61-845f-462b-b0db-0d9de6f20688.jpg', 'POP MART泡泡玛特 迪士尼100周年公主小时候系列手办盲盒玩具生日礼物 整盒', 828.00, 999, 1696922467);
INSERT INTO `s_blind_box_goods` VALUES (14, 3, 4, 'platinum', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/82b0a345-81f7-4684-8734-2a30254ec2e1.jpg', 'POP MART泡泡玛特 INSTINCTOY慕奇盛大庆祝手办大号公仔潮玩玩具生日礼物 慕奇盛大庆祝手办', 999.00, 999, 1696922467);
INSERT INTO `s_blind_box_goods` VALUES (15, 3, 5, 'diamond', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/b41e54bb-869a-47c6-ba66-35a3b1bed690.jpg', 'Apple iPhone 15 Pro Max (A3108) 256GB 蓝色钛金属 支持移动联通电信5G 双卡双待手机', 9999.00, 999, 1696922467);
INSERT INTO `s_blind_box_goods` VALUES (16, 4, 1, 'bronze', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/8c920db2-703e-4910-a50f-4ba3fbb9ad78.jpg', 'POP MART泡泡玛特 BOBO COCO×ONE PIECE绵绵世界手办大号潮玩生日礼物 BOBO&COCO×ONE PIEC绵绵世界手办', 599.00, 999, 1696922554);
INSERT INTO `s_blind_box_goods` VALUES (17, 4, 2, 'silver', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/3b6bb48c-d849-4aaf-bdaa-f64dda5a6ee2.jpg', 'POP MART泡泡玛特 Minico兔宝儿的回忆手办公仔潮流玩具礼物娃娃生日礼物 Minico兔宝儿的回忆手办', 699.00, 999, 1696922554);
INSERT INTO `s_blind_box_goods` VALUES (18, 4, 3, 'gold', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/e9d30c61-845f-462b-b0db-0d9de6f20688.jpg', 'POP MART泡泡玛特 迪士尼100周年公主小时候系列手办盲盒玩具生日礼物 整盒', 828.00, 999, 1696922554);
INSERT INTO `s_blind_box_goods` VALUES (19, 4, 4, 'platinum', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/82b0a345-81f7-4684-8734-2a30254ec2e1.jpg', 'POP MART泡泡玛特 INSTINCTOY慕奇盛大庆祝手办大号公仔潮玩玩具生日礼物 慕奇盛大庆祝手办', 999.00, 999, 1696922554);
INSERT INTO `s_blind_box_goods` VALUES (20, 4, 5, 'diamond', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/b41e54bb-869a-47c6-ba66-35a3b1bed690.jpg', 'Apple iPhone 15 Pro Max (A3108) 256GB 蓝色钛金属 支持移动联通电信5G 双卡双待手机', 9999.00, 999, 1696922554);
INSERT INTO `s_blind_box_goods` VALUES (21, 5, 1, 'bronze', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/8c920db2-703e-4910-a50f-4ba3fbb9ad78.jpg', 'POP MART泡泡玛特 BOBO COCO×ONE PIECE绵绵世界手办大号潮玩生日礼物 BOBO&COCO×ONE PIEC绵绵世界手办', 599.00, 999, 1696922645);
INSERT INTO `s_blind_box_goods` VALUES (22, 5, 2, 'silver', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/3b6bb48c-d849-4aaf-bdaa-f64dda5a6ee2.jpg', 'POP MART泡泡玛特 Minico兔宝儿的回忆手办公仔潮流玩具礼物娃娃生日礼物 Minico兔宝儿的回忆手办', 699.00, 999, 1696922645);
INSERT INTO `s_blind_box_goods` VALUES (23, 5, 3, 'gold', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/e9d30c61-845f-462b-b0db-0d9de6f20688.jpg', 'POP MART泡泡玛特 迪士尼100周年公主小时候系列手办盲盒玩具生日礼物 整盒', 828.00, 999, 1696922645);
INSERT INTO `s_blind_box_goods` VALUES (24, 5, 4, 'platinum', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/82b0a345-81f7-4684-8734-2a30254ec2e1.jpg', 'POP MART泡泡玛特 INSTINCTOY慕奇盛大庆祝手办大号公仔潮玩玩具生日礼物 慕奇盛大庆祝手办', 999.00, 999, 1696922645);
INSERT INTO `s_blind_box_goods` VALUES (25, 5, 5, 'diamond', 1.00, 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/b41e54bb-869a-47c6-ba66-35a3b1bed690.jpg', 'Apple iPhone 15 Pro Max (A3108) 256GB 蓝色钛金属 支持移动联通电信5G 双卡双待手机', 9999.00, 999, 1696922645);
COMMIT;

-- ----------------------------
-- Table structure for s_blind_box_order
-- ----------------------------
DROP TABLE IF EXISTS `s_blind_box_order`;
CREATE TABLE `s_blind_box_order` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `order_sn` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单编号',
  `blind_box_id` int(8) DEFAULT NULL COMMENT '盲盒id',
  `member_id` int(8) DEFAULT NULL COMMENT '会员id',
  `blind_box_num` int(8) DEFAULT NULL COMMENT '盲盒抽奖次数',
  `price` decimal(20,2) DEFAULT NULL COMMENT '订单总价',
  `price_rule` text COLLATE utf8mb4_bin COMMENT '价格规则',
  `status` int(1) DEFAULT '0' COMMENT '订单状态',
  `payment_order_no` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '第三方支付单号',
  `pay_time` bigint(20) DEFAULT NULL COMMENT '支付时间',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='盲盒订单表';

-- ----------------------------
-- Records of s_blind_box_order
-- ----------------------------
BEGIN;
INSERT INTO `s_blind_box_order` VALUES (1, '3231010170001', 3, 1, 1, 0.01, '{\"id\":\"one\",\"name\":\"一发入魂\",\"buyNum\":1.0,\"price\":0.01}', 1, '4200002001202310102212971246', 1696928869, 1696928861);
COMMIT;

-- ----------------------------
-- Table structure for s_blind_box_order_item
-- ----------------------------
DROP TABLE IF EXISTS `s_blind_box_order_item`;
CREATE TABLE `s_blind_box_order_item` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `goods_id` int(8) NOT NULL COMMENT '商品id',
  `member_id` int(8) NOT NULL COMMENT '会员id',
  `blind_box_order_id` int(8) NOT NULL COMMENT '盲盒订单id',
  `blind_box_id` int(8) NOT NULL COMMENT '盲盒商品id',
  `goods_name` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品名称',
  `goods_img` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品主图',
  `goods_price` decimal(20,2) DEFAULT NULL COMMENT '商品价格',
  `num` int(8) DEFAULT NULL COMMENT '商品数量',
  `ship_order_sn` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发货订单编号',
  `status` int(1) DEFAULT '0' COMMENT '状态（未提货=0、已提货=1）',
  `pickup_time` bigint(20) DEFAULT NULL COMMENT '提货时间',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='盲盒订单项';

-- ----------------------------
-- Records of s_blind_box_order_item
-- ----------------------------
BEGIN;
INSERT INTO `s_blind_box_order_item` VALUES (1, 2, 1, 1, 3, 'POP MART泡泡玛特 Minico兔宝儿的回忆手办公仔潮流玩具礼物娃娃生日礼物 Minico兔宝儿的回忆手办', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/3b6bb48c-d849-4aaf-bdaa-f64dda5a6ee2.jpg', 699.00, 1, NULL, 1, NULL, 1696928869);
COMMIT;

-- ----------------------------
-- Table structure for s_brand
-- ----------------------------
DROP TABLE IF EXISTS `s_brand`;
CREATE TABLE `s_brand` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `brand_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '品牌名称',
  `brand_img` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '图片地址',
  `sort` int(8) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='商品品牌';

-- ----------------------------
-- Table structure for s_cart
-- ----------------------------
DROP TABLE IF EXISTS `s_cart`;
CREATE TABLE `s_cart` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `goods_id` int(8) DEFAULT NULL COMMENT '商品id',
  `num` int(4) DEFAULT '1' COMMENT '数量',
  `member_id` int(8) DEFAULT NULL COMMENT '会员id',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='购物车表';

-- ----------------------------
-- Table structure for s_goods
-- ----------------------------
DROP TABLE IF EXISTS `s_goods`;
CREATE TABLE `s_goods` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `goods_classify_id` int(10) NOT NULL COMMENT '商品分类id',
  `goods_brand_id` int(10) NOT NULL COMMENT '商品品牌id',
  `goods_type` int(8) DEFAULT '0' COMMENT '商品类型',
  `goods_name` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT '商品名称',
  `goods_sn` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '商品编号',
  `goods_desc` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品简介',
  `goods_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品销售价格',
  `goods_cost_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品成本价格',
  `goods_market_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '商品市场价格',
  `goods_sku_sn` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '商品货号',
  `goods_status` int(1) NOT NULL DEFAULT '0' COMMENT '商品状态',
  `goods_imgs` text COLLATE utf8mb4_bin COMMENT '商品图片',
  `goods_intro` longtext COLLATE utf8mb4_bin NOT NULL COMMENT '商品详情',
  `goods_grade` int(1) DEFAULT '0' COMMENT '商品新旧等级',
  `goods_tags` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品标签',
  `create_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `disable` int(1) DEFAULT '0' COMMENT '禁用状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='商品';

-- ----------------------------
-- Records of s_goods
-- ----------------------------
BEGIN;
INSERT INTO `s_goods` VALUES (1, 658, 0, 2, 'POP MART泡泡玛特 BOBO COCO×ONE PIECE绵绵世界手办大号潮玩生日礼物 BOBO&COCO×ONE PIEC绵绵世界手办', '10031115249997', '正版商品', 599.00, 0.00, 0.00, '10031115249997', 1, '[\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/8c920db2-703e-4910-a50f-4ba3fbb9ad78.jpg\"]', '<p><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/02d61757-8e9e-41ab-b960-11d77078a5cc.jpg\" alt=\"e701fc2c7a649bfe.jpg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/e8112a99-754a-4a85-847e-b4940aa862cf.jpg\" alt=\"68314e5a7a8594d0.jpg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/1b5c9e32-a3e4-458e-b4af-107dec25fb19.jpg\" alt=\"afdc9c4e107f2d7d.jpg\" data-href=\"null\" style=\"\"/></p>', 1, '[\"正版\"]', 1696920734, 0);
INSERT INTO `s_goods` VALUES (2, 658, 0, 2, 'POP MART泡泡玛特 Minico兔宝儿的回忆手办公仔潮流玩具礼物娃娃生日礼物 Minico兔宝儿的回忆手办', '10037517402715', '正版商品', 699.00, 0.00, 0.00, '10037517402715', 1, '[\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/3b6bb48c-d849-4aaf-bdaa-f64dda5a6ee2.jpg\"]', '<p><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/27c73266-355c-4a8b-b6ff-aacf781b98ea.jpeg\" alt=\"1.jpeg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/83d8010b-7971-446c-bfc5-303a0a3d7922.jpeg\" alt=\"2.jpeg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/fce0944f-6a9f-4732-8c99-8e302dea16b0.jpeg\" alt=\"3.jpeg\" data-href=\"null\" style=\"\"/></p>', 1, '[\"正版\"]', 1696920920, 0);
INSERT INTO `s_goods` VALUES (3, 658, 0, 2, 'POP MART泡泡玛特 迪士尼100周年公主小时候系列手办盲盒玩具生日礼物 整盒', '10082744496849', '正版商品', 828.00, 0.00, 0.00, '10082744496849', 1, '[\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/e9d30c61-845f-462b-b0db-0d9de6f20688.jpg\"]', '<p><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/251db661-965b-4578-aa7c-c980b2a8d4fe.jpeg\" alt=\"11.jpeg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/0748e005-a97e-4e1c-be01-dce918260c20.jpeg\" alt=\"22.jpeg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/ab01e26f-833a-49c4-9585-54473fc26b24.jpeg\" alt=\"33.jpeg\" data-href=\"null\" style=\"\"/></p>', 1, '[\"正版\"]', 1696921041, 0);
INSERT INTO `s_goods` VALUES (4, 658, 0, 2, 'POP MART泡泡玛特 INSTINCTOY慕奇盛大庆祝手办大号公仔潮玩玩具生日礼物 慕奇盛大庆祝手办', '10027752949070', '正版商品', 999.00, 0.00, 0.00, '10027752949070', 1, '[\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/82b0a345-81f7-4684-8734-2a30254ec2e1.jpg\"]', '<p><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/5a0e15f9-86ef-484b-85c1-d72f60c9ba4d.jpeg\" alt=\"111.jpeg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/cfb29c9e-2685-4173-9ec9-5fbff8822695.jpeg\" alt=\"666.jpeg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/0ba3efce-5e8a-4b53-89c1-0ff06e63cfd5.jpeg\" alt=\"777.jpeg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/6fff38ec-053a-4433-ad1e-275910eb5c1d.jpeg\" alt=\"222.jpeg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/d06f955d-8d33-4298-8bcf-e8215666a4fd.jpeg\" alt=\"333.jpeg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/1827405d-50ad-4255-9b81-74ceb2133ff5.jpeg\" alt=\"444.jpeg\" data-href=\"null\" style=\"\"/><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/7c9d9842-348a-4f60-83d3-35264a8549a6.jpeg\" alt=\"555.jpeg\" data-href=\"null\" style=\"\"/></p>', 1, '[\"正版\"]', 1696921362, 0);
INSERT INTO `s_goods` VALUES (5, 3, 0, 0, 'Apple iPhone 15 Pro Max (A3108) 256GB 蓝色钛金属 支持移动联通电信5G 双卡双待手机', '100066896468', '苹果授权商品，假一赔三', 9999.00, 0.00, 0.00, '100066896468', 1, '[\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/b41e54bb-869a-47c6-ba66-35a3b1bed690.jpg\",\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/65bf7c07-53eb-4cec-898e-f0ce8568f9b9.jpg\",\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/61b82566-75ec-44b5-b170-6d4eea604438.jpg\"]', '<p><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/41dd825e-f2d9-4e39-a4d9-fda44cc52e80.jpg\" alt=\"172cdd76d74fec14.jpg\" data-href=\"null\" style=\"\"/></p><p><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/1d74d70c-de0f-4790-9769-dc27e1c81417.jpg\" alt=\"731caaf084392dfb.jpg\" data-href=\"null\" style=\"\"/></p><p><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/11fb18d9-bbe4-4346-90ce-6bf855bdb7c6.jpg\" alt=\"5cccda6644a5f9b7.jpg\" data-href=\"null\" style=\"\"/></p><p><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/eb44bffd-87a2-4258-bf4f-ae77a39cd5ec.jpg\" alt=\"689b77cfb889e977.jpg\" data-href=\"null\" style=\"\"/></p>', 1, '[\"苹果授权\"]', 1696922127, 0);
COMMIT;

-- ----------------------------
-- Table structure for s_goods_classify
-- ----------------------------
DROP TABLE IF EXISTS `s_goods_classify`;
CREATE TABLE `s_goods_classify` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `classify_name` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT '分类名称',
  `classify_img` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '分类图片',
  `is_show` int(1) NOT NULL DEFAULT '0' COMMENT '是否展示,0=展示,1=隐藏',
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '父id',
  `classify_path` varchar(512) COLLATE utf8mb4_bin DEFAULT '' COMMENT '分类路径0|1|2,便于查询',
  `disable` int(1) NOT NULL DEFAULT '0' COMMENT '是否禁用,0=正常,1=禁用',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=714 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='商品分类';

-- ----------------------------
-- Records of s_goods_classify
-- ----------------------------
BEGIN;
INSERT INTO `s_goods_classify` VALUES (1, '数码办公', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/3f26aec6ce4b4b2f9201b5f56111cafe.png', 0, 0, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (2, '手机通讯', '', 0, 1, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (3, '手机', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204943.png', 0, 2, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (4, '对讲机', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204944.png', 0, 2, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (5, '摄影摄像', '', 0, 1, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (6, '数码相机', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204947.png', 0, 5, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (7, '单电/微单相机', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204948.png', 0, 5, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (8, '单反相机', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204949.png', 0, 5, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (9, '摄像机', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204950.png', 0, 5, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (10, '拍立得', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204951.png', 0, 5, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (11, '镜头', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204952.png', 0, 5, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (12, '数码配件', '', 0, 1, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (13, '存储卡', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204954.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (14, '读卡器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204955.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (15, '滤镜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204956.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (16, '闪光灯/手柄', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204957.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (17, '相机包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204958.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (18, '三脚架/云台', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204959.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (19, '相机清洁', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204960.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (20, '相机贴膜	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204961.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (21, '机身附件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204962.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (22, '镜头附件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204963.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (23, '电池/充电器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204964.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (24, '移动电源', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204965.png', 0, 12, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (25, '时尚影音', '', 0, 1, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (26, 'MP3/MP4', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204967.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (27, '智能设备', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204968.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (28, '耳机/耳麦', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204969.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (29, '音箱', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204970.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (30, '高清播放器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204971.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (31, '电子书', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204972.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (32, '电子词典', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204973.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (33, 'MP3/MP4配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204974.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (34, '录音笔', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204975.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (35, '麦克风', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204976.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (36, '专业音频', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204977.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (37, '电子教育', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204978.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (38, '数码相框', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204979.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (39, '苹果配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204980.png', 0, 25, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (40, '电脑整机', '', 0, 1, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (41, '笔记本', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204982.png', 0, 40, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (42, '超极本', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204983.png', 0, 40, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (43, '游戏本', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204984.png', 0, 40, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (44, '平板电脑', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204985.png', 0, 40, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (45, '平板电脑配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204986.png', 0, 40, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (46, '台式机', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204987.png', 0, 40, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (47, '服务器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204988.png', 0, 40, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (48, '笔记本配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204989.png', 0, 40, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (49, '电脑配件', '', 0, 1, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (50, 'CPU', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399105.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (51, '主板', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399106.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (52, '显卡', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399107.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (53, '硬盘', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399108.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (54, 'SSD固态硬盘	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399109.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (55, '内存', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399110.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (56, '机箱	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399111.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (57, '电源', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399112.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (58, '显示器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399113.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (59, '刻录机/光驱	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399114.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (60, '散热器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399115.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (61, '声卡/扩展卡', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399116.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (62, '装机配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399117.png', 0, 49, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (63, '外设产品', '', 0, 1, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (64, '鼠标', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399119.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (65, '键盘', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399120.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (66, '移动硬盘', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399121.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (67, 'U盘', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399122.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (68, '摄像头', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399123.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (69, '外置盒', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399124.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (70, '游戏设备', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399125.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (71, '电视盒', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399126.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (72, '手写板	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399127.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (73, '鼠标垫', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399128.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (74, '插座', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399129.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (75, 'UPS电源	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399130.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (76, '线缆	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399131.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (77, '电脑工具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399132.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (78, '电脑清洁', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399133.png', 0, 63, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (79, '网络产品', '', 0, 1, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (80, '路由器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399135.png', 0, 79, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (81, '网卡', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399136.png', 0, 79, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (82, '交换机', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399137.png', 0, 79, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (83, '网络存储', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399138.png', 0, 79, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (84, '3G上网', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399139.png', 0, 79, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (85, '网络盒子	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399140.png', 0, 79, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (86, '办公打印', '', 0, 1, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (87, '打印机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399142.png', 0, 86, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (88, '一体机', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399143.png', 0, 86, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (89, '投影机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399144.png', 0, 86, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (90, '投影配件	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399145.png', 0, 86, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (91, '传真机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399146.png', 0, 86, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (92, '复合机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399147.png', 0, 86, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (93, '碎纸机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399148.png', 0, 86, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (94, '扫描仪	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399149.png', 0, 86, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (95, '墨盒	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399150.png', 0, 86, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (96, '墨粉', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399151.png', 0, 86, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (97, '色带', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399152.png', 0, 86, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (98, '办公文仪', '', 0, 1, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (99, '办公文具	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399154.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (100, '文件管理	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399155.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (101, '笔类	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399156.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (102, '纸类	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399157.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (103, '本册/便签	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399158.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (104, '学生文具	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399159.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (105, '财务用品	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399160.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (106, '计算器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399161.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (107, '激光笔', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399162.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (108, '白板/封装	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399163.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (109, '考勤机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399164.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (110, '刻录碟片/附件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399165.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (111, '点钞机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399166.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (112, '支付设备/POS机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399167.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (113, '安防监控	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399168.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (114, '呼叫/会议设备	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399169.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (115, '保险柜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399170.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (116, '办公家具	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399171.png', 0, 98, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (117, '家用电器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/144b091bf9bf4e158bdc7bcb48643a09.png', 1, 0, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (118, '大家电', '', 0, 117, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (119, '平板电视	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399555.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (120, '空调	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399556.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (121, '冰箱', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399557.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (122, '洗衣机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399558.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (123, '家庭影院	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399559.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (124, 'DVD播放机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399560.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (125, '迷你音响	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399561.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (126, '烟机/灶具	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399562.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (127, '热水器	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399563.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (128, '消毒柜/洗碗机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399564.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (129, '酒柜/冰吧/冷柜	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399565.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (130, '家电配件	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399566.png', 0, 118, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (131, '生活电器', '', 0, 117, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (132, '电风扇	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399568.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (133, '吸尘器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399569.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (134, '加湿器	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399570.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (135, '净水设备	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399571.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (136, '饮水机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399572.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (137, '冷风扇	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399573.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (138, '电话机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399574.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (139, '插座	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399575.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (140, '除湿/干衣机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399576.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (141, '清洁机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399577.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (142, '取暖电器	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399578.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (143, '其它生活电器	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399579.png', 0, 131, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (144, '厨房电器', '', 0, 117, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (145, '料理/榨汁机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399581.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (146, '豆浆机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399582.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (147, '电饭煲	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399583.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (148, '电压力锅	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399584.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (149, '面包机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399585.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (150, '咖啡机	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399586.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (151, '微波炉	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399587.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (152, '电烤箱	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399588.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (153, '电饼铛/烧烤盘	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399589.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (154, '煮蛋器	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399590.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (155, '电水壶/热水瓶	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399591.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (156, '其它厨房电器	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399592.png', 0, 144, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (157, '五金家装', '', 0, 117, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (158, '电动工具	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399609.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (159, '手动工具	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399610.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (160, '仪器仪表	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399611.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (161, '浴霸/排气扇	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399612.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (162, '灯具	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399613.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (163, 'LED灯	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399614.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (164, '水槽	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399615.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (165, '龙头', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399616.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (166, '淋浴花洒	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399617.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (167, '厨卫五金	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399618.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (168, '家具五金', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399619.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (169, '插座	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399620.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (170, '电工电料	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399621.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (171, '监控安防	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399622.png', 0, 157, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (172, '服装鞋帽', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/baab654a10654d509aaf94565e5f2a79.png', 1, 0, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (173, '女装', '', 0, 172, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (174, 'T恤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204801.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (175, '衬衫', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204802.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (176, '雪纺衫', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204803.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (177, '卫衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204804.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (178, '马甲', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204805.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (179, '连衣裙', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204806.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (180, '半身裙', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204807.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (181, '牛仔裤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204808.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (182, '休闲裤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204809.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (183, '正装裤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204810.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (184, '西服', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204811.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (185, '短外套', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204812.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (186, '风衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204813.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (187, '大衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204814.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (188, '皮衣皮草', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204815.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (189, '棉服', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204816.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (190, '羽绒服', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204817.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (191, '孕妇装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204818.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (192, '大码装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204819.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (193, '婚纱礼服', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204820.png', 0, 173, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (194, '男装', '', 0, 172, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (195, '衬衫', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204822.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (196, 'T恤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204823.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (197, 'POLO衫', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204824.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (198, '针织衫', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204825.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (199, '卫衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204826.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (200, '马甲／背心', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204827.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (201, '夹克', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204828.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (202, '风衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204829.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (203, '大衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204830.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (204, '皮衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204831.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (205, '外套', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204832.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (206, '西服', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204833.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (207, '牛仔裤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204834.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (208, '休闲裤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204835.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (209, '西裤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204836.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (210, '西服套装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204837.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (211, '大码装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204838.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (212, '中老年装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204839.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (213, '唐装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204840.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (214, '工装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204841.png', 0, 194, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (215, '内衣', '', 0, 172, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (216, '文胸', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204843.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (217, '女式内裤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204844.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (218, '男式内裤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204845.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (219, '家居', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204846.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (220, '睡衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204847.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (221, '塑身衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204848.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (222, '睡袍／浴袍', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204849.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (223, '泳衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204850.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (224, '背心', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204851.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (225, '抹胸', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204852.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (226, '连裤袜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204853.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (227, '美腿袜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204854.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (228, '男袜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204855.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (229, '女袜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204856.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (230, '情趣内衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204857.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (231, '保暖内衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204858.png', 0, 215, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (232, '运动', '', 0, 172, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (233, '休闲鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204860.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (234, '帆布鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204861.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (235, '跑步鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204862.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (236, '篮球鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204863.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (237, '足球鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204864.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (238, '训练鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204865.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (239, '乒羽鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204866.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (240, '拖鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204867.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (241, '卫衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204868.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (242, '夹克', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204869.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (243, 'T恤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204870.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (244, '棉服／羽绒服', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204871.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (245, '运动裤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204872.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (246, '套装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204873.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (247, '运动包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204874.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (248, '运动配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204875.png', 0, 232, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (249, '女鞋', '', 0, 172, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (250, '平底鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204877.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (251, '高跟鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204878.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (252, '单鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204879.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (253, '休闲鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204880.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (254, '凉鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204881.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (255, '女靴', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204882.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (256, '雪地靴', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204883.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (257, '拖鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204884.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (258, '裸靴', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204885.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (259, '筒靴', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204886.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (260, '帆布鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204887.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (261, '雨鞋／雨靴', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204888.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (262, '妈妈鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204889.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (263, '鞋配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204890.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (264, '特色鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204891.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (265, '鱼嘴鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204892.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (266, '布鞋／绣花鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204893.png', 0, 249, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (267, '男鞋', '', 0, 172, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (268, '商务休闲鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204895.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (269, '正装鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204896.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (270, '休闲鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204897.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (271, '凉鞋／沙滩鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204898.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (272, '男靴', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204899.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (273, '功能鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204900.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (274, '拖鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204901.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (275, '传统布鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204902.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (276, '鞋配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204903.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (277, '帆布鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204904.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (278, '豆豆鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204905.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (279, '驾车鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204906.png', 0, 267, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (280, '配饰', '', 0, 172, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (281, '太阳镜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204908.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (282, '框镜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204909.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (283, '皮带', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204910.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (284, '围巾', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204911.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (285, '手套', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204912.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (286, '帽子', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204913.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (287, '领带', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204914.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (288, '袖扣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204915.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (289, '其他配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204916.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (290, '丝巾', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204917.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (291, '披肩', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204918.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (292, '腰带', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204919.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (293, '腰链／腰封', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204920.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (294, '棒球帽', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204921.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (295, '毛线', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204922.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (296, '遮阳帽', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204923.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (297, '防紫外线手套', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204924.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (298, '草帽', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204925.png', 0, 280, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (299, '童装', '', 0, 172, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (300, '套装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204927.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (301, '上衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204928.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (302, '裤子', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204929.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (303, '裙子', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204930.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (304, '内衣／家居服', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204931.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (305, '羽绒服／棉服', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204932.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (306, '亲子装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204933.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (307, '儿童配饰', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204934.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (308, '礼服／演出服', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204935.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (309, '运动鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204936.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (310, '单鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204937.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (311, '靴子', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204938.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (312, '凉鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204939.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (313, '功能鞋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427264204940.png', 0, 299, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (314, '食品饮料', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/28077d744f19430388e5bdceb091b786.jpeg', 0, 0, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (315, '进口食品', '', 0, 314, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (316, '饼干蛋糕', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399365.png', 0, 315, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (317, '糖果巧克力', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399366.png', 0, 315, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (318, '休闲零食', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399367.png', 0, 315, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (319, '冲调饮品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399368.png', 0, 315, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (320, '粮油调味', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399369.png', 0, 315, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (321, '地方特产', '', 0, 314, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (322, '华北', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399371.png', 0, 321, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (323, '西北', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399372.png', 0, 321, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (324, '西南', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399373.png', 0, 321, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (325, '东北', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399374.png', 0, 321, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (326, '华南', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399375.png', 0, 321, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (327, '华东', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399376.png', 0, 321, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (328, '华中', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399377.png', 0, 321, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (329, '休闲食品', '', 0, 314, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (330, '休闲零食', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399379.png', 0, 329, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (331, '坚果炒货', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399380.png', 0, 329, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (332, '肉干肉松', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399381.png', 0, 329, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (333, '蜜饯果脯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399382.png', 0, 329, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (334, '糖果/巧克力', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399383.png', 0, 329, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (335, '饼干蛋糕', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399384.png', 0, 329, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (336, '粮油调味', '', 0, 314, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (337, '米面杂粮', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399386.png', 0, 336, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (338, '食用油', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399387.png', 0, 336, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (339, '调味品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399388.png', 0, 336, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (340, '南北干货', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399389.png', 0, 336, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (341, '方便食品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399390.png', 0, 336, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (342, '有机食品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399391.png', 0, 336, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (343, '中外名酒', '', 0, 314, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (344, '白酒', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399393.png', 0, 343, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (345, '洋酒', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399394.png', 0, 343, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (346, '葡萄酒', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399395.png', 0, 343, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (347, '啤酒', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399396.png', 0, 343, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (348, '黄酒', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399397.png', 0, 343, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (349, '饮料冲调', '', 0, 314, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (350, '水', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399399.png', 0, 349, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (351, '饮料', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399400.png', 0, 349, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (352, '牛奶', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399401.png', 0, 349, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (353, '茶叶', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399402.png', 0, 349, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (354, '咖啡/奶茶', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399403.png', 0, 349, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (355, '冲饮谷物', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399404.png', 0, 349, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (356, '营养健康', '', 0, 314, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (357, '基础营养', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399406.png', 0, 356, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (358, '美体养颜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399407.png', 0, 356, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (359, '滋补调养', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399408.png', 0, 356, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (360, '骨骼健康', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399409.png', 0, 356, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (361, '保健茶饮', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399410.png', 0, 356, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (362, '成分保健', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399411.png', 0, 356, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (363, '无糖食品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399412.png', 0, 356, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (364, '亚健康调理', '', 0, 314, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (365, '调节三高', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399414.png', 0, 364, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (366, '心脑养护', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399415.png', 0, 364, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (367, '改善睡眠', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399416.png', 0, 364, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (368, '肝肾养护', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399417.png', 0, 364, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (369, '免疫调节', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399418.png', 0, 364, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (370, '更多调理', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399419.png', 0, 364, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (371, '健康礼品', '', 0, 314, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (372, '参茸礼品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399421.png', 0, 371, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (373, '更多礼品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399422.png', 0, 371, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (374, '生鲜食品', '', 0, 314, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (375, '水果', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399424.png', 0, 374, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (376, '蔬菜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399425.png', 0, 374, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (377, '海鲜水产', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399426.png', 0, 374, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (378, '禽蛋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399427.png', 0, 374, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (379, '鲜肉', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399428.png', 0, 374, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (380, '加工类肉食', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399429.png', 0, 374, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (381, '冻品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399430.png', 0, 374, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (382, '半成品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399431.png', 0, 374, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (383, '礼品箱包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/122db4358a524e3bb0105a756542ff1c.png', 1, 0, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (384, '潮流女包', '', 0, 383, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (385, '钱包/卡包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399174.png', 0, 384, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (386, '手拿包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399175.png', 0, 384, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (387, '单肩包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399176.png', 0, 384, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (388, '双肩包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399177.png', 0, 384, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (389, '手提包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399178.png', 0, 384, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (390, '斜挎包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399179.png', 0, 384, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (391, '时尚男包', '', 0, 383, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (392, '钱包/卡包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399181.png', 0, 391, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (393, '男士手包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399182.png', 0, 391, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (394, '腰带／礼盒', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399183.png', 0, 391, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (395, '商务公文包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399184.png', 0, 391, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (396, '功能箱包', '', 0, 383, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (397, '电脑数码包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399186.png', 0, 396, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (398, '拉杆箱', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399187.png', 0, 396, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (399, '旅行包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399188.png', 0, 396, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (400, '旅行用品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399189.png', 0, 396, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (401, '休闲运动包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399190.png', 0, 396, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (402, '登山包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399191.png', 0, 396, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (403, '妈咪包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399192.png', 0, 396, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (404, '书包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399193.png', 0, 396, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (405, '礼品', '', 0, 383, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (406, '火机烟具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399195.png', 0, 405, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (407, '礼品文具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399196.png', 0, 405, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (408, '瑞士军刀', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399197.png', 0, 405, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (409, '收藏品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399198.png', 0, 405, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (410, '工艺礼品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399199.png', 0, 405, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (411, '创意礼品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399200.png', 0, 405, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (412, '礼卡礼卷', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399201.png', 0, 405, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (413, '鲜花速递', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399202.png', 0, 405, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (414, '婚庆用品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399203.png', 0, 405, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (415, '奢侈品', '', 0, 383, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (416, '奢侈品箱包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399205.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (417, '钱包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399206.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (418, '服饰', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399207.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (419, '腰带', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399208.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (420, '太阳镜眼镜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399209.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (421, '配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399210.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (422, 'GUCCI', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399211.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (423, 'PRADA', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399212.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (424, 'FENDI', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399213.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (425, 'BURBERRY', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399214.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (426, 'ARMANI', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399215.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (427, 'COACH', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399216.png', 0, 415, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (428, '个护化妆', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/da02afbcf3b34885a1bff38c828fc295.jpeg', 1, 0, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (429, '面部护理', '', 0, 428, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (430, '洁面乳', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399320.png', 0, 429, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (431, '爽肤水	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399321.png', 0, 429, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (432, '精华露	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399322.png', 0, 429, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (433, '乳液面霜	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399323.png', 0, 429, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (434, '面膜面贴	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399324.png', 0, 429, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (435, '眼部护理	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399325.png', 0, 429, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (436, 'T区护理	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399326.png', 0, 429, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (437, '护肤套装	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399327.png', 0, 429, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (438, '防晒隔离	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399328.png', 0, 429, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (439, '身体护理', '', 0, 428, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (440, '洗发护发	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399330.png', 0, 439, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (441, '染发/造型	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399331.png', 0, 439, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (442, '沐浴', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399332.png', 0, 439, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (443, '身体乳	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399333.png', 0, 439, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (444, '香薰精油	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399334.png', 0, 439, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (445, '纤体瘦身	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399335.png', 0, 439, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (446, '脱毛膏', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399336.png', 0, 439, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (447, '手足护理	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399337.png', 0, 439, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (448, '洗护套装	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399338.png', 0, 439, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (449, '口腔护理', '', 0, 428, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (450, '牙膏/牙粉	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399340.png', 0, 449, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (451, '漱口水	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399341.png', 0, 449, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (452, '女性护理', '', 0, 428, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (453, '卫生巾	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399343.png', 0, 452, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (454, '卫生护垫	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399344.png', 0, 452, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (455, '洗液	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399345.png', 0, 452, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (456, '男士护理', '', 0, 428, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (457, '男士香水	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399347.png', 0, 456, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (458, '剃须护理	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399348.png', 0, 456, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (459, '魅力彩妆', '', 0, 428, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (460, '粉底/遮瑕	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399350.png', 0, 459, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (461, '腮红	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399351.png', 0, 459, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (462, '眼影/眼线	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399352.png', 0, 459, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (463, '眉笔	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399353.png', 0, 459, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (464, '睫毛膏	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399354.png', 0, 459, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (465, '唇膏唇彩	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399355.png', 0, 459, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (466, '彩妆组合	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399356.png', 0, 459, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (467, '卸妆', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399357.png', 0, 459, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (468, '美甲	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399358.png', 0, 459, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (469, '假发', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399359.png', 0, 459, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (470, '香水SPA', '', 0, 428, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (471, '女士香水', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399361.png', 0, 470, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (472, '男士香水	', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399362.png', 0, 470, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (473, '厨房餐饮', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/2ece687236434f9983a8b4d9d6de50ef.png', 1, 0, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (474, '烹饪锅具', '', 0, 473, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (475, '炒锅', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399276.png', 0, 474, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (476, '煎锅', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399277.png', 0, 474, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (477, '压力锅', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399278.png', 0, 474, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (478, '汤锅', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399279.png', 0, 474, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (479, '蒸锅', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399280.png', 0, 474, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (480, '奶锅', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399281.png', 0, 474, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (481, '套锅', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399282.png', 0, 474, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (482, '煲锅', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399283.png', 0, 474, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (483, '水壶', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399284.png', 0, 474, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (484, '刀剪菜板', '', 0, 473, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (485, '单刀', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399286.png', 0, 484, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (486, '剪刀', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399287.png', 0, 484, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (487, '套刀', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399288.png', 0, 484, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (488, '砧板', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399289.png', 0, 484, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (489, '刀具配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399290.png', 0, 484, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (490, '收纳保鲜', '', 0, 473, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (491, '保鲜盒', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399292.png', 0, 490, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (492, '保鲜膜/袋', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399293.png', 0, 490, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (493, '塑料器皿', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399294.png', 0, 490, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (494, '饭盒/提锅', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399295.png', 0, 490, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (495, '水具酒具', '', 0, 473, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (496, '塑料杯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399297.png', 0, 495, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (497, '运动水壶', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399298.png', 0, 495, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (498, '玻璃杯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399299.png', 0, 495, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (499, '陶瓷马克杯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399300.png', 0, 495, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (500, '保温壶', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399301.png', 0, 495, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (501, '杯具套件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399302.png', 0, 495, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (502, '餐具', '', 0, 473, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (503, '餐具套装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399304.png', 0, 502, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (504, '碗/碟/盘', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399305.png', 0, 502, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (505, '筷勺/刀叉', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399306.png', 0, 502, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (506, '一次性餐具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399307.png', 0, 502, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (507, '茶具/咖啡具', '', 0, 473, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (508, '整套茶具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399309.png', 0, 507, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (509, '茶杯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399310.png', 0, 507, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (510, '茶壶', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399311.png', 0, 507, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (511, '茶盘茶托', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399312.png', 0, 507, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (512, '茶叶罐', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399313.png', 0, 507, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (513, '茶具配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399314.png', 0, 507, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (514, '茶宠摆件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399315.png', 0, 507, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (515, '咖啡具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399316.png', 0, 507, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (516, '其他', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399317.png', 0, 507, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (517, '家居家装', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/f32cfef17acc403880d25b11ad501976.png', 0, 0, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (518, '家纺', '', 0, 517, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (519, '床品套件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399219.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (520, '被子', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399220.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (521, '枕芯枕套', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399221.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (522, '床单被罩', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399222.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (523, '毛巾被/毯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399223.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (524, '床垫/床褥', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399224.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (525, '蚊帐/凉席', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399225.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (526, '抱枕坐垫', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399226.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (527, '毛巾家纺', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399227.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (528, '', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399228.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (529, '窗帘/窗纱', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399229.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (530, '酒店用品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399230.png', 0, 518, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (531, '灯具', '', 0, 517, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (532, '台灯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399232.png', 0, 531, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (533, '节能灯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399233.png', 0, 531, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (534, '装饰灯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399234.png', 0, 531, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (535, '落地灯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399235.png', 0, 531, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (536, '应急灯/手电', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399236.png', 0, 531, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (537, 'LED灯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399237.png', 0, 531, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (538, '吸顶灯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399238.png', 0, 531, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (539, '五金电器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399239.png', 0, 531, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (540, '吊灯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399240.png', 0, 531, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (541, '氛围照明', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399241.png', 0, 531, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (542, '生活日用', '', 0, 517, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (543, '收纳用品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399243.png', 0, 542, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (544, '雨伞雨具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399244.png', 0, 542, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (545, '浴室用品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399245.png', 0, 542, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (546, '缝纫用品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399246.png', 0, 542, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (547, '洗晒用品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399247.png', 0, 542, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (548, '净化除味', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399248.png', 0, 542, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (549, '家装软饰', '', 0, 517, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (550, '桌布/罩件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399250.png', 0, 549, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (551, '地毯地垫', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399251.png', 0, 549, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (552, '沙发垫套', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399252.png', 0, 549, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (553, '相框/相片墙', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399253.png', 0, 549, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (554, '墙画墙贴', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399254.png', 0, 549, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (555, '节庆饰品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399255.png', 0, 549, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (556, '手工/十字绣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399256.png', 0, 549, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (557, '工艺摆件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399257.png', 0, 549, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (558, '其他', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399258.png', 0, 549, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (559, '清洁日用', '', 0, 517, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (560, '纸品湿巾', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399260.png', 0, 559, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (561, '衣物清洁', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399261.png', 0, 559, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (562, '清洁工具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399262.png', 0, 559, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (563, '驱虫用品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399263.png', 0, 559, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (564, '居室清洁', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399264.png', 0, 559, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (565, '皮具护理', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399265.png', 0, 559, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (566, '宠物生活', '', 0, 517, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (567, '宠物主粮', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399267.png', 0, 566, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (568, '宠物零食', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399268.png', 0, 566, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (569, '营养品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399269.png', 0, 566, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (570, '家居日用', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399270.png', 0, 566, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (571, '玩具服饰', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399271.png', 0, 566, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (572, '出行装备', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399272.png', 0, 566, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (573, '医护美容', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399273.png', 0, 566, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (574, '汽车用品', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/78c1344a525f4b859fdd7b7f640824cd.png', 0, 0, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (575, '电子电器', '', 0, 574, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (576, '导航仪', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399488.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (577, '安全预警仪', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399489.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (578, '行车记录仪', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399490.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (579, '倒车雷达', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399491.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (580, '车载电源', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399492.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (581, '车载蓝牙', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399493.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (582, '车载影音', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399494.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (583, '车载净化器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399495.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (584, '车载冰箱', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399496.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (585, '车载吸尘器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399497.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (586, '充气泵', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399498.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (587, '车载生活电器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399499.png', 0, 575, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (588, '系统养护', '', 0, 574, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (589, '机油', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399501.png', 0, 588, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (590, '添加剂', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399502.png', 0, 588, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (591, '防冻冷却液', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399503.png', 0, 588, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (592, '附属油', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399504.png', 0, 588, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (593, '底盘装甲', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399505.png', 0, 588, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (594, '空调清洗剂', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399506.png', 0, 588, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (595, '改装配件', '', 0, 574, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (596, '雨刷', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399508.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (597, '车灯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399509.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (598, '轮胎', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399510.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (599, '贴膜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399511.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (600, '后视镜', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399512.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (601, '机油滤', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399513.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (602, '火花塞', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399514.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (603, '刹车片', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399515.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (604, '蓄电池', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399516.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (605, '车身装饰', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399517.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (606, '维修配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399518.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (607, '汽修工具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399519.png', 0, 595, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (608, '汽车美容', '', 0, 574, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (609, '漆面美容', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399521.png', 0, 608, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (610, '漆面修复', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399522.png', 0, 608, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (611, '内饰清洁', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399523.png', 0, 608, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (612, '玻璃美容', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399524.png', 0, 608, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (613, '洗车机', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399525.png', 0, 608, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (614, '洗车水枪', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399526.png', 0, 608, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (615, '洗车配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399527.png', 0, 608, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (616, '车掸', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399528.png', 0, 608, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (617, '洗车液', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399529.png', 0, 608, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (618, '坐垫脚垫', '', 0, 574, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (619, '脚垫', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399531.png', 0, 618, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (620, '坐垫', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399532.png', 0, 618, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (621, '座套', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399533.png', 0, 618, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (622, '内饰精品', '', 0, 574, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (623, '车用香水', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399535.png', 0, 622, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (624, '车用炭包', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399536.png', 0, 622, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (625, '头枕腰枕', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399537.png', 0, 622, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (626, '方向盘套', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399538.png', 0, 622, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (627, '功能小件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399539.png', 0, 622, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (628, '车内挂饰', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399540.png', 0, 622, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (629, '安全自驾', '', 0, 574, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (630, '安全座椅', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399542.png', 0, 629, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (631, '应急救援', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399543.png', 0, 629, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (632, '汽修工具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399544.png', 0, 629, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (633, '保温箱', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399545.png', 0, 629, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (634, '储物箱', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399546.png', 0, 629, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (635, '车衣', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399547.png', 0, 629, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (636, '车锁', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399548.png', 0, 629, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (637, '摩托车设备', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399549.png', 0, 629, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (638, '整车', '', 0, 574, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (639, '新车', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399551.png', 0, 638, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (640, '二手车', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399552.png', 0, 638, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (641, '玩具乐器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/d4197422a66441cd88ec492d56bbf560.png', 0, 0, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (642, '适用年龄', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (643, '1-3岁', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399678.png', 0, 642, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (644, '3-6岁', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399679.png', 0, 642, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (645, '6-14岁', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399680.png', 0, 642, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (646, '14岁以上', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399681.png', 0, 642, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (647, '遥控/电动', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (648, '遥控车', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399683.png', 0, 647, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (649, '遥控飞机', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399684.png', 0, 647, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (650, '遥控船', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399685.png', 0, 647, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (651, '机器人/电动', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399686.png', 0, 647, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (652, '轨道/助力', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399687.png', 0, 647, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (653, '毛绒布艺', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (654, '毛绒/布艺', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399689.png', 0, 653, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (655, '靠垫/抱枕', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399690.png', 0, 653, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (656, '娃娃玩具', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (657, '芭比娃娃', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399692.png', 0, 656, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (658, '卡通娃娃', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399693.png', 0, 656, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (659, '智能娃娃', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399694.png', 0, 656, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (660, '模型玩具', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (661, '仿真模型', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399696.png', 0, 660, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (662, '拼插模型', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399697.png', 0, 660, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (663, '收藏爱好', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399698.png', 0, 660, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (664, '健身玩具', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (665, '炫舞毯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399700.png', 0, 664, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (666, '爬行垫/毯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399701.png', 0, 664, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (667, '户外玩具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399702.png', 0, 664, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (668, '戏水玩具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399703.png', 0, 664, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (669, '动漫玩具', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (670, '电影周边', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399705.png', 0, 669, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (671, '卡通周边', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399706.png', 0, 669, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (672, '网游周边', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399707.png', 0, 669, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (673, '益智玩具', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (674, '摇铃/床铃', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399709.png', 0, 673, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (675, '健身架', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399710.png', 0, 673, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (676, '早教启智', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399711.png', 0, 673, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (677, '拖拉玩具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399712.png', 0, 673, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (678, '积木拼插', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (679, '积木', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399714.png', 0, 678, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (680, '拼图', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399715.png', 0, 678, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (681, '磁力棒', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399716.png', 0, 678, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (682, '立体拼插', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399717.png', 0, 678, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (683, 'DIY玩具', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (684, '手工彩泥', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399719.png', 0, 683, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (685, '绘画工具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399720.png', 0, 683, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (686, '情景玩具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399721.png', 0, 683, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (687, '创意减压', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (688, '减压玩具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399723.png', 0, 687, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (689, '创意玩具', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399724.png', 0, 687, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (690, '乐器相关', '', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (691, '钢琴', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399726.png', 0, 690, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (692, '电子琴', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399727.png', 0, 690, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (693, '手风琴', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399728.png', 0, 690, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (694, '吉他/贝斯', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399729.png', 0, 690, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (695, '民族管弦乐器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399730.png', 0, 690, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (696, '西洋管弦乐', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399731.png', 0, 690, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (697, '口琴/口风琴/竖笛', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399732.png', 0, 690, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (698, '西洋打击乐器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399733.png', 0, 690, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (699, '各式乐器配件', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399734.png', 0, 690, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (700, '电脑音乐', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399735.png', 0, 690, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (701, '工艺礼品乐器', 'https://lilishop-oss.oss-cn-beijing.aliyuncs.com/1348576427268399736.png', 0, 690, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (705, '运动户外', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/classify/20230531/5883c333-2a42-44b2-ba12-e1d2f4eef8f2.jpeg', 1, 0, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (706, '健身体育', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/classify/20230607/3dd7f88f-6d90-43e5-b3a5-e0fefef89179.jpg', 0, 705, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (707, '跑步机', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/classify/20230531/a2cfc538-f51e-48b0-a78e-a83098313f53.jpeg', 0, 706, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (708, '动感单车', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/classify/20230529/c9f325d6-4e88-441d-b4f3-75f07f2d65d3.png', 0, 706, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (709, '手机配件', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/classify/20230529/daba466e-b49c-464f-85c5-193247729692.jpg', 0, 2, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (710, '潮流盲盒', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/classify/20230531/91ce020d-6e84-4967-ae64-c221f56c198f.jpg', 0, 669, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (711, '童车童床', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/classify/20230607/b4c96441-99dc-4c9a-857b-9a7cf9ea878e.jpg', 0, 641, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (712, '自行车', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/classify/20230607/3d3b753a-9c0e-4416-9454-7a07313aa4db.jpg', 0, 711, '', 0, 0);
INSERT INTO `s_goods_classify` VALUES (713, '三轮车', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/classify/20230607/de42ed75-4b44-4bf3-8c28-47eefa20a351.jpg', 0, 711, '', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for s_image_order
-- ----------------------------
DROP TABLE IF EXISTS `s_image_order`;
CREATE TABLE `s_image_order` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `order_sn` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单编号',
  `member_id` int(11) NOT NULL COMMENT '会员id',
  `price` decimal(20,2) NOT NULL COMMENT '金额',
  `image_url` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '图片地址',
  `status` int(1) DEFAULT '0' COMMENT '状态0未支付1已支付',
  `pay_order_sn` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '支付单号',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='图片下载订单';

-- ----------------------------
-- Table structure for s_logistics
-- ----------------------------
DROP TABLE IF EXISTS `s_logistics`;
CREATE TABLE `s_logistics` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `logistics_name` varchar(64) DEFAULT NULL COMMENT '公司名称',
  `code` varchar(64) DEFAULT NULL COMMENT '公司代码',
  `status` int(8) DEFAULT '0' COMMENT '状态',
  `sort` int(8) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='物流公司';

-- ----------------------------
-- Records of s_logistics
-- ----------------------------
BEGIN;
INSERT INTO `s_logistics` VALUES (1, '中通快读', 'zhongtong', 1, 0);
INSERT INTO `s_logistics` VALUES (2, '邮政', 'youzheng', 1, 0);
INSERT INTO `s_logistics` VALUES (3, '申通快递', 'shentong', 1, 0);
INSERT INTO `s_logistics` VALUES (4, '顺丰物流', 'shunfeng', 1, 6);
INSERT INTO `s_logistics` VALUES (5, '韵达快递', 'yunda', 1, 0);
INSERT INTO `s_logistics` VALUES (6, '圆通快递', 'yuantong', 1, 0);
COMMIT;

-- ----------------------------
-- Table structure for s_member
-- ----------------------------
DROP TABLE IF EXISTS `s_member`;
CREATE TABLE `s_member` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '会员名称',
  `member_no` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '会员编号',
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '会员密码',
  `mobile` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机号',
  `sex` int(1) DEFAULT '0' COMMENT '性别,1=男,0=女',
  `nickname` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '昵称',
  `face` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像地址',
  `balance` decimal(10,2) DEFAULT '0.00' COMMENT '余额',
  `points` decimal(20,2) DEFAULT '0.00' COMMENT '积分',
  `bonus_points` decimal(20,2) DEFAULT NULL COMMENT '会员累计强制消费积分',
  `grade_id` int(10) DEFAULT '0' COMMENT '会员等级id',
  `invite_member_id` int(8) NOT NULL DEFAULT '0' COMMENT '邀请人会员id',
  `status` int(1) DEFAULT '0' COMMENT '状态:0=正常,1=禁用',
  `open_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信openid',
  `create_time` bigint(20) DEFAULT '0' COMMENT '创建时间(注册时间)',
  `last_login` bigint(20) DEFAULT NULL COMMENT '上次登录时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='会员';

-- ----------------------------
-- Records of s_member
-- ----------------------------
BEGIN;
INSERT INTO `s_member` VALUES (1, 'Sylow', 'm_15911100004', '$2a$10$2.UQsYkpPvR766G5bZXzmuFfSF3HSX.Hyzs8X7D9wLhMQ2qPjGWpu', '15911100004', 0, 'Sylow', NULL, 0.00, 0.00, NULL, 1, 0, 0, NULL, 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for s_member_activity_income
-- ----------------------------
DROP TABLE IF EXISTS `s_member_activity_income`;
CREATE TABLE `s_member_activity_income` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(8) NOT NULL COMMENT '会员id',
  `buy_price` decimal(10,2) NOT NULL COMMENT '买入价格',
  `sell_price` decimal(10,2) NOT NULL COMMENT '卖出价格',
  `service_price` decimal(10,2) NOT NULL COMMENT '服务费',
  `profit` decimal(10,2) NOT NULL COMMENT '利润=卖出价格减去买入和服务费',
  `rush_purchase_detail_id` int(8) NOT NULL COMMENT '活动明细id',
  `activity_id` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT ' 活动期数',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='会员收益表';

-- ----------------------------
-- Table structure for s_member_address
-- ----------------------------
DROP TABLE IF EXISTS `s_member_address`;
CREATE TABLE `s_member_address` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(8) unsigned DEFAULT NULL COMMENT '会员id',
  `area_id` int(8) unsigned DEFAULT NULL COMMENT '收货地区ID',
  `area_name` varchar(512) DEFAULT NULL COMMENT '收货地区名称，省市区',
  `address` varchar(512) DEFAULT NULL COMMENT '收货详细地址',
  `name` varchar(64) DEFAULT NULL COMMENT '收货人姓名',
  `mobile` varchar(16) DEFAULT NULL COMMENT '收货电话',
  `is_def` int(1) DEFAULT '0' COMMENT '是否默认 1=默认 0=不默认',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='会员收货地址';

-- ----------------------------
-- Table structure for s_member_balance_detail
-- ----------------------------
DROP TABLE IF EXISTS `s_member_balance_detail`;
CREATE TABLE `s_member_balance_detail` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(8) DEFAULT NULL COMMENT '会员id',
  `source` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '来源',
  `type` int(1) DEFAULT NULL COMMENT '0系统默认, 1服务费分成',
  `price` decimal(10,2) DEFAULT NULL COMMENT '金额',
  `memo` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` bigint(20) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户余额明细';

-- ----------------------------
-- Table structure for s_member_grade
-- ----------------------------
DROP TABLE IF EXISTS `s_member_grade`;
CREATE TABLE `s_member_grade` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `grade_name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '等级名称',
  `grade_id` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '等级标识',
  `disable` int(1) NOT NULL DEFAULT '0' COMMENT '禁用状态0=正常，1禁用，2删除',
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='会员等级';

-- ----------------------------
-- Records of s_member_grade
-- ----------------------------
BEGIN;
INSERT INTO `s_member_grade` VALUES (1, '普通会员', 'putong', 0, 4);
INSERT INTO `s_member_grade` VALUES (2, '优先会员(提前抢购)', 'youxian', 0, 3);
INSERT INTO `s_member_grade` VALUES (3, '高级会员(无限,提前)', 'gaoji', 0, 1);
INSERT INTO `s_member_grade` VALUES (4, '团长(提前,3块)', 'tuanzhang', 0, 2);
INSERT INTO `s_member_grade` VALUES (5, '购物车', 'cart', 0, 0);
INSERT INTO `s_member_grade` VALUES (6, '团长购物车', 'tuanzhang_cart', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for s_member_payment
-- ----------------------------
DROP TABLE IF EXISTS `s_member_payment`;
CREATE TABLE `s_member_payment` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '主健',
  `member_id` int(8) DEFAULT NULL COMMENT '会员id',
  `bank_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '银行代码',
  `bank_name` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '银行名称',
  `account_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '账户名称',
  `account_number` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '账号',
  `alipay` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '支付宝收款码',
  `wxpay` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信收款码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='会员收款方式';

-- ----------------------------
-- Records of s_member_payment
-- ----------------------------
BEGIN;
INSERT INTO `s_member_payment` VALUES (1, 1, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for s_member_points_detail
-- ----------------------------
DROP TABLE IF EXISTS `s_member_points_detail`;
CREATE TABLE `s_member_points_detail` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(8) DEFAULT NULL COMMENT '会员id',
  `source` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '来源',
  `type` int(1) DEFAULT NULL COMMENT '0上架商品1系统赠送',
  `points` decimal(10,2) DEFAULT NULL COMMENT '积分',
  `memo` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  `create_time` bigint(20) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户积分明细';

-- ----------------------------
-- Table structure for s_member_sign
-- ----------------------------
DROP TABLE IF EXISTS `s_member_sign`;
CREATE TABLE `s_member_sign` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(8) NOT NULL COMMENT '会员id',
  `sign_img` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '签名图片地址',
  `sign_time` bigint(20) DEFAULT NULL COMMENT '签名时间',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态0未通过,1通过',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='会员签名记录表';

-- ----------------------------
-- Table structure for s_member_withdrawal
-- ----------------------------
DROP TABLE IF EXISTS `s_member_withdrawal`;
CREATE TABLE `s_member_withdrawal` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `member_id` int(8) DEFAULT NULL COMMENT '会员id',
  `price` decimal(10,2) DEFAULT NULL COMMENT '提现金额',
  `status` int(1) DEFAULT '0' COMMENT '状态0 待审核 1审核通过 2审核不通过 3打款成功 4关闭(用户撤销)',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户提现记录';

-- ----------------------------
-- Table structure for s_order
-- ----------------------------
DROP TABLE IF EXISTS `s_order`;
CREATE TABLE `s_order` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `order_sn` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '订单编号',
  `order_type` int(1) NOT NULL DEFAULT '0' COMMENT '订单类型',
  `member_id` int(8) NOT NULL COMMENT '会员id',
  `member_name` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '会员名称',
  `order_status` int(1) NOT NULL DEFAULT '0' COMMENT '订单状态',
  `pay_status` int(1) NOT NULL DEFAULT '0' COMMENT '付款状态',
  `payment_time` bigint(20) DEFAULT NULL COMMENT '支付时间',
  `payment_sn` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '支付单单号',
  `order_price` decimal(20,2) NOT NULL COMMENT '订单总金额',
  `goods_price` decimal(20,2) NOT NULL COMMENT '商品总金额',
  `shipping_price` decimal(20,2) DEFAULT NULL COMMENT '配送金额',
  `discount_price` decimal(20,2) DEFAULT NULL COMMENT '优惠金额',
  `discount_rule` text COLLATE utf8mb4_bin COMMENT '优惠规则',
  `goods_num` int(8) NOT NULL DEFAULT '0' COMMENT '商品总数',
  `ship_area_id` int(8) DEFAULT NULL COMMENT '配送区域id',
  `ship_area_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '配送区域名称',
  `ship_full_address` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '配送详细地址',
  `ship_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '收货人姓名',
  `ship_mobile` varchar(18) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '收货人手机号',
  `ship_time` bigint(20) DEFAULT NULL COMMENT '发货时间',
  `logistics_sn` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '物流单号',
  `logistics_id` int(8) DEFAULT NULL COMMENT '物流公司id',
  `logistics_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '物流公司名称',
  `remark` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单备注',
  `cancel_reason` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单取消原因',
  `create_time` bigint(20) NOT NULL COMMENT '订单创建时间',
  `complete_time` bigint(20) DEFAULT NULL COMMENT '完成时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单表';

-- ----------------------------
-- Table structure for s_order_item
-- ----------------------------
DROP TABLE IF EXISTS `s_order_item`;
CREATE TABLE `s_order_item` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `order_id` int(8) DEFAULT NULL COMMENT '订单ID',
  `order_sn` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单编号',
  `goods_id` int(8) DEFAULT NULL COMMENT '商品ID',
  `goods_name` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品名称',
  `goods_img` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品主图',
  `goods_num` int(8) DEFAULT '0' COMMENT '商品数量',
  `goods_original_price` decimal(20,2) DEFAULT NULL COMMENT '商品原价',
  `goods_price` decimal(20,2) DEFAULT NULL COMMENT '商品成交价',
  `goods_discount_price` decimal(20,2) DEFAULT NULL COMMENT '商品优惠金额',
  `discount_rule` text COLLATE utf8mb4_bin COMMENT '优惠规则',
  `status` int(1) DEFAULT '0' COMMENT '状态',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单子项表';

-- ----------------------------
-- Table structure for s_put_sale_order
-- ----------------------------
DROP TABLE IF EXISTS `s_put_sale_order`;
CREATE TABLE `s_put_sale_order` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `order_sn` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '订单号与抢购表一致',
  `rush_purchase_id` int(8) DEFAULT NULL COMMENT '活动场次id',
  `rush_purchase_detail_id` int(8) DEFAULT NULL COMMENT '抢购活动明细id',
  `next_rush_purchase_detail_id` int(8) DEFAULT NULL COMMENT '上架后，下一场抢购活动明细id',
  `original_price` decimal(10,2) DEFAULT NULL COMMENT '原价（购买价）',
  `new_price` decimal(10,2) DEFAULT NULL COMMENT '上架后新价格',
  `service_price` decimal(10,2) DEFAULT NULL COMMENT '服务费（上架费）',
  `payment_order_no` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '第三方支付单号',
  `member_id` int(8) DEFAULT NULL COMMENT '上架会员id',
  `status` int(1) DEFAULT '0' COMMENT '状态0默认1支付成功',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='上架订单列表';

-- ----------------------------
-- Table structure for s_rush_purchase
-- ----------------------------
DROP TABLE IF EXISTS `s_rush_purchase`;
CREATE TABLE `s_rush_purchase` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '主健',
  `activity_id` varchar(24) COLLATE utf8mb4_bin NOT NULL COMMENT '活动id',
  `activity_name` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT '活动名称',
  `start_time` bigint(10) NOT NULL COMMENT '开始时间',
  `end_time` bigint(10) NOT NULL COMMENT '结束时间',
  `create_time` bigint(10) NOT NULL COMMENT '创建时间',
  `last_id` int(8) DEFAULT '0' COMMENT '上一场id，自动生成就会有',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '活动状态：正常、关闭',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='抢购活动表';

-- ----------------------------
-- Records of s_rush_purchase
-- ----------------------------
BEGIN;
INSERT INTO `s_rush_purchase` VALUES (1, '1111', '111111', 1691596800, 1691596800, 1691638195, 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for s_rush_purchase_cart
-- ----------------------------
DROP TABLE IF EXISTS `s_rush_purchase_cart`;
CREATE TABLE `s_rush_purchase_cart` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `rush_purchase_detail_id` int(8) DEFAULT NULL COMMENT '抢购商品id',
  `goods_name` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品名称',
  `goods_img` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '商品图片',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `member_id` int(8) DEFAULT NULL COMMENT '会员id',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='抢购购物车';

-- ----------------------------
-- Table structure for s_rush_purchase_detail
-- ----------------------------
DROP TABLE IF EXISTS `s_rush_purchase_detail`;
CREATE TABLE `s_rush_purchase_detail` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '主健',
  `rush_purchase_id` int(8) NOT NULL COMMENT '活动id',
  `activity_id` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '活动场次id',
  `order_sn` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '订单编号',
  `goods_id` int(8) NOT NULL COMMENT '商品id',
  `goods_name` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT '商品名称',
  `seller_id` int(8) DEFAULT NULL COMMENT '卖家会员id',
  `seller_name` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '卖家会员名称',
  `seller_mobile` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '卖家手机',
  `price` decimal(10,2) DEFAULT NULL COMMENT '售价',
  `original_price` decimal(10,2) DEFAULT NULL COMMENT '原价',
  `points` decimal(20,2) DEFAULT NULL COMMENT '可获得积分',
  `buyer_id` int(8) DEFAULT NULL COMMENT '买家会员id',
  `buyer_name` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '买家会员名称',
  `buyer_mobile` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '买家手机号',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `pay_proof` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '支付凭证',
  `pay_order_no` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '第三方支付单号',
  `is_pick_up` int(1) NOT NULL DEFAULT '0' COMMENT '是否提货,0=不提货,1=提货',
  `booking_member_id` int(8) DEFAULT '0' COMMENT '预订会员id',
  `buy_time` bigint(20) DEFAULT NULL COMMENT '购买时间',
  `create_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='抢购活动明细表';

-- ----------------------------
-- Records of s_rush_purchase_detail
-- ----------------------------
BEGIN;
INSERT INTO `s_rush_purchase_detail` VALUES (1, 1, '1111', '2230810110001', 1, '18k金镶玉吊坠和田玉吊坠水滴羊脂玉坠女款 大款', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, 1691638206);
COMMIT;

-- ----------------------------
-- Table structure for sys_admin
-- ----------------------------
DROP TABLE IF EXISTS `sys_admin`;
CREATE TABLE `sys_admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户名',
  `avatar` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像',
  `password` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '密码',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
  `nick_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '昵称',
  `full_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '全名',
  `mobile` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '邮箱',
  `status` int(1) DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='系统用户';

-- ----------------------------
-- Records of sys_admin
-- ----------------------------
BEGIN;
INSERT INTO `sys_admin` VALUES (1, 'superadmin', 'no', '$2a$10$qSUnq1oqGUMF0ZmCFncNVeUE6YkT66iztn8ScgSPoUTGAuqPLpvO6', NULL, 'sylow', NULL, NULL, NULL, 0);
INSERT INTO `sys_admin` VALUES (2, 'admin', 'no', '$2a$10$uYynSEQHSU0F0/ccRZsbjuKwIGeL0eaEPHxML/kvq1/VycZ0spnHq', '信隆优创', '信隆优创', '信隆优创', '15911100004', 'sylow@xinlongtech.com.cn', 0);
INSERT INTO `sys_admin` VALUES (3, 'sylow', NULL, '$2a$10$qSUnq1oqGUMF0ZmCFncNVeUE6YkT66iztn8ScgSPoUTGAuqPLpvO6', NULL, 'sylow', NULL, NULL, NULL, 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_adv
-- ----------------------------
DROP TABLE IF EXISTS `sys_adv`;
CREATE TABLE `sys_adv` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `adv_name` varchar(64) DEFAULT NULL COMMENT '广告名称',
  `adv_img` varchar(255) DEFAULT NULL COMMENT '广告图片地址',
  `url` varchar(255) DEFAULT NULL COMMENT '广告地址',
  `promotion_id` int(8) DEFAULT NULL COMMENT '所属广告位id',
  `status` int(1) DEFAULT '0' COMMENT '状态',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='广告表';

-- ----------------------------
-- Records of sys_adv
-- ----------------------------
BEGIN;
INSERT INTO `sys_adv` VALUES (1, '盲盒轮播', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/advImg/20231008/1bc5cb53-d706-48e4-a240-21c6cc1796bb.png', '/', 1, 0, 1686583211);
INSERT INTO `sys_adv` VALUES (5, '上线预告', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/advImg/20230630/4ac83d55-942f-47b1-a71e-67ae2f5e0902.jpeg', '/', 2, 0, 1686885456);
INSERT INTO `sys_adv` VALUES (6, '上线预告2', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/advImg/20230630/14d39c73-606f-410b-b3db-7bd93a14b0e1.jpeg', '/', 2, 0, 1688104179);
COMMIT;

-- ----------------------------
-- Table structure for sys_adv_promotion
-- ----------------------------
DROP TABLE IF EXISTS `sys_adv_promotion`;
CREATE TABLE `sys_adv_promotion` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `promotion_name` varchar(64) NOT NULL COMMENT '广告位名称',
  `promotion_code` varchar(64) NOT NULL COMMENT '广告位模块编码',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='广告位';

-- ----------------------------
-- Records of sys_adv_promotion
-- ----------------------------
BEGIN;
INSERT INTO `sys_adv_promotion` VALUES (1, '首页轮播图', 'home_adv', 1686583167);
INSERT INTO `sys_adv_promotion` VALUES (2, '抢购轮播图', 'purchase_adv', 1686583186);
COMMIT;

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area` (
  `id` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '地区ID',
  `parent_id` int(8) unsigned DEFAULT '0' COMMENT '父级ID',
  `depth` int(1) unsigned DEFAULT '0' COMMENT '地区深度',
  `name` varchar(64) DEFAULT NULL COMMENT '地区名称',
  `postal_code` varchar(12) NOT NULL DEFAULT '' COMMENT '邮编',
  `sort` int(8) NOT NULL DEFAULT '100' COMMENT '地区排序',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `id` (`id`,`parent_id`,`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='地区表';

-- ----------------------------
-- Records of sys_area
-- ----------------------------
BEGIN;
INSERT INTO `sys_area` VALUES (110000, 0, 1, '北京市', '100000', 100);
INSERT INTO `sys_area` VALUES (110100, 110000, 2, '北京市', '100000', 100);
INSERT INTO `sys_area` VALUES (110101, 110100, 3, '东城区', '0', 100);
INSERT INTO `sys_area` VALUES (110102, 110100, 3, '西城区', '0', 100);
INSERT INTO `sys_area` VALUES (110105, 110100, 3, '朝阳区', '0', 100);
INSERT INTO `sys_area` VALUES (110106, 110100, 3, '丰台区', '0', 100);
INSERT INTO `sys_area` VALUES (110107, 110100, 3, '石景山区', '0', 100);
INSERT INTO `sys_area` VALUES (110108, 110100, 3, '海淀区', '0', 100);
INSERT INTO `sys_area` VALUES (110109, 110100, 3, '门头沟区', '0', 100);
INSERT INTO `sys_area` VALUES (110111, 110100, 3, '房山区', '0', 100);
INSERT INTO `sys_area` VALUES (110112, 110100, 3, '通州区', '0', 100);
INSERT INTO `sys_area` VALUES (110113, 110100, 3, '顺义区', '0', 100);
INSERT INTO `sys_area` VALUES (110114, 110100, 3, '昌平区', '0', 100);
INSERT INTO `sys_area` VALUES (110115, 110100, 3, '大兴区', '0', 100);
INSERT INTO `sys_area` VALUES (110116, 110100, 3, '怀柔区', '0', 100);
INSERT INTO `sys_area` VALUES (110117, 110100, 3, '平谷区', '0', 100);
INSERT INTO `sys_area` VALUES (110118, 110100, 3, '密云区', '0', 100);
INSERT INTO `sys_area` VALUES (110119, 110100, 3, '延庆区', '0', 100);
INSERT INTO `sys_area` VALUES (120000, 0, 1, '天津市', '0', 100);
INSERT INTO `sys_area` VALUES (120100, 120000, 2, '天津市', '0', 100);
INSERT INTO `sys_area` VALUES (120101, 120100, 3, '和平区', '0', 100);
INSERT INTO `sys_area` VALUES (120102, 120100, 3, '河东区', '0', 100);
INSERT INTO `sys_area` VALUES (120103, 120100, 3, '河西区', '0', 100);
INSERT INTO `sys_area` VALUES (120104, 120100, 3, '南开区', '0', 100);
INSERT INTO `sys_area` VALUES (120105, 120100, 3, '河北区', '0', 100);
INSERT INTO `sys_area` VALUES (120106, 120100, 3, '红桥区', '0', 100);
INSERT INTO `sys_area` VALUES (120110, 120100, 3, '东丽区', '0', 100);
INSERT INTO `sys_area` VALUES (120111, 120100, 3, '西青区', '0', 100);
INSERT INTO `sys_area` VALUES (120112, 120100, 3, '津南区', '0', 100);
INSERT INTO `sys_area` VALUES (120113, 120100, 3, '北辰区', '0', 100);
INSERT INTO `sys_area` VALUES (120114, 120100, 3, '武清区', '0', 100);
INSERT INTO `sys_area` VALUES (120115, 120100, 3, '宝坻区', '0', 100);
INSERT INTO `sys_area` VALUES (120116, 120100, 3, '滨海新区', '0', 100);
INSERT INTO `sys_area` VALUES (120117, 120100, 3, '宁河区', '0', 100);
INSERT INTO `sys_area` VALUES (120118, 120100, 3, '静海区', '0', 100);
INSERT INTO `sys_area` VALUES (120119, 120100, 3, '蓟州区', '0', 100);
INSERT INTO `sys_area` VALUES (130000, 0, 1, '河北省', '0', 100);
INSERT INTO `sys_area` VALUES (130100, 130000, 2, '石家庄市', '0', 100);
INSERT INTO `sys_area` VALUES (130101, 130100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (130102, 130100, 3, '长安区', '0', 100);
INSERT INTO `sys_area` VALUES (130104, 130100, 3, '桥西区', '0', 100);
INSERT INTO `sys_area` VALUES (130105, 130100, 3, '新华区', '0', 100);
INSERT INTO `sys_area` VALUES (130107, 130100, 3, '井陉矿区', '0', 100);
INSERT INTO `sys_area` VALUES (130108, 130100, 3, '裕华区', '0', 100);
INSERT INTO `sys_area` VALUES (130109, 130100, 3, '藁城区', '0', 100);
INSERT INTO `sys_area` VALUES (130110, 130100, 3, '鹿泉区', '0', 100);
INSERT INTO `sys_area` VALUES (130111, 130100, 3, '栾城区', '0', 100);
INSERT INTO `sys_area` VALUES (130121, 130100, 3, '井陉县', '0', 100);
INSERT INTO `sys_area` VALUES (130123, 130100, 3, '正定县', '0', 100);
INSERT INTO `sys_area` VALUES (130125, 130100, 3, '行唐县', '0', 100);
INSERT INTO `sys_area` VALUES (130126, 130100, 3, '灵寿县', '0', 100);
INSERT INTO `sys_area` VALUES (130127, 130100, 3, '高邑县', '0', 100);
INSERT INTO `sys_area` VALUES (130128, 130100, 3, '深泽县', '0', 100);
INSERT INTO `sys_area` VALUES (130129, 130100, 3, '赞皇县', '0', 100);
INSERT INTO `sys_area` VALUES (130130, 130100, 3, '无极县', '0', 100);
INSERT INTO `sys_area` VALUES (130131, 130100, 3, '平山县', '0', 100);
INSERT INTO `sys_area` VALUES (130132, 130100, 3, '元氏县', '0', 100);
INSERT INTO `sys_area` VALUES (130133, 130100, 3, '赵县', '0', 100);
INSERT INTO `sys_area` VALUES (130183, 130100, 3, '晋州市', '0', 100);
INSERT INTO `sys_area` VALUES (130184, 130100, 3, '新乐市', '0', 100);
INSERT INTO `sys_area` VALUES (130200, 130000, 2, '唐山市', '0', 100);
INSERT INTO `sys_area` VALUES (130201, 130200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (130202, 130200, 3, '路南区', '0', 100);
INSERT INTO `sys_area` VALUES (130203, 130200, 3, '路北区', '0', 100);
INSERT INTO `sys_area` VALUES (130204, 130200, 3, '古冶区', '0', 100);
INSERT INTO `sys_area` VALUES (130205, 130200, 3, '开平区', '0', 100);
INSERT INTO `sys_area` VALUES (130207, 130200, 3, '丰南区', '0', 100);
INSERT INTO `sys_area` VALUES (130208, 130200, 3, '丰润区', '0', 100);
INSERT INTO `sys_area` VALUES (130209, 130200, 3, '曹妃甸区', '0', 100);
INSERT INTO `sys_area` VALUES (130223, 130200, 3, '滦县', '0', 100);
INSERT INTO `sys_area` VALUES (130224, 130200, 3, '滦南县', '0', 100);
INSERT INTO `sys_area` VALUES (130225, 130200, 3, '乐亭县', '0', 100);
INSERT INTO `sys_area` VALUES (130227, 130200, 3, '迁西县', '0', 100);
INSERT INTO `sys_area` VALUES (130229, 130200, 3, '玉田县', '0', 100);
INSERT INTO `sys_area` VALUES (130281, 130200, 3, '遵化市', '0', 100);
INSERT INTO `sys_area` VALUES (130283, 130200, 3, '迁安市', '0', 100);
INSERT INTO `sys_area` VALUES (130300, 130000, 2, '秦皇岛市', '0', 100);
INSERT INTO `sys_area` VALUES (130301, 130300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (130302, 130300, 3, '海港区', '0', 100);
INSERT INTO `sys_area` VALUES (130303, 130300, 3, '山海关区', '0', 100);
INSERT INTO `sys_area` VALUES (130304, 130300, 3, '北戴河区', '0', 100);
INSERT INTO `sys_area` VALUES (130306, 130300, 3, '抚宁区', '0', 100);
INSERT INTO `sys_area` VALUES (130321, 130300, 3, '青龙满族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (130322, 130300, 3, '昌黎县', '0', 100);
INSERT INTO `sys_area` VALUES (130324, 130300, 3, '卢龙县', '0', 100);
INSERT INTO `sys_area` VALUES (130400, 130000, 2, '邯郸市', '0', 100);
INSERT INTO `sys_area` VALUES (130401, 130400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (130402, 130400, 3, '邯山区', '0', 100);
INSERT INTO `sys_area` VALUES (130403, 130400, 3, '丛台区', '0', 100);
INSERT INTO `sys_area` VALUES (130404, 130400, 3, '复兴区', '0', 100);
INSERT INTO `sys_area` VALUES (130406, 130400, 3, '峰峰矿区', '0', 100);
INSERT INTO `sys_area` VALUES (130421, 130400, 3, '邯郸县', '0', 100);
INSERT INTO `sys_area` VALUES (130423, 130400, 3, '临漳县', '0', 100);
INSERT INTO `sys_area` VALUES (130424, 130400, 3, '成安县', '0', 100);
INSERT INTO `sys_area` VALUES (130425, 130400, 3, '大名县', '0', 100);
INSERT INTO `sys_area` VALUES (130426, 130400, 3, '涉县', '0', 100);
INSERT INTO `sys_area` VALUES (130427, 130400, 3, '磁县', '0', 100);
INSERT INTO `sys_area` VALUES (130428, 130400, 3, '肥乡县', '0', 100);
INSERT INTO `sys_area` VALUES (130429, 130400, 3, '永年县', '0', 100);
INSERT INTO `sys_area` VALUES (130430, 130400, 3, '邱县', '0', 100);
INSERT INTO `sys_area` VALUES (130431, 130400, 3, '鸡泽县', '0', 100);
INSERT INTO `sys_area` VALUES (130432, 130400, 3, '广平县', '0', 100);
INSERT INTO `sys_area` VALUES (130433, 130400, 3, '馆陶县', '0', 100);
INSERT INTO `sys_area` VALUES (130434, 130400, 3, '魏县', '0', 100);
INSERT INTO `sys_area` VALUES (130435, 130400, 3, '曲周县', '0', 100);
INSERT INTO `sys_area` VALUES (130481, 130400, 3, '武安市', '0', 100);
INSERT INTO `sys_area` VALUES (130500, 130000, 2, '邢台市', '0', 100);
INSERT INTO `sys_area` VALUES (130501, 130500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (130502, 130500, 3, '桥东区', '0', 100);
INSERT INTO `sys_area` VALUES (130503, 130500, 3, '桥西区', '0', 100);
INSERT INTO `sys_area` VALUES (130521, 130500, 3, '邢台县', '0', 100);
INSERT INTO `sys_area` VALUES (130522, 130500, 3, '临城县', '0', 100);
INSERT INTO `sys_area` VALUES (130523, 130500, 3, '内丘县', '0', 100);
INSERT INTO `sys_area` VALUES (130524, 130500, 3, '柏乡县', '0', 100);
INSERT INTO `sys_area` VALUES (130525, 130500, 3, '隆尧县', '0', 100);
INSERT INTO `sys_area` VALUES (130526, 130500, 3, '任县', '0', 100);
INSERT INTO `sys_area` VALUES (130527, 130500, 3, '南和县', '0', 100);
INSERT INTO `sys_area` VALUES (130528, 130500, 3, '宁晋县', '0', 100);
INSERT INTO `sys_area` VALUES (130529, 130500, 3, '巨鹿县', '0', 100);
INSERT INTO `sys_area` VALUES (130530, 130500, 3, '新河县', '0', 100);
INSERT INTO `sys_area` VALUES (130531, 130500, 3, '广宗县', '0', 100);
INSERT INTO `sys_area` VALUES (130532, 130500, 3, '平乡县', '0', 100);
INSERT INTO `sys_area` VALUES (130533, 130500, 3, '威县', '0', 100);
INSERT INTO `sys_area` VALUES (130534, 130500, 3, '清河县', '0', 100);
INSERT INTO `sys_area` VALUES (130535, 130500, 3, '临西县', '0', 100);
INSERT INTO `sys_area` VALUES (130581, 130500, 3, '南宫市', '0', 100);
INSERT INTO `sys_area` VALUES (130582, 130500, 3, '沙河市', '0', 100);
INSERT INTO `sys_area` VALUES (130600, 130000, 2, '保定市', '0', 100);
INSERT INTO `sys_area` VALUES (130601, 130600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (130602, 130600, 3, '竞秀区', '0', 100);
INSERT INTO `sys_area` VALUES (130606, 130600, 3, '莲池区', '0', 100);
INSERT INTO `sys_area` VALUES (130607, 130600, 3, '满城区', '0', 100);
INSERT INTO `sys_area` VALUES (130608, 130600, 3, '清苑区', '0', 100);
INSERT INTO `sys_area` VALUES (130609, 130600, 3, '徐水区', '0', 100);
INSERT INTO `sys_area` VALUES (130623, 130600, 3, '涞水县', '0', 100);
INSERT INTO `sys_area` VALUES (130624, 130600, 3, '阜平县', '0', 100);
INSERT INTO `sys_area` VALUES (130626, 130600, 3, '定兴县', '0', 100);
INSERT INTO `sys_area` VALUES (130627, 130600, 3, '唐县', '0', 100);
INSERT INTO `sys_area` VALUES (130628, 130600, 3, '高阳县', '0', 100);
INSERT INTO `sys_area` VALUES (130629, 130600, 3, '容城县', '0', 100);
INSERT INTO `sys_area` VALUES (130630, 130600, 3, '涞源县', '0', 100);
INSERT INTO `sys_area` VALUES (130631, 130600, 3, '望都县', '0', 100);
INSERT INTO `sys_area` VALUES (130632, 130600, 3, '安新县', '0', 100);
INSERT INTO `sys_area` VALUES (130633, 130600, 3, '易县', '0', 100);
INSERT INTO `sys_area` VALUES (130634, 130600, 3, '曲阳县', '0', 100);
INSERT INTO `sys_area` VALUES (130635, 130600, 3, '蠡县', '0', 100);
INSERT INTO `sys_area` VALUES (130636, 130600, 3, '顺平县', '0', 100);
INSERT INTO `sys_area` VALUES (130637, 130600, 3, '博野县', '0', 100);
INSERT INTO `sys_area` VALUES (130638, 130600, 3, '雄县', '0', 100);
INSERT INTO `sys_area` VALUES (130681, 130600, 3, '涿州市', '0', 100);
INSERT INTO `sys_area` VALUES (130683, 130600, 3, '安国市', '0', 100);
INSERT INTO `sys_area` VALUES (130684, 130600, 3, '高碑店市', '0', 100);
INSERT INTO `sys_area` VALUES (130700, 130000, 2, '张家口市', '0', 100);
INSERT INTO `sys_area` VALUES (130701, 130700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (130702, 130700, 3, '桥东区', '0', 100);
INSERT INTO `sys_area` VALUES (130703, 130700, 3, '桥西区', '0', 100);
INSERT INTO `sys_area` VALUES (130705, 130700, 3, '宣化区', '0', 100);
INSERT INTO `sys_area` VALUES (130706, 130700, 3, '下花园区', '0', 100);
INSERT INTO `sys_area` VALUES (130708, 130700, 3, '万全区', '0', 100);
INSERT INTO `sys_area` VALUES (130709, 130700, 3, '崇礼区', '0', 100);
INSERT INTO `sys_area` VALUES (130722, 130700, 3, '张北县', '0', 100);
INSERT INTO `sys_area` VALUES (130723, 130700, 3, '康保县', '0', 100);
INSERT INTO `sys_area` VALUES (130724, 130700, 3, '沽源县', '0', 100);
INSERT INTO `sys_area` VALUES (130725, 130700, 3, '尚义县', '0', 100);
INSERT INTO `sys_area` VALUES (130726, 130700, 3, '蔚县', '0', 100);
INSERT INTO `sys_area` VALUES (130727, 130700, 3, '阳原县', '0', 100);
INSERT INTO `sys_area` VALUES (130728, 130700, 3, '怀安县', '0', 100);
INSERT INTO `sys_area` VALUES (130730, 130700, 3, '怀来县', '0', 100);
INSERT INTO `sys_area` VALUES (130731, 130700, 3, '涿鹿县', '0', 100);
INSERT INTO `sys_area` VALUES (130732, 130700, 3, '赤城县', '0', 100);
INSERT INTO `sys_area` VALUES (130800, 130000, 2, '承德市', '0', 100);
INSERT INTO `sys_area` VALUES (130801, 130800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (130802, 130800, 3, '双桥区', '0', 100);
INSERT INTO `sys_area` VALUES (130803, 130800, 3, '双滦区', '0', 100);
INSERT INTO `sys_area` VALUES (130804, 130800, 3, '鹰手营子矿区', '0', 100);
INSERT INTO `sys_area` VALUES (130821, 130800, 3, '承德县', '0', 100);
INSERT INTO `sys_area` VALUES (130822, 130800, 3, '兴隆县', '0', 100);
INSERT INTO `sys_area` VALUES (130823, 130800, 3, '平泉县', '0', 100);
INSERT INTO `sys_area` VALUES (130824, 130800, 3, '滦平县', '0', 100);
INSERT INTO `sys_area` VALUES (130825, 130800, 3, '隆化县', '0', 100);
INSERT INTO `sys_area` VALUES (130826, 130800, 3, '丰宁满族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (130827, 130800, 3, '宽城满族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (130828, 130800, 3, '围场满族蒙古族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (130900, 130000, 2, '沧州市', '0', 100);
INSERT INTO `sys_area` VALUES (130901, 130900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (130902, 130900, 3, '新华区', '0', 100);
INSERT INTO `sys_area` VALUES (130903, 130900, 3, '运河区', '0', 100);
INSERT INTO `sys_area` VALUES (130921, 130900, 3, '沧县', '0', 100);
INSERT INTO `sys_area` VALUES (130922, 130900, 3, '青县', '0', 100);
INSERT INTO `sys_area` VALUES (130923, 130900, 3, '东光县', '0', 100);
INSERT INTO `sys_area` VALUES (130924, 130900, 3, '海兴县', '0', 100);
INSERT INTO `sys_area` VALUES (130925, 130900, 3, '盐山县', '0', 100);
INSERT INTO `sys_area` VALUES (130926, 130900, 3, '肃宁县', '0', 100);
INSERT INTO `sys_area` VALUES (130927, 130900, 3, '南皮县', '0', 100);
INSERT INTO `sys_area` VALUES (130928, 130900, 3, '吴桥县', '0', 100);
INSERT INTO `sys_area` VALUES (130929, 130900, 3, '献县', '0', 100);
INSERT INTO `sys_area` VALUES (130930, 130900, 3, '孟村回族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (130981, 130900, 3, '泊头市', '0', 100);
INSERT INTO `sys_area` VALUES (130982, 130900, 3, '任丘市', '0', 100);
INSERT INTO `sys_area` VALUES (130983, 130900, 3, '黄骅市', '0', 100);
INSERT INTO `sys_area` VALUES (130984, 130900, 3, '河间市', '0', 100);
INSERT INTO `sys_area` VALUES (131000, 130000, 2, '廊坊市', '0', 100);
INSERT INTO `sys_area` VALUES (131001, 131000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (131002, 131000, 3, '安次区', '0', 100);
INSERT INTO `sys_area` VALUES (131003, 131000, 3, '广阳区', '0', 100);
INSERT INTO `sys_area` VALUES (131022, 131000, 3, '固安县', '0', 100);
INSERT INTO `sys_area` VALUES (131023, 131000, 3, '永清县', '0', 100);
INSERT INTO `sys_area` VALUES (131024, 131000, 3, '香河县', '0', 100);
INSERT INTO `sys_area` VALUES (131025, 131000, 3, '大城县', '0', 100);
INSERT INTO `sys_area` VALUES (131026, 131000, 3, '文安县', '0', 100);
INSERT INTO `sys_area` VALUES (131028, 131000, 3, '大厂回族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (131081, 131000, 3, '霸州市', '0', 100);
INSERT INTO `sys_area` VALUES (131082, 131000, 3, '三河市', '0', 100);
INSERT INTO `sys_area` VALUES (131100, 130000, 2, '衡水市', '0', 100);
INSERT INTO `sys_area` VALUES (131101, 131100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (131102, 131100, 3, '桃城区', '0', 100);
INSERT INTO `sys_area` VALUES (131103, 131100, 3, '冀州区', '0', 100);
INSERT INTO `sys_area` VALUES (131121, 131100, 3, '枣强县', '0', 100);
INSERT INTO `sys_area` VALUES (131122, 131100, 3, '武邑县', '0', 100);
INSERT INTO `sys_area` VALUES (131123, 131100, 3, '武强县', '0', 100);
INSERT INTO `sys_area` VALUES (131124, 131100, 3, '饶阳县', '0', 100);
INSERT INTO `sys_area` VALUES (131125, 131100, 3, '安平县', '0', 100);
INSERT INTO `sys_area` VALUES (131126, 131100, 3, '故城县', '0', 100);
INSERT INTO `sys_area` VALUES (131127, 131100, 3, '景县', '0', 100);
INSERT INTO `sys_area` VALUES (131128, 131100, 3, '阜城县', '0', 100);
INSERT INTO `sys_area` VALUES (131182, 131100, 3, '深州市', '0', 100);
INSERT INTO `sys_area` VALUES (139000, 130000, 2, '省直辖县级行政区划', '0', 100);
INSERT INTO `sys_area` VALUES (139001, 139000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (139002, 139000, 3, '辛集市', '0', 100);
INSERT INTO `sys_area` VALUES (140000, 0, 1, '山西省', '0', 100);
INSERT INTO `sys_area` VALUES (140100, 140000, 2, '太原市', '0', 100);
INSERT INTO `sys_area` VALUES (140101, 140100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (140105, 140100, 3, '小店区', '0', 100);
INSERT INTO `sys_area` VALUES (140106, 140100, 3, '迎泽区', '0', 100);
INSERT INTO `sys_area` VALUES (140107, 140100, 3, '杏花岭区', '0', 100);
INSERT INTO `sys_area` VALUES (140108, 140100, 3, '尖草坪区', '0', 100);
INSERT INTO `sys_area` VALUES (140109, 140100, 3, '万柏林区', '0', 100);
INSERT INTO `sys_area` VALUES (140110, 140100, 3, '晋源区', '0', 100);
INSERT INTO `sys_area` VALUES (140121, 140100, 3, '清徐县', '0', 100);
INSERT INTO `sys_area` VALUES (140122, 140100, 3, '阳曲县', '0', 100);
INSERT INTO `sys_area` VALUES (140123, 140100, 3, '娄烦县', '0', 100);
INSERT INTO `sys_area` VALUES (140181, 140100, 3, '古交市', '0', 100);
INSERT INTO `sys_area` VALUES (140200, 140000, 2, '大同市', '0', 100);
INSERT INTO `sys_area` VALUES (140201, 140200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (140202, 140200, 3, '城区', '0', 100);
INSERT INTO `sys_area` VALUES (140203, 140200, 3, '矿区', '0', 100);
INSERT INTO `sys_area` VALUES (140211, 140200, 3, '南郊区', '0', 100);
INSERT INTO `sys_area` VALUES (140212, 140200, 3, '新荣区', '0', 100);
INSERT INTO `sys_area` VALUES (140221, 140200, 3, '阳高县', '0', 100);
INSERT INTO `sys_area` VALUES (140222, 140200, 3, '天镇县', '0', 100);
INSERT INTO `sys_area` VALUES (140223, 140200, 3, '广灵县', '0', 100);
INSERT INTO `sys_area` VALUES (140224, 140200, 3, '灵丘县', '0', 100);
INSERT INTO `sys_area` VALUES (140225, 140200, 3, '浑源县', '0', 100);
INSERT INTO `sys_area` VALUES (140226, 140200, 3, '左云县', '0', 100);
INSERT INTO `sys_area` VALUES (140227, 140200, 3, '大同县', '0', 100);
INSERT INTO `sys_area` VALUES (140300, 140000, 2, '阳泉市', '0', 100);
INSERT INTO `sys_area` VALUES (140301, 140300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (140302, 140300, 3, '城区', '0', 100);
INSERT INTO `sys_area` VALUES (140303, 140300, 3, '矿区', '0', 100);
INSERT INTO `sys_area` VALUES (140311, 140300, 3, '郊区', '0', 100);
INSERT INTO `sys_area` VALUES (140321, 140300, 3, '平定县', '0', 100);
INSERT INTO `sys_area` VALUES (140322, 140300, 3, '盂县', '0', 100);
INSERT INTO `sys_area` VALUES (140400, 140000, 2, '长治市', '0', 100);
INSERT INTO `sys_area` VALUES (140401, 140400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (140402, 140400, 3, '城区', '0', 100);
INSERT INTO `sys_area` VALUES (140411, 140400, 3, '郊区', '0', 100);
INSERT INTO `sys_area` VALUES (140421, 140400, 3, '长治县', '0', 100);
INSERT INTO `sys_area` VALUES (140423, 140400, 3, '襄垣县', '0', 100);
INSERT INTO `sys_area` VALUES (140424, 140400, 3, '屯留县', '0', 100);
INSERT INTO `sys_area` VALUES (140425, 140400, 3, '平顺县', '0', 100);
INSERT INTO `sys_area` VALUES (140426, 140400, 3, '黎城县', '0', 100);
INSERT INTO `sys_area` VALUES (140427, 140400, 3, '壶关县', '0', 100);
INSERT INTO `sys_area` VALUES (140428, 140400, 3, '长子县', '0', 100);
INSERT INTO `sys_area` VALUES (140429, 140400, 3, '武乡县', '0', 100);
INSERT INTO `sys_area` VALUES (140430, 140400, 3, '沁县', '0', 100);
INSERT INTO `sys_area` VALUES (140431, 140400, 3, '沁源县', '0', 100);
INSERT INTO `sys_area` VALUES (140481, 140400, 3, '潞城市', '0', 100);
INSERT INTO `sys_area` VALUES (140500, 140000, 2, '晋城市', '0', 100);
INSERT INTO `sys_area` VALUES (140501, 140500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (140502, 140500, 3, '城区', '0', 100);
INSERT INTO `sys_area` VALUES (140521, 140500, 3, '沁水县', '0', 100);
INSERT INTO `sys_area` VALUES (140522, 140500, 3, '阳城县', '0', 100);
INSERT INTO `sys_area` VALUES (140524, 140500, 3, '陵川县', '0', 100);
INSERT INTO `sys_area` VALUES (140525, 140500, 3, '泽州县', '0', 100);
INSERT INTO `sys_area` VALUES (140581, 140500, 3, '高平市', '0', 100);
INSERT INTO `sys_area` VALUES (140600, 140000, 2, '朔州市', '0', 100);
INSERT INTO `sys_area` VALUES (140601, 140600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (140602, 140600, 3, '朔城区', '0', 100);
INSERT INTO `sys_area` VALUES (140603, 140600, 3, '平鲁区', '0', 100);
INSERT INTO `sys_area` VALUES (140621, 140600, 3, '山阴县', '0', 100);
INSERT INTO `sys_area` VALUES (140622, 140600, 3, '应县', '0', 100);
INSERT INTO `sys_area` VALUES (140623, 140600, 3, '右玉县', '0', 100);
INSERT INTO `sys_area` VALUES (140624, 140600, 3, '怀仁县', '0', 100);
INSERT INTO `sys_area` VALUES (140700, 140000, 2, '晋中市', '0', 100);
INSERT INTO `sys_area` VALUES (140701, 140700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (140702, 140700, 3, '榆次区', '0', 100);
INSERT INTO `sys_area` VALUES (140721, 140700, 3, '榆社县', '0', 100);
INSERT INTO `sys_area` VALUES (140722, 140700, 3, '左权县', '0', 100);
INSERT INTO `sys_area` VALUES (140723, 140700, 3, '和顺县', '0', 100);
INSERT INTO `sys_area` VALUES (140724, 140700, 3, '昔阳县', '0', 100);
INSERT INTO `sys_area` VALUES (140725, 140700, 3, '寿阳县', '0', 100);
INSERT INTO `sys_area` VALUES (140726, 140700, 3, '太谷县', '0', 100);
INSERT INTO `sys_area` VALUES (140727, 140700, 3, '祁县', '0', 100);
INSERT INTO `sys_area` VALUES (140728, 140700, 3, '平遥县', '0', 100);
INSERT INTO `sys_area` VALUES (140729, 140700, 3, '灵石县', '0', 100);
INSERT INTO `sys_area` VALUES (140781, 140700, 3, '介休市', '0', 100);
INSERT INTO `sys_area` VALUES (140800, 140000, 2, '运城市', '0', 100);
INSERT INTO `sys_area` VALUES (140801, 140800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (140802, 140800, 3, '盐湖区', '0', 100);
INSERT INTO `sys_area` VALUES (140821, 140800, 3, '临猗县', '0', 100);
INSERT INTO `sys_area` VALUES (140822, 140800, 3, '万荣县', '0', 100);
INSERT INTO `sys_area` VALUES (140823, 140800, 3, '闻喜县', '0', 100);
INSERT INTO `sys_area` VALUES (140824, 140800, 3, '稷山县', '0', 100);
INSERT INTO `sys_area` VALUES (140825, 140800, 3, '新绛县', '0', 100);
INSERT INTO `sys_area` VALUES (140826, 140800, 3, '绛县', '0', 100);
INSERT INTO `sys_area` VALUES (140827, 140800, 3, '垣曲县', '0', 100);
INSERT INTO `sys_area` VALUES (140828, 140800, 3, '夏县', '0', 100);
INSERT INTO `sys_area` VALUES (140829, 140800, 3, '平陆县', '0', 100);
INSERT INTO `sys_area` VALUES (140830, 140800, 3, '芮城县', '0', 100);
INSERT INTO `sys_area` VALUES (140881, 140800, 3, '永济市', '0', 100);
INSERT INTO `sys_area` VALUES (140882, 140800, 3, '河津市', '0', 100);
INSERT INTO `sys_area` VALUES (140900, 140000, 2, '忻州市', '0', 100);
INSERT INTO `sys_area` VALUES (140901, 140900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (140902, 140900, 3, '忻府区', '0', 100);
INSERT INTO `sys_area` VALUES (140921, 140900, 3, '定襄县', '0', 100);
INSERT INTO `sys_area` VALUES (140922, 140900, 3, '五台县', '0', 100);
INSERT INTO `sys_area` VALUES (140923, 140900, 3, '代县', '0', 100);
INSERT INTO `sys_area` VALUES (140924, 140900, 3, '繁峙县', '0', 100);
INSERT INTO `sys_area` VALUES (140925, 140900, 3, '宁武县', '0', 100);
INSERT INTO `sys_area` VALUES (140926, 140900, 3, '静乐县', '0', 100);
INSERT INTO `sys_area` VALUES (140927, 140900, 3, '神池县', '0', 100);
INSERT INTO `sys_area` VALUES (140928, 140900, 3, '五寨县', '0', 100);
INSERT INTO `sys_area` VALUES (140929, 140900, 3, '岢岚县', '0', 100);
INSERT INTO `sys_area` VALUES (140930, 140900, 3, '河曲县', '0', 100);
INSERT INTO `sys_area` VALUES (140931, 140900, 3, '保德县', '0', 100);
INSERT INTO `sys_area` VALUES (140932, 140900, 3, '偏关县', '0', 100);
INSERT INTO `sys_area` VALUES (140981, 140900, 3, '原平市', '0', 100);
INSERT INTO `sys_area` VALUES (141000, 140000, 2, '临汾市', '0', 100);
INSERT INTO `sys_area` VALUES (141001, 141000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (141002, 141000, 3, '尧都区', '0', 100);
INSERT INTO `sys_area` VALUES (141021, 141000, 3, '曲沃县', '0', 100);
INSERT INTO `sys_area` VALUES (141022, 141000, 3, '翼城县', '0', 100);
INSERT INTO `sys_area` VALUES (141023, 141000, 3, '襄汾县', '0', 100);
INSERT INTO `sys_area` VALUES (141024, 141000, 3, '洪洞县', '0', 100);
INSERT INTO `sys_area` VALUES (141025, 141000, 3, '古县', '0', 100);
INSERT INTO `sys_area` VALUES (141026, 141000, 3, '安泽县', '0', 100);
INSERT INTO `sys_area` VALUES (141027, 141000, 3, '浮山县', '0', 100);
INSERT INTO `sys_area` VALUES (141028, 141000, 3, '吉县', '0', 100);
INSERT INTO `sys_area` VALUES (141029, 141000, 3, '乡宁县', '0', 100);
INSERT INTO `sys_area` VALUES (141030, 141000, 3, '大宁县', '0', 100);
INSERT INTO `sys_area` VALUES (141031, 141000, 3, '隰县', '0', 100);
INSERT INTO `sys_area` VALUES (141032, 141000, 3, '永和县', '0', 100);
INSERT INTO `sys_area` VALUES (141033, 141000, 3, '蒲县', '0', 100);
INSERT INTO `sys_area` VALUES (141034, 141000, 3, '汾西县', '0', 100);
INSERT INTO `sys_area` VALUES (141081, 141000, 3, '侯马市', '0', 100);
INSERT INTO `sys_area` VALUES (141082, 141000, 3, '霍州市', '0', 100);
INSERT INTO `sys_area` VALUES (141100, 140000, 2, '吕梁市', '0', 100);
INSERT INTO `sys_area` VALUES (141101, 141100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (141102, 141100, 3, '离石区', '0', 100);
INSERT INTO `sys_area` VALUES (141121, 141100, 3, '文水县', '0', 100);
INSERT INTO `sys_area` VALUES (141122, 141100, 3, '交城县', '0', 100);
INSERT INTO `sys_area` VALUES (141123, 141100, 3, '兴县', '0', 100);
INSERT INTO `sys_area` VALUES (141124, 141100, 3, '临县', '0', 100);
INSERT INTO `sys_area` VALUES (141125, 141100, 3, '柳林县', '0', 100);
INSERT INTO `sys_area` VALUES (141126, 141100, 3, '石楼县', '0', 100);
INSERT INTO `sys_area` VALUES (141127, 141100, 3, '岚县', '0', 100);
INSERT INTO `sys_area` VALUES (141128, 141100, 3, '方山县', '0', 100);
INSERT INTO `sys_area` VALUES (141129, 141100, 3, '中阳县', '0', 100);
INSERT INTO `sys_area` VALUES (141130, 141100, 3, '交口县', '0', 100);
INSERT INTO `sys_area` VALUES (141181, 141100, 3, '孝义市', '0', 100);
INSERT INTO `sys_area` VALUES (141182, 141100, 3, '汾阳市', '0', 100);
INSERT INTO `sys_area` VALUES (150000, 0, 1, '内蒙古自治区', '0', 100);
INSERT INTO `sys_area` VALUES (150100, 150000, 2, '呼和浩特市', '0', 100);
INSERT INTO `sys_area` VALUES (150101, 150100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (150102, 150100, 3, '新城区', '0', 100);
INSERT INTO `sys_area` VALUES (150103, 150100, 3, '回民区', '0', 100);
INSERT INTO `sys_area` VALUES (150104, 150100, 3, '玉泉区', '0', 100);
INSERT INTO `sys_area` VALUES (150105, 150100, 3, '赛罕区', '0', 100);
INSERT INTO `sys_area` VALUES (150121, 150100, 3, '土默特左旗', '0', 100);
INSERT INTO `sys_area` VALUES (150122, 150100, 3, '托克托县', '0', 100);
INSERT INTO `sys_area` VALUES (150123, 150100, 3, '和林格尔县', '0', 100);
INSERT INTO `sys_area` VALUES (150124, 150100, 3, '清水河县', '0', 100);
INSERT INTO `sys_area` VALUES (150125, 150100, 3, '武川县', '0', 100);
INSERT INTO `sys_area` VALUES (150200, 150000, 2, '包头市', '0', 100);
INSERT INTO `sys_area` VALUES (150201, 150200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (150202, 150200, 3, '东河区', '0', 100);
INSERT INTO `sys_area` VALUES (150203, 150200, 3, '昆都仑区', '0', 100);
INSERT INTO `sys_area` VALUES (150204, 150200, 3, '青山区', '0', 100);
INSERT INTO `sys_area` VALUES (150205, 150200, 3, '石拐区', '0', 100);
INSERT INTO `sys_area` VALUES (150206, 150200, 3, '白云鄂博矿区', '0', 100);
INSERT INTO `sys_area` VALUES (150207, 150200, 3, '九原区', '0', 100);
INSERT INTO `sys_area` VALUES (150221, 150200, 3, '土默特右旗', '0', 100);
INSERT INTO `sys_area` VALUES (150222, 150200, 3, '固阳县', '0', 100);
INSERT INTO `sys_area` VALUES (150223, 150200, 3, '达尔罕茂明安联合旗', '0', 100);
INSERT INTO `sys_area` VALUES (150300, 150000, 2, '乌海市', '0', 100);
INSERT INTO `sys_area` VALUES (150301, 150300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (150302, 150300, 3, '海勃湾区', '0', 100);
INSERT INTO `sys_area` VALUES (150303, 150300, 3, '海南区', '0', 100);
INSERT INTO `sys_area` VALUES (150304, 150300, 3, '乌达区', '0', 100);
INSERT INTO `sys_area` VALUES (150400, 150000, 2, '赤峰市', '0', 100);
INSERT INTO `sys_area` VALUES (150401, 150400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (150402, 150400, 3, '红山区', '0', 100);
INSERT INTO `sys_area` VALUES (150403, 150400, 3, '元宝山区', '0', 100);
INSERT INTO `sys_area` VALUES (150404, 150400, 3, '松山区', '0', 100);
INSERT INTO `sys_area` VALUES (150421, 150400, 3, '阿鲁科尔沁旗', '0', 100);
INSERT INTO `sys_area` VALUES (150422, 150400, 3, '巴林左旗', '0', 100);
INSERT INTO `sys_area` VALUES (150423, 150400, 3, '巴林右旗', '0', 100);
INSERT INTO `sys_area` VALUES (150424, 150400, 3, '林西县', '0', 100);
INSERT INTO `sys_area` VALUES (150425, 150400, 3, '克什克腾旗', '0', 100);
INSERT INTO `sys_area` VALUES (150426, 150400, 3, '翁牛特旗', '0', 100);
INSERT INTO `sys_area` VALUES (150428, 150400, 3, '喀喇沁旗', '0', 100);
INSERT INTO `sys_area` VALUES (150429, 150400, 3, '宁城县', '0', 100);
INSERT INTO `sys_area` VALUES (150430, 150400, 3, '敖汉旗', '0', 100);
INSERT INTO `sys_area` VALUES (150500, 150000, 2, '通辽市', '0', 100);
INSERT INTO `sys_area` VALUES (150501, 150500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (150502, 150500, 3, '科尔沁区', '0', 100);
INSERT INTO `sys_area` VALUES (150521, 150500, 3, '科尔沁左翼中旗', '0', 100);
INSERT INTO `sys_area` VALUES (150522, 150500, 3, '科尔沁左翼后旗', '0', 100);
INSERT INTO `sys_area` VALUES (150523, 150500, 3, '开鲁县', '0', 100);
INSERT INTO `sys_area` VALUES (150524, 150500, 3, '库伦旗', '0', 100);
INSERT INTO `sys_area` VALUES (150525, 150500, 3, '奈曼旗', '0', 100);
INSERT INTO `sys_area` VALUES (150526, 150500, 3, '扎鲁特旗', '0', 100);
INSERT INTO `sys_area` VALUES (150581, 150500, 3, '霍林郭勒市', '0', 100);
INSERT INTO `sys_area` VALUES (150600, 150000, 2, '鄂尔多斯市', '0', 100);
INSERT INTO `sys_area` VALUES (150601, 150600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (150602, 150600, 3, '东胜区', '0', 100);
INSERT INTO `sys_area` VALUES (150603, 150600, 3, '康巴什区', '0', 100);
INSERT INTO `sys_area` VALUES (150621, 150600, 3, '达拉特旗', '0', 100);
INSERT INTO `sys_area` VALUES (150622, 150600, 3, '准格尔旗', '0', 100);
INSERT INTO `sys_area` VALUES (150623, 150600, 3, '鄂托克前旗', '0', 100);
INSERT INTO `sys_area` VALUES (150624, 150600, 3, '鄂托克旗', '0', 100);
INSERT INTO `sys_area` VALUES (150625, 150600, 3, '杭锦旗', '0', 100);
INSERT INTO `sys_area` VALUES (150626, 150600, 3, '乌审旗', '0', 100);
INSERT INTO `sys_area` VALUES (150627, 150600, 3, '伊金霍洛旗', '0', 100);
INSERT INTO `sys_area` VALUES (150700, 150000, 2, '呼伦贝尔市', '0', 100);
INSERT INTO `sys_area` VALUES (150701, 150700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (150702, 150700, 3, '海拉尔区', '0', 100);
INSERT INTO `sys_area` VALUES (150703, 150700, 3, '扎赉诺尔区', '0', 100);
INSERT INTO `sys_area` VALUES (150721, 150700, 3, '阿荣旗', '0', 100);
INSERT INTO `sys_area` VALUES (150722, 150700, 3, '莫力达瓦达斡尔族自治旗', '0', 100);
INSERT INTO `sys_area` VALUES (150723, 150700, 3, '鄂伦春自治旗', '0', 100);
INSERT INTO `sys_area` VALUES (150724, 150700, 3, '鄂温克族自治旗', '0', 100);
INSERT INTO `sys_area` VALUES (150725, 150700, 3, '陈巴尔虎旗', '0', 100);
INSERT INTO `sys_area` VALUES (150726, 150700, 3, '新巴尔虎左旗', '0', 100);
INSERT INTO `sys_area` VALUES (150727, 150700, 3, '新巴尔虎右旗', '0', 100);
INSERT INTO `sys_area` VALUES (150781, 150700, 3, '满洲里市', '0', 100);
INSERT INTO `sys_area` VALUES (150782, 150700, 3, '牙克石市', '0', 100);
INSERT INTO `sys_area` VALUES (150783, 150700, 3, '扎兰屯市', '0', 100);
INSERT INTO `sys_area` VALUES (150784, 150700, 3, '额尔古纳市', '0', 100);
INSERT INTO `sys_area` VALUES (150785, 150700, 3, '根河市', '0', 100);
INSERT INTO `sys_area` VALUES (150800, 150000, 2, '巴彦淖尔市', '0', 100);
INSERT INTO `sys_area` VALUES (150801, 150800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (150802, 150800, 3, '临河区', '0', 100);
INSERT INTO `sys_area` VALUES (150821, 150800, 3, '五原县', '0', 100);
INSERT INTO `sys_area` VALUES (150822, 150800, 3, '磴口县', '0', 100);
INSERT INTO `sys_area` VALUES (150823, 150800, 3, '乌拉特前旗', '0', 100);
INSERT INTO `sys_area` VALUES (150824, 150800, 3, '乌拉特中旗', '0', 100);
INSERT INTO `sys_area` VALUES (150825, 150800, 3, '乌拉特后旗', '0', 100);
INSERT INTO `sys_area` VALUES (150826, 150800, 3, '杭锦后旗', '0', 100);
INSERT INTO `sys_area` VALUES (150900, 150000, 2, '乌兰察布市', '0', 100);
INSERT INTO `sys_area` VALUES (150901, 150900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (150902, 150900, 3, '集宁区', '0', 100);
INSERT INTO `sys_area` VALUES (150921, 150900, 3, '卓资县', '0', 100);
INSERT INTO `sys_area` VALUES (150922, 150900, 3, '化德县', '0', 100);
INSERT INTO `sys_area` VALUES (150923, 150900, 3, '商都县', '0', 100);
INSERT INTO `sys_area` VALUES (150924, 150900, 3, '兴和县', '0', 100);
INSERT INTO `sys_area` VALUES (150925, 150900, 3, '凉城县', '0', 100);
INSERT INTO `sys_area` VALUES (150926, 150900, 3, '察哈尔右翼前旗', '0', 100);
INSERT INTO `sys_area` VALUES (150927, 150900, 3, '察哈尔右翼中旗', '0', 100);
INSERT INTO `sys_area` VALUES (150928, 150900, 3, '察哈尔右翼后旗', '0', 100);
INSERT INTO `sys_area` VALUES (150929, 150900, 3, '四子王旗', '0', 100);
INSERT INTO `sys_area` VALUES (150981, 150900, 3, '丰镇市', '0', 100);
INSERT INTO `sys_area` VALUES (152200, 150000, 2, '兴安盟', '0', 100);
INSERT INTO `sys_area` VALUES (152201, 152200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (152202, 152200, 3, '阿尔山市', '0', 100);
INSERT INTO `sys_area` VALUES (152221, 152200, 3, '科尔沁右翼前旗', '0', 100);
INSERT INTO `sys_area` VALUES (152222, 152200, 3, '科尔沁右翼中旗', '0', 100);
INSERT INTO `sys_area` VALUES (152223, 152200, 3, '扎赉特旗', '0', 100);
INSERT INTO `sys_area` VALUES (152224, 152200, 3, '突泉县', '0', 100);
INSERT INTO `sys_area` VALUES (152500, 150000, 2, '锡林郭勒盟', '0', 100);
INSERT INTO `sys_area` VALUES (152501, 152500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (152502, 152500, 3, '锡林浩特市', '0', 100);
INSERT INTO `sys_area` VALUES (152522, 152500, 3, '阿巴嘎旗', '0', 100);
INSERT INTO `sys_area` VALUES (152523, 152500, 3, '苏尼特左旗', '0', 100);
INSERT INTO `sys_area` VALUES (152524, 152500, 3, '苏尼特右旗', '0', 100);
INSERT INTO `sys_area` VALUES (152525, 152500, 3, '东乌珠穆沁旗', '0', 100);
INSERT INTO `sys_area` VALUES (152526, 152500, 3, '西乌珠穆沁旗', '0', 100);
INSERT INTO `sys_area` VALUES (152527, 152500, 3, '太仆寺旗', '0', 100);
INSERT INTO `sys_area` VALUES (152528, 152500, 3, '镶黄旗', '0', 100);
INSERT INTO `sys_area` VALUES (152529, 152500, 3, '正镶白旗', '0', 100);
INSERT INTO `sys_area` VALUES (152530, 152500, 3, '正蓝旗', '0', 100);
INSERT INTO `sys_area` VALUES (152531, 152500, 3, '多伦县', '0', 100);
INSERT INTO `sys_area` VALUES (152900, 150000, 2, '阿拉善盟', '0', 100);
INSERT INTO `sys_area` VALUES (152921, 152900, 3, '阿拉善左旗', '0', 100);
INSERT INTO `sys_area` VALUES (152922, 152900, 3, '阿拉善右旗', '0', 100);
INSERT INTO `sys_area` VALUES (152923, 152900, 3, '额济纳旗', '0', 100);
INSERT INTO `sys_area` VALUES (210000, 0, 1, '辽宁省', '0', 100);
INSERT INTO `sys_area` VALUES (210100, 210000, 2, '沈阳市', '0', 100);
INSERT INTO `sys_area` VALUES (210101, 210100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (210102, 210100, 3, '和平区', '0', 100);
INSERT INTO `sys_area` VALUES (210103, 210100, 3, '沈河区', '0', 100);
INSERT INTO `sys_area` VALUES (210104, 210100, 3, '大东区', '0', 100);
INSERT INTO `sys_area` VALUES (210105, 210100, 3, '皇姑区', '0', 100);
INSERT INTO `sys_area` VALUES (210106, 210100, 3, '铁西区', '0', 100);
INSERT INTO `sys_area` VALUES (210111, 210100, 3, '苏家屯区', '0', 100);
INSERT INTO `sys_area` VALUES (210112, 210100, 3, '浑南区', '0', 100);
INSERT INTO `sys_area` VALUES (210113, 210100, 3, '沈北新区', '0', 100);
INSERT INTO `sys_area` VALUES (210114, 210100, 3, '于洪区', '0', 100);
INSERT INTO `sys_area` VALUES (210115, 210100, 3, '辽中区', '0', 100);
INSERT INTO `sys_area` VALUES (210123, 210100, 3, '康平县', '0', 100);
INSERT INTO `sys_area` VALUES (210124, 210100, 3, '法库县', '0', 100);
INSERT INTO `sys_area` VALUES (210181, 210100, 3, '新民市', '0', 100);
INSERT INTO `sys_area` VALUES (210200, 210000, 2, '大连市', '0', 100);
INSERT INTO `sys_area` VALUES (210201, 210200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (210202, 210200, 3, '中山区', '0', 100);
INSERT INTO `sys_area` VALUES (210203, 210200, 3, '西岗区', '0', 100);
INSERT INTO `sys_area` VALUES (210204, 210200, 3, '沙河口区', '0', 100);
INSERT INTO `sys_area` VALUES (210211, 210200, 3, '甘井子区', '0', 100);
INSERT INTO `sys_area` VALUES (210212, 210200, 3, '旅顺口区', '0', 100);
INSERT INTO `sys_area` VALUES (210213, 210200, 3, '金州区', '0', 100);
INSERT INTO `sys_area` VALUES (210214, 210200, 3, '普兰店区', '0', 100);
INSERT INTO `sys_area` VALUES (210224, 210200, 3, '长海县', '0', 100);
INSERT INTO `sys_area` VALUES (210281, 210200, 3, '瓦房店市', '0', 100);
INSERT INTO `sys_area` VALUES (210283, 210200, 3, '庄河市', '0', 100);
INSERT INTO `sys_area` VALUES (210300, 210000, 2, '鞍山市', '0', 100);
INSERT INTO `sys_area` VALUES (210301, 210300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (210302, 210300, 3, '铁东区', '0', 100);
INSERT INTO `sys_area` VALUES (210303, 210300, 3, '铁西区', '0', 100);
INSERT INTO `sys_area` VALUES (210304, 210300, 3, '立山区', '0', 100);
INSERT INTO `sys_area` VALUES (210311, 210300, 3, '千山区', '0', 100);
INSERT INTO `sys_area` VALUES (210321, 210300, 3, '台安县', '0', 100);
INSERT INTO `sys_area` VALUES (210323, 210300, 3, '岫岩满族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (210381, 210300, 3, '海城市', '0', 100);
INSERT INTO `sys_area` VALUES (210400, 210000, 2, '抚顺市', '0', 100);
INSERT INTO `sys_area` VALUES (210401, 210400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (210402, 210400, 3, '新抚区', '0', 100);
INSERT INTO `sys_area` VALUES (210403, 210400, 3, '东洲区', '0', 100);
INSERT INTO `sys_area` VALUES (210404, 210400, 3, '望花区', '0', 100);
INSERT INTO `sys_area` VALUES (210411, 210400, 3, '顺城区', '0', 100);
INSERT INTO `sys_area` VALUES (210421, 210400, 3, '抚顺县', '0', 100);
INSERT INTO `sys_area` VALUES (210422, 210400, 3, '新宾满族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (210423, 210400, 3, '清原满族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (210500, 210000, 2, '本溪市', '0', 100);
INSERT INTO `sys_area` VALUES (210501, 210500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (210502, 210500, 3, '平山区', '0', 100);
INSERT INTO `sys_area` VALUES (210503, 210500, 3, '溪湖区', '0', 100);
INSERT INTO `sys_area` VALUES (210504, 210500, 3, '明山区', '0', 100);
INSERT INTO `sys_area` VALUES (210505, 210500, 3, '南芬区', '0', 100);
INSERT INTO `sys_area` VALUES (210521, 210500, 3, '本溪满族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (210522, 210500, 3, '桓仁满族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (210600, 210000, 2, '丹东市', '0', 100);
INSERT INTO `sys_area` VALUES (210601, 210600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (210602, 210600, 3, '元宝区', '0', 100);
INSERT INTO `sys_area` VALUES (210603, 210600, 3, '振兴区', '0', 100);
INSERT INTO `sys_area` VALUES (210604, 210600, 3, '振安区', '0', 100);
INSERT INTO `sys_area` VALUES (210624, 210600, 3, '宽甸满族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (210681, 210600, 3, '东港市', '0', 100);
INSERT INTO `sys_area` VALUES (210682, 210600, 3, '凤城市', '0', 100);
INSERT INTO `sys_area` VALUES (210700, 210000, 2, '锦州市', '0', 100);
INSERT INTO `sys_area` VALUES (210701, 210700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (210702, 210700, 3, '古塔区', '0', 100);
INSERT INTO `sys_area` VALUES (210703, 210700, 3, '凌河区', '0', 100);
INSERT INTO `sys_area` VALUES (210711, 210700, 3, '太和区', '0', 100);
INSERT INTO `sys_area` VALUES (210726, 210700, 3, '黑山县', '0', 100);
INSERT INTO `sys_area` VALUES (210727, 210700, 3, '义县', '0', 100);
INSERT INTO `sys_area` VALUES (210781, 210700, 3, '凌海市', '0', 100);
INSERT INTO `sys_area` VALUES (210782, 210700, 3, '北镇市', '0', 100);
INSERT INTO `sys_area` VALUES (210800, 210000, 2, '营口市', '0', 100);
INSERT INTO `sys_area` VALUES (210801, 210800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (210802, 210800, 3, '站前区', '0', 100);
INSERT INTO `sys_area` VALUES (210803, 210800, 3, '西市区', '0', 100);
INSERT INTO `sys_area` VALUES (210804, 210800, 3, '鲅鱼圈区', '0', 100);
INSERT INTO `sys_area` VALUES (210811, 210800, 3, '老边区', '0', 100);
INSERT INTO `sys_area` VALUES (210881, 210800, 3, '盖州市', '0', 100);
INSERT INTO `sys_area` VALUES (210882, 210800, 3, '大石桥市', '0', 100);
INSERT INTO `sys_area` VALUES (210900, 210000, 2, '阜新市', '0', 100);
INSERT INTO `sys_area` VALUES (210901, 210900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (210902, 210900, 3, '海州区', '0', 100);
INSERT INTO `sys_area` VALUES (210903, 210900, 3, '新邱区', '0', 100);
INSERT INTO `sys_area` VALUES (210904, 210900, 3, '太平区', '0', 100);
INSERT INTO `sys_area` VALUES (210905, 210900, 3, '清河门区', '0', 100);
INSERT INTO `sys_area` VALUES (210911, 210900, 3, '细河区', '0', 100);
INSERT INTO `sys_area` VALUES (210921, 210900, 3, '阜新蒙古族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (210922, 210900, 3, '彰武县', '0', 100);
INSERT INTO `sys_area` VALUES (211000, 210000, 2, '辽阳市', '0', 100);
INSERT INTO `sys_area` VALUES (211001, 211000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (211002, 211000, 3, '白塔区', '0', 100);
INSERT INTO `sys_area` VALUES (211003, 211000, 3, '文圣区', '0', 100);
INSERT INTO `sys_area` VALUES (211004, 211000, 3, '宏伟区', '0', 100);
INSERT INTO `sys_area` VALUES (211005, 211000, 3, '弓长岭区', '0', 100);
INSERT INTO `sys_area` VALUES (211011, 211000, 3, '太子河区', '0', 100);
INSERT INTO `sys_area` VALUES (211021, 211000, 3, '辽阳县', '0', 100);
INSERT INTO `sys_area` VALUES (211081, 211000, 3, '灯塔市', '0', 100);
INSERT INTO `sys_area` VALUES (211100, 210000, 2, '盘锦市', '0', 100);
INSERT INTO `sys_area` VALUES (211101, 211100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (211102, 211100, 3, '双台子区', '0', 100);
INSERT INTO `sys_area` VALUES (211103, 211100, 3, '兴隆台区', '0', 100);
INSERT INTO `sys_area` VALUES (211104, 211100, 3, '大洼区', '0', 100);
INSERT INTO `sys_area` VALUES (211122, 211100, 3, '盘山县', '0', 100);
INSERT INTO `sys_area` VALUES (211200, 210000, 2, '铁岭市', '0', 100);
INSERT INTO `sys_area` VALUES (211201, 211200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (211202, 211200, 3, '银州区', '0', 100);
INSERT INTO `sys_area` VALUES (211204, 211200, 3, '清河区', '0', 100);
INSERT INTO `sys_area` VALUES (211221, 211200, 3, '铁岭县', '0', 100);
INSERT INTO `sys_area` VALUES (211223, 211200, 3, '西丰县', '0', 100);
INSERT INTO `sys_area` VALUES (211224, 211200, 3, '昌图县', '0', 100);
INSERT INTO `sys_area` VALUES (211281, 211200, 3, '调兵山市', '0', 100);
INSERT INTO `sys_area` VALUES (211282, 211200, 3, '开原市', '0', 100);
INSERT INTO `sys_area` VALUES (211300, 210000, 2, '朝阳市', '0', 100);
INSERT INTO `sys_area` VALUES (211301, 211300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (211302, 211300, 3, '双塔区', '0', 100);
INSERT INTO `sys_area` VALUES (211303, 211300, 3, '龙城区', '0', 100);
INSERT INTO `sys_area` VALUES (211321, 211300, 3, '朝阳县', '0', 100);
INSERT INTO `sys_area` VALUES (211322, 211300, 3, '建平县', '0', 100);
INSERT INTO `sys_area` VALUES (211324, 211300, 3, '喀喇沁左翼蒙古族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (211381, 211300, 3, '北票市', '0', 100);
INSERT INTO `sys_area` VALUES (211382, 211300, 3, '凌源市', '0', 100);
INSERT INTO `sys_area` VALUES (211400, 210000, 2, '葫芦岛市', '0', 100);
INSERT INTO `sys_area` VALUES (211401, 211400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (211402, 211400, 3, '连山区', '0', 100);
INSERT INTO `sys_area` VALUES (211403, 211400, 3, '龙港区', '0', 100);
INSERT INTO `sys_area` VALUES (211404, 211400, 3, '南票区', '0', 100);
INSERT INTO `sys_area` VALUES (211421, 211400, 3, '绥中县', '0', 100);
INSERT INTO `sys_area` VALUES (211422, 211400, 3, '建昌县', '0', 100);
INSERT INTO `sys_area` VALUES (211481, 211400, 3, '兴城市', '0', 100);
INSERT INTO `sys_area` VALUES (220000, 0, 1, '吉林省', '0', 100);
INSERT INTO `sys_area` VALUES (220100, 220000, 2, '长春市', '0', 100);
INSERT INTO `sys_area` VALUES (220101, 220100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (220102, 220100, 3, '南关区', '0', 100);
INSERT INTO `sys_area` VALUES (220103, 220100, 3, '宽城区', '0', 100);
INSERT INTO `sys_area` VALUES (220104, 220100, 3, '朝阳区', '0', 100);
INSERT INTO `sys_area` VALUES (220105, 220100, 3, '二道区', '0', 100);
INSERT INTO `sys_area` VALUES (220106, 220100, 3, '绿园区', '0', 100);
INSERT INTO `sys_area` VALUES (220112, 220100, 3, '双阳区', '0', 100);
INSERT INTO `sys_area` VALUES (220113, 220100, 3, '九台区', '0', 100);
INSERT INTO `sys_area` VALUES (220122, 220100, 3, '农安县', '0', 100);
INSERT INTO `sys_area` VALUES (220182, 220100, 3, '榆树市', '0', 100);
INSERT INTO `sys_area` VALUES (220183, 220100, 3, '德惠市', '0', 100);
INSERT INTO `sys_area` VALUES (220200, 220000, 2, '吉林市', '0', 100);
INSERT INTO `sys_area` VALUES (220201, 220200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (220202, 220200, 3, '昌邑区', '0', 100);
INSERT INTO `sys_area` VALUES (220203, 220200, 3, '龙潭区', '0', 100);
INSERT INTO `sys_area` VALUES (220204, 220200, 3, '船营区', '0', 100);
INSERT INTO `sys_area` VALUES (220211, 220200, 3, '丰满区', '0', 100);
INSERT INTO `sys_area` VALUES (220221, 220200, 3, '永吉县', '0', 100);
INSERT INTO `sys_area` VALUES (220281, 220200, 3, '蛟河市', '0', 100);
INSERT INTO `sys_area` VALUES (220282, 220200, 3, '桦甸市', '0', 100);
INSERT INTO `sys_area` VALUES (220283, 220200, 3, '舒兰市', '0', 100);
INSERT INTO `sys_area` VALUES (220284, 220200, 3, '磐石市', '0', 100);
INSERT INTO `sys_area` VALUES (220300, 220000, 2, '四平市', '0', 100);
INSERT INTO `sys_area` VALUES (220301, 220300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (220302, 220300, 3, '铁西区', '0', 100);
INSERT INTO `sys_area` VALUES (220303, 220300, 3, '铁东区', '0', 100);
INSERT INTO `sys_area` VALUES (220322, 220300, 3, '梨树县', '0', 100);
INSERT INTO `sys_area` VALUES (220323, 220300, 3, '伊通满族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (220381, 220300, 3, '公主岭市', '0', 100);
INSERT INTO `sys_area` VALUES (220382, 220300, 3, '双辽市', '0', 100);
INSERT INTO `sys_area` VALUES (220400, 220000, 2, '辽源市', '0', 100);
INSERT INTO `sys_area` VALUES (220401, 220400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (220402, 220400, 3, '龙山区', '0', 100);
INSERT INTO `sys_area` VALUES (220403, 220400, 3, '西安区', '0', 100);
INSERT INTO `sys_area` VALUES (220421, 220400, 3, '东丰县', '0', 100);
INSERT INTO `sys_area` VALUES (220422, 220400, 3, '东辽县', '0', 100);
INSERT INTO `sys_area` VALUES (220500, 220000, 2, '通化市', '0', 100);
INSERT INTO `sys_area` VALUES (220501, 220500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (220502, 220500, 3, '东昌区', '0', 100);
INSERT INTO `sys_area` VALUES (220503, 220500, 3, '二道江区', '0', 100);
INSERT INTO `sys_area` VALUES (220521, 220500, 3, '通化县', '0', 100);
INSERT INTO `sys_area` VALUES (220523, 220500, 3, '辉南县', '0', 100);
INSERT INTO `sys_area` VALUES (220524, 220500, 3, '柳河县', '0', 100);
INSERT INTO `sys_area` VALUES (220581, 220500, 3, '梅河口市', '0', 100);
INSERT INTO `sys_area` VALUES (220582, 220500, 3, '集安市', '0', 100);
INSERT INTO `sys_area` VALUES (220600, 220000, 2, '白山市', '0', 100);
INSERT INTO `sys_area` VALUES (220601, 220600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (220602, 220600, 3, '浑江区', '0', 100);
INSERT INTO `sys_area` VALUES (220605, 220600, 3, '江源区', '0', 100);
INSERT INTO `sys_area` VALUES (220621, 220600, 3, '抚松县', '0', 100);
INSERT INTO `sys_area` VALUES (220622, 220600, 3, '靖宇县', '0', 100);
INSERT INTO `sys_area` VALUES (220623, 220600, 3, '长白朝鲜族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (220681, 220600, 3, '临江市', '0', 100);
INSERT INTO `sys_area` VALUES (220700, 220000, 2, '松原市', '0', 100);
INSERT INTO `sys_area` VALUES (220701, 220700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (220702, 220700, 3, '宁江区', '0', 100);
INSERT INTO `sys_area` VALUES (220721, 220700, 3, '前郭尔罗斯蒙古族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (220722, 220700, 3, '长岭县', '0', 100);
INSERT INTO `sys_area` VALUES (220723, 220700, 3, '乾安县', '0', 100);
INSERT INTO `sys_area` VALUES (220781, 220700, 3, '扶余市', '0', 100);
INSERT INTO `sys_area` VALUES (220800, 220000, 2, '白城市', '0', 100);
INSERT INTO `sys_area` VALUES (220801, 220800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (220802, 220800, 3, '洮北区', '0', 100);
INSERT INTO `sys_area` VALUES (220821, 220800, 3, '镇赉县', '0', 100);
INSERT INTO `sys_area` VALUES (220822, 220800, 3, '通榆县', '0', 100);
INSERT INTO `sys_area` VALUES (220881, 220800, 3, '洮南市', '0', 100);
INSERT INTO `sys_area` VALUES (220882, 220800, 3, '大安市', '0', 100);
INSERT INTO `sys_area` VALUES (222400, 220000, 2, '延边朝鲜族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (222401, 222400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (222402, 222400, 3, '图们市', '0', 100);
INSERT INTO `sys_area` VALUES (222403, 222400, 3, '敦化市', '0', 100);
INSERT INTO `sys_area` VALUES (222404, 222400, 3, '珲春市', '0', 100);
INSERT INTO `sys_area` VALUES (222405, 222400, 3, '龙井市', '0', 100);
INSERT INTO `sys_area` VALUES (222406, 222400, 3, '和龙市', '0', 100);
INSERT INTO `sys_area` VALUES (222424, 222400, 3, '汪清县', '0', 100);
INSERT INTO `sys_area` VALUES (222426, 222400, 3, '安图县', '0', 100);
INSERT INTO `sys_area` VALUES (230000, 0, 1, '黑龙江省', '0', 100);
INSERT INTO `sys_area` VALUES (230100, 230000, 2, '哈尔滨市', '0', 100);
INSERT INTO `sys_area` VALUES (230101, 230100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (230102, 230100, 3, '道里区', '0', 100);
INSERT INTO `sys_area` VALUES (230103, 230100, 3, '南岗区', '0', 100);
INSERT INTO `sys_area` VALUES (230104, 230100, 3, '道外区', '0', 100);
INSERT INTO `sys_area` VALUES (230108, 230100, 3, '平房区', '0', 100);
INSERT INTO `sys_area` VALUES (230109, 230100, 3, '松北区', '0', 100);
INSERT INTO `sys_area` VALUES (230110, 230100, 3, '香坊区', '0', 100);
INSERT INTO `sys_area` VALUES (230111, 230100, 3, '呼兰区', '0', 100);
INSERT INTO `sys_area` VALUES (230112, 230100, 3, '阿城区', '0', 100);
INSERT INTO `sys_area` VALUES (230113, 230100, 3, '双城区', '0', 100);
INSERT INTO `sys_area` VALUES (230123, 230100, 3, '依兰县', '0', 100);
INSERT INTO `sys_area` VALUES (230124, 230100, 3, '方正县', '0', 100);
INSERT INTO `sys_area` VALUES (230125, 230100, 3, '宾县', '0', 100);
INSERT INTO `sys_area` VALUES (230126, 230100, 3, '巴彦县', '0', 100);
INSERT INTO `sys_area` VALUES (230127, 230100, 3, '木兰县', '0', 100);
INSERT INTO `sys_area` VALUES (230128, 230100, 3, '通河县', '0', 100);
INSERT INTO `sys_area` VALUES (230129, 230100, 3, '延寿县', '0', 100);
INSERT INTO `sys_area` VALUES (230183, 230100, 3, '尚志市', '0', 100);
INSERT INTO `sys_area` VALUES (230184, 230100, 3, '五常市', '0', 100);
INSERT INTO `sys_area` VALUES (230200, 230000, 2, '齐齐哈尔市', '0', 100);
INSERT INTO `sys_area` VALUES (230201, 230200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (230202, 230200, 3, '龙沙区', '0', 100);
INSERT INTO `sys_area` VALUES (230203, 230200, 3, '建华区', '0', 100);
INSERT INTO `sys_area` VALUES (230204, 230200, 3, '铁锋区', '0', 100);
INSERT INTO `sys_area` VALUES (230205, 230200, 3, '昂昂溪区', '0', 100);
INSERT INTO `sys_area` VALUES (230206, 230200, 3, '富拉尔基区', '0', 100);
INSERT INTO `sys_area` VALUES (230207, 230200, 3, '碾子山区', '0', 100);
INSERT INTO `sys_area` VALUES (230208, 230200, 3, '梅里斯达斡尔族区', '0', 100);
INSERT INTO `sys_area` VALUES (230221, 230200, 3, '龙江县', '0', 100);
INSERT INTO `sys_area` VALUES (230223, 230200, 3, '依安县', '0', 100);
INSERT INTO `sys_area` VALUES (230224, 230200, 3, '泰来县', '0', 100);
INSERT INTO `sys_area` VALUES (230225, 230200, 3, '甘南县', '0', 100);
INSERT INTO `sys_area` VALUES (230227, 230200, 3, '富裕县', '0', 100);
INSERT INTO `sys_area` VALUES (230229, 230200, 3, '克山县', '0', 100);
INSERT INTO `sys_area` VALUES (230230, 230200, 3, '克东县', '0', 100);
INSERT INTO `sys_area` VALUES (230231, 230200, 3, '拜泉县', '0', 100);
INSERT INTO `sys_area` VALUES (230281, 230200, 3, '讷河市', '0', 100);
INSERT INTO `sys_area` VALUES (230300, 230000, 2, '鸡西市', '0', 100);
INSERT INTO `sys_area` VALUES (230301, 230300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (230302, 230300, 3, '鸡冠区', '0', 100);
INSERT INTO `sys_area` VALUES (230303, 230300, 3, '恒山区', '0', 100);
INSERT INTO `sys_area` VALUES (230304, 230300, 3, '滴道区', '0', 100);
INSERT INTO `sys_area` VALUES (230305, 230300, 3, '梨树区', '0', 100);
INSERT INTO `sys_area` VALUES (230306, 230300, 3, '城子河区', '0', 100);
INSERT INTO `sys_area` VALUES (230307, 230300, 3, '麻山区', '0', 100);
INSERT INTO `sys_area` VALUES (230321, 230300, 3, '鸡东县', '0', 100);
INSERT INTO `sys_area` VALUES (230381, 230300, 3, '虎林市', '0', 100);
INSERT INTO `sys_area` VALUES (230382, 230300, 3, '密山市', '0', 100);
INSERT INTO `sys_area` VALUES (230400, 230000, 2, '鹤岗市', '0', 100);
INSERT INTO `sys_area` VALUES (230401, 230400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (230402, 230400, 3, '向阳区', '0', 100);
INSERT INTO `sys_area` VALUES (230403, 230400, 3, '工农区', '0', 100);
INSERT INTO `sys_area` VALUES (230404, 230400, 3, '南山区', '0', 100);
INSERT INTO `sys_area` VALUES (230405, 230400, 3, '兴安区', '0', 100);
INSERT INTO `sys_area` VALUES (230406, 230400, 3, '东山区', '0', 100);
INSERT INTO `sys_area` VALUES (230407, 230400, 3, '兴山区', '0', 100);
INSERT INTO `sys_area` VALUES (230421, 230400, 3, '萝北县', '0', 100);
INSERT INTO `sys_area` VALUES (230422, 230400, 3, '绥滨县', '0', 100);
INSERT INTO `sys_area` VALUES (230500, 230000, 2, '双鸭山市', '0', 100);
INSERT INTO `sys_area` VALUES (230501, 230500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (230502, 230500, 3, '尖山区', '0', 100);
INSERT INTO `sys_area` VALUES (230503, 230500, 3, '岭东区', '0', 100);
INSERT INTO `sys_area` VALUES (230505, 230500, 3, '四方台区', '0', 100);
INSERT INTO `sys_area` VALUES (230506, 230500, 3, '宝山区', '0', 100);
INSERT INTO `sys_area` VALUES (230521, 230500, 3, '集贤县', '0', 100);
INSERT INTO `sys_area` VALUES (230522, 230500, 3, '友谊县', '0', 100);
INSERT INTO `sys_area` VALUES (230523, 230500, 3, '宝清县', '0', 100);
INSERT INTO `sys_area` VALUES (230524, 230500, 3, '饶河县', '0', 100);
INSERT INTO `sys_area` VALUES (230600, 230000, 2, '大庆市', '0', 100);
INSERT INTO `sys_area` VALUES (230601, 230600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (230602, 230600, 3, '萨尔图区', '0', 100);
INSERT INTO `sys_area` VALUES (230603, 230600, 3, '龙凤区', '0', 100);
INSERT INTO `sys_area` VALUES (230604, 230600, 3, '让胡路区', '0', 100);
INSERT INTO `sys_area` VALUES (230605, 230600, 3, '红岗区', '0', 100);
INSERT INTO `sys_area` VALUES (230606, 230600, 3, '大同区', '0', 100);
INSERT INTO `sys_area` VALUES (230621, 230600, 3, '肇州县', '0', 100);
INSERT INTO `sys_area` VALUES (230622, 230600, 3, '肇源县', '0', 100);
INSERT INTO `sys_area` VALUES (230623, 230600, 3, '林甸县', '0', 100);
INSERT INTO `sys_area` VALUES (230624, 230600, 3, '杜尔伯特蒙古族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (230700, 230000, 2, '伊春市', '0', 100);
INSERT INTO `sys_area` VALUES (230701, 230700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (230702, 230700, 3, '伊春区', '0', 100);
INSERT INTO `sys_area` VALUES (230703, 230700, 3, '南岔区', '0', 100);
INSERT INTO `sys_area` VALUES (230704, 230700, 3, '友好区', '0', 100);
INSERT INTO `sys_area` VALUES (230705, 230700, 3, '西林区', '0', 100);
INSERT INTO `sys_area` VALUES (230706, 230700, 3, '翠峦区', '0', 100);
INSERT INTO `sys_area` VALUES (230707, 230700, 3, '新青区', '0', 100);
INSERT INTO `sys_area` VALUES (230708, 230700, 3, '美溪区', '0', 100);
INSERT INTO `sys_area` VALUES (230709, 230700, 3, '金山屯区', '0', 100);
INSERT INTO `sys_area` VALUES (230710, 230700, 3, '五营区', '0', 100);
INSERT INTO `sys_area` VALUES (230711, 230700, 3, '乌马河区', '0', 100);
INSERT INTO `sys_area` VALUES (230712, 230700, 3, '汤旺河区', '0', 100);
INSERT INTO `sys_area` VALUES (230713, 230700, 3, '带岭区', '0', 100);
INSERT INTO `sys_area` VALUES (230714, 230700, 3, '乌伊岭区', '0', 100);
INSERT INTO `sys_area` VALUES (230715, 230700, 3, '红星区', '0', 100);
INSERT INTO `sys_area` VALUES (230716, 230700, 3, '上甘岭区', '0', 100);
INSERT INTO `sys_area` VALUES (230722, 230700, 3, '嘉荫县', '0', 100);
INSERT INTO `sys_area` VALUES (230781, 230700, 3, '铁力市', '0', 100);
INSERT INTO `sys_area` VALUES (230800, 230000, 2, '佳木斯市', '0', 100);
INSERT INTO `sys_area` VALUES (230801, 230800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (230803, 230800, 3, '向阳区', '0', 100);
INSERT INTO `sys_area` VALUES (230804, 230800, 3, '前进区', '0', 100);
INSERT INTO `sys_area` VALUES (230805, 230800, 3, '东风区', '0', 100);
INSERT INTO `sys_area` VALUES (230811, 230800, 3, '郊区', '0', 100);
INSERT INTO `sys_area` VALUES (230822, 230800, 3, '桦南县', '0', 100);
INSERT INTO `sys_area` VALUES (230826, 230800, 3, '桦川县', '0', 100);
INSERT INTO `sys_area` VALUES (230828, 230800, 3, '汤原县', '0', 100);
INSERT INTO `sys_area` VALUES (230881, 230800, 3, '同江市', '0', 100);
INSERT INTO `sys_area` VALUES (230882, 230800, 3, '富锦市', '0', 100);
INSERT INTO `sys_area` VALUES (230883, 230800, 3, '抚远市', '0', 100);
INSERT INTO `sys_area` VALUES (230900, 230000, 2, '七台河市', '0', 100);
INSERT INTO `sys_area` VALUES (230901, 230900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (230902, 230900, 3, '新兴区', '0', 100);
INSERT INTO `sys_area` VALUES (230903, 230900, 3, '桃山区', '0', 100);
INSERT INTO `sys_area` VALUES (230904, 230900, 3, '茄子河区', '0', 100);
INSERT INTO `sys_area` VALUES (230921, 230900, 3, '勃利县', '0', 100);
INSERT INTO `sys_area` VALUES (231000, 230000, 2, '牡丹江市', '0', 100);
INSERT INTO `sys_area` VALUES (231001, 231000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (231002, 231000, 3, '东安区', '0', 100);
INSERT INTO `sys_area` VALUES (231003, 231000, 3, '阳明区', '0', 100);
INSERT INTO `sys_area` VALUES (231004, 231000, 3, '爱民区', '0', 100);
INSERT INTO `sys_area` VALUES (231005, 231000, 3, '西安区', '0', 100);
INSERT INTO `sys_area` VALUES (231025, 231000, 3, '林口县', '0', 100);
INSERT INTO `sys_area` VALUES (231081, 231000, 3, '绥芬河市', '0', 100);
INSERT INTO `sys_area` VALUES (231083, 231000, 3, '海林市', '0', 100);
INSERT INTO `sys_area` VALUES (231084, 231000, 3, '宁安市', '0', 100);
INSERT INTO `sys_area` VALUES (231085, 231000, 3, '穆棱市', '0', 100);
INSERT INTO `sys_area` VALUES (231086, 231000, 3, '东宁市', '0', 100);
INSERT INTO `sys_area` VALUES (231100, 230000, 2, '黑河市', '0', 100);
INSERT INTO `sys_area` VALUES (231101, 231100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (231102, 231100, 3, '爱辉区', '0', 100);
INSERT INTO `sys_area` VALUES (231121, 231100, 3, '嫩江县', '0', 100);
INSERT INTO `sys_area` VALUES (231123, 231100, 3, '逊克县', '0', 100);
INSERT INTO `sys_area` VALUES (231124, 231100, 3, '孙吴县', '0', 100);
INSERT INTO `sys_area` VALUES (231181, 231100, 3, '北安市', '0', 100);
INSERT INTO `sys_area` VALUES (231182, 231100, 3, '五大连池市', '0', 100);
INSERT INTO `sys_area` VALUES (231200, 230000, 2, '绥化市', '0', 100);
INSERT INTO `sys_area` VALUES (231201, 231200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (231202, 231200, 3, '北林区', '0', 100);
INSERT INTO `sys_area` VALUES (231221, 231200, 3, '望奎县', '0', 100);
INSERT INTO `sys_area` VALUES (231222, 231200, 3, '兰西县', '0', 100);
INSERT INTO `sys_area` VALUES (231223, 231200, 3, '青冈县', '0', 100);
INSERT INTO `sys_area` VALUES (231224, 231200, 3, '庆安县', '0', 100);
INSERT INTO `sys_area` VALUES (231225, 231200, 3, '明水县', '0', 100);
INSERT INTO `sys_area` VALUES (231226, 231200, 3, '绥棱县', '0', 100);
INSERT INTO `sys_area` VALUES (231281, 231200, 3, '安达市', '0', 100);
INSERT INTO `sys_area` VALUES (231282, 231200, 3, '肇东市', '0', 100);
INSERT INTO `sys_area` VALUES (231283, 231200, 3, '海伦市', '0', 100);
INSERT INTO `sys_area` VALUES (232700, 230000, 2, '大兴安岭地区', '0', 100);
INSERT INTO `sys_area` VALUES (232721, 232700, 3, '呼玛县', '0', 100);
INSERT INTO `sys_area` VALUES (232722, 232700, 3, '塔河县', '0', 100);
INSERT INTO `sys_area` VALUES (232723, 232700, 3, '漠河县', '0', 100);
INSERT INTO `sys_area` VALUES (310000, 0, 1, '上海市', '0', 100);
INSERT INTO `sys_area` VALUES (310100, 310000, 2, '上海市', '0', 100);
INSERT INTO `sys_area` VALUES (310101, 310100, 3, '黄浦区', '0', 100);
INSERT INTO `sys_area` VALUES (310104, 310100, 3, '徐汇区', '0', 100);
INSERT INTO `sys_area` VALUES (310105, 310100, 3, '长宁区', '0', 100);
INSERT INTO `sys_area` VALUES (310106, 310100, 3, '静安区', '0', 100);
INSERT INTO `sys_area` VALUES (310107, 310100, 3, '普陀区', '0', 100);
INSERT INTO `sys_area` VALUES (310109, 310100, 3, '虹口区', '0', 100);
INSERT INTO `sys_area` VALUES (310110, 310100, 3, '杨浦区', '0', 100);
INSERT INTO `sys_area` VALUES (310112, 310100, 3, '闵行区', '0', 100);
INSERT INTO `sys_area` VALUES (310113, 310100, 3, '宝山区', '0', 100);
INSERT INTO `sys_area` VALUES (310114, 310100, 3, '嘉定区', '0', 100);
INSERT INTO `sys_area` VALUES (310115, 310100, 3, '浦东新区', '0', 100);
INSERT INTO `sys_area` VALUES (310116, 310100, 3, '金山区', '0', 100);
INSERT INTO `sys_area` VALUES (310117, 310100, 3, '松江区', '0', 100);
INSERT INTO `sys_area` VALUES (310118, 310100, 3, '青浦区', '0', 100);
INSERT INTO `sys_area` VALUES (310120, 310100, 3, '奉贤区', '0', 100);
INSERT INTO `sys_area` VALUES (310151, 310100, 3, '崇明区', '0', 100);
INSERT INTO `sys_area` VALUES (320000, 0, 1, '江苏省', '0', 100);
INSERT INTO `sys_area` VALUES (320100, 320000, 2, '南京市', '0', 100);
INSERT INTO `sys_area` VALUES (320101, 320100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (320102, 320100, 3, '玄武区', '0', 100);
INSERT INTO `sys_area` VALUES (320104, 320100, 3, '秦淮区', '0', 100);
INSERT INTO `sys_area` VALUES (320105, 320100, 3, '建邺区', '0', 100);
INSERT INTO `sys_area` VALUES (320106, 320100, 3, '鼓楼区', '0', 100);
INSERT INTO `sys_area` VALUES (320111, 320100, 3, '浦口区', '0', 100);
INSERT INTO `sys_area` VALUES (320113, 320100, 3, '栖霞区', '0', 100);
INSERT INTO `sys_area` VALUES (320114, 320100, 3, '雨花台区', '0', 100);
INSERT INTO `sys_area` VALUES (320115, 320100, 3, '江宁区', '0', 100);
INSERT INTO `sys_area` VALUES (320116, 320100, 3, '六合区', '0', 100);
INSERT INTO `sys_area` VALUES (320117, 320100, 3, '溧水区', '0', 100);
INSERT INTO `sys_area` VALUES (320118, 320100, 3, '高淳区', '0', 100);
INSERT INTO `sys_area` VALUES (320200, 320000, 2, '无锡市', '0', 100);
INSERT INTO `sys_area` VALUES (320201, 320200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (320205, 320200, 3, '锡山区', '0', 100);
INSERT INTO `sys_area` VALUES (320206, 320200, 3, '惠山区', '0', 100);
INSERT INTO `sys_area` VALUES (320211, 320200, 3, '滨湖区', '0', 100);
INSERT INTO `sys_area` VALUES (320213, 320200, 3, '梁溪区', '0', 100);
INSERT INTO `sys_area` VALUES (320214, 320200, 3, '新吴区', '0', 100);
INSERT INTO `sys_area` VALUES (320281, 320200, 3, '江阴市', '0', 100);
INSERT INTO `sys_area` VALUES (320282, 320200, 3, '宜兴市', '0', 100);
INSERT INTO `sys_area` VALUES (320300, 320000, 2, '徐州市', '0', 100);
INSERT INTO `sys_area` VALUES (320301, 320300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (320302, 320300, 3, '鼓楼区', '0', 100);
INSERT INTO `sys_area` VALUES (320303, 320300, 3, '云龙区', '0', 100);
INSERT INTO `sys_area` VALUES (320305, 320300, 3, '贾汪区', '0', 100);
INSERT INTO `sys_area` VALUES (320311, 320300, 3, '泉山区', '0', 100);
INSERT INTO `sys_area` VALUES (320312, 320300, 3, '铜山区', '0', 100);
INSERT INTO `sys_area` VALUES (320321, 320300, 3, '丰县', '0', 100);
INSERT INTO `sys_area` VALUES (320322, 320300, 3, '沛县', '0', 100);
INSERT INTO `sys_area` VALUES (320324, 320300, 3, '睢宁县', '0', 100);
INSERT INTO `sys_area` VALUES (320381, 320300, 3, '新沂市', '0', 100);
INSERT INTO `sys_area` VALUES (320382, 320300, 3, '邳州市', '0', 100);
INSERT INTO `sys_area` VALUES (320400, 320000, 2, '常州市', '0', 100);
INSERT INTO `sys_area` VALUES (320401, 320400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (320402, 320400, 3, '天宁区', '0', 100);
INSERT INTO `sys_area` VALUES (320404, 320400, 3, '钟楼区', '0', 100);
INSERT INTO `sys_area` VALUES (320411, 320400, 3, '新北区', '0', 100);
INSERT INTO `sys_area` VALUES (320412, 320400, 3, '武进区', '0', 100);
INSERT INTO `sys_area` VALUES (320413, 320400, 3, '金坛区', '0', 100);
INSERT INTO `sys_area` VALUES (320481, 320400, 3, '溧阳市', '0', 100);
INSERT INTO `sys_area` VALUES (320500, 320000, 2, '苏州市', '0', 100);
INSERT INTO `sys_area` VALUES (320501, 320500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (320505, 320500, 3, '虎丘区', '0', 100);
INSERT INTO `sys_area` VALUES (320506, 320500, 3, '吴中区', '0', 100);
INSERT INTO `sys_area` VALUES (320507, 320500, 3, '相城区', '0', 100);
INSERT INTO `sys_area` VALUES (320508, 320500, 3, '姑苏区', '0', 100);
INSERT INTO `sys_area` VALUES (320509, 320500, 3, '吴江区', '0', 100);
INSERT INTO `sys_area` VALUES (320581, 320500, 3, '常熟市', '0', 100);
INSERT INTO `sys_area` VALUES (320582, 320500, 3, '张家港市', '0', 100);
INSERT INTO `sys_area` VALUES (320583, 320500, 3, '昆山市', '0', 100);
INSERT INTO `sys_area` VALUES (320585, 320500, 3, '太仓市', '0', 100);
INSERT INTO `sys_area` VALUES (320600, 320000, 2, '南通市', '0', 100);
INSERT INTO `sys_area` VALUES (320601, 320600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (320602, 320600, 3, '崇川区', '0', 100);
INSERT INTO `sys_area` VALUES (320611, 320600, 3, '港闸区', '0', 100);
INSERT INTO `sys_area` VALUES (320612, 320600, 3, '通州区', '0', 100);
INSERT INTO `sys_area` VALUES (320621, 320600, 3, '海安县', '0', 100);
INSERT INTO `sys_area` VALUES (320623, 320600, 3, '如东县', '0', 100);
INSERT INTO `sys_area` VALUES (320681, 320600, 3, '启东市', '0', 100);
INSERT INTO `sys_area` VALUES (320682, 320600, 3, '如皋市', '0', 100);
INSERT INTO `sys_area` VALUES (320684, 320600, 3, '海门市', '0', 100);
INSERT INTO `sys_area` VALUES (320700, 320000, 2, '连云港市', '0', 100);
INSERT INTO `sys_area` VALUES (320701, 320700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (320703, 320700, 3, '连云区', '0', 100);
INSERT INTO `sys_area` VALUES (320706, 320700, 3, '海州区', '0', 100);
INSERT INTO `sys_area` VALUES (320707, 320700, 3, '赣榆区', '0', 100);
INSERT INTO `sys_area` VALUES (320722, 320700, 3, '东海县', '0', 100);
INSERT INTO `sys_area` VALUES (320723, 320700, 3, '灌云县', '0', 100);
INSERT INTO `sys_area` VALUES (320724, 320700, 3, '灌南县', '0', 100);
INSERT INTO `sys_area` VALUES (320800, 320000, 2, '淮安市', '0', 100);
INSERT INTO `sys_area` VALUES (320801, 320800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (320803, 320800, 3, '淮安区', '0', 100);
INSERT INTO `sys_area` VALUES (320804, 320800, 3, '淮阴区', '0', 100);
INSERT INTO `sys_area` VALUES (320812, 320800, 3, '清江浦区', '0', 100);
INSERT INTO `sys_area` VALUES (320813, 320800, 3, '洪泽区', '0', 100);
INSERT INTO `sys_area` VALUES (320826, 320800, 3, '涟水县', '0', 100);
INSERT INTO `sys_area` VALUES (320830, 320800, 3, '盱眙县', '0', 100);
INSERT INTO `sys_area` VALUES (320831, 320800, 3, '金湖县', '0', 100);
INSERT INTO `sys_area` VALUES (320900, 320000, 2, '盐城市', '0', 100);
INSERT INTO `sys_area` VALUES (320901, 320900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (320902, 320900, 3, '亭湖区', '0', 100);
INSERT INTO `sys_area` VALUES (320903, 320900, 3, '盐都区', '0', 100);
INSERT INTO `sys_area` VALUES (320904, 320900, 3, '大丰区', '0', 100);
INSERT INTO `sys_area` VALUES (320921, 320900, 3, '响水县', '0', 100);
INSERT INTO `sys_area` VALUES (320922, 320900, 3, '滨海县', '0', 100);
INSERT INTO `sys_area` VALUES (320923, 320900, 3, '阜宁县', '0', 100);
INSERT INTO `sys_area` VALUES (320924, 320900, 3, '射阳县', '0', 100);
INSERT INTO `sys_area` VALUES (320925, 320900, 3, '建湖县', '0', 100);
INSERT INTO `sys_area` VALUES (320981, 320900, 3, '东台市', '0', 100);
INSERT INTO `sys_area` VALUES (321000, 320000, 2, '扬州市', '0', 100);
INSERT INTO `sys_area` VALUES (321001, 321000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (321002, 321000, 3, '广陵区', '0', 100);
INSERT INTO `sys_area` VALUES (321003, 321000, 3, '邗江区', '0', 100);
INSERT INTO `sys_area` VALUES (321012, 321000, 3, '江都区', '0', 100);
INSERT INTO `sys_area` VALUES (321023, 321000, 3, '宝应县', '0', 100);
INSERT INTO `sys_area` VALUES (321081, 321000, 3, '仪征市', '0', 100);
INSERT INTO `sys_area` VALUES (321084, 321000, 3, '高邮市', '0', 100);
INSERT INTO `sys_area` VALUES (321100, 320000, 2, '镇江市', '0', 100);
INSERT INTO `sys_area` VALUES (321101, 321100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (321102, 321100, 3, '京口区', '0', 100);
INSERT INTO `sys_area` VALUES (321111, 321100, 3, '润州区', '0', 100);
INSERT INTO `sys_area` VALUES (321112, 321100, 3, '丹徒区', '0', 100);
INSERT INTO `sys_area` VALUES (321181, 321100, 3, '丹阳市', '0', 100);
INSERT INTO `sys_area` VALUES (321182, 321100, 3, '扬中市', '0', 100);
INSERT INTO `sys_area` VALUES (321183, 321100, 3, '句容市', '0', 100);
INSERT INTO `sys_area` VALUES (321200, 320000, 2, '泰州市', '0', 100);
INSERT INTO `sys_area` VALUES (321201, 321200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (321202, 321200, 3, '海陵区', '0', 100);
INSERT INTO `sys_area` VALUES (321203, 321200, 3, '高港区', '0', 100);
INSERT INTO `sys_area` VALUES (321204, 321200, 3, '姜堰区', '0', 100);
INSERT INTO `sys_area` VALUES (321281, 321200, 3, '兴化市', '0', 100);
INSERT INTO `sys_area` VALUES (321282, 321200, 3, '靖江市', '0', 100);
INSERT INTO `sys_area` VALUES (321283, 321200, 3, '泰兴市', '0', 100);
INSERT INTO `sys_area` VALUES (321300, 320000, 2, '宿迁市', '0', 100);
INSERT INTO `sys_area` VALUES (321301, 321300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (321302, 321300, 3, '宿城区', '0', 100);
INSERT INTO `sys_area` VALUES (321311, 321300, 3, '宿豫区', '0', 100);
INSERT INTO `sys_area` VALUES (321322, 321300, 3, '沭阳县', '0', 100);
INSERT INTO `sys_area` VALUES (321323, 321300, 3, '泗阳县', '0', 100);
INSERT INTO `sys_area` VALUES (321324, 321300, 3, '泗洪县', '0', 100);
INSERT INTO `sys_area` VALUES (330000, 0, 1, '浙江省', '0', 100);
INSERT INTO `sys_area` VALUES (330100, 330000, 2, '杭州市', '0', 100);
INSERT INTO `sys_area` VALUES (330101, 330100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (330102, 330100, 3, '上城区', '0', 100);
INSERT INTO `sys_area` VALUES (330103, 330100, 3, '下城区', '0', 100);
INSERT INTO `sys_area` VALUES (330104, 330100, 3, '江干区', '0', 100);
INSERT INTO `sys_area` VALUES (330105, 330100, 3, '拱墅区', '0', 100);
INSERT INTO `sys_area` VALUES (330106, 330100, 3, '西湖区', '0', 100);
INSERT INTO `sys_area` VALUES (330108, 330100, 3, '滨江区', '0', 100);
INSERT INTO `sys_area` VALUES (330109, 330100, 3, '萧山区', '0', 100);
INSERT INTO `sys_area` VALUES (330110, 330100, 3, '余杭区', '0', 100);
INSERT INTO `sys_area` VALUES (330111, 330100, 3, '富阳区', '0', 100);
INSERT INTO `sys_area` VALUES (330122, 330100, 3, '桐庐县', '0', 100);
INSERT INTO `sys_area` VALUES (330127, 330100, 3, '淳安县', '0', 100);
INSERT INTO `sys_area` VALUES (330182, 330100, 3, '建德市', '0', 100);
INSERT INTO `sys_area` VALUES (330185, 330100, 3, '临安市', '0', 100);
INSERT INTO `sys_area` VALUES (330200, 330000, 2, '宁波市', '0', 100);
INSERT INTO `sys_area` VALUES (330201, 330200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (330203, 330200, 3, '海曙区', '0', 100);
INSERT INTO `sys_area` VALUES (330204, 330200, 3, '江东区', '0', 100);
INSERT INTO `sys_area` VALUES (330205, 330200, 3, '江北区', '0', 100);
INSERT INTO `sys_area` VALUES (330206, 330200, 3, '北仑区', '0', 100);
INSERT INTO `sys_area` VALUES (330211, 330200, 3, '镇海区', '0', 100);
INSERT INTO `sys_area` VALUES (330212, 330200, 3, '鄞州区', '0', 100);
INSERT INTO `sys_area` VALUES (330225, 330200, 3, '象山县', '0', 100);
INSERT INTO `sys_area` VALUES (330226, 330200, 3, '宁海县', '0', 100);
INSERT INTO `sys_area` VALUES (330281, 330200, 3, '余姚市', '0', 100);
INSERT INTO `sys_area` VALUES (330282, 330200, 3, '慈溪市', '0', 100);
INSERT INTO `sys_area` VALUES (330283, 330200, 3, '奉化市', '0', 100);
INSERT INTO `sys_area` VALUES (330300, 330000, 2, '温州市', '0', 100);
INSERT INTO `sys_area` VALUES (330301, 330300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (330302, 330300, 3, '鹿城区', '0', 100);
INSERT INTO `sys_area` VALUES (330303, 330300, 3, '龙湾区', '0', 100);
INSERT INTO `sys_area` VALUES (330304, 330300, 3, '瓯海区', '0', 100);
INSERT INTO `sys_area` VALUES (330305, 330300, 3, '洞头区', '0', 100);
INSERT INTO `sys_area` VALUES (330324, 330300, 3, '永嘉县', '0', 100);
INSERT INTO `sys_area` VALUES (330326, 330300, 3, '平阳县', '0', 100);
INSERT INTO `sys_area` VALUES (330327, 330300, 3, '苍南县', '0', 100);
INSERT INTO `sys_area` VALUES (330328, 330300, 3, '文成县', '0', 100);
INSERT INTO `sys_area` VALUES (330329, 330300, 3, '泰顺县', '0', 100);
INSERT INTO `sys_area` VALUES (330381, 330300, 3, '瑞安市', '0', 100);
INSERT INTO `sys_area` VALUES (330382, 330300, 3, '乐清市', '0', 100);
INSERT INTO `sys_area` VALUES (330400, 330000, 2, '嘉兴市', '0', 100);
INSERT INTO `sys_area` VALUES (330401, 330400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (330402, 330400, 3, '南湖区', '0', 100);
INSERT INTO `sys_area` VALUES (330411, 330400, 3, '秀洲区', '0', 100);
INSERT INTO `sys_area` VALUES (330421, 330400, 3, '嘉善县', '0', 100);
INSERT INTO `sys_area` VALUES (330424, 330400, 3, '海盐县', '0', 100);
INSERT INTO `sys_area` VALUES (330481, 330400, 3, '海宁市', '0', 100);
INSERT INTO `sys_area` VALUES (330482, 330400, 3, '平湖市', '0', 100);
INSERT INTO `sys_area` VALUES (330483, 330400, 3, '桐乡市', '0', 100);
INSERT INTO `sys_area` VALUES (330500, 330000, 2, '湖州市', '0', 100);
INSERT INTO `sys_area` VALUES (330501, 330500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (330502, 330500, 3, '吴兴区', '0', 100);
INSERT INTO `sys_area` VALUES (330503, 330500, 3, '南浔区', '0', 100);
INSERT INTO `sys_area` VALUES (330521, 330500, 3, '德清县', '0', 100);
INSERT INTO `sys_area` VALUES (330522, 330500, 3, '长兴县', '0', 100);
INSERT INTO `sys_area` VALUES (330523, 330500, 3, '安吉县', '0', 100);
INSERT INTO `sys_area` VALUES (330600, 330000, 2, '绍兴市', '0', 100);
INSERT INTO `sys_area` VALUES (330601, 330600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (330602, 330600, 3, '越城区', '0', 100);
INSERT INTO `sys_area` VALUES (330603, 330600, 3, '柯桥区', '0', 100);
INSERT INTO `sys_area` VALUES (330604, 330600, 3, '上虞区', '0', 100);
INSERT INTO `sys_area` VALUES (330624, 330600, 3, '新昌县', '0', 100);
INSERT INTO `sys_area` VALUES (330681, 330600, 3, '诸暨市', '0', 100);
INSERT INTO `sys_area` VALUES (330683, 330600, 3, '嵊州市', '0', 100);
INSERT INTO `sys_area` VALUES (330700, 330000, 2, '金华市', '0', 100);
INSERT INTO `sys_area` VALUES (330701, 330700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (330702, 330700, 3, '婺城区', '0', 100);
INSERT INTO `sys_area` VALUES (330703, 330700, 3, '金东区', '0', 100);
INSERT INTO `sys_area` VALUES (330723, 330700, 3, '武义县', '0', 100);
INSERT INTO `sys_area` VALUES (330726, 330700, 3, '浦江县', '0', 100);
INSERT INTO `sys_area` VALUES (330727, 330700, 3, '磐安县', '0', 100);
INSERT INTO `sys_area` VALUES (330781, 330700, 3, '兰溪市', '0', 100);
INSERT INTO `sys_area` VALUES (330782, 330700, 3, '义乌市', '0', 100);
INSERT INTO `sys_area` VALUES (330783, 330700, 3, '东阳市', '0', 100);
INSERT INTO `sys_area` VALUES (330784, 330700, 3, '永康市', '0', 100);
INSERT INTO `sys_area` VALUES (330800, 330000, 2, '衢州市', '0', 100);
INSERT INTO `sys_area` VALUES (330801, 330800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (330802, 330800, 3, '柯城区', '0', 100);
INSERT INTO `sys_area` VALUES (330803, 330800, 3, '衢江区', '0', 100);
INSERT INTO `sys_area` VALUES (330822, 330800, 3, '常山县', '0', 100);
INSERT INTO `sys_area` VALUES (330824, 330800, 3, '开化县', '0', 100);
INSERT INTO `sys_area` VALUES (330825, 330800, 3, '龙游县', '0', 100);
INSERT INTO `sys_area` VALUES (330881, 330800, 3, '江山市', '0', 100);
INSERT INTO `sys_area` VALUES (330900, 330000, 2, '舟山市', '0', 100);
INSERT INTO `sys_area` VALUES (330901, 330900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (330902, 330900, 3, '定海区', '0', 100);
INSERT INTO `sys_area` VALUES (330903, 330900, 3, '普陀区', '0', 100);
INSERT INTO `sys_area` VALUES (330921, 330900, 3, '岱山县', '0', 100);
INSERT INTO `sys_area` VALUES (330922, 330900, 3, '嵊泗县', '0', 100);
INSERT INTO `sys_area` VALUES (331000, 330000, 2, '台州市', '0', 100);
INSERT INTO `sys_area` VALUES (331001, 331000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (331002, 331000, 3, '椒江区', '0', 100);
INSERT INTO `sys_area` VALUES (331003, 331000, 3, '黄岩区', '0', 100);
INSERT INTO `sys_area` VALUES (331004, 331000, 3, '路桥区', '0', 100);
INSERT INTO `sys_area` VALUES (331021, 331000, 3, '玉环县', '0', 100);
INSERT INTO `sys_area` VALUES (331022, 331000, 3, '三门县', '0', 100);
INSERT INTO `sys_area` VALUES (331023, 331000, 3, '天台县', '0', 100);
INSERT INTO `sys_area` VALUES (331024, 331000, 3, '仙居县', '0', 100);
INSERT INTO `sys_area` VALUES (331081, 331000, 3, '温岭市', '0', 100);
INSERT INTO `sys_area` VALUES (331082, 331000, 3, '临海市', '0', 100);
INSERT INTO `sys_area` VALUES (331100, 330000, 2, '丽水市', '0', 100);
INSERT INTO `sys_area` VALUES (331101, 331100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (331102, 331100, 3, '莲都区', '0', 100);
INSERT INTO `sys_area` VALUES (331121, 331100, 3, '青田县', '0', 100);
INSERT INTO `sys_area` VALUES (331122, 331100, 3, '缙云县', '0', 100);
INSERT INTO `sys_area` VALUES (331123, 331100, 3, '遂昌县', '0', 100);
INSERT INTO `sys_area` VALUES (331124, 331100, 3, '松阳县', '0', 100);
INSERT INTO `sys_area` VALUES (331125, 331100, 3, '云和县', '0', 100);
INSERT INTO `sys_area` VALUES (331126, 331100, 3, '庆元县', '0', 100);
INSERT INTO `sys_area` VALUES (331127, 331100, 3, '景宁畲族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (331181, 331100, 3, '龙泉市', '0', 100);
INSERT INTO `sys_area` VALUES (340000, 0, 1, '安徽省', '0', 100);
INSERT INTO `sys_area` VALUES (340100, 340000, 2, '合肥市', '0', 100);
INSERT INTO `sys_area` VALUES (340101, 340100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (340102, 340100, 3, '瑶海区', '0', 100);
INSERT INTO `sys_area` VALUES (340103, 340100, 3, '庐阳区', '0', 100);
INSERT INTO `sys_area` VALUES (340104, 340100, 3, '蜀山区', '0', 100);
INSERT INTO `sys_area` VALUES (340111, 340100, 3, '包河区', '0', 100);
INSERT INTO `sys_area` VALUES (340121, 340100, 3, '长丰县', '0', 100);
INSERT INTO `sys_area` VALUES (340122, 340100, 3, '肥东县', '0', 100);
INSERT INTO `sys_area` VALUES (340123, 340100, 3, '肥西县', '0', 100);
INSERT INTO `sys_area` VALUES (340124, 340100, 3, '庐江县', '0', 100);
INSERT INTO `sys_area` VALUES (340181, 340100, 3, '巢湖市', '0', 100);
INSERT INTO `sys_area` VALUES (340200, 340000, 2, '芜湖市', '0', 100);
INSERT INTO `sys_area` VALUES (340201, 340200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (340202, 340200, 3, '镜湖区', '0', 100);
INSERT INTO `sys_area` VALUES (340203, 340200, 3, '弋江区', '0', 100);
INSERT INTO `sys_area` VALUES (340207, 340200, 3, '鸠江区', '0', 100);
INSERT INTO `sys_area` VALUES (340208, 340200, 3, '三山区', '0', 100);
INSERT INTO `sys_area` VALUES (340221, 340200, 3, '芜湖县', '0', 100);
INSERT INTO `sys_area` VALUES (340222, 340200, 3, '繁昌县', '0', 100);
INSERT INTO `sys_area` VALUES (340223, 340200, 3, '南陵县', '0', 100);
INSERT INTO `sys_area` VALUES (340225, 340200, 3, '无为县', '0', 100);
INSERT INTO `sys_area` VALUES (340300, 340000, 2, '蚌埠市', '0', 100);
INSERT INTO `sys_area` VALUES (340301, 340300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (340302, 340300, 3, '龙子湖区', '0', 100);
INSERT INTO `sys_area` VALUES (340303, 340300, 3, '蚌山区', '0', 100);
INSERT INTO `sys_area` VALUES (340304, 340300, 3, '禹会区', '0', 100);
INSERT INTO `sys_area` VALUES (340311, 340300, 3, '淮上区', '0', 100);
INSERT INTO `sys_area` VALUES (340321, 340300, 3, '怀远县', '0', 100);
INSERT INTO `sys_area` VALUES (340322, 340300, 3, '五河县', '0', 100);
INSERT INTO `sys_area` VALUES (340323, 340300, 3, '固镇县', '0', 100);
INSERT INTO `sys_area` VALUES (340400, 340000, 2, '淮南市', '0', 100);
INSERT INTO `sys_area` VALUES (340401, 340400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (340402, 340400, 3, '大通区', '0', 100);
INSERT INTO `sys_area` VALUES (340403, 340400, 3, '田家庵区', '0', 100);
INSERT INTO `sys_area` VALUES (340404, 340400, 3, '谢家集区', '0', 100);
INSERT INTO `sys_area` VALUES (340405, 340400, 3, '八公山区', '0', 100);
INSERT INTO `sys_area` VALUES (340406, 340400, 3, '潘集区', '0', 100);
INSERT INTO `sys_area` VALUES (340421, 340400, 3, '凤台县', '0', 100);
INSERT INTO `sys_area` VALUES (340422, 340400, 3, '寿县', '0', 100);
INSERT INTO `sys_area` VALUES (340500, 340000, 2, '马鞍山市', '0', 100);
INSERT INTO `sys_area` VALUES (340501, 340500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (340503, 340500, 3, '花山区', '0', 100);
INSERT INTO `sys_area` VALUES (340504, 340500, 3, '雨山区', '0', 100);
INSERT INTO `sys_area` VALUES (340506, 340500, 3, '博望区', '0', 100);
INSERT INTO `sys_area` VALUES (340521, 340500, 3, '当涂县', '0', 100);
INSERT INTO `sys_area` VALUES (340522, 340500, 3, '含山县', '0', 100);
INSERT INTO `sys_area` VALUES (340523, 340500, 3, '和县', '0', 100);
INSERT INTO `sys_area` VALUES (340600, 340000, 2, '淮北市', '0', 100);
INSERT INTO `sys_area` VALUES (340601, 340600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (340602, 340600, 3, '杜集区', '0', 100);
INSERT INTO `sys_area` VALUES (340603, 340600, 3, '相山区', '0', 100);
INSERT INTO `sys_area` VALUES (340604, 340600, 3, '烈山区', '0', 100);
INSERT INTO `sys_area` VALUES (340621, 340600, 3, '濉溪县', '0', 100);
INSERT INTO `sys_area` VALUES (340700, 340000, 2, '铜陵市', '0', 100);
INSERT INTO `sys_area` VALUES (340701, 340700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (340705, 340700, 3, '铜官区', '0', 100);
INSERT INTO `sys_area` VALUES (340706, 340700, 3, '义安区', '0', 100);
INSERT INTO `sys_area` VALUES (340711, 340700, 3, '郊区', '0', 100);
INSERT INTO `sys_area` VALUES (340722, 340700, 3, '枞阳县', '0', 100);
INSERT INTO `sys_area` VALUES (340800, 340000, 2, '安庆市', '0', 100);
INSERT INTO `sys_area` VALUES (340801, 340800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (340802, 340800, 3, '迎江区', '0', 100);
INSERT INTO `sys_area` VALUES (340803, 340800, 3, '大观区', '0', 100);
INSERT INTO `sys_area` VALUES (340811, 340800, 3, '宜秀区', '0', 100);
INSERT INTO `sys_area` VALUES (340822, 340800, 3, '怀宁县', '0', 100);
INSERT INTO `sys_area` VALUES (340824, 340800, 3, '潜山县', '0', 100);
INSERT INTO `sys_area` VALUES (340825, 340800, 3, '太湖县', '0', 100);
INSERT INTO `sys_area` VALUES (340826, 340800, 3, '宿松县', '0', 100);
INSERT INTO `sys_area` VALUES (340827, 340800, 3, '望江县', '0', 100);
INSERT INTO `sys_area` VALUES (340828, 340800, 3, '岳西县', '0', 100);
INSERT INTO `sys_area` VALUES (340881, 340800, 3, '桐城市', '0', 100);
INSERT INTO `sys_area` VALUES (341000, 340000, 2, '黄山市', '0', 100);
INSERT INTO `sys_area` VALUES (341001, 341000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (341002, 341000, 3, '屯溪区', '0', 100);
INSERT INTO `sys_area` VALUES (341003, 341000, 3, '黄山区', '0', 100);
INSERT INTO `sys_area` VALUES (341004, 341000, 3, '徽州区', '0', 100);
INSERT INTO `sys_area` VALUES (341021, 341000, 3, '歙县', '0', 100);
INSERT INTO `sys_area` VALUES (341022, 341000, 3, '休宁县', '0', 100);
INSERT INTO `sys_area` VALUES (341023, 341000, 3, '黟县', '0', 100);
INSERT INTO `sys_area` VALUES (341024, 341000, 3, '祁门县', '0', 100);
INSERT INTO `sys_area` VALUES (341100, 340000, 2, '滁州市', '0', 100);
INSERT INTO `sys_area` VALUES (341101, 341100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (341102, 341100, 3, '琅琊区', '0', 100);
INSERT INTO `sys_area` VALUES (341103, 341100, 3, '南谯区', '0', 100);
INSERT INTO `sys_area` VALUES (341122, 341100, 3, '来安县', '0', 100);
INSERT INTO `sys_area` VALUES (341124, 341100, 3, '全椒县', '0', 100);
INSERT INTO `sys_area` VALUES (341125, 341100, 3, '定远县', '0', 100);
INSERT INTO `sys_area` VALUES (341126, 341100, 3, '凤阳县', '0', 100);
INSERT INTO `sys_area` VALUES (341181, 341100, 3, '天长市', '0', 100);
INSERT INTO `sys_area` VALUES (341182, 341100, 3, '明光市', '0', 100);
INSERT INTO `sys_area` VALUES (341200, 340000, 2, '阜阳市', '0', 100);
INSERT INTO `sys_area` VALUES (341201, 341200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (341202, 341200, 3, '颍州区', '0', 100);
INSERT INTO `sys_area` VALUES (341203, 341200, 3, '颍东区', '0', 100);
INSERT INTO `sys_area` VALUES (341204, 341200, 3, '颍泉区', '0', 100);
INSERT INTO `sys_area` VALUES (341221, 341200, 3, '临泉县', '0', 100);
INSERT INTO `sys_area` VALUES (341222, 341200, 3, '太和县', '0', 100);
INSERT INTO `sys_area` VALUES (341225, 341200, 3, '阜南县', '0', 100);
INSERT INTO `sys_area` VALUES (341226, 341200, 3, '颍上县', '0', 100);
INSERT INTO `sys_area` VALUES (341282, 341200, 3, '界首市', '0', 100);
INSERT INTO `sys_area` VALUES (341300, 340000, 2, '宿州市', '0', 100);
INSERT INTO `sys_area` VALUES (341301, 341300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (341302, 341300, 3, '埇桥区', '0', 100);
INSERT INTO `sys_area` VALUES (341321, 341300, 3, '砀山县', '0', 100);
INSERT INTO `sys_area` VALUES (341322, 341300, 3, '萧县', '0', 100);
INSERT INTO `sys_area` VALUES (341323, 341300, 3, '灵璧县', '0', 100);
INSERT INTO `sys_area` VALUES (341324, 341300, 3, '泗县', '0', 100);
INSERT INTO `sys_area` VALUES (341500, 340000, 2, '六安市', '0', 100);
INSERT INTO `sys_area` VALUES (341501, 341500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (341502, 341500, 3, '金安区', '0', 100);
INSERT INTO `sys_area` VALUES (341503, 341500, 3, '裕安区', '0', 100);
INSERT INTO `sys_area` VALUES (341504, 341500, 3, '叶集区', '0', 100);
INSERT INTO `sys_area` VALUES (341522, 341500, 3, '霍邱县', '0', 100);
INSERT INTO `sys_area` VALUES (341523, 341500, 3, '舒城县', '0', 100);
INSERT INTO `sys_area` VALUES (341524, 341500, 3, '金寨县', '0', 100);
INSERT INTO `sys_area` VALUES (341525, 341500, 3, '霍山县', '0', 100);
INSERT INTO `sys_area` VALUES (341600, 340000, 2, '亳州市', '0', 100);
INSERT INTO `sys_area` VALUES (341601, 341600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (341602, 341600, 3, '谯城区', '0', 100);
INSERT INTO `sys_area` VALUES (341621, 341600, 3, '涡阳县', '0', 100);
INSERT INTO `sys_area` VALUES (341622, 341600, 3, '蒙城县', '0', 100);
INSERT INTO `sys_area` VALUES (341623, 341600, 3, '利辛县', '0', 100);
INSERT INTO `sys_area` VALUES (341700, 340000, 2, '池州市', '0', 100);
INSERT INTO `sys_area` VALUES (341701, 341700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (341702, 341700, 3, '贵池区', '0', 100);
INSERT INTO `sys_area` VALUES (341721, 341700, 3, '东至县', '0', 100);
INSERT INTO `sys_area` VALUES (341722, 341700, 3, '石台县', '0', 100);
INSERT INTO `sys_area` VALUES (341723, 341700, 3, '青阳县', '0', 100);
INSERT INTO `sys_area` VALUES (341800, 340000, 2, '宣城市', '0', 100);
INSERT INTO `sys_area` VALUES (341801, 341800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (341802, 341800, 3, '宣州区', '0', 100);
INSERT INTO `sys_area` VALUES (341821, 341800, 3, '郎溪县', '0', 100);
INSERT INTO `sys_area` VALUES (341822, 341800, 3, '广德县', '0', 100);
INSERT INTO `sys_area` VALUES (341823, 341800, 3, '泾县', '0', 100);
INSERT INTO `sys_area` VALUES (341824, 341800, 3, '绩溪县', '0', 100);
INSERT INTO `sys_area` VALUES (341825, 341800, 3, '旌德县', '0', 100);
INSERT INTO `sys_area` VALUES (341881, 341800, 3, '宁国市', '0', 100);
INSERT INTO `sys_area` VALUES (350000, 0, 1, '福建省', '0', 100);
INSERT INTO `sys_area` VALUES (350100, 350000, 2, '福州市', '0', 100);
INSERT INTO `sys_area` VALUES (350101, 350100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (350102, 350100, 3, '鼓楼区', '0', 100);
INSERT INTO `sys_area` VALUES (350103, 350100, 3, '台江区', '0', 100);
INSERT INTO `sys_area` VALUES (350104, 350100, 3, '仓山区', '0', 100);
INSERT INTO `sys_area` VALUES (350105, 350100, 3, '马尾区', '0', 100);
INSERT INTO `sys_area` VALUES (350111, 350100, 3, '晋安区', '0', 100);
INSERT INTO `sys_area` VALUES (350121, 350100, 3, '闽侯县', '0', 100);
INSERT INTO `sys_area` VALUES (350122, 350100, 3, '连江县', '0', 100);
INSERT INTO `sys_area` VALUES (350123, 350100, 3, '罗源县', '0', 100);
INSERT INTO `sys_area` VALUES (350124, 350100, 3, '闽清县', '0', 100);
INSERT INTO `sys_area` VALUES (350125, 350100, 3, '永泰县', '0', 100);
INSERT INTO `sys_area` VALUES (350128, 350100, 3, '平潭县', '0', 100);
INSERT INTO `sys_area` VALUES (350181, 350100, 3, '福清市', '0', 100);
INSERT INTO `sys_area` VALUES (350182, 350100, 3, '长乐市', '0', 100);
INSERT INTO `sys_area` VALUES (350200, 350000, 2, '厦门市', '0', 100);
INSERT INTO `sys_area` VALUES (350201, 350200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (350203, 350200, 3, '思明区', '0', 100);
INSERT INTO `sys_area` VALUES (350205, 350200, 3, '海沧区', '0', 100);
INSERT INTO `sys_area` VALUES (350206, 350200, 3, '湖里区', '0', 100);
INSERT INTO `sys_area` VALUES (350211, 350200, 3, '集美区', '0', 100);
INSERT INTO `sys_area` VALUES (350212, 350200, 3, '同安区', '0', 100);
INSERT INTO `sys_area` VALUES (350213, 350200, 3, '翔安区', '0', 100);
INSERT INTO `sys_area` VALUES (350300, 350000, 2, '莆田市', '0', 100);
INSERT INTO `sys_area` VALUES (350301, 350300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (350302, 350300, 3, '城厢区', '0', 100);
INSERT INTO `sys_area` VALUES (350303, 350300, 3, '涵江区', '0', 100);
INSERT INTO `sys_area` VALUES (350304, 350300, 3, '荔城区', '0', 100);
INSERT INTO `sys_area` VALUES (350305, 350300, 3, '秀屿区', '0', 100);
INSERT INTO `sys_area` VALUES (350322, 350300, 3, '仙游县', '0', 100);
INSERT INTO `sys_area` VALUES (350400, 350000, 2, '三明市', '0', 100);
INSERT INTO `sys_area` VALUES (350401, 350400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (350402, 350400, 3, '梅列区', '0', 100);
INSERT INTO `sys_area` VALUES (350403, 350400, 3, '三元区', '0', 100);
INSERT INTO `sys_area` VALUES (350421, 350400, 3, '明溪县', '0', 100);
INSERT INTO `sys_area` VALUES (350423, 350400, 3, '清流县', '0', 100);
INSERT INTO `sys_area` VALUES (350424, 350400, 3, '宁化县', '0', 100);
INSERT INTO `sys_area` VALUES (350425, 350400, 3, '大田县', '0', 100);
INSERT INTO `sys_area` VALUES (350426, 350400, 3, '尤溪县', '0', 100);
INSERT INTO `sys_area` VALUES (350427, 350400, 3, '沙县', '0', 100);
INSERT INTO `sys_area` VALUES (350428, 350400, 3, '将乐县', '0', 100);
INSERT INTO `sys_area` VALUES (350429, 350400, 3, '泰宁县', '0', 100);
INSERT INTO `sys_area` VALUES (350430, 350400, 3, '建宁县', '0', 100);
INSERT INTO `sys_area` VALUES (350481, 350400, 3, '永安市', '0', 100);
INSERT INTO `sys_area` VALUES (350500, 350000, 2, '泉州市', '0', 100);
INSERT INTO `sys_area` VALUES (350501, 350500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (350502, 350500, 3, '鲤城区', '0', 100);
INSERT INTO `sys_area` VALUES (350503, 350500, 3, '丰泽区', '0', 100);
INSERT INTO `sys_area` VALUES (350504, 350500, 3, '洛江区', '0', 100);
INSERT INTO `sys_area` VALUES (350505, 350500, 3, '泉港区', '0', 100);
INSERT INTO `sys_area` VALUES (350521, 350500, 3, '惠安县', '0', 100);
INSERT INTO `sys_area` VALUES (350524, 350500, 3, '安溪县', '0', 100);
INSERT INTO `sys_area` VALUES (350525, 350500, 3, '永春县', '0', 100);
INSERT INTO `sys_area` VALUES (350526, 350500, 3, '德化县', '0', 100);
INSERT INTO `sys_area` VALUES (350527, 350500, 3, '金门县', '0', 100);
INSERT INTO `sys_area` VALUES (350581, 350500, 3, '石狮市', '0', 100);
INSERT INTO `sys_area` VALUES (350582, 350500, 3, '晋江市', '0', 100);
INSERT INTO `sys_area` VALUES (350583, 350500, 3, '南安市', '0', 100);
INSERT INTO `sys_area` VALUES (350600, 350000, 2, '漳州市', '0', 100);
INSERT INTO `sys_area` VALUES (350601, 350600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (350602, 350600, 3, '芗城区', '0', 100);
INSERT INTO `sys_area` VALUES (350603, 350600, 3, '龙文区', '0', 100);
INSERT INTO `sys_area` VALUES (350622, 350600, 3, '云霄县', '0', 100);
INSERT INTO `sys_area` VALUES (350623, 350600, 3, '漳浦县', '0', 100);
INSERT INTO `sys_area` VALUES (350624, 350600, 3, '诏安县', '0', 100);
INSERT INTO `sys_area` VALUES (350625, 350600, 3, '长泰县', '0', 100);
INSERT INTO `sys_area` VALUES (350626, 350600, 3, '东山县', '0', 100);
INSERT INTO `sys_area` VALUES (350627, 350600, 3, '南靖县', '0', 100);
INSERT INTO `sys_area` VALUES (350628, 350600, 3, '平和县', '0', 100);
INSERT INTO `sys_area` VALUES (350629, 350600, 3, '华安县', '0', 100);
INSERT INTO `sys_area` VALUES (350681, 350600, 3, '龙海市', '0', 100);
INSERT INTO `sys_area` VALUES (350700, 350000, 2, '南平市', '0', 100);
INSERT INTO `sys_area` VALUES (350701, 350700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (350702, 350700, 3, '延平区', '0', 100);
INSERT INTO `sys_area` VALUES (350703, 350700, 3, '建阳区', '0', 100);
INSERT INTO `sys_area` VALUES (350721, 350700, 3, '顺昌县', '0', 100);
INSERT INTO `sys_area` VALUES (350722, 350700, 3, '浦城县', '0', 100);
INSERT INTO `sys_area` VALUES (350723, 350700, 3, '光泽县', '0', 100);
INSERT INTO `sys_area` VALUES (350724, 350700, 3, '松溪县', '0', 100);
INSERT INTO `sys_area` VALUES (350725, 350700, 3, '政和县', '0', 100);
INSERT INTO `sys_area` VALUES (350781, 350700, 3, '邵武市', '0', 100);
INSERT INTO `sys_area` VALUES (350782, 350700, 3, '武夷山市', '0', 100);
INSERT INTO `sys_area` VALUES (350783, 350700, 3, '建瓯市', '0', 100);
INSERT INTO `sys_area` VALUES (350800, 350000, 2, '龙岩市', '0', 100);
INSERT INTO `sys_area` VALUES (350801, 350800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (350802, 350800, 3, '新罗区', '0', 100);
INSERT INTO `sys_area` VALUES (350803, 350800, 3, '永定区', '0', 100);
INSERT INTO `sys_area` VALUES (350821, 350800, 3, '长汀县', '0', 100);
INSERT INTO `sys_area` VALUES (350823, 350800, 3, '上杭县', '0', 100);
INSERT INTO `sys_area` VALUES (350824, 350800, 3, '武平县', '0', 100);
INSERT INTO `sys_area` VALUES (350825, 350800, 3, '连城县', '0', 100);
INSERT INTO `sys_area` VALUES (350881, 350800, 3, '漳平市', '0', 100);
INSERT INTO `sys_area` VALUES (350900, 350000, 2, '宁德市', '0', 100);
INSERT INTO `sys_area` VALUES (350901, 350900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (350902, 350900, 3, '蕉城区', '0', 100);
INSERT INTO `sys_area` VALUES (350921, 350900, 3, '霞浦县', '0', 100);
INSERT INTO `sys_area` VALUES (350922, 350900, 3, '古田县', '0', 100);
INSERT INTO `sys_area` VALUES (350923, 350900, 3, '屏南县', '0', 100);
INSERT INTO `sys_area` VALUES (350924, 350900, 3, '寿宁县', '0', 100);
INSERT INTO `sys_area` VALUES (350925, 350900, 3, '周宁县', '0', 100);
INSERT INTO `sys_area` VALUES (350926, 350900, 3, '柘荣县', '0', 100);
INSERT INTO `sys_area` VALUES (350981, 350900, 3, '福安市', '0', 100);
INSERT INTO `sys_area` VALUES (350982, 350900, 3, '福鼎市', '0', 100);
INSERT INTO `sys_area` VALUES (360000, 0, 1, '江西省', '0', 100);
INSERT INTO `sys_area` VALUES (360100, 360000, 2, '南昌市', '0', 100);
INSERT INTO `sys_area` VALUES (360101, 360100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (360102, 360100, 3, '东湖区', '0', 100);
INSERT INTO `sys_area` VALUES (360103, 360100, 3, '西湖区', '0', 100);
INSERT INTO `sys_area` VALUES (360104, 360100, 3, '青云谱区', '0', 100);
INSERT INTO `sys_area` VALUES (360105, 360100, 3, '湾里区', '0', 100);
INSERT INTO `sys_area` VALUES (360111, 360100, 3, '青山湖区', '0', 100);
INSERT INTO `sys_area` VALUES (360112, 360100, 3, '新建区', '0', 100);
INSERT INTO `sys_area` VALUES (360121, 360100, 3, '南昌县', '0', 100);
INSERT INTO `sys_area` VALUES (360123, 360100, 3, '安义县', '0', 100);
INSERT INTO `sys_area` VALUES (360124, 360100, 3, '进贤县', '0', 100);
INSERT INTO `sys_area` VALUES (360200, 360000, 2, '景德镇市', '0', 100);
INSERT INTO `sys_area` VALUES (360201, 360200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (360202, 360200, 3, '昌江区', '0', 100);
INSERT INTO `sys_area` VALUES (360203, 360200, 3, '珠山区', '0', 100);
INSERT INTO `sys_area` VALUES (360222, 360200, 3, '浮梁县', '0', 100);
INSERT INTO `sys_area` VALUES (360281, 360200, 3, '乐平市', '0', 100);
INSERT INTO `sys_area` VALUES (360300, 360000, 2, '萍乡市', '0', 100);
INSERT INTO `sys_area` VALUES (360301, 360300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (360302, 360300, 3, '安源区', '0', 100);
INSERT INTO `sys_area` VALUES (360313, 360300, 3, '湘东区', '0', 100);
INSERT INTO `sys_area` VALUES (360321, 360300, 3, '莲花县', '0', 100);
INSERT INTO `sys_area` VALUES (360322, 360300, 3, '上栗县', '0', 100);
INSERT INTO `sys_area` VALUES (360323, 360300, 3, '芦溪县', '0', 100);
INSERT INTO `sys_area` VALUES (360400, 360000, 2, '九江市', '0', 100);
INSERT INTO `sys_area` VALUES (360401, 360400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (360402, 360400, 3, '濂溪区', '0', 100);
INSERT INTO `sys_area` VALUES (360403, 360400, 3, '浔阳区', '0', 100);
INSERT INTO `sys_area` VALUES (360421, 360400, 3, '九江县', '0', 100);
INSERT INTO `sys_area` VALUES (360423, 360400, 3, '武宁县', '0', 100);
INSERT INTO `sys_area` VALUES (360424, 360400, 3, '修水县', '0', 100);
INSERT INTO `sys_area` VALUES (360425, 360400, 3, '永修县', '0', 100);
INSERT INTO `sys_area` VALUES (360426, 360400, 3, '德安县', '0', 100);
INSERT INTO `sys_area` VALUES (360428, 360400, 3, '都昌县', '0', 100);
INSERT INTO `sys_area` VALUES (360429, 360400, 3, '湖口县', '0', 100);
INSERT INTO `sys_area` VALUES (360430, 360400, 3, '彭泽县', '0', 100);
INSERT INTO `sys_area` VALUES (360481, 360400, 3, '瑞昌市', '0', 100);
INSERT INTO `sys_area` VALUES (360482, 360400, 3, '共青城市', '0', 100);
INSERT INTO `sys_area` VALUES (360483, 360400, 3, '庐山市', '0', 100);
INSERT INTO `sys_area` VALUES (360500, 360000, 2, '新余市', '0', 100);
INSERT INTO `sys_area` VALUES (360501, 360500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (360502, 360500, 3, '渝水区', '0', 100);
INSERT INTO `sys_area` VALUES (360521, 360500, 3, '分宜县', '0', 100);
INSERT INTO `sys_area` VALUES (360600, 360000, 2, '鹰潭市', '0', 100);
INSERT INTO `sys_area` VALUES (360601, 360600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (360602, 360600, 3, '月湖区', '0', 100);
INSERT INTO `sys_area` VALUES (360622, 360600, 3, '余江县', '0', 100);
INSERT INTO `sys_area` VALUES (360681, 360600, 3, '贵溪市', '0', 100);
INSERT INTO `sys_area` VALUES (360700, 360000, 2, '赣州市', '0', 100);
INSERT INTO `sys_area` VALUES (360701, 360700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (360702, 360700, 3, '章贡区', '0', 100);
INSERT INTO `sys_area` VALUES (360703, 360700, 3, '南康区', '0', 100);
INSERT INTO `sys_area` VALUES (360721, 360700, 3, '赣县', '0', 100);
INSERT INTO `sys_area` VALUES (360722, 360700, 3, '信丰县', '0', 100);
INSERT INTO `sys_area` VALUES (360723, 360700, 3, '大余县', '0', 100);
INSERT INTO `sys_area` VALUES (360724, 360700, 3, '上犹县', '0', 100);
INSERT INTO `sys_area` VALUES (360725, 360700, 3, '崇义县', '0', 100);
INSERT INTO `sys_area` VALUES (360726, 360700, 3, '安远县', '0', 100);
INSERT INTO `sys_area` VALUES (360727, 360700, 3, '龙南县', '0', 100);
INSERT INTO `sys_area` VALUES (360728, 360700, 3, '定南县', '0', 100);
INSERT INTO `sys_area` VALUES (360729, 360700, 3, '全南县', '0', 100);
INSERT INTO `sys_area` VALUES (360730, 360700, 3, '宁都县', '0', 100);
INSERT INTO `sys_area` VALUES (360731, 360700, 3, '于都县', '0', 100);
INSERT INTO `sys_area` VALUES (360732, 360700, 3, '兴国县', '0', 100);
INSERT INTO `sys_area` VALUES (360733, 360700, 3, '会昌县', '0', 100);
INSERT INTO `sys_area` VALUES (360734, 360700, 3, '寻乌县', '0', 100);
INSERT INTO `sys_area` VALUES (360735, 360700, 3, '石城县', '0', 100);
INSERT INTO `sys_area` VALUES (360781, 360700, 3, '瑞金市', '0', 100);
INSERT INTO `sys_area` VALUES (360800, 360000, 2, '吉安市', '0', 100);
INSERT INTO `sys_area` VALUES (360801, 360800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (360802, 360800, 3, '吉州区', '0', 100);
INSERT INTO `sys_area` VALUES (360803, 360800, 3, '青原区', '0', 100);
INSERT INTO `sys_area` VALUES (360821, 360800, 3, '吉安县', '0', 100);
INSERT INTO `sys_area` VALUES (360822, 360800, 3, '吉水县', '0', 100);
INSERT INTO `sys_area` VALUES (360823, 360800, 3, '峡江县', '0', 100);
INSERT INTO `sys_area` VALUES (360824, 360800, 3, '新干县', '0', 100);
INSERT INTO `sys_area` VALUES (360825, 360800, 3, '永丰县', '0', 100);
INSERT INTO `sys_area` VALUES (360826, 360800, 3, '泰和县', '0', 100);
INSERT INTO `sys_area` VALUES (360827, 360800, 3, '遂川县', '0', 100);
INSERT INTO `sys_area` VALUES (360828, 360800, 3, '万安县', '0', 100);
INSERT INTO `sys_area` VALUES (360829, 360800, 3, '安福县', '0', 100);
INSERT INTO `sys_area` VALUES (360830, 360800, 3, '永新县', '0', 100);
INSERT INTO `sys_area` VALUES (360881, 360800, 3, '井冈山市', '0', 100);
INSERT INTO `sys_area` VALUES (360900, 360000, 2, '宜春市', '0', 100);
INSERT INTO `sys_area` VALUES (360901, 360900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (360902, 360900, 3, '袁州区', '0', 100);
INSERT INTO `sys_area` VALUES (360921, 360900, 3, '奉新县', '0', 100);
INSERT INTO `sys_area` VALUES (360922, 360900, 3, '万载县', '0', 100);
INSERT INTO `sys_area` VALUES (360923, 360900, 3, '上高县', '0', 100);
INSERT INTO `sys_area` VALUES (360924, 360900, 3, '宜丰县', '0', 100);
INSERT INTO `sys_area` VALUES (360925, 360900, 3, '靖安县', '0', 100);
INSERT INTO `sys_area` VALUES (360926, 360900, 3, '铜鼓县', '0', 100);
INSERT INTO `sys_area` VALUES (360981, 360900, 3, '丰城市', '0', 100);
INSERT INTO `sys_area` VALUES (360982, 360900, 3, '樟树市', '0', 100);
INSERT INTO `sys_area` VALUES (360983, 360900, 3, '高安市', '0', 100);
INSERT INTO `sys_area` VALUES (361000, 360000, 2, '抚州市', '0', 100);
INSERT INTO `sys_area` VALUES (361001, 361000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (361002, 361000, 3, '临川区', '0', 100);
INSERT INTO `sys_area` VALUES (361021, 361000, 3, '南城县', '0', 100);
INSERT INTO `sys_area` VALUES (361022, 361000, 3, '黎川县', '0', 100);
INSERT INTO `sys_area` VALUES (361023, 361000, 3, '南丰县', '0', 100);
INSERT INTO `sys_area` VALUES (361024, 361000, 3, '崇仁县', '0', 100);
INSERT INTO `sys_area` VALUES (361025, 361000, 3, '乐安县', '0', 100);
INSERT INTO `sys_area` VALUES (361026, 361000, 3, '宜黄县', '0', 100);
INSERT INTO `sys_area` VALUES (361027, 361000, 3, '金溪县', '0', 100);
INSERT INTO `sys_area` VALUES (361028, 361000, 3, '资溪县', '0', 100);
INSERT INTO `sys_area` VALUES (361029, 361000, 3, '东乡县', '0', 100);
INSERT INTO `sys_area` VALUES (361030, 361000, 3, '广昌县', '0', 100);
INSERT INTO `sys_area` VALUES (361100, 360000, 2, '上饶市', '0', 100);
INSERT INTO `sys_area` VALUES (361101, 361100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (361102, 361100, 3, '信州区', '0', 100);
INSERT INTO `sys_area` VALUES (361103, 361100, 3, '广丰区', '0', 100);
INSERT INTO `sys_area` VALUES (361121, 361100, 3, '上饶县', '0', 100);
INSERT INTO `sys_area` VALUES (361123, 361100, 3, '玉山县', '0', 100);
INSERT INTO `sys_area` VALUES (361124, 361100, 3, '铅山县', '0', 100);
INSERT INTO `sys_area` VALUES (361125, 361100, 3, '横峰县', '0', 100);
INSERT INTO `sys_area` VALUES (361126, 361100, 3, '弋阳县', '0', 100);
INSERT INTO `sys_area` VALUES (361127, 361100, 3, '余干县', '0', 100);
INSERT INTO `sys_area` VALUES (361128, 361100, 3, '鄱阳县', '0', 100);
INSERT INTO `sys_area` VALUES (361129, 361100, 3, '万年县', '0', 100);
INSERT INTO `sys_area` VALUES (361130, 361100, 3, '婺源县', '0', 100);
INSERT INTO `sys_area` VALUES (361181, 361100, 3, '德兴市', '0', 100);
INSERT INTO `sys_area` VALUES (370000, 0, 1, '山东省', '0', 100);
INSERT INTO `sys_area` VALUES (370100, 370000, 2, '济南市', '0', 100);
INSERT INTO `sys_area` VALUES (370101, 370100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (370102, 370100, 3, '历下区', '0', 100);
INSERT INTO `sys_area` VALUES (370103, 370100, 3, '市中区', '0', 100);
INSERT INTO `sys_area` VALUES (370104, 370100, 3, '槐荫区', '0', 100);
INSERT INTO `sys_area` VALUES (370105, 370100, 3, '天桥区', '0', 100);
INSERT INTO `sys_area` VALUES (370112, 370100, 3, '历城区', '0', 100);
INSERT INTO `sys_area` VALUES (370113, 370100, 3, '长清区', '0', 100);
INSERT INTO `sys_area` VALUES (370124, 370100, 3, '平阴县', '0', 100);
INSERT INTO `sys_area` VALUES (370125, 370100, 3, '济阳县', '0', 100);
INSERT INTO `sys_area` VALUES (370126, 370100, 3, '商河县', '0', 100);
INSERT INTO `sys_area` VALUES (370181, 370100, 3, '章丘市', '0', 100);
INSERT INTO `sys_area` VALUES (370200, 370000, 2, '青岛市', '0', 100);
INSERT INTO `sys_area` VALUES (370201, 370200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (370202, 370200, 3, '市南区', '0', 100);
INSERT INTO `sys_area` VALUES (370203, 370200, 3, '市北区', '0', 100);
INSERT INTO `sys_area` VALUES (370211, 370200, 3, '黄岛区', '0', 100);
INSERT INTO `sys_area` VALUES (370212, 370200, 3, '崂山区', '0', 100);
INSERT INTO `sys_area` VALUES (370213, 370200, 3, '李沧区', '0', 100);
INSERT INTO `sys_area` VALUES (370214, 370200, 3, '城阳区', '0', 100);
INSERT INTO `sys_area` VALUES (370281, 370200, 3, '胶州市', '0', 100);
INSERT INTO `sys_area` VALUES (370282, 370200, 3, '即墨市', '0', 100);
INSERT INTO `sys_area` VALUES (370283, 370200, 3, '平度市', '0', 100);
INSERT INTO `sys_area` VALUES (370285, 370200, 3, '莱西市', '0', 100);
INSERT INTO `sys_area` VALUES (370300, 370000, 2, '淄博市', '0', 100);
INSERT INTO `sys_area` VALUES (370301, 370300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (370302, 370300, 3, '淄川区', '0', 100);
INSERT INTO `sys_area` VALUES (370303, 370300, 3, '张店区', '0', 100);
INSERT INTO `sys_area` VALUES (370304, 370300, 3, '博山区', '0', 100);
INSERT INTO `sys_area` VALUES (370305, 370300, 3, '临淄区', '0', 100);
INSERT INTO `sys_area` VALUES (370306, 370300, 3, '周村区', '0', 100);
INSERT INTO `sys_area` VALUES (370321, 370300, 3, '桓台县', '0', 100);
INSERT INTO `sys_area` VALUES (370322, 370300, 3, '高青县', '0', 100);
INSERT INTO `sys_area` VALUES (370323, 370300, 3, '沂源县', '0', 100);
INSERT INTO `sys_area` VALUES (370400, 370000, 2, '枣庄市', '0', 100);
INSERT INTO `sys_area` VALUES (370401, 370400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (370402, 370400, 3, '市中区', '0', 100);
INSERT INTO `sys_area` VALUES (370403, 370400, 3, '薛城区', '0', 100);
INSERT INTO `sys_area` VALUES (370404, 370400, 3, '峄城区', '0', 100);
INSERT INTO `sys_area` VALUES (370405, 370400, 3, '台儿庄区', '0', 100);
INSERT INTO `sys_area` VALUES (370406, 370400, 3, '山亭区', '0', 100);
INSERT INTO `sys_area` VALUES (370481, 370400, 3, '滕州市', '0', 100);
INSERT INTO `sys_area` VALUES (370500, 370000, 2, '东营市', '0', 100);
INSERT INTO `sys_area` VALUES (370501, 370500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (370502, 370500, 3, '东营区', '0', 100);
INSERT INTO `sys_area` VALUES (370503, 370500, 3, '河口区', '0', 100);
INSERT INTO `sys_area` VALUES (370505, 370500, 3, '垦利区', '0', 100);
INSERT INTO `sys_area` VALUES (370522, 370500, 3, '利津县', '0', 100);
INSERT INTO `sys_area` VALUES (370523, 370500, 3, '广饶县', '0', 100);
INSERT INTO `sys_area` VALUES (370600, 370000, 2, '烟台市', '0', 100);
INSERT INTO `sys_area` VALUES (370601, 370600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (370602, 370600, 3, '芝罘区', '0', 100);
INSERT INTO `sys_area` VALUES (370611, 370600, 3, '福山区', '0', 100);
INSERT INTO `sys_area` VALUES (370612, 370600, 3, '牟平区', '0', 100);
INSERT INTO `sys_area` VALUES (370613, 370600, 3, '莱山区', '0', 100);
INSERT INTO `sys_area` VALUES (370634, 370600, 3, '长岛县', '0', 100);
INSERT INTO `sys_area` VALUES (370681, 370600, 3, '龙口市', '0', 100);
INSERT INTO `sys_area` VALUES (370682, 370600, 3, '莱阳市', '0', 100);
INSERT INTO `sys_area` VALUES (370683, 370600, 3, '莱州市', '0', 100);
INSERT INTO `sys_area` VALUES (370684, 370600, 3, '蓬莱市', '0', 100);
INSERT INTO `sys_area` VALUES (370685, 370600, 3, '招远市', '0', 100);
INSERT INTO `sys_area` VALUES (370686, 370600, 3, '栖霞市', '0', 100);
INSERT INTO `sys_area` VALUES (370687, 370600, 3, '海阳市', '0', 100);
INSERT INTO `sys_area` VALUES (370700, 370000, 2, '潍坊市', '0', 100);
INSERT INTO `sys_area` VALUES (370701, 370700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (370702, 370700, 3, '潍城区', '0', 100);
INSERT INTO `sys_area` VALUES (370703, 370700, 3, '寒亭区', '0', 100);
INSERT INTO `sys_area` VALUES (370704, 370700, 3, '坊子区', '0', 100);
INSERT INTO `sys_area` VALUES (370705, 370700, 3, '奎文区', '0', 100);
INSERT INTO `sys_area` VALUES (370724, 370700, 3, '临朐县', '0', 100);
INSERT INTO `sys_area` VALUES (370725, 370700, 3, '昌乐县', '0', 100);
INSERT INTO `sys_area` VALUES (370781, 370700, 3, '青州市', '0', 100);
INSERT INTO `sys_area` VALUES (370782, 370700, 3, '诸城市', '0', 100);
INSERT INTO `sys_area` VALUES (370783, 370700, 3, '寿光市', '0', 100);
INSERT INTO `sys_area` VALUES (370784, 370700, 3, '安丘市', '0', 100);
INSERT INTO `sys_area` VALUES (370785, 370700, 3, '高密市', '0', 100);
INSERT INTO `sys_area` VALUES (370786, 370700, 3, '昌邑市', '0', 100);
INSERT INTO `sys_area` VALUES (370800, 370000, 2, '济宁市', '0', 100);
INSERT INTO `sys_area` VALUES (370801, 370800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (370811, 370800, 3, '任城区', '0', 100);
INSERT INTO `sys_area` VALUES (370812, 370800, 3, '兖州区', '0', 100);
INSERT INTO `sys_area` VALUES (370826, 370800, 3, '微山县', '0', 100);
INSERT INTO `sys_area` VALUES (370827, 370800, 3, '鱼台县', '0', 100);
INSERT INTO `sys_area` VALUES (370828, 370800, 3, '金乡县', '0', 100);
INSERT INTO `sys_area` VALUES (370829, 370800, 3, '嘉祥县', '0', 100);
INSERT INTO `sys_area` VALUES (370830, 370800, 3, '汶上县', '0', 100);
INSERT INTO `sys_area` VALUES (370831, 370800, 3, '泗水县', '0', 100);
INSERT INTO `sys_area` VALUES (370832, 370800, 3, '梁山县', '0', 100);
INSERT INTO `sys_area` VALUES (370881, 370800, 3, '曲阜市', '0', 100);
INSERT INTO `sys_area` VALUES (370883, 370800, 3, '邹城市', '0', 100);
INSERT INTO `sys_area` VALUES (370900, 370000, 2, '泰安市', '0', 100);
INSERT INTO `sys_area` VALUES (370901, 370900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (370902, 370900, 3, '泰山区', '0', 100);
INSERT INTO `sys_area` VALUES (370911, 370900, 3, '岱岳区', '0', 100);
INSERT INTO `sys_area` VALUES (370921, 370900, 3, '宁阳县', '0', 100);
INSERT INTO `sys_area` VALUES (370923, 370900, 3, '东平县', '0', 100);
INSERT INTO `sys_area` VALUES (370982, 370900, 3, '新泰市', '0', 100);
INSERT INTO `sys_area` VALUES (370983, 370900, 3, '肥城市', '0', 100);
INSERT INTO `sys_area` VALUES (371000, 370000, 2, '威海市', '0', 100);
INSERT INTO `sys_area` VALUES (371001, 371000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (371002, 371000, 3, '环翠区', '0', 100);
INSERT INTO `sys_area` VALUES (371003, 371000, 3, '文登区', '0', 100);
INSERT INTO `sys_area` VALUES (371082, 371000, 3, '荣成市', '0', 100);
INSERT INTO `sys_area` VALUES (371083, 371000, 3, '乳山市', '0', 100);
INSERT INTO `sys_area` VALUES (371100, 370000, 2, '日照市', '0', 100);
INSERT INTO `sys_area` VALUES (371101, 371100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (371102, 371100, 3, '东港区', '0', 100);
INSERT INTO `sys_area` VALUES (371103, 371100, 3, '岚山区', '0', 100);
INSERT INTO `sys_area` VALUES (371121, 371100, 3, '五莲县', '0', 100);
INSERT INTO `sys_area` VALUES (371122, 371100, 3, '莒县', '0', 100);
INSERT INTO `sys_area` VALUES (371200, 370000, 2, '莱芜市', '0', 100);
INSERT INTO `sys_area` VALUES (371201, 371200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (371202, 371200, 3, '莱城区', '0', 100);
INSERT INTO `sys_area` VALUES (371203, 371200, 3, '钢城区', '0', 100);
INSERT INTO `sys_area` VALUES (371300, 370000, 2, '临沂市', '0', 100);
INSERT INTO `sys_area` VALUES (371301, 371300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (371302, 371300, 3, '兰山区', '0', 100);
INSERT INTO `sys_area` VALUES (371311, 371300, 3, '罗庄区', '0', 100);
INSERT INTO `sys_area` VALUES (371312, 371300, 3, '河东区', '0', 100);
INSERT INTO `sys_area` VALUES (371321, 371300, 3, '沂南县', '0', 100);
INSERT INTO `sys_area` VALUES (371322, 371300, 3, '郯城县', '0', 100);
INSERT INTO `sys_area` VALUES (371323, 371300, 3, '沂水县', '0', 100);
INSERT INTO `sys_area` VALUES (371324, 371300, 3, '兰陵县', '0', 100);
INSERT INTO `sys_area` VALUES (371325, 371300, 3, '费县', '0', 100);
INSERT INTO `sys_area` VALUES (371326, 371300, 3, '平邑县', '0', 100);
INSERT INTO `sys_area` VALUES (371327, 371300, 3, '莒南县', '0', 100);
INSERT INTO `sys_area` VALUES (371328, 371300, 3, '蒙阴县', '0', 100);
INSERT INTO `sys_area` VALUES (371329, 371300, 3, '临沭县', '0', 100);
INSERT INTO `sys_area` VALUES (371400, 370000, 2, '德州市', '0', 100);
INSERT INTO `sys_area` VALUES (371401, 371400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (371402, 371400, 3, '德城区', '0', 100);
INSERT INTO `sys_area` VALUES (371403, 371400, 3, '陵城区', '0', 100);
INSERT INTO `sys_area` VALUES (371422, 371400, 3, '宁津县', '0', 100);
INSERT INTO `sys_area` VALUES (371423, 371400, 3, '庆云县', '0', 100);
INSERT INTO `sys_area` VALUES (371424, 371400, 3, '临邑县', '0', 100);
INSERT INTO `sys_area` VALUES (371425, 371400, 3, '齐河县', '0', 100);
INSERT INTO `sys_area` VALUES (371426, 371400, 3, '平原县', '0', 100);
INSERT INTO `sys_area` VALUES (371427, 371400, 3, '夏津县', '0', 100);
INSERT INTO `sys_area` VALUES (371428, 371400, 3, '武城县', '0', 100);
INSERT INTO `sys_area` VALUES (371481, 371400, 3, '乐陵市', '0', 100);
INSERT INTO `sys_area` VALUES (371482, 371400, 3, '禹城市', '0', 100);
INSERT INTO `sys_area` VALUES (371500, 370000, 2, '聊城市', '0', 100);
INSERT INTO `sys_area` VALUES (371501, 371500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (371502, 371500, 3, '东昌府区', '0', 100);
INSERT INTO `sys_area` VALUES (371521, 371500, 3, '阳谷县', '0', 100);
INSERT INTO `sys_area` VALUES (371522, 371500, 3, '莘县', '0', 100);
INSERT INTO `sys_area` VALUES (371523, 371500, 3, '茌平县', '0', 100);
INSERT INTO `sys_area` VALUES (371524, 371500, 3, '东阿县', '0', 100);
INSERT INTO `sys_area` VALUES (371525, 371500, 3, '冠县', '0', 100);
INSERT INTO `sys_area` VALUES (371526, 371500, 3, '高唐县', '0', 100);
INSERT INTO `sys_area` VALUES (371581, 371500, 3, '临清市', '0', 100);
INSERT INTO `sys_area` VALUES (371600, 370000, 2, '滨州市', '0', 100);
INSERT INTO `sys_area` VALUES (371601, 371600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (371602, 371600, 3, '滨城区', '0', 100);
INSERT INTO `sys_area` VALUES (371603, 371600, 3, '沾化区', '0', 100);
INSERT INTO `sys_area` VALUES (371621, 371600, 3, '惠民县', '0', 100);
INSERT INTO `sys_area` VALUES (371622, 371600, 3, '阳信县', '0', 100);
INSERT INTO `sys_area` VALUES (371623, 371600, 3, '无棣县', '0', 100);
INSERT INTO `sys_area` VALUES (371625, 371600, 3, '博兴县', '0', 100);
INSERT INTO `sys_area` VALUES (371626, 371600, 3, '邹平县', '0', 100);
INSERT INTO `sys_area` VALUES (371700, 370000, 2, '菏泽市', '0', 100);
INSERT INTO `sys_area` VALUES (371701, 371700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (371702, 371700, 3, '牡丹区', '0', 100);
INSERT INTO `sys_area` VALUES (371703, 371700, 3, '定陶区', '0', 100);
INSERT INTO `sys_area` VALUES (371721, 371700, 3, '曹县', '0', 100);
INSERT INTO `sys_area` VALUES (371722, 371700, 3, '单县', '0', 100);
INSERT INTO `sys_area` VALUES (371723, 371700, 3, '成武县', '0', 100);
INSERT INTO `sys_area` VALUES (371724, 371700, 3, '巨野县', '0', 100);
INSERT INTO `sys_area` VALUES (371725, 371700, 3, '郓城县', '0', 100);
INSERT INTO `sys_area` VALUES (371726, 371700, 3, '鄄城县', '0', 100);
INSERT INTO `sys_area` VALUES (371728, 371700, 3, '东明县', '0', 100);
INSERT INTO `sys_area` VALUES (410000, 0, 1, '河南省', '0', 100);
INSERT INTO `sys_area` VALUES (410100, 410000, 2, '郑州市', '0', 100);
INSERT INTO `sys_area` VALUES (410101, 410100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (410102, 410100, 3, '中原区', '0', 100);
INSERT INTO `sys_area` VALUES (410103, 410100, 3, '二七区', '0', 100);
INSERT INTO `sys_area` VALUES (410104, 410100, 3, '管城回族区', '0', 100);
INSERT INTO `sys_area` VALUES (410105, 410100, 3, '金水区', '0', 100);
INSERT INTO `sys_area` VALUES (410106, 410100, 3, '上街区', '0', 100);
INSERT INTO `sys_area` VALUES (410108, 410100, 3, '惠济区', '0', 100);
INSERT INTO `sys_area` VALUES (410122, 410100, 3, '中牟县', '0', 100);
INSERT INTO `sys_area` VALUES (410181, 410100, 3, '巩义市', '0', 100);
INSERT INTO `sys_area` VALUES (410182, 410100, 3, '荥阳市', '0', 100);
INSERT INTO `sys_area` VALUES (410183, 410100, 3, '新密市', '0', 100);
INSERT INTO `sys_area` VALUES (410184, 410100, 3, '新郑市', '0', 100);
INSERT INTO `sys_area` VALUES (410185, 410100, 3, '登封市', '0', 100);
INSERT INTO `sys_area` VALUES (410200, 410000, 2, '开封市', '0', 100);
INSERT INTO `sys_area` VALUES (410201, 410200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (410202, 410200, 3, '龙亭区', '0', 100);
INSERT INTO `sys_area` VALUES (410203, 410200, 3, '顺河回族区', '0', 100);
INSERT INTO `sys_area` VALUES (410204, 410200, 3, '鼓楼区', '0', 100);
INSERT INTO `sys_area` VALUES (410205, 410200, 3, '禹王台区', '0', 100);
INSERT INTO `sys_area` VALUES (410211, 410200, 3, '金明区', '0', 100);
INSERT INTO `sys_area` VALUES (410212, 410200, 3, '祥符区', '0', 100);
INSERT INTO `sys_area` VALUES (410221, 410200, 3, '杞县', '0', 100);
INSERT INTO `sys_area` VALUES (410222, 410200, 3, '通许县', '0', 100);
INSERT INTO `sys_area` VALUES (410223, 410200, 3, '尉氏县', '0', 100);
INSERT INTO `sys_area` VALUES (410225, 410200, 3, '兰考县', '0', 100);
INSERT INTO `sys_area` VALUES (410300, 410000, 2, '洛阳市', '0', 100);
INSERT INTO `sys_area` VALUES (410301, 410300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (410302, 410300, 3, '老城区', '0', 100);
INSERT INTO `sys_area` VALUES (410303, 410300, 3, '西工区', '0', 100);
INSERT INTO `sys_area` VALUES (410304, 410300, 3, '瀍河回族区', '0', 100);
INSERT INTO `sys_area` VALUES (410305, 410300, 3, '涧西区', '0', 100);
INSERT INTO `sys_area` VALUES (410306, 410300, 3, '吉利区', '0', 100);
INSERT INTO `sys_area` VALUES (410311, 410300, 3, '洛龙区', '0', 100);
INSERT INTO `sys_area` VALUES (410322, 410300, 3, '孟津县', '0', 100);
INSERT INTO `sys_area` VALUES (410323, 410300, 3, '新安县', '0', 100);
INSERT INTO `sys_area` VALUES (410324, 410300, 3, '栾川县', '0', 100);
INSERT INTO `sys_area` VALUES (410325, 410300, 3, '嵩县', '0', 100);
INSERT INTO `sys_area` VALUES (410326, 410300, 3, '汝阳县', '0', 100);
INSERT INTO `sys_area` VALUES (410327, 410300, 3, '宜阳县', '0', 100);
INSERT INTO `sys_area` VALUES (410328, 410300, 3, '洛宁县', '0', 100);
INSERT INTO `sys_area` VALUES (410329, 410300, 3, '伊川县', '0', 100);
INSERT INTO `sys_area` VALUES (410381, 410300, 3, '偃师市', '0', 100);
INSERT INTO `sys_area` VALUES (410400, 410000, 2, '平顶山市', '0', 100);
INSERT INTO `sys_area` VALUES (410401, 410400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (410402, 410400, 3, '新华区', '0', 100);
INSERT INTO `sys_area` VALUES (410403, 410400, 3, '卫东区', '0', 100);
INSERT INTO `sys_area` VALUES (410404, 410400, 3, '石龙区', '0', 100);
INSERT INTO `sys_area` VALUES (410411, 410400, 3, '湛河区', '0', 100);
INSERT INTO `sys_area` VALUES (410421, 410400, 3, '宝丰县', '0', 100);
INSERT INTO `sys_area` VALUES (410422, 410400, 3, '叶县', '0', 100);
INSERT INTO `sys_area` VALUES (410423, 410400, 3, '鲁山县', '0', 100);
INSERT INTO `sys_area` VALUES (410425, 410400, 3, '郏县', '0', 100);
INSERT INTO `sys_area` VALUES (410481, 410400, 3, '舞钢市', '0', 100);
INSERT INTO `sys_area` VALUES (410482, 410400, 3, '汝州市', '0', 100);
INSERT INTO `sys_area` VALUES (410500, 410000, 2, '安阳市', '0', 100);
INSERT INTO `sys_area` VALUES (410501, 410500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (410502, 410500, 3, '文峰区', '0', 100);
INSERT INTO `sys_area` VALUES (410503, 410500, 3, '北关区', '0', 100);
INSERT INTO `sys_area` VALUES (410505, 410500, 3, '殷都区', '0', 100);
INSERT INTO `sys_area` VALUES (410506, 410500, 3, '龙安区', '0', 100);
INSERT INTO `sys_area` VALUES (410522, 410500, 3, '安阳县', '0', 100);
INSERT INTO `sys_area` VALUES (410523, 410500, 3, '汤阴县', '0', 100);
INSERT INTO `sys_area` VALUES (410526, 410500, 3, '滑县', '0', 100);
INSERT INTO `sys_area` VALUES (410527, 410500, 3, '内黄县', '0', 100);
INSERT INTO `sys_area` VALUES (410581, 410500, 3, '林州市', '0', 100);
INSERT INTO `sys_area` VALUES (410600, 410000, 2, '鹤壁市', '0', 100);
INSERT INTO `sys_area` VALUES (410601, 410600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (410602, 410600, 3, '鹤山区', '0', 100);
INSERT INTO `sys_area` VALUES (410603, 410600, 3, '山城区', '0', 100);
INSERT INTO `sys_area` VALUES (410611, 410600, 3, '淇滨区', '0', 100);
INSERT INTO `sys_area` VALUES (410621, 410600, 3, '浚县', '0', 100);
INSERT INTO `sys_area` VALUES (410622, 410600, 3, '淇县', '0', 100);
INSERT INTO `sys_area` VALUES (410700, 410000, 2, '新乡市', '0', 100);
INSERT INTO `sys_area` VALUES (410701, 410700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (410702, 410700, 3, '红旗区', '0', 100);
INSERT INTO `sys_area` VALUES (410703, 410700, 3, '卫滨区', '0', 100);
INSERT INTO `sys_area` VALUES (410704, 410700, 3, '凤泉区', '0', 100);
INSERT INTO `sys_area` VALUES (410711, 410700, 3, '牧野区', '0', 100);
INSERT INTO `sys_area` VALUES (410721, 410700, 3, '新乡县', '0', 100);
INSERT INTO `sys_area` VALUES (410724, 410700, 3, '获嘉县', '0', 100);
INSERT INTO `sys_area` VALUES (410725, 410700, 3, '原阳县', '0', 100);
INSERT INTO `sys_area` VALUES (410726, 410700, 3, '延津县', '0', 100);
INSERT INTO `sys_area` VALUES (410727, 410700, 3, '封丘县', '0', 100);
INSERT INTO `sys_area` VALUES (410728, 410700, 3, '长垣县', '0', 100);
INSERT INTO `sys_area` VALUES (410781, 410700, 3, '卫辉市', '0', 100);
INSERT INTO `sys_area` VALUES (410782, 410700, 3, '辉县市', '0', 100);
INSERT INTO `sys_area` VALUES (410800, 410000, 2, '焦作市', '0', 100);
INSERT INTO `sys_area` VALUES (410801, 410800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (410802, 410800, 3, '解放区', '0', 100);
INSERT INTO `sys_area` VALUES (410803, 410800, 3, '中站区', '0', 100);
INSERT INTO `sys_area` VALUES (410804, 410800, 3, '马村区', '0', 100);
INSERT INTO `sys_area` VALUES (410811, 410800, 3, '山阳区', '0', 100);
INSERT INTO `sys_area` VALUES (410821, 410800, 3, '修武县', '0', 100);
INSERT INTO `sys_area` VALUES (410822, 410800, 3, '博爱县', '0', 100);
INSERT INTO `sys_area` VALUES (410823, 410800, 3, '武陟县', '0', 100);
INSERT INTO `sys_area` VALUES (410825, 410800, 3, '温县', '0', 100);
INSERT INTO `sys_area` VALUES (410882, 410800, 3, '沁阳市', '0', 100);
INSERT INTO `sys_area` VALUES (410883, 410800, 3, '孟州市', '0', 100);
INSERT INTO `sys_area` VALUES (410900, 410000, 2, '濮阳市', '0', 100);
INSERT INTO `sys_area` VALUES (410901, 410900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (410902, 410900, 3, '华龙区', '0', 100);
INSERT INTO `sys_area` VALUES (410922, 410900, 3, '清丰县', '0', 100);
INSERT INTO `sys_area` VALUES (410923, 410900, 3, '南乐县', '0', 100);
INSERT INTO `sys_area` VALUES (410926, 410900, 3, '范县', '0', 100);
INSERT INTO `sys_area` VALUES (410927, 410900, 3, '台前县', '0', 100);
INSERT INTO `sys_area` VALUES (410928, 410900, 3, '濮阳县', '0', 100);
INSERT INTO `sys_area` VALUES (411000, 410000, 2, '许昌市', '0', 100);
INSERT INTO `sys_area` VALUES (411001, 411000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (411002, 411000, 3, '魏都区', '0', 100);
INSERT INTO `sys_area` VALUES (411023, 411000, 3, '许昌县', '0', 100);
INSERT INTO `sys_area` VALUES (411024, 411000, 3, '鄢陵县', '0', 100);
INSERT INTO `sys_area` VALUES (411025, 411000, 3, '襄城县', '0', 100);
INSERT INTO `sys_area` VALUES (411081, 411000, 3, '禹州市', '0', 100);
INSERT INTO `sys_area` VALUES (411082, 411000, 3, '长葛市', '0', 100);
INSERT INTO `sys_area` VALUES (411100, 410000, 2, '漯河市', '0', 100);
INSERT INTO `sys_area` VALUES (411101, 411100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (411102, 411100, 3, '源汇区', '0', 100);
INSERT INTO `sys_area` VALUES (411103, 411100, 3, '郾城区', '0', 100);
INSERT INTO `sys_area` VALUES (411104, 411100, 3, '召陵区', '0', 100);
INSERT INTO `sys_area` VALUES (411121, 411100, 3, '舞阳县', '0', 100);
INSERT INTO `sys_area` VALUES (411122, 411100, 3, '临颍县', '0', 100);
INSERT INTO `sys_area` VALUES (411200, 410000, 2, '三门峡市', '0', 100);
INSERT INTO `sys_area` VALUES (411201, 411200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (411202, 411200, 3, '湖滨区', '0', 100);
INSERT INTO `sys_area` VALUES (411203, 411200, 3, '陕州区', '0', 100);
INSERT INTO `sys_area` VALUES (411221, 411200, 3, '渑池县', '0', 100);
INSERT INTO `sys_area` VALUES (411224, 411200, 3, '卢氏县', '0', 100);
INSERT INTO `sys_area` VALUES (411281, 411200, 3, '义马市', '0', 100);
INSERT INTO `sys_area` VALUES (411282, 411200, 3, '灵宝市', '0', 100);
INSERT INTO `sys_area` VALUES (411300, 410000, 2, '南阳市', '0', 100);
INSERT INTO `sys_area` VALUES (411301, 411300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (411302, 411300, 3, '宛城区', '0', 100);
INSERT INTO `sys_area` VALUES (411303, 411300, 3, '卧龙区', '0', 100);
INSERT INTO `sys_area` VALUES (411321, 411300, 3, '南召县', '0', 100);
INSERT INTO `sys_area` VALUES (411322, 411300, 3, '方城县', '0', 100);
INSERT INTO `sys_area` VALUES (411323, 411300, 3, '西峡县', '0', 100);
INSERT INTO `sys_area` VALUES (411324, 411300, 3, '镇平县', '0', 100);
INSERT INTO `sys_area` VALUES (411325, 411300, 3, '内乡县', '0', 100);
INSERT INTO `sys_area` VALUES (411326, 411300, 3, '淅川县', '0', 100);
INSERT INTO `sys_area` VALUES (411327, 411300, 3, '社旗县', '0', 100);
INSERT INTO `sys_area` VALUES (411328, 411300, 3, '唐河县', '0', 100);
INSERT INTO `sys_area` VALUES (411329, 411300, 3, '新野县', '0', 100);
INSERT INTO `sys_area` VALUES (411330, 411300, 3, '桐柏县', '0', 100);
INSERT INTO `sys_area` VALUES (411381, 411300, 3, '邓州市', '0', 100);
INSERT INTO `sys_area` VALUES (411400, 410000, 2, '商丘市', '0', 100);
INSERT INTO `sys_area` VALUES (411401, 411400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (411402, 411400, 3, '梁园区', '0', 100);
INSERT INTO `sys_area` VALUES (411403, 411400, 3, '睢阳区', '0', 100);
INSERT INTO `sys_area` VALUES (411421, 411400, 3, '民权县', '0', 100);
INSERT INTO `sys_area` VALUES (411422, 411400, 3, '睢县', '0', 100);
INSERT INTO `sys_area` VALUES (411423, 411400, 3, '宁陵县', '0', 100);
INSERT INTO `sys_area` VALUES (411424, 411400, 3, '柘城县', '0', 100);
INSERT INTO `sys_area` VALUES (411425, 411400, 3, '虞城县', '0', 100);
INSERT INTO `sys_area` VALUES (411426, 411400, 3, '夏邑县', '0', 100);
INSERT INTO `sys_area` VALUES (411481, 411400, 3, '永城市', '0', 100);
INSERT INTO `sys_area` VALUES (411500, 410000, 2, '信阳市', '0', 100);
INSERT INTO `sys_area` VALUES (411501, 411500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (411502, 411500, 3, '浉河区', '0', 100);
INSERT INTO `sys_area` VALUES (411503, 411500, 3, '平桥区', '0', 100);
INSERT INTO `sys_area` VALUES (411521, 411500, 3, '罗山县', '0', 100);
INSERT INTO `sys_area` VALUES (411522, 411500, 3, '光山县', '0', 100);
INSERT INTO `sys_area` VALUES (411523, 411500, 3, '新县', '0', 100);
INSERT INTO `sys_area` VALUES (411524, 411500, 3, '商城县', '0', 100);
INSERT INTO `sys_area` VALUES (411525, 411500, 3, '固始县', '0', 100);
INSERT INTO `sys_area` VALUES (411526, 411500, 3, '潢川县', '0', 100);
INSERT INTO `sys_area` VALUES (411527, 411500, 3, '淮滨县', '0', 100);
INSERT INTO `sys_area` VALUES (411528, 411500, 3, '息县', '0', 100);
INSERT INTO `sys_area` VALUES (411600, 410000, 2, '周口市', '0', 100);
INSERT INTO `sys_area` VALUES (411601, 411600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (411602, 411600, 3, '川汇区', '0', 100);
INSERT INTO `sys_area` VALUES (411621, 411600, 3, '扶沟县', '0', 100);
INSERT INTO `sys_area` VALUES (411622, 411600, 3, '西华县', '0', 100);
INSERT INTO `sys_area` VALUES (411623, 411600, 3, '商水县', '0', 100);
INSERT INTO `sys_area` VALUES (411624, 411600, 3, '沈丘县', '0', 100);
INSERT INTO `sys_area` VALUES (411625, 411600, 3, '郸城县', '0', 100);
INSERT INTO `sys_area` VALUES (411626, 411600, 3, '淮阳县', '0', 100);
INSERT INTO `sys_area` VALUES (411627, 411600, 3, '太康县', '0', 100);
INSERT INTO `sys_area` VALUES (411628, 411600, 3, '鹿邑县', '0', 100);
INSERT INTO `sys_area` VALUES (411681, 411600, 3, '项城市', '0', 100);
INSERT INTO `sys_area` VALUES (411700, 410000, 2, '驻马店市', '0', 100);
INSERT INTO `sys_area` VALUES (411701, 411700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (411702, 411700, 3, '驿城区', '0', 100);
INSERT INTO `sys_area` VALUES (411721, 411700, 3, '西平县', '0', 100);
INSERT INTO `sys_area` VALUES (411722, 411700, 3, '上蔡县', '0', 100);
INSERT INTO `sys_area` VALUES (411723, 411700, 3, '平舆县', '0', 100);
INSERT INTO `sys_area` VALUES (411724, 411700, 3, '正阳县', '0', 100);
INSERT INTO `sys_area` VALUES (411725, 411700, 3, '确山县', '0', 100);
INSERT INTO `sys_area` VALUES (411726, 411700, 3, '泌阳县', '0', 100);
INSERT INTO `sys_area` VALUES (411727, 411700, 3, '汝南县', '0', 100);
INSERT INTO `sys_area` VALUES (411728, 411700, 3, '遂平县', '0', 100);
INSERT INTO `sys_area` VALUES (411729, 411700, 3, '新蔡县', '0', 100);
INSERT INTO `sys_area` VALUES (419000, 410000, 2, '省直辖县级行政区划', '0', 100);
INSERT INTO `sys_area` VALUES (419001, 419000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (420000, 0, 1, '湖北省', '0', 100);
INSERT INTO `sys_area` VALUES (420100, 420000, 2, '武汉市', '0', 100);
INSERT INTO `sys_area` VALUES (420101, 420100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (420102, 420100, 3, '江岸区', '0', 100);
INSERT INTO `sys_area` VALUES (420103, 420100, 3, '江汉区', '0', 100);
INSERT INTO `sys_area` VALUES (420104, 420100, 3, '硚口区', '0', 100);
INSERT INTO `sys_area` VALUES (420105, 420100, 3, '汉阳区', '0', 100);
INSERT INTO `sys_area` VALUES (420106, 420100, 3, '武昌区', '0', 100);
INSERT INTO `sys_area` VALUES (420107, 420100, 3, '青山区', '0', 100);
INSERT INTO `sys_area` VALUES (420111, 420100, 3, '洪山区', '0', 100);
INSERT INTO `sys_area` VALUES (420112, 420100, 3, '东西湖区', '0', 100);
INSERT INTO `sys_area` VALUES (420113, 420100, 3, '汉南区', '0', 100);
INSERT INTO `sys_area` VALUES (420114, 420100, 3, '蔡甸区', '0', 100);
INSERT INTO `sys_area` VALUES (420115, 420100, 3, '江夏区', '0', 100);
INSERT INTO `sys_area` VALUES (420116, 420100, 3, '黄陂区', '0', 100);
INSERT INTO `sys_area` VALUES (420117, 420100, 3, '新洲区', '0', 100);
INSERT INTO `sys_area` VALUES (420200, 420000, 2, '黄石市', '0', 100);
INSERT INTO `sys_area` VALUES (420201, 420200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (420202, 420200, 3, '黄石港区', '0', 100);
INSERT INTO `sys_area` VALUES (420203, 420200, 3, '西塞山区', '0', 100);
INSERT INTO `sys_area` VALUES (420204, 420200, 3, '下陆区', '0', 100);
INSERT INTO `sys_area` VALUES (420205, 420200, 3, '铁山区', '0', 100);
INSERT INTO `sys_area` VALUES (420222, 420200, 3, '阳新县', '0', 100);
INSERT INTO `sys_area` VALUES (420281, 420200, 3, '大冶市', '0', 100);
INSERT INTO `sys_area` VALUES (420300, 420000, 2, '十堰市', '0', 100);
INSERT INTO `sys_area` VALUES (420301, 420300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (420302, 420300, 3, '茅箭区', '0', 100);
INSERT INTO `sys_area` VALUES (420303, 420300, 3, '张湾区', '0', 100);
INSERT INTO `sys_area` VALUES (420304, 420300, 3, '郧阳区', '0', 100);
INSERT INTO `sys_area` VALUES (420322, 420300, 3, '郧西县', '0', 100);
INSERT INTO `sys_area` VALUES (420323, 420300, 3, '竹山县', '0', 100);
INSERT INTO `sys_area` VALUES (420324, 420300, 3, '竹溪县', '0', 100);
INSERT INTO `sys_area` VALUES (420325, 420300, 3, '房县', '0', 100);
INSERT INTO `sys_area` VALUES (420381, 420300, 3, '丹江口市', '0', 100);
INSERT INTO `sys_area` VALUES (420500, 420000, 2, '宜昌市', '0', 100);
INSERT INTO `sys_area` VALUES (420501, 420500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (420502, 420500, 3, '西陵区', '0', 100);
INSERT INTO `sys_area` VALUES (420503, 420500, 3, '伍家岗区', '0', 100);
INSERT INTO `sys_area` VALUES (420504, 420500, 3, '点军区', '0', 100);
INSERT INTO `sys_area` VALUES (420505, 420500, 3, '猇亭区', '0', 100);
INSERT INTO `sys_area` VALUES (420506, 420500, 3, '夷陵区', '0', 100);
INSERT INTO `sys_area` VALUES (420525, 420500, 3, '远安县', '0', 100);
INSERT INTO `sys_area` VALUES (420526, 420500, 3, '兴山县', '0', 100);
INSERT INTO `sys_area` VALUES (420527, 420500, 3, '秭归县', '0', 100);
INSERT INTO `sys_area` VALUES (420528, 420500, 3, '长阳土家族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (420529, 420500, 3, '五峰土家族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (420581, 420500, 3, '宜都市', '0', 100);
INSERT INTO `sys_area` VALUES (420582, 420500, 3, '当阳市', '0', 100);
INSERT INTO `sys_area` VALUES (420583, 420500, 3, '枝江市', '0', 100);
INSERT INTO `sys_area` VALUES (420600, 420000, 2, '襄阳市', '0', 100);
INSERT INTO `sys_area` VALUES (420601, 420600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (420602, 420600, 3, '襄城区', '0', 100);
INSERT INTO `sys_area` VALUES (420606, 420600, 3, '樊城区', '0', 100);
INSERT INTO `sys_area` VALUES (420607, 420600, 3, '襄州区', '0', 100);
INSERT INTO `sys_area` VALUES (420624, 420600, 3, '南漳县', '0', 100);
INSERT INTO `sys_area` VALUES (420625, 420600, 3, '谷城县', '0', 100);
INSERT INTO `sys_area` VALUES (420626, 420600, 3, '保康县', '0', 100);
INSERT INTO `sys_area` VALUES (420682, 420600, 3, '老河口市', '0', 100);
INSERT INTO `sys_area` VALUES (420683, 420600, 3, '枣阳市', '0', 100);
INSERT INTO `sys_area` VALUES (420684, 420600, 3, '宜城市', '0', 100);
INSERT INTO `sys_area` VALUES (420700, 420000, 2, '鄂州市', '0', 100);
INSERT INTO `sys_area` VALUES (420701, 420700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (420702, 420700, 3, '梁子湖区', '0', 100);
INSERT INTO `sys_area` VALUES (420703, 420700, 3, '华容区', '0', 100);
INSERT INTO `sys_area` VALUES (420704, 420700, 3, '鄂城区', '0', 100);
INSERT INTO `sys_area` VALUES (420800, 420000, 2, '荆门市', '0', 100);
INSERT INTO `sys_area` VALUES (420801, 420800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (420802, 420800, 3, '东宝区', '0', 100);
INSERT INTO `sys_area` VALUES (420804, 420800, 3, '掇刀区', '0', 100);
INSERT INTO `sys_area` VALUES (420821, 420800, 3, '京山县', '0', 100);
INSERT INTO `sys_area` VALUES (420822, 420800, 3, '沙洋县', '0', 100);
INSERT INTO `sys_area` VALUES (420881, 420800, 3, '钟祥市', '0', 100);
INSERT INTO `sys_area` VALUES (420900, 420000, 2, '孝感市', '0', 100);
INSERT INTO `sys_area` VALUES (420901, 420900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (420902, 420900, 3, '孝南区', '0', 100);
INSERT INTO `sys_area` VALUES (420921, 420900, 3, '孝昌县', '0', 100);
INSERT INTO `sys_area` VALUES (420922, 420900, 3, '大悟县', '0', 100);
INSERT INTO `sys_area` VALUES (420923, 420900, 3, '云梦县', '0', 100);
INSERT INTO `sys_area` VALUES (420981, 420900, 3, '应城市', '0', 100);
INSERT INTO `sys_area` VALUES (420982, 420900, 3, '安陆市', '0', 100);
INSERT INTO `sys_area` VALUES (420984, 420900, 3, '汉川市', '0', 100);
INSERT INTO `sys_area` VALUES (421000, 420000, 2, '荆州市', '0', 100);
INSERT INTO `sys_area` VALUES (421001, 421000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (421002, 421000, 3, '沙市区', '0', 100);
INSERT INTO `sys_area` VALUES (421003, 421000, 3, '荆州区', '0', 100);
INSERT INTO `sys_area` VALUES (421022, 421000, 3, '公安县', '0', 100);
INSERT INTO `sys_area` VALUES (421023, 421000, 3, '监利县', '0', 100);
INSERT INTO `sys_area` VALUES (421024, 421000, 3, '江陵县', '0', 100);
INSERT INTO `sys_area` VALUES (421081, 421000, 3, '石首市', '0', 100);
INSERT INTO `sys_area` VALUES (421083, 421000, 3, '洪湖市', '0', 100);
INSERT INTO `sys_area` VALUES (421087, 421000, 3, '松滋市', '0', 100);
INSERT INTO `sys_area` VALUES (421100, 420000, 2, '黄冈市', '0', 100);
INSERT INTO `sys_area` VALUES (421101, 421100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (421102, 421100, 3, '黄州区', '0', 100);
INSERT INTO `sys_area` VALUES (421121, 421100, 3, '团风县', '0', 100);
INSERT INTO `sys_area` VALUES (421122, 421100, 3, '红安县', '0', 100);
INSERT INTO `sys_area` VALUES (421123, 421100, 3, '罗田县', '0', 100);
INSERT INTO `sys_area` VALUES (421124, 421100, 3, '英山县', '0', 100);
INSERT INTO `sys_area` VALUES (421125, 421100, 3, '浠水县', '0', 100);
INSERT INTO `sys_area` VALUES (421126, 421100, 3, '蕲春县', '0', 100);
INSERT INTO `sys_area` VALUES (421127, 421100, 3, '黄梅县', '0', 100);
INSERT INTO `sys_area` VALUES (421181, 421100, 3, '麻城市', '0', 100);
INSERT INTO `sys_area` VALUES (421182, 421100, 3, '武穴市', '0', 100);
INSERT INTO `sys_area` VALUES (421200, 420000, 2, '咸宁市', '0', 100);
INSERT INTO `sys_area` VALUES (421201, 421200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (421202, 421200, 3, '咸安区', '0', 100);
INSERT INTO `sys_area` VALUES (421221, 421200, 3, '嘉鱼县', '0', 100);
INSERT INTO `sys_area` VALUES (421222, 421200, 3, '通城县', '0', 100);
INSERT INTO `sys_area` VALUES (421223, 421200, 3, '崇阳县', '0', 100);
INSERT INTO `sys_area` VALUES (421224, 421200, 3, '通山县', '0', 100);
INSERT INTO `sys_area` VALUES (421281, 421200, 3, '赤壁市', '0', 100);
INSERT INTO `sys_area` VALUES (421300, 420000, 2, '随州市', '0', 100);
INSERT INTO `sys_area` VALUES (421301, 421300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (421303, 421300, 3, '曾都区', '0', 100);
INSERT INTO `sys_area` VALUES (421321, 421300, 3, '随县', '0', 100);
INSERT INTO `sys_area` VALUES (421381, 421300, 3, '广水市', '0', 100);
INSERT INTO `sys_area` VALUES (422800, 420000, 2, '恩施土家族苗族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (422801, 422800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (422802, 422800, 3, '利川市', '0', 100);
INSERT INTO `sys_area` VALUES (422822, 422800, 3, '建始县', '0', 100);
INSERT INTO `sys_area` VALUES (422823, 422800, 3, '巴东县', '0', 100);
INSERT INTO `sys_area` VALUES (422825, 422800, 3, '宣恩县', '0', 100);
INSERT INTO `sys_area` VALUES (422826, 422800, 3, '咸丰县', '0', 100);
INSERT INTO `sys_area` VALUES (422827, 422800, 3, '来凤县', '0', 100);
INSERT INTO `sys_area` VALUES (422828, 422800, 3, '鹤峰县', '0', 100);
INSERT INTO `sys_area` VALUES (429000, 420000, 2, '省直辖县级行政区划', '0', 100);
INSERT INTO `sys_area` VALUES (429004, 429000, 3, '仙桃市', '0', 100);
INSERT INTO `sys_area` VALUES (429005, 429000, 3, '潜江市', '0', 100);
INSERT INTO `sys_area` VALUES (429006, 429000, 3, '天门市', '0', 100);
INSERT INTO `sys_area` VALUES (429021, 429000, 3, '神农架林区', '0', 100);
INSERT INTO `sys_area` VALUES (430000, 0, 1, '湖南省', '0', 100);
INSERT INTO `sys_area` VALUES (430100, 430000, 2, '长沙市', '0', 100);
INSERT INTO `sys_area` VALUES (430101, 430100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (430102, 430100, 3, '芙蓉区', '0', 100);
INSERT INTO `sys_area` VALUES (430103, 430100, 3, '天心区', '0', 100);
INSERT INTO `sys_area` VALUES (430104, 430100, 3, '岳麓区', '0', 100);
INSERT INTO `sys_area` VALUES (430105, 430100, 3, '开福区', '0', 100);
INSERT INTO `sys_area` VALUES (430111, 430100, 3, '雨花区', '0', 100);
INSERT INTO `sys_area` VALUES (430112, 430100, 3, '望城区', '0', 100);
INSERT INTO `sys_area` VALUES (430121, 430100, 3, '长沙县', '0', 100);
INSERT INTO `sys_area` VALUES (430124, 430100, 3, '宁乡县', '0', 100);
INSERT INTO `sys_area` VALUES (430181, 430100, 3, '浏阳市', '0', 100);
INSERT INTO `sys_area` VALUES (430200, 430000, 2, '株洲市', '0', 100);
INSERT INTO `sys_area` VALUES (430201, 430200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (430202, 430200, 3, '荷塘区', '0', 100);
INSERT INTO `sys_area` VALUES (430203, 430200, 3, '芦淞区', '0', 100);
INSERT INTO `sys_area` VALUES (430204, 430200, 3, '石峰区', '0', 100);
INSERT INTO `sys_area` VALUES (430211, 430200, 3, '天元区', '0', 100);
INSERT INTO `sys_area` VALUES (430221, 430200, 3, '株洲县', '0', 100);
INSERT INTO `sys_area` VALUES (430223, 430200, 3, '攸县', '0', 100);
INSERT INTO `sys_area` VALUES (430224, 430200, 3, '茶陵县', '0', 100);
INSERT INTO `sys_area` VALUES (430225, 430200, 3, '炎陵县', '0', 100);
INSERT INTO `sys_area` VALUES (430281, 430200, 3, '醴陵市', '0', 100);
INSERT INTO `sys_area` VALUES (430300, 430000, 2, '湘潭市', '0', 100);
INSERT INTO `sys_area` VALUES (430301, 430300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (430302, 430300, 3, '雨湖区', '0', 100);
INSERT INTO `sys_area` VALUES (430304, 430300, 3, '岳塘区', '0', 100);
INSERT INTO `sys_area` VALUES (430321, 430300, 3, '湘潭县', '0', 100);
INSERT INTO `sys_area` VALUES (430381, 430300, 3, '湘乡市', '0', 100);
INSERT INTO `sys_area` VALUES (430382, 430300, 3, '韶山市', '0', 100);
INSERT INTO `sys_area` VALUES (430400, 430000, 2, '衡阳市', '0', 100);
INSERT INTO `sys_area` VALUES (430401, 430400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (430405, 430400, 3, '珠晖区', '0', 100);
INSERT INTO `sys_area` VALUES (430406, 430400, 3, '雁峰区', '0', 100);
INSERT INTO `sys_area` VALUES (430407, 430400, 3, '石鼓区', '0', 100);
INSERT INTO `sys_area` VALUES (430408, 430400, 3, '蒸湘区', '0', 100);
INSERT INTO `sys_area` VALUES (430412, 430400, 3, '南岳区', '0', 100);
INSERT INTO `sys_area` VALUES (430421, 430400, 3, '衡阳县', '0', 100);
INSERT INTO `sys_area` VALUES (430422, 430400, 3, '衡南县', '0', 100);
INSERT INTO `sys_area` VALUES (430423, 430400, 3, '衡山县', '0', 100);
INSERT INTO `sys_area` VALUES (430424, 430400, 3, '衡东县', '0', 100);
INSERT INTO `sys_area` VALUES (430426, 430400, 3, '祁东县', '0', 100);
INSERT INTO `sys_area` VALUES (430481, 430400, 3, '耒阳市', '0', 100);
INSERT INTO `sys_area` VALUES (430482, 430400, 3, '常宁市', '0', 100);
INSERT INTO `sys_area` VALUES (430500, 430000, 2, '邵阳市', '0', 100);
INSERT INTO `sys_area` VALUES (430501, 430500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (430502, 430500, 3, '双清区', '0', 100);
INSERT INTO `sys_area` VALUES (430503, 430500, 3, '大祥区', '0', 100);
INSERT INTO `sys_area` VALUES (430511, 430500, 3, '北塔区', '0', 100);
INSERT INTO `sys_area` VALUES (430521, 430500, 3, '邵东县', '0', 100);
INSERT INTO `sys_area` VALUES (430522, 430500, 3, '新邵县', '0', 100);
INSERT INTO `sys_area` VALUES (430523, 430500, 3, '邵阳县', '0', 100);
INSERT INTO `sys_area` VALUES (430524, 430500, 3, '隆回县', '0', 100);
INSERT INTO `sys_area` VALUES (430525, 430500, 3, '洞口县', '0', 100);
INSERT INTO `sys_area` VALUES (430527, 430500, 3, '绥宁县', '0', 100);
INSERT INTO `sys_area` VALUES (430528, 430500, 3, '新宁县', '0', 100);
INSERT INTO `sys_area` VALUES (430529, 430500, 3, '城步苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (430581, 430500, 3, '武冈市', '0', 100);
INSERT INTO `sys_area` VALUES (430600, 430000, 2, '岳阳市', '0', 100);
INSERT INTO `sys_area` VALUES (430601, 430600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (430602, 430600, 3, '岳阳楼区', '0', 100);
INSERT INTO `sys_area` VALUES (430603, 430600, 3, '云溪区', '0', 100);
INSERT INTO `sys_area` VALUES (430611, 430600, 3, '君山区', '0', 100);
INSERT INTO `sys_area` VALUES (430621, 430600, 3, '岳阳县', '0', 100);
INSERT INTO `sys_area` VALUES (430623, 430600, 3, '华容县', '0', 100);
INSERT INTO `sys_area` VALUES (430624, 430600, 3, '湘阴县', '0', 100);
INSERT INTO `sys_area` VALUES (430626, 430600, 3, '平江县', '0', 100);
INSERT INTO `sys_area` VALUES (430681, 430600, 3, '汨罗市', '0', 100);
INSERT INTO `sys_area` VALUES (430682, 430600, 3, '临湘市', '0', 100);
INSERT INTO `sys_area` VALUES (430700, 430000, 2, '常德市', '0', 100);
INSERT INTO `sys_area` VALUES (430701, 430700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (430702, 430700, 3, '武陵区', '0', 100);
INSERT INTO `sys_area` VALUES (430703, 430700, 3, '鼎城区', '0', 100);
INSERT INTO `sys_area` VALUES (430721, 430700, 3, '安乡县', '0', 100);
INSERT INTO `sys_area` VALUES (430722, 430700, 3, '汉寿县', '0', 100);
INSERT INTO `sys_area` VALUES (430723, 430700, 3, '澧县', '0', 100);
INSERT INTO `sys_area` VALUES (430724, 430700, 3, '临澧县', '0', 100);
INSERT INTO `sys_area` VALUES (430725, 430700, 3, '桃源县', '0', 100);
INSERT INTO `sys_area` VALUES (430726, 430700, 3, '石门县', '0', 100);
INSERT INTO `sys_area` VALUES (430781, 430700, 3, '津市市', '0', 100);
INSERT INTO `sys_area` VALUES (430800, 430000, 2, '张家界市', '0', 100);
INSERT INTO `sys_area` VALUES (430801, 430800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (430802, 430800, 3, '永定区', '0', 100);
INSERT INTO `sys_area` VALUES (430811, 430800, 3, '武陵源区', '0', 100);
INSERT INTO `sys_area` VALUES (430821, 430800, 3, '慈利县', '0', 100);
INSERT INTO `sys_area` VALUES (430822, 430800, 3, '桑植县', '0', 100);
INSERT INTO `sys_area` VALUES (430900, 430000, 2, '益阳市', '0', 100);
INSERT INTO `sys_area` VALUES (430901, 430900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (430902, 430900, 3, '资阳区', '0', 100);
INSERT INTO `sys_area` VALUES (430903, 430900, 3, '赫山区', '0', 100);
INSERT INTO `sys_area` VALUES (430921, 430900, 3, '南县', '0', 100);
INSERT INTO `sys_area` VALUES (430922, 430900, 3, '桃江县', '0', 100);
INSERT INTO `sys_area` VALUES (430923, 430900, 3, '安化县', '0', 100);
INSERT INTO `sys_area` VALUES (430981, 430900, 3, '沅江市', '0', 100);
INSERT INTO `sys_area` VALUES (431000, 430000, 2, '郴州市', '0', 100);
INSERT INTO `sys_area` VALUES (431001, 431000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (431002, 431000, 3, '北湖区', '0', 100);
INSERT INTO `sys_area` VALUES (431003, 431000, 3, '苏仙区', '0', 100);
INSERT INTO `sys_area` VALUES (431021, 431000, 3, '桂阳县', '0', 100);
INSERT INTO `sys_area` VALUES (431022, 431000, 3, '宜章县', '0', 100);
INSERT INTO `sys_area` VALUES (431023, 431000, 3, '永兴县', '0', 100);
INSERT INTO `sys_area` VALUES (431024, 431000, 3, '嘉禾县', '0', 100);
INSERT INTO `sys_area` VALUES (431025, 431000, 3, '临武县', '0', 100);
INSERT INTO `sys_area` VALUES (431026, 431000, 3, '汝城县', '0', 100);
INSERT INTO `sys_area` VALUES (431027, 431000, 3, '桂东县', '0', 100);
INSERT INTO `sys_area` VALUES (431028, 431000, 3, '安仁县', '0', 100);
INSERT INTO `sys_area` VALUES (431081, 431000, 3, '资兴市', '0', 100);
INSERT INTO `sys_area` VALUES (431100, 430000, 2, '永州市', '0', 100);
INSERT INTO `sys_area` VALUES (431101, 431100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (431102, 431100, 3, '零陵区', '0', 100);
INSERT INTO `sys_area` VALUES (431103, 431100, 3, '冷水滩区', '0', 100);
INSERT INTO `sys_area` VALUES (431121, 431100, 3, '祁阳县', '0', 100);
INSERT INTO `sys_area` VALUES (431122, 431100, 3, '东安县', '0', 100);
INSERT INTO `sys_area` VALUES (431123, 431100, 3, '双牌县', '0', 100);
INSERT INTO `sys_area` VALUES (431124, 431100, 3, '道县', '0', 100);
INSERT INTO `sys_area` VALUES (431125, 431100, 3, '江永县', '0', 100);
INSERT INTO `sys_area` VALUES (431126, 431100, 3, '宁远县', '0', 100);
INSERT INTO `sys_area` VALUES (431127, 431100, 3, '蓝山县', '0', 100);
INSERT INTO `sys_area` VALUES (431128, 431100, 3, '新田县', '0', 100);
INSERT INTO `sys_area` VALUES (431129, 431100, 3, '江华瑶族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (431200, 430000, 2, '怀化市', '0', 100);
INSERT INTO `sys_area` VALUES (431201, 431200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (431202, 431200, 3, '鹤城区', '0', 100);
INSERT INTO `sys_area` VALUES (431221, 431200, 3, '中方县', '0', 100);
INSERT INTO `sys_area` VALUES (431222, 431200, 3, '沅陵县', '0', 100);
INSERT INTO `sys_area` VALUES (431223, 431200, 3, '辰溪县', '0', 100);
INSERT INTO `sys_area` VALUES (431224, 431200, 3, '溆浦县', '0', 100);
INSERT INTO `sys_area` VALUES (431225, 431200, 3, '会同县', '0', 100);
INSERT INTO `sys_area` VALUES (431226, 431200, 3, '麻阳苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (431227, 431200, 3, '新晃侗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (431228, 431200, 3, '芷江侗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (431229, 431200, 3, '靖州苗族侗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (431230, 431200, 3, '通道侗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (431281, 431200, 3, '洪江市', '0', 100);
INSERT INTO `sys_area` VALUES (431300, 430000, 2, '娄底市', '0', 100);
INSERT INTO `sys_area` VALUES (431301, 431300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (431302, 431300, 3, '娄星区', '0', 100);
INSERT INTO `sys_area` VALUES (431321, 431300, 3, '双峰县', '0', 100);
INSERT INTO `sys_area` VALUES (431322, 431300, 3, '新化县', '0', 100);
INSERT INTO `sys_area` VALUES (431381, 431300, 3, '冷水江市', '0', 100);
INSERT INTO `sys_area` VALUES (431382, 431300, 3, '涟源市', '0', 100);
INSERT INTO `sys_area` VALUES (433100, 430000, 2, '湘西土家族苗族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (433101, 433100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (433122, 433100, 3, '泸溪县', '0', 100);
INSERT INTO `sys_area` VALUES (433123, 433100, 3, '凤凰县', '0', 100);
INSERT INTO `sys_area` VALUES (433124, 433100, 3, '花垣县', '0', 100);
INSERT INTO `sys_area` VALUES (433125, 433100, 3, '保靖县', '0', 100);
INSERT INTO `sys_area` VALUES (433126, 433100, 3, '古丈县', '0', 100);
INSERT INTO `sys_area` VALUES (433127, 433100, 3, '永顺县', '0', 100);
INSERT INTO `sys_area` VALUES (433130, 433100, 3, '龙山县', '0', 100);
INSERT INTO `sys_area` VALUES (440000, 0, 1, '广东省', '0', 100);
INSERT INTO `sys_area` VALUES (440100, 440000, 2, '广州市', '0', 100);
INSERT INTO `sys_area` VALUES (440101, 440100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (440103, 440100, 3, '荔湾区', '0', 100);
INSERT INTO `sys_area` VALUES (440104, 440100, 3, '越秀区', '0', 100);
INSERT INTO `sys_area` VALUES (440105, 440100, 3, '海珠区', '0', 100);
INSERT INTO `sys_area` VALUES (440106, 440100, 3, '天河区', '0', 100);
INSERT INTO `sys_area` VALUES (440111, 440100, 3, '白云区', '0', 100);
INSERT INTO `sys_area` VALUES (440112, 440100, 3, '黄埔区', '0', 100);
INSERT INTO `sys_area` VALUES (440113, 440100, 3, '番禺区', '0', 100);
INSERT INTO `sys_area` VALUES (440114, 440100, 3, '花都区', '0', 100);
INSERT INTO `sys_area` VALUES (440115, 440100, 3, '南沙区', '0', 100);
INSERT INTO `sys_area` VALUES (440117, 440100, 3, '从化区', '0', 100);
INSERT INTO `sys_area` VALUES (440118, 440100, 3, '增城区', '0', 100);
INSERT INTO `sys_area` VALUES (440200, 440000, 2, '韶关市', '0', 100);
INSERT INTO `sys_area` VALUES (440201, 440200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (440203, 440200, 3, '武江区', '0', 100);
INSERT INTO `sys_area` VALUES (440204, 440200, 3, '浈江区', '0', 100);
INSERT INTO `sys_area` VALUES (440205, 440200, 3, '曲江区', '0', 100);
INSERT INTO `sys_area` VALUES (440222, 440200, 3, '始兴县', '0', 100);
INSERT INTO `sys_area` VALUES (440224, 440200, 3, '仁化县', '0', 100);
INSERT INTO `sys_area` VALUES (440229, 440200, 3, '翁源县', '0', 100);
INSERT INTO `sys_area` VALUES (440232, 440200, 3, '乳源瑶族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (440233, 440200, 3, '新丰县', '0', 100);
INSERT INTO `sys_area` VALUES (440281, 440200, 3, '乐昌市', '0', 100);
INSERT INTO `sys_area` VALUES (440282, 440200, 3, '南雄市', '0', 100);
INSERT INTO `sys_area` VALUES (440300, 440000, 2, '深圳市', '0', 100);
INSERT INTO `sys_area` VALUES (440301, 440300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (440303, 440300, 3, '罗湖区', '0', 100);
INSERT INTO `sys_area` VALUES (440304, 440300, 3, '福田区', '0', 100);
INSERT INTO `sys_area` VALUES (440305, 440300, 3, '南山区', '0', 100);
INSERT INTO `sys_area` VALUES (440306, 440300, 3, '宝安区', '0', 100);
INSERT INTO `sys_area` VALUES (440307, 440300, 3, '龙岗区', '0', 100);
INSERT INTO `sys_area` VALUES (440308, 440300, 3, '盐田区', '0', 100);
INSERT INTO `sys_area` VALUES (440400, 440000, 2, '珠海市', '0', 100);
INSERT INTO `sys_area` VALUES (440401, 440400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (440402, 440400, 3, '香洲区', '0', 100);
INSERT INTO `sys_area` VALUES (440403, 440400, 3, '斗门区', '0', 100);
INSERT INTO `sys_area` VALUES (440404, 440400, 3, '金湾区', '0', 100);
INSERT INTO `sys_area` VALUES (440500, 440000, 2, '汕头市', '0', 100);
INSERT INTO `sys_area` VALUES (440501, 440500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (440507, 440500, 3, '龙湖区', '0', 100);
INSERT INTO `sys_area` VALUES (440511, 440500, 3, '金平区', '0', 100);
INSERT INTO `sys_area` VALUES (440512, 440500, 3, '濠江区', '0', 100);
INSERT INTO `sys_area` VALUES (440513, 440500, 3, '潮阳区', '0', 100);
INSERT INTO `sys_area` VALUES (440514, 440500, 3, '潮南区', '0', 100);
INSERT INTO `sys_area` VALUES (440515, 440500, 3, '澄海区', '0', 100);
INSERT INTO `sys_area` VALUES (440523, 440500, 3, '南澳县', '0', 100);
INSERT INTO `sys_area` VALUES (440600, 440000, 2, '佛山市', '0', 100);
INSERT INTO `sys_area` VALUES (440601, 440600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (440604, 440600, 3, '禅城区', '0', 100);
INSERT INTO `sys_area` VALUES (440605, 440600, 3, '南海区', '0', 100);
INSERT INTO `sys_area` VALUES (440606, 440600, 3, '顺德区', '0', 100);
INSERT INTO `sys_area` VALUES (440607, 440600, 3, '三水区', '0', 100);
INSERT INTO `sys_area` VALUES (440608, 440600, 3, '高明区', '0', 100);
INSERT INTO `sys_area` VALUES (440700, 440000, 2, '江门市', '0', 100);
INSERT INTO `sys_area` VALUES (440701, 440700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (440703, 440700, 3, '蓬江区', '0', 100);
INSERT INTO `sys_area` VALUES (440704, 440700, 3, '江海区', '0', 100);
INSERT INTO `sys_area` VALUES (440705, 440700, 3, '新会区', '0', 100);
INSERT INTO `sys_area` VALUES (440781, 440700, 3, '台山市', '0', 100);
INSERT INTO `sys_area` VALUES (440783, 440700, 3, '开平市', '0', 100);
INSERT INTO `sys_area` VALUES (440784, 440700, 3, '鹤山市', '0', 100);
INSERT INTO `sys_area` VALUES (440785, 440700, 3, '恩平市', '0', 100);
INSERT INTO `sys_area` VALUES (440800, 440000, 2, '湛江市', '0', 100);
INSERT INTO `sys_area` VALUES (440801, 440800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (440802, 440800, 3, '赤坎区', '0', 100);
INSERT INTO `sys_area` VALUES (440803, 440800, 3, '霞山区', '0', 100);
INSERT INTO `sys_area` VALUES (440804, 440800, 3, '坡头区', '0', 100);
INSERT INTO `sys_area` VALUES (440811, 440800, 3, '麻章区', '0', 100);
INSERT INTO `sys_area` VALUES (440823, 440800, 3, '遂溪县', '0', 100);
INSERT INTO `sys_area` VALUES (440825, 440800, 3, '徐闻县', '0', 100);
INSERT INTO `sys_area` VALUES (440881, 440800, 3, '廉江市', '0', 100);
INSERT INTO `sys_area` VALUES (440882, 440800, 3, '雷州市', '0', 100);
INSERT INTO `sys_area` VALUES (440883, 440800, 3, '吴川市', '0', 100);
INSERT INTO `sys_area` VALUES (440900, 440000, 2, '茂名市', '0', 100);
INSERT INTO `sys_area` VALUES (440901, 440900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (440902, 440900, 3, '茂南区', '0', 100);
INSERT INTO `sys_area` VALUES (440904, 440900, 3, '电白区', '0', 100);
INSERT INTO `sys_area` VALUES (440981, 440900, 3, '高州市', '0', 100);
INSERT INTO `sys_area` VALUES (440982, 440900, 3, '化州市', '0', 100);
INSERT INTO `sys_area` VALUES (440983, 440900, 3, '信宜市', '0', 100);
INSERT INTO `sys_area` VALUES (441200, 440000, 2, '肇庆市', '0', 100);
INSERT INTO `sys_area` VALUES (441201, 441200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (441202, 441200, 3, '端州区', '0', 100);
INSERT INTO `sys_area` VALUES (441203, 441200, 3, '鼎湖区', '0', 100);
INSERT INTO `sys_area` VALUES (441204, 441200, 3, '高要区', '0', 100);
INSERT INTO `sys_area` VALUES (441223, 441200, 3, '广宁县', '0', 100);
INSERT INTO `sys_area` VALUES (441224, 441200, 3, '怀集县', '0', 100);
INSERT INTO `sys_area` VALUES (441225, 441200, 3, '封开县', '0', 100);
INSERT INTO `sys_area` VALUES (441226, 441200, 3, '德庆县', '0', 100);
INSERT INTO `sys_area` VALUES (441284, 441200, 3, '四会市', '0', 100);
INSERT INTO `sys_area` VALUES (441300, 440000, 2, '惠州市', '0', 100);
INSERT INTO `sys_area` VALUES (441301, 441300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (441302, 441300, 3, '惠城区', '0', 100);
INSERT INTO `sys_area` VALUES (441303, 441300, 3, '惠阳区', '0', 100);
INSERT INTO `sys_area` VALUES (441322, 441300, 3, '博罗县', '0', 100);
INSERT INTO `sys_area` VALUES (441323, 441300, 3, '惠东县', '0', 100);
INSERT INTO `sys_area` VALUES (441324, 441300, 3, '龙门县', '0', 100);
INSERT INTO `sys_area` VALUES (441400, 440000, 2, '梅州市', '0', 100);
INSERT INTO `sys_area` VALUES (441401, 441400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (441402, 441400, 3, '梅江区', '0', 100);
INSERT INTO `sys_area` VALUES (441403, 441400, 3, '梅县区', '0', 100);
INSERT INTO `sys_area` VALUES (441422, 441400, 3, '大埔县', '0', 100);
INSERT INTO `sys_area` VALUES (441423, 441400, 3, '丰顺县', '0', 100);
INSERT INTO `sys_area` VALUES (441424, 441400, 3, '五华县', '0', 100);
INSERT INTO `sys_area` VALUES (441426, 441400, 3, '平远县', '0', 100);
INSERT INTO `sys_area` VALUES (441427, 441400, 3, '蕉岭县', '0', 100);
INSERT INTO `sys_area` VALUES (441481, 441400, 3, '兴宁市', '0', 100);
INSERT INTO `sys_area` VALUES (441500, 440000, 2, '汕尾市', '0', 100);
INSERT INTO `sys_area` VALUES (441501, 441500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (441502, 441500, 3, '城区', '0', 100);
INSERT INTO `sys_area` VALUES (441521, 441500, 3, '海丰县', '0', 100);
INSERT INTO `sys_area` VALUES (441523, 441500, 3, '陆河县', '0', 100);
INSERT INTO `sys_area` VALUES (441581, 441500, 3, '陆丰市', '0', 100);
INSERT INTO `sys_area` VALUES (441600, 440000, 2, '河源市', '0', 100);
INSERT INTO `sys_area` VALUES (441601, 441600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (441602, 441600, 3, '源城区', '0', 100);
INSERT INTO `sys_area` VALUES (441621, 441600, 3, '紫金县', '0', 100);
INSERT INTO `sys_area` VALUES (441622, 441600, 3, '龙川县', '0', 100);
INSERT INTO `sys_area` VALUES (441623, 441600, 3, '连平县', '0', 100);
INSERT INTO `sys_area` VALUES (441624, 441600, 3, '和平县', '0', 100);
INSERT INTO `sys_area` VALUES (441625, 441600, 3, '东源县', '0', 100);
INSERT INTO `sys_area` VALUES (441700, 440000, 2, '阳江市', '0', 100);
INSERT INTO `sys_area` VALUES (441701, 441700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (441702, 441700, 3, '江城区', '0', 100);
INSERT INTO `sys_area` VALUES (441704, 441700, 3, '阳东区', '0', 100);
INSERT INTO `sys_area` VALUES (441721, 441700, 3, '阳西县', '0', 100);
INSERT INTO `sys_area` VALUES (441781, 441700, 3, '阳春市', '0', 100);
INSERT INTO `sys_area` VALUES (441800, 440000, 2, '清远市', '0', 100);
INSERT INTO `sys_area` VALUES (441801, 441800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (441802, 441800, 3, '清城区', '0', 100);
INSERT INTO `sys_area` VALUES (441803, 441800, 3, '清新区', '0', 100);
INSERT INTO `sys_area` VALUES (441821, 441800, 3, '佛冈县', '0', 100);
INSERT INTO `sys_area` VALUES (441823, 441800, 3, '阳山县', '0', 100);
INSERT INTO `sys_area` VALUES (441825, 441800, 3, '连山壮族瑶族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (441826, 441800, 3, '连南瑶族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (441881, 441800, 3, '英德市', '0', 100);
INSERT INTO `sys_area` VALUES (441882, 441800, 3, '连州市', '0', 100);
INSERT INTO `sys_area` VALUES (441900, 440000, 2, '东莞市', '0', 100);
INSERT INTO `sys_area` VALUES (442000, 440000, 2, '中山市', '0', 100);
INSERT INTO `sys_area` VALUES (445100, 440000, 2, '潮州市', '0', 100);
INSERT INTO `sys_area` VALUES (445101, 445100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (445102, 445100, 3, '湘桥区', '0', 100);
INSERT INTO `sys_area` VALUES (445103, 445100, 3, '潮安区', '0', 100);
INSERT INTO `sys_area` VALUES (445122, 445100, 3, '饶平县', '0', 100);
INSERT INTO `sys_area` VALUES (445200, 440000, 2, '揭阳市', '0', 100);
INSERT INTO `sys_area` VALUES (445201, 445200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (445202, 445200, 3, '榕城区', '0', 100);
INSERT INTO `sys_area` VALUES (445203, 445200, 3, '揭东区', '0', 100);
INSERT INTO `sys_area` VALUES (445222, 445200, 3, '揭西县', '0', 100);
INSERT INTO `sys_area` VALUES (445224, 445200, 3, '惠来县', '0', 100);
INSERT INTO `sys_area` VALUES (445281, 445200, 3, '普宁市', '0', 100);
INSERT INTO `sys_area` VALUES (445300, 440000, 2, '云浮市', '0', 100);
INSERT INTO `sys_area` VALUES (445301, 445300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (445302, 445300, 3, '云城区', '0', 100);
INSERT INTO `sys_area` VALUES (445303, 445300, 3, '云安区', '0', 100);
INSERT INTO `sys_area` VALUES (445321, 445300, 3, '新兴县', '0', 100);
INSERT INTO `sys_area` VALUES (445322, 445300, 3, '郁南县', '0', 100);
INSERT INTO `sys_area` VALUES (445381, 445300, 3, '罗定市', '0', 100);
INSERT INTO `sys_area` VALUES (450000, 0, 1, '广西壮族自治区', '0', 100);
INSERT INTO `sys_area` VALUES (450100, 450000, 2, '南宁市', '0', 100);
INSERT INTO `sys_area` VALUES (450101, 450100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (450102, 450100, 3, '兴宁区', '0', 100);
INSERT INTO `sys_area` VALUES (450103, 450100, 3, '青秀区', '0', 100);
INSERT INTO `sys_area` VALUES (450105, 450100, 3, '江南区', '0', 100);
INSERT INTO `sys_area` VALUES (450107, 450100, 3, '西乡塘区', '0', 100);
INSERT INTO `sys_area` VALUES (450108, 450100, 3, '良庆区', '0', 100);
INSERT INTO `sys_area` VALUES (450109, 450100, 3, '邕宁区', '0', 100);
INSERT INTO `sys_area` VALUES (450110, 450100, 3, '武鸣区', '0', 100);
INSERT INTO `sys_area` VALUES (450123, 450100, 3, '隆安县', '0', 100);
INSERT INTO `sys_area` VALUES (450124, 450100, 3, '马山县', '0', 100);
INSERT INTO `sys_area` VALUES (450125, 450100, 3, '上林县', '0', 100);
INSERT INTO `sys_area` VALUES (450126, 450100, 3, '宾阳县', '0', 100);
INSERT INTO `sys_area` VALUES (450127, 450100, 3, '横县', '0', 100);
INSERT INTO `sys_area` VALUES (450200, 450000, 2, '柳州市', '0', 100);
INSERT INTO `sys_area` VALUES (450201, 450200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (450202, 450200, 3, '城中区', '0', 100);
INSERT INTO `sys_area` VALUES (450203, 450200, 3, '鱼峰区', '0', 100);
INSERT INTO `sys_area` VALUES (450204, 450200, 3, '柳南区', '0', 100);
INSERT INTO `sys_area` VALUES (450205, 450200, 3, '柳北区', '0', 100);
INSERT INTO `sys_area` VALUES (450206, 450200, 3, '柳江区', '0', 100);
INSERT INTO `sys_area` VALUES (450222, 450200, 3, '柳城县', '0', 100);
INSERT INTO `sys_area` VALUES (450223, 450200, 3, '鹿寨县', '0', 100);
INSERT INTO `sys_area` VALUES (450224, 450200, 3, '融安县', '0', 100);
INSERT INTO `sys_area` VALUES (450225, 450200, 3, '融水苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (450226, 450200, 3, '三江侗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (450300, 450000, 2, '桂林市', '0', 100);
INSERT INTO `sys_area` VALUES (450301, 450300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (450302, 450300, 3, '秀峰区', '0', 100);
INSERT INTO `sys_area` VALUES (450303, 450300, 3, '叠彩区', '0', 100);
INSERT INTO `sys_area` VALUES (450304, 450300, 3, '象山区', '0', 100);
INSERT INTO `sys_area` VALUES (450305, 450300, 3, '七星区', '0', 100);
INSERT INTO `sys_area` VALUES (450311, 450300, 3, '雁山区', '0', 100);
INSERT INTO `sys_area` VALUES (450312, 450300, 3, '临桂区', '0', 100);
INSERT INTO `sys_area` VALUES (450321, 450300, 3, '阳朔县', '0', 100);
INSERT INTO `sys_area` VALUES (450323, 450300, 3, '灵川县', '0', 100);
INSERT INTO `sys_area` VALUES (450324, 450300, 3, '全州县', '0', 100);
INSERT INTO `sys_area` VALUES (450325, 450300, 3, '兴安县', '0', 100);
INSERT INTO `sys_area` VALUES (450326, 450300, 3, '永福县', '0', 100);
INSERT INTO `sys_area` VALUES (450327, 450300, 3, '灌阳县', '0', 100);
INSERT INTO `sys_area` VALUES (450328, 450300, 3, '龙胜各族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (450329, 450300, 3, '资源县', '0', 100);
INSERT INTO `sys_area` VALUES (450330, 450300, 3, '平乐县', '0', 100);
INSERT INTO `sys_area` VALUES (450331, 450300, 3, '荔浦县', '0', 100);
INSERT INTO `sys_area` VALUES (450332, 450300, 3, '恭城瑶族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (450400, 450000, 2, '梧州市', '0', 100);
INSERT INTO `sys_area` VALUES (450401, 450400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (450403, 450400, 3, '万秀区', '0', 100);
INSERT INTO `sys_area` VALUES (450405, 450400, 3, '长洲区', '0', 100);
INSERT INTO `sys_area` VALUES (450406, 450400, 3, '龙圩区', '0', 100);
INSERT INTO `sys_area` VALUES (450421, 450400, 3, '苍梧县', '0', 100);
INSERT INTO `sys_area` VALUES (450422, 450400, 3, '藤县', '0', 100);
INSERT INTO `sys_area` VALUES (450423, 450400, 3, '蒙山县', '0', 100);
INSERT INTO `sys_area` VALUES (450481, 450400, 3, '岑溪市', '0', 100);
INSERT INTO `sys_area` VALUES (450500, 450000, 2, '北海市', '0', 100);
INSERT INTO `sys_area` VALUES (450501, 450500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (450502, 450500, 3, '海城区', '0', 100);
INSERT INTO `sys_area` VALUES (450503, 450500, 3, '银海区', '0', 100);
INSERT INTO `sys_area` VALUES (450512, 450500, 3, '铁山港区', '0', 100);
INSERT INTO `sys_area` VALUES (450521, 450500, 3, '合浦县', '0', 100);
INSERT INTO `sys_area` VALUES (450600, 450000, 2, '防城港市', '0', 100);
INSERT INTO `sys_area` VALUES (450601, 450600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (450602, 450600, 3, '港口区', '0', 100);
INSERT INTO `sys_area` VALUES (450603, 450600, 3, '防城区', '0', 100);
INSERT INTO `sys_area` VALUES (450621, 450600, 3, '上思县', '0', 100);
INSERT INTO `sys_area` VALUES (450681, 450600, 3, '东兴市', '0', 100);
INSERT INTO `sys_area` VALUES (450700, 450000, 2, '钦州市', '0', 100);
INSERT INTO `sys_area` VALUES (450701, 450700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (450702, 450700, 3, '钦南区', '0', 100);
INSERT INTO `sys_area` VALUES (450703, 450700, 3, '钦北区', '0', 100);
INSERT INTO `sys_area` VALUES (450721, 450700, 3, '灵山县', '0', 100);
INSERT INTO `sys_area` VALUES (450722, 450700, 3, '浦北县', '0', 100);
INSERT INTO `sys_area` VALUES (450800, 450000, 2, '贵港市', '0', 100);
INSERT INTO `sys_area` VALUES (450801, 450800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (450802, 450800, 3, '港北区', '0', 100);
INSERT INTO `sys_area` VALUES (450803, 450800, 3, '港南区', '0', 100);
INSERT INTO `sys_area` VALUES (450804, 450800, 3, '覃塘区', '0', 100);
INSERT INTO `sys_area` VALUES (450821, 450800, 3, '平南县', '0', 100);
INSERT INTO `sys_area` VALUES (450881, 450800, 3, '桂平市', '0', 100);
INSERT INTO `sys_area` VALUES (450900, 450000, 2, '玉林市', '0', 100);
INSERT INTO `sys_area` VALUES (450901, 450900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (450902, 450900, 3, '玉州区', '0', 100);
INSERT INTO `sys_area` VALUES (450903, 450900, 3, '福绵区', '0', 100);
INSERT INTO `sys_area` VALUES (450921, 450900, 3, '容县', '0', 100);
INSERT INTO `sys_area` VALUES (450922, 450900, 3, '陆川县', '0', 100);
INSERT INTO `sys_area` VALUES (450923, 450900, 3, '博白县', '0', 100);
INSERT INTO `sys_area` VALUES (450924, 450900, 3, '兴业县', '0', 100);
INSERT INTO `sys_area` VALUES (450981, 450900, 3, '北流市', '0', 100);
INSERT INTO `sys_area` VALUES (451000, 450000, 2, '百色市', '0', 100);
INSERT INTO `sys_area` VALUES (451001, 451000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (451002, 451000, 3, '右江区', '0', 100);
INSERT INTO `sys_area` VALUES (451021, 451000, 3, '田阳县', '0', 100);
INSERT INTO `sys_area` VALUES (451022, 451000, 3, '田东县', '0', 100);
INSERT INTO `sys_area` VALUES (451023, 451000, 3, '平果县', '0', 100);
INSERT INTO `sys_area` VALUES (451024, 451000, 3, '德保县', '0', 100);
INSERT INTO `sys_area` VALUES (451026, 451000, 3, '那坡县', '0', 100);
INSERT INTO `sys_area` VALUES (451027, 451000, 3, '凌云县', '0', 100);
INSERT INTO `sys_area` VALUES (451028, 451000, 3, '乐业县', '0', 100);
INSERT INTO `sys_area` VALUES (451029, 451000, 3, '田林县', '0', 100);
INSERT INTO `sys_area` VALUES (451030, 451000, 3, '西林县', '0', 100);
INSERT INTO `sys_area` VALUES (451031, 451000, 3, '隆林各族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (451081, 451000, 3, '靖西市', '0', 100);
INSERT INTO `sys_area` VALUES (451100, 450000, 2, '贺州市', '0', 100);
INSERT INTO `sys_area` VALUES (451101, 451100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (451102, 451100, 3, '八步区', '0', 100);
INSERT INTO `sys_area` VALUES (451103, 451100, 3, '平桂区', '0', 100);
INSERT INTO `sys_area` VALUES (451121, 451100, 3, '昭平县', '0', 100);
INSERT INTO `sys_area` VALUES (451122, 451100, 3, '钟山县', '0', 100);
INSERT INTO `sys_area` VALUES (451123, 451100, 3, '富川瑶族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (451200, 450000, 2, '河池市', '0', 100);
INSERT INTO `sys_area` VALUES (451201, 451200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (451202, 451200, 3, '金城江区', '0', 100);
INSERT INTO `sys_area` VALUES (451221, 451200, 3, '南丹县', '0', 100);
INSERT INTO `sys_area` VALUES (451222, 451200, 3, '天峨县', '0', 100);
INSERT INTO `sys_area` VALUES (451223, 451200, 3, '凤山县', '0', 100);
INSERT INTO `sys_area` VALUES (451224, 451200, 3, '东兰县', '0', 100);
INSERT INTO `sys_area` VALUES (451225, 451200, 3, '罗城仫佬族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (451226, 451200, 3, '环江毛南族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (451227, 451200, 3, '巴马瑶族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (451228, 451200, 3, '都安瑶族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (451229, 451200, 3, '大化瑶族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (451281, 451200, 3, '宜州市', '0', 100);
INSERT INTO `sys_area` VALUES (451300, 450000, 2, '来宾市', '0', 100);
INSERT INTO `sys_area` VALUES (451301, 451300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (451302, 451300, 3, '兴宾区', '0', 100);
INSERT INTO `sys_area` VALUES (451321, 451300, 3, '忻城县', '0', 100);
INSERT INTO `sys_area` VALUES (451322, 451300, 3, '象州县', '0', 100);
INSERT INTO `sys_area` VALUES (451323, 451300, 3, '武宣县', '0', 100);
INSERT INTO `sys_area` VALUES (451324, 451300, 3, '金秀瑶族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (451381, 451300, 3, '合山市', '0', 100);
INSERT INTO `sys_area` VALUES (451400, 450000, 2, '崇左市', '0', 100);
INSERT INTO `sys_area` VALUES (451401, 451400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (451402, 451400, 3, '江州区', '0', 100);
INSERT INTO `sys_area` VALUES (451421, 451400, 3, '扶绥县', '0', 100);
INSERT INTO `sys_area` VALUES (451422, 451400, 3, '宁明县', '0', 100);
INSERT INTO `sys_area` VALUES (451423, 451400, 3, '龙州县', '0', 100);
INSERT INTO `sys_area` VALUES (451424, 451400, 3, '大新县', '0', 100);
INSERT INTO `sys_area` VALUES (451425, 451400, 3, '天等县', '0', 100);
INSERT INTO `sys_area` VALUES (451481, 451400, 3, '凭祥市', '0', 100);
INSERT INTO `sys_area` VALUES (460000, 0, 1, '海南省', '0', 100);
INSERT INTO `sys_area` VALUES (460100, 460000, 2, '海口市', '0', 100);
INSERT INTO `sys_area` VALUES (460101, 460100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (460105, 460100, 3, '秀英区', '0', 100);
INSERT INTO `sys_area` VALUES (460106, 460100, 3, '龙华区', '0', 100);
INSERT INTO `sys_area` VALUES (460107, 460100, 3, '琼山区', '0', 100);
INSERT INTO `sys_area` VALUES (460108, 460100, 3, '美兰区', '0', 100);
INSERT INTO `sys_area` VALUES (460200, 460000, 2, '三亚市', '0', 100);
INSERT INTO `sys_area` VALUES (460201, 460200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (460202, 460200, 3, '海棠区', '0', 100);
INSERT INTO `sys_area` VALUES (460203, 460200, 3, '吉阳区', '0', 100);
INSERT INTO `sys_area` VALUES (460204, 460200, 3, '天涯区', '0', 100);
INSERT INTO `sys_area` VALUES (460205, 460200, 3, '崖州区', '0', 100);
INSERT INTO `sys_area` VALUES (460300, 460000, 2, '三沙市', '0', 100);
INSERT INTO `sys_area` VALUES (460400, 460000, 2, '儋州市', '0', 100);
INSERT INTO `sys_area` VALUES (469000, 460000, 2, '省直辖县级行政区划', '0', 100);
INSERT INTO `sys_area` VALUES (469001, 469000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (469002, 469000, 3, '琼海市', '0', 100);
INSERT INTO `sys_area` VALUES (469005, 469000, 3, '文昌市', '0', 100);
INSERT INTO `sys_area` VALUES (469006, 469000, 3, '万宁市', '0', 100);
INSERT INTO `sys_area` VALUES (469007, 469000, 3, '东方市', '0', 100);
INSERT INTO `sys_area` VALUES (469021, 469000, 3, '定安县', '0', 100);
INSERT INTO `sys_area` VALUES (469022, 469000, 3, '屯昌县', '0', 100);
INSERT INTO `sys_area` VALUES (469023, 469000, 3, '澄迈县', '0', 100);
INSERT INTO `sys_area` VALUES (469024, 469000, 3, '临高县', '0', 100);
INSERT INTO `sys_area` VALUES (469025, 469000, 3, '白沙黎族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (469026, 469000, 3, '昌江黎族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (469027, 469000, 3, '乐东黎族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (469028, 469000, 3, '陵水黎族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (469029, 469000, 3, '保亭黎族苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (469030, 469000, 3, '琼中黎族苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (500000, 0, 1, '重庆市', '0', 100);
INSERT INTO `sys_area` VALUES (500100, 500000, 2, '重庆市', '0', 100);
INSERT INTO `sys_area` VALUES (500101, 500100, 3, '万州区', '0', 100);
INSERT INTO `sys_area` VALUES (500102, 500100, 3, '涪陵区', '0', 100);
INSERT INTO `sys_area` VALUES (500103, 500100, 3, '渝中区', '0', 100);
INSERT INTO `sys_area` VALUES (500104, 500100, 3, '大渡口区', '0', 100);
INSERT INTO `sys_area` VALUES (500105, 500100, 3, '江北区', '0', 100);
INSERT INTO `sys_area` VALUES (500106, 500100, 3, '沙坪坝区', '0', 100);
INSERT INTO `sys_area` VALUES (500107, 500100, 3, '九龙坡区', '0', 100);
INSERT INTO `sys_area` VALUES (500108, 500100, 3, '南岸区', '0', 100);
INSERT INTO `sys_area` VALUES (500109, 500100, 3, '北碚区', '0', 100);
INSERT INTO `sys_area` VALUES (500110, 500100, 3, '綦江区', '0', 100);
INSERT INTO `sys_area` VALUES (500111, 500100, 3, '大足区', '0', 100);
INSERT INTO `sys_area` VALUES (500112, 500100, 3, '渝北区', '0', 100);
INSERT INTO `sys_area` VALUES (500113, 500100, 3, '巴南区', '0', 100);
INSERT INTO `sys_area` VALUES (500114, 500100, 3, '黔江区', '0', 100);
INSERT INTO `sys_area` VALUES (500115, 500100, 3, '长寿区', '0', 100);
INSERT INTO `sys_area` VALUES (500116, 500100, 3, '江津区', '0', 100);
INSERT INTO `sys_area` VALUES (500117, 500100, 3, '合川区', '0', 100);
INSERT INTO `sys_area` VALUES (500118, 500100, 3, '永川区', '0', 100);
INSERT INTO `sys_area` VALUES (500119, 500100, 3, '南川区', '0', 100);
INSERT INTO `sys_area` VALUES (500120, 500100, 3, '璧山区', '0', 100);
INSERT INTO `sys_area` VALUES (500151, 500100, 3, '铜梁区', '0', 100);
INSERT INTO `sys_area` VALUES (500152, 500100, 3, '潼南区', '0', 100);
INSERT INTO `sys_area` VALUES (500153, 500100, 3, '荣昌区', '0', 100);
INSERT INTO `sys_area` VALUES (500154, 500100, 3, '开州区', '0', 100);
INSERT INTO `sys_area` VALUES (500200, 500000, 2, '县', '0', 100);
INSERT INTO `sys_area` VALUES (500228, 500200, 3, '梁平县', '0', 100);
INSERT INTO `sys_area` VALUES (500229, 500200, 3, '城口县', '0', 100);
INSERT INTO `sys_area` VALUES (500230, 500200, 3, '丰都县', '0', 100);
INSERT INTO `sys_area` VALUES (500231, 500200, 3, '垫江县', '0', 100);
INSERT INTO `sys_area` VALUES (500232, 500200, 3, '武隆县', '0', 100);
INSERT INTO `sys_area` VALUES (500233, 500200, 3, '忠县', '0', 100);
INSERT INTO `sys_area` VALUES (500235, 500200, 3, '云阳县', '0', 100);
INSERT INTO `sys_area` VALUES (500236, 500200, 3, '奉节县', '0', 100);
INSERT INTO `sys_area` VALUES (500237, 500200, 3, '巫山县', '0', 100);
INSERT INTO `sys_area` VALUES (500238, 500200, 3, '巫溪县', '0', 100);
INSERT INTO `sys_area` VALUES (500240, 500200, 3, '石柱土家族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (500241, 500200, 3, '秀山土家族苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (500242, 500200, 3, '酉阳土家族苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (500243, 500200, 3, '彭水苗族土家族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (510000, 0, 1, '四川省', '0', 100);
INSERT INTO `sys_area` VALUES (510100, 510000, 2, '成都市', '0', 100);
INSERT INTO `sys_area` VALUES (510101, 510100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (510104, 510100, 3, '锦江区', '0', 100);
INSERT INTO `sys_area` VALUES (510105, 510100, 3, '青羊区', '0', 100);
INSERT INTO `sys_area` VALUES (510106, 510100, 3, '金牛区', '0', 100);
INSERT INTO `sys_area` VALUES (510107, 510100, 3, '武侯区', '0', 100);
INSERT INTO `sys_area` VALUES (510108, 510100, 3, '成华区', '0', 100);
INSERT INTO `sys_area` VALUES (510112, 510100, 3, '龙泉驿区', '0', 100);
INSERT INTO `sys_area` VALUES (510113, 510100, 3, '青白江区', '0', 100);
INSERT INTO `sys_area` VALUES (510114, 510100, 3, '新都区', '0', 100);
INSERT INTO `sys_area` VALUES (510115, 510100, 3, '温江区', '0', 100);
INSERT INTO `sys_area` VALUES (510116, 510100, 3, '双流区', '0', 100);
INSERT INTO `sys_area` VALUES (510121, 510100, 3, '金堂县', '0', 100);
INSERT INTO `sys_area` VALUES (510124, 510100, 3, '郫县', '0', 100);
INSERT INTO `sys_area` VALUES (510129, 510100, 3, '大邑县', '0', 100);
INSERT INTO `sys_area` VALUES (510131, 510100, 3, '蒲江县', '0', 100);
INSERT INTO `sys_area` VALUES (510132, 510100, 3, '新津县', '0', 100);
INSERT INTO `sys_area` VALUES (510181, 510100, 3, '都江堰市', '0', 100);
INSERT INTO `sys_area` VALUES (510182, 510100, 3, '彭州市', '0', 100);
INSERT INTO `sys_area` VALUES (510183, 510100, 3, '邛崃市', '0', 100);
INSERT INTO `sys_area` VALUES (510184, 510100, 3, '崇州市', '0', 100);
INSERT INTO `sys_area` VALUES (510185, 510100, 3, '简阳市', '0', 100);
INSERT INTO `sys_area` VALUES (510300, 510000, 2, '自贡市', '0', 100);
INSERT INTO `sys_area` VALUES (510301, 510300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (510302, 510300, 3, '自流井区', '0', 100);
INSERT INTO `sys_area` VALUES (510303, 510300, 3, '贡井区', '0', 100);
INSERT INTO `sys_area` VALUES (510304, 510300, 3, '大安区', '0', 100);
INSERT INTO `sys_area` VALUES (510311, 510300, 3, '沿滩区', '0', 100);
INSERT INTO `sys_area` VALUES (510321, 510300, 3, '荣县', '0', 100);
INSERT INTO `sys_area` VALUES (510322, 510300, 3, '富顺县', '0', 100);
INSERT INTO `sys_area` VALUES (510400, 510000, 2, '攀枝花市', '0', 100);
INSERT INTO `sys_area` VALUES (510401, 510400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (510402, 510400, 3, '东区', '0', 100);
INSERT INTO `sys_area` VALUES (510403, 510400, 3, '西区', '0', 100);
INSERT INTO `sys_area` VALUES (510411, 510400, 3, '仁和区', '0', 100);
INSERT INTO `sys_area` VALUES (510421, 510400, 3, '米易县', '0', 100);
INSERT INTO `sys_area` VALUES (510422, 510400, 3, '盐边县', '0', 100);
INSERT INTO `sys_area` VALUES (510500, 510000, 2, '泸州市', '0', 100);
INSERT INTO `sys_area` VALUES (510501, 510500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (510502, 510500, 3, '江阳区', '0', 100);
INSERT INTO `sys_area` VALUES (510503, 510500, 3, '纳溪区', '0', 100);
INSERT INTO `sys_area` VALUES (510504, 510500, 3, '龙马潭区', '0', 100);
INSERT INTO `sys_area` VALUES (510521, 510500, 3, '泸县', '0', 100);
INSERT INTO `sys_area` VALUES (510522, 510500, 3, '合江县', '0', 100);
INSERT INTO `sys_area` VALUES (510524, 510500, 3, '叙永县', '0', 100);
INSERT INTO `sys_area` VALUES (510525, 510500, 3, '古蔺县', '0', 100);
INSERT INTO `sys_area` VALUES (510600, 510000, 2, '德阳市', '0', 100);
INSERT INTO `sys_area` VALUES (510601, 510600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (510603, 510600, 3, '旌阳区', '0', 100);
INSERT INTO `sys_area` VALUES (510623, 510600, 3, '中江县', '0', 100);
INSERT INTO `sys_area` VALUES (510626, 510600, 3, '罗江县', '0', 100);
INSERT INTO `sys_area` VALUES (510681, 510600, 3, '广汉市', '0', 100);
INSERT INTO `sys_area` VALUES (510682, 510600, 3, '什邡市', '0', 100);
INSERT INTO `sys_area` VALUES (510683, 510600, 3, '绵竹市', '0', 100);
INSERT INTO `sys_area` VALUES (510700, 510000, 2, '绵阳市', '0', 100);
INSERT INTO `sys_area` VALUES (510701, 510700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (510703, 510700, 3, '涪城区', '0', 100);
INSERT INTO `sys_area` VALUES (510704, 510700, 3, '游仙区', '0', 100);
INSERT INTO `sys_area` VALUES (510705, 510700, 3, '安州区', '0', 100);
INSERT INTO `sys_area` VALUES (510722, 510700, 3, '三台县', '0', 100);
INSERT INTO `sys_area` VALUES (510723, 510700, 3, '盐亭县', '0', 100);
INSERT INTO `sys_area` VALUES (510725, 510700, 3, '梓潼县', '0', 100);
INSERT INTO `sys_area` VALUES (510726, 510700, 3, '北川羌族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (510727, 510700, 3, '平武县', '0', 100);
INSERT INTO `sys_area` VALUES (510781, 510700, 3, '江油市', '0', 100);
INSERT INTO `sys_area` VALUES (510800, 510000, 2, '广元市', '0', 100);
INSERT INTO `sys_area` VALUES (510801, 510800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (510802, 510800, 3, '利州区', '0', 100);
INSERT INTO `sys_area` VALUES (510811, 510800, 3, '昭化区', '0', 100);
INSERT INTO `sys_area` VALUES (510812, 510800, 3, '朝天区', '0', 100);
INSERT INTO `sys_area` VALUES (510821, 510800, 3, '旺苍县', '0', 100);
INSERT INTO `sys_area` VALUES (510822, 510800, 3, '青川县', '0', 100);
INSERT INTO `sys_area` VALUES (510823, 510800, 3, '剑阁县', '0', 100);
INSERT INTO `sys_area` VALUES (510824, 510800, 3, '苍溪县', '0', 100);
INSERT INTO `sys_area` VALUES (510900, 510000, 2, '遂宁市', '0', 100);
INSERT INTO `sys_area` VALUES (510901, 510900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (510903, 510900, 3, '船山区', '0', 100);
INSERT INTO `sys_area` VALUES (510904, 510900, 3, '安居区', '0', 100);
INSERT INTO `sys_area` VALUES (510921, 510900, 3, '蓬溪县', '0', 100);
INSERT INTO `sys_area` VALUES (510922, 510900, 3, '射洪县', '0', 100);
INSERT INTO `sys_area` VALUES (510923, 510900, 3, '大英县', '0', 100);
INSERT INTO `sys_area` VALUES (511000, 510000, 2, '内江市', '0', 100);
INSERT INTO `sys_area` VALUES (511001, 511000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (511002, 511000, 3, '市中区', '0', 100);
INSERT INTO `sys_area` VALUES (511011, 511000, 3, '东兴区', '0', 100);
INSERT INTO `sys_area` VALUES (511024, 511000, 3, '威远县', '0', 100);
INSERT INTO `sys_area` VALUES (511025, 511000, 3, '资中县', '0', 100);
INSERT INTO `sys_area` VALUES (511028, 511000, 3, '隆昌县', '0', 100);
INSERT INTO `sys_area` VALUES (511100, 510000, 2, '乐山市', '0', 100);
INSERT INTO `sys_area` VALUES (511101, 511100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (511102, 511100, 3, '市中区', '0', 100);
INSERT INTO `sys_area` VALUES (511111, 511100, 3, '沙湾区', '0', 100);
INSERT INTO `sys_area` VALUES (511112, 511100, 3, '五通桥区', '0', 100);
INSERT INTO `sys_area` VALUES (511113, 511100, 3, '金口河区', '0', 100);
INSERT INTO `sys_area` VALUES (511123, 511100, 3, '犍为县', '0', 100);
INSERT INTO `sys_area` VALUES (511124, 511100, 3, '井研县', '0', 100);
INSERT INTO `sys_area` VALUES (511126, 511100, 3, '夹江县', '0', 100);
INSERT INTO `sys_area` VALUES (511129, 511100, 3, '沐川县', '0', 100);
INSERT INTO `sys_area` VALUES (511132, 511100, 3, '峨边彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (511133, 511100, 3, '马边彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (511181, 511100, 3, '峨眉山市', '0', 100);
INSERT INTO `sys_area` VALUES (511300, 510000, 2, '南充市', '0', 100);
INSERT INTO `sys_area` VALUES (511301, 511300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (511302, 511300, 3, '顺庆区', '0', 100);
INSERT INTO `sys_area` VALUES (511303, 511300, 3, '高坪区', '0', 100);
INSERT INTO `sys_area` VALUES (511304, 511300, 3, '嘉陵区', '0', 100);
INSERT INTO `sys_area` VALUES (511321, 511300, 3, '南部县', '0', 100);
INSERT INTO `sys_area` VALUES (511322, 511300, 3, '营山县', '0', 100);
INSERT INTO `sys_area` VALUES (511323, 511300, 3, '蓬安县', '0', 100);
INSERT INTO `sys_area` VALUES (511324, 511300, 3, '仪陇县', '0', 100);
INSERT INTO `sys_area` VALUES (511325, 511300, 3, '西充县', '0', 100);
INSERT INTO `sys_area` VALUES (511381, 511300, 3, '阆中市', '0', 100);
INSERT INTO `sys_area` VALUES (511400, 510000, 2, '眉山市', '0', 100);
INSERT INTO `sys_area` VALUES (511401, 511400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (511402, 511400, 3, '东坡区', '0', 100);
INSERT INTO `sys_area` VALUES (511403, 511400, 3, '彭山区', '0', 100);
INSERT INTO `sys_area` VALUES (511421, 511400, 3, '仁寿县', '0', 100);
INSERT INTO `sys_area` VALUES (511423, 511400, 3, '洪雅县', '0', 100);
INSERT INTO `sys_area` VALUES (511424, 511400, 3, '丹棱县', '0', 100);
INSERT INTO `sys_area` VALUES (511425, 511400, 3, '青神县', '0', 100);
INSERT INTO `sys_area` VALUES (511500, 510000, 2, '宜宾市', '0', 100);
INSERT INTO `sys_area` VALUES (511501, 511500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (511502, 511500, 3, '翠屏区', '0', 100);
INSERT INTO `sys_area` VALUES (511503, 511500, 3, '南溪区', '0', 100);
INSERT INTO `sys_area` VALUES (511521, 511500, 3, '宜宾县', '0', 100);
INSERT INTO `sys_area` VALUES (511523, 511500, 3, '江安县', '0', 100);
INSERT INTO `sys_area` VALUES (511524, 511500, 3, '长宁县', '0', 100);
INSERT INTO `sys_area` VALUES (511525, 511500, 3, '高县', '0', 100);
INSERT INTO `sys_area` VALUES (511526, 511500, 3, '珙县', '0', 100);
INSERT INTO `sys_area` VALUES (511527, 511500, 3, '筠连县', '0', 100);
INSERT INTO `sys_area` VALUES (511528, 511500, 3, '兴文县', '0', 100);
INSERT INTO `sys_area` VALUES (511529, 511500, 3, '屏山县', '0', 100);
INSERT INTO `sys_area` VALUES (511600, 510000, 2, '广安市', '0', 100);
INSERT INTO `sys_area` VALUES (511601, 511600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (511602, 511600, 3, '广安区', '0', 100);
INSERT INTO `sys_area` VALUES (511603, 511600, 3, '前锋区', '0', 100);
INSERT INTO `sys_area` VALUES (511621, 511600, 3, '岳池县', '0', 100);
INSERT INTO `sys_area` VALUES (511622, 511600, 3, '武胜县', '0', 100);
INSERT INTO `sys_area` VALUES (511623, 511600, 3, '邻水县', '0', 100);
INSERT INTO `sys_area` VALUES (511681, 511600, 3, '华蓥市', '0', 100);
INSERT INTO `sys_area` VALUES (511700, 510000, 2, '达州市', '0', 100);
INSERT INTO `sys_area` VALUES (511701, 511700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (511702, 511700, 3, '通川区', '0', 100);
INSERT INTO `sys_area` VALUES (511703, 511700, 3, '达川区', '0', 100);
INSERT INTO `sys_area` VALUES (511722, 511700, 3, '宣汉县', '0', 100);
INSERT INTO `sys_area` VALUES (511723, 511700, 3, '开江县', '0', 100);
INSERT INTO `sys_area` VALUES (511724, 511700, 3, '大竹县', '0', 100);
INSERT INTO `sys_area` VALUES (511725, 511700, 3, '渠县', '0', 100);
INSERT INTO `sys_area` VALUES (511781, 511700, 3, '万源市', '0', 100);
INSERT INTO `sys_area` VALUES (511800, 510000, 2, '雅安市', '0', 100);
INSERT INTO `sys_area` VALUES (511801, 511800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (511802, 511800, 3, '雨城区', '0', 100);
INSERT INTO `sys_area` VALUES (511803, 511800, 3, '名山区', '0', 100);
INSERT INTO `sys_area` VALUES (511822, 511800, 3, '荥经县', '0', 100);
INSERT INTO `sys_area` VALUES (511823, 511800, 3, '汉源县', '0', 100);
INSERT INTO `sys_area` VALUES (511824, 511800, 3, '石棉县', '0', 100);
INSERT INTO `sys_area` VALUES (511825, 511800, 3, '天全县', '0', 100);
INSERT INTO `sys_area` VALUES (511826, 511800, 3, '芦山县', '0', 100);
INSERT INTO `sys_area` VALUES (511827, 511800, 3, '宝兴县', '0', 100);
INSERT INTO `sys_area` VALUES (511900, 510000, 2, '巴中市', '0', 100);
INSERT INTO `sys_area` VALUES (511901, 511900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (511902, 511900, 3, '巴州区', '0', 100);
INSERT INTO `sys_area` VALUES (511903, 511900, 3, '恩阳区', '0', 100);
INSERT INTO `sys_area` VALUES (511921, 511900, 3, '通江县', '0', 100);
INSERT INTO `sys_area` VALUES (511922, 511900, 3, '南江县', '0', 100);
INSERT INTO `sys_area` VALUES (511923, 511900, 3, '平昌县', '0', 100);
INSERT INTO `sys_area` VALUES (512000, 510000, 2, '资阳市', '0', 100);
INSERT INTO `sys_area` VALUES (512001, 512000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (512002, 512000, 3, '雁江区', '0', 100);
INSERT INTO `sys_area` VALUES (512021, 512000, 3, '安岳县', '0', 100);
INSERT INTO `sys_area` VALUES (512022, 512000, 3, '乐至县', '0', 100);
INSERT INTO `sys_area` VALUES (513200, 510000, 2, '阿坝藏族羌族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (513201, 513200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (513221, 513200, 3, '汶川县', '0', 100);
INSERT INTO `sys_area` VALUES (513222, 513200, 3, '理县', '0', 100);
INSERT INTO `sys_area` VALUES (513223, 513200, 3, '茂县', '0', 100);
INSERT INTO `sys_area` VALUES (513224, 513200, 3, '松潘县', '0', 100);
INSERT INTO `sys_area` VALUES (513225, 513200, 3, '九寨沟县', '0', 100);
INSERT INTO `sys_area` VALUES (513226, 513200, 3, '金川县', '0', 100);
INSERT INTO `sys_area` VALUES (513227, 513200, 3, '小金县', '0', 100);
INSERT INTO `sys_area` VALUES (513228, 513200, 3, '黑水县', '0', 100);
INSERT INTO `sys_area` VALUES (513230, 513200, 3, '壤塘县', '0', 100);
INSERT INTO `sys_area` VALUES (513231, 513200, 3, '阿坝县', '0', 100);
INSERT INTO `sys_area` VALUES (513232, 513200, 3, '若尔盖县', '0', 100);
INSERT INTO `sys_area` VALUES (513233, 513200, 3, '红原县', '0', 100);
INSERT INTO `sys_area` VALUES (513300, 510000, 2, '甘孜藏族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (513301, 513300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (513322, 513300, 3, '泸定县', '0', 100);
INSERT INTO `sys_area` VALUES (513323, 513300, 3, '丹巴县', '0', 100);
INSERT INTO `sys_area` VALUES (513324, 513300, 3, '九龙县', '0', 100);
INSERT INTO `sys_area` VALUES (513325, 513300, 3, '雅江县', '0', 100);
INSERT INTO `sys_area` VALUES (513326, 513300, 3, '道孚县', '0', 100);
INSERT INTO `sys_area` VALUES (513327, 513300, 3, '炉霍县', '0', 100);
INSERT INTO `sys_area` VALUES (513328, 513300, 3, '甘孜县', '0', 100);
INSERT INTO `sys_area` VALUES (513329, 513300, 3, '新龙县', '0', 100);
INSERT INTO `sys_area` VALUES (513330, 513300, 3, '德格县', '0', 100);
INSERT INTO `sys_area` VALUES (513331, 513300, 3, '白玉县', '0', 100);
INSERT INTO `sys_area` VALUES (513332, 513300, 3, '石渠县', '0', 100);
INSERT INTO `sys_area` VALUES (513333, 513300, 3, '色达县', '0', 100);
INSERT INTO `sys_area` VALUES (513334, 513300, 3, '理塘县', '0', 100);
INSERT INTO `sys_area` VALUES (513335, 513300, 3, '巴塘县', '0', 100);
INSERT INTO `sys_area` VALUES (513336, 513300, 3, '乡城县', '0', 100);
INSERT INTO `sys_area` VALUES (513337, 513300, 3, '稻城县', '0', 100);
INSERT INTO `sys_area` VALUES (513338, 513300, 3, '得荣县', '0', 100);
INSERT INTO `sys_area` VALUES (513400, 510000, 2, '凉山彝族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (513401, 513400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (513422, 513400, 3, '木里藏族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (513423, 513400, 3, '盐源县', '0', 100);
INSERT INTO `sys_area` VALUES (513424, 513400, 3, '德昌县', '0', 100);
INSERT INTO `sys_area` VALUES (513425, 513400, 3, '会理县', '0', 100);
INSERT INTO `sys_area` VALUES (513426, 513400, 3, '会东县', '0', 100);
INSERT INTO `sys_area` VALUES (513427, 513400, 3, '宁南县', '0', 100);
INSERT INTO `sys_area` VALUES (513428, 513400, 3, '普格县', '0', 100);
INSERT INTO `sys_area` VALUES (513429, 513400, 3, '布拖县', '0', 100);
INSERT INTO `sys_area` VALUES (513430, 513400, 3, '金阳县', '0', 100);
INSERT INTO `sys_area` VALUES (513431, 513400, 3, '昭觉县', '0', 100);
INSERT INTO `sys_area` VALUES (513432, 513400, 3, '喜德县', '0', 100);
INSERT INTO `sys_area` VALUES (513433, 513400, 3, '冕宁县', '0', 100);
INSERT INTO `sys_area` VALUES (513434, 513400, 3, '越西县', '0', 100);
INSERT INTO `sys_area` VALUES (513435, 513400, 3, '甘洛县', '0', 100);
INSERT INTO `sys_area` VALUES (513436, 513400, 3, '美姑县', '0', 100);
INSERT INTO `sys_area` VALUES (513437, 513400, 3, '雷波县', '0', 100);
INSERT INTO `sys_area` VALUES (520000, 0, 1, '贵州省', '0', 100);
INSERT INTO `sys_area` VALUES (520100, 520000, 2, '贵阳市', '0', 100);
INSERT INTO `sys_area` VALUES (520101, 520100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (520102, 520100, 3, '南明区', '0', 100);
INSERT INTO `sys_area` VALUES (520103, 520100, 3, '云岩区', '0', 100);
INSERT INTO `sys_area` VALUES (520111, 520100, 3, '花溪区', '0', 100);
INSERT INTO `sys_area` VALUES (520112, 520100, 3, '乌当区', '0', 100);
INSERT INTO `sys_area` VALUES (520113, 520100, 3, '白云区', '0', 100);
INSERT INTO `sys_area` VALUES (520115, 520100, 3, '观山湖区', '0', 100);
INSERT INTO `sys_area` VALUES (520121, 520100, 3, '开阳县', '0', 100);
INSERT INTO `sys_area` VALUES (520122, 520100, 3, '息烽县', '0', 100);
INSERT INTO `sys_area` VALUES (520123, 520100, 3, '修文县', '0', 100);
INSERT INTO `sys_area` VALUES (520181, 520100, 3, '清镇市', '0', 100);
INSERT INTO `sys_area` VALUES (520200, 520000, 2, '六盘水市', '0', 100);
INSERT INTO `sys_area` VALUES (520201, 520200, 3, '钟山区', '0', 100);
INSERT INTO `sys_area` VALUES (520203, 520200, 3, '六枝特区', '0', 100);
INSERT INTO `sys_area` VALUES (520221, 520200, 3, '水城县', '0', 100);
INSERT INTO `sys_area` VALUES (520222, 520200, 3, '盘县', '0', 100);
INSERT INTO `sys_area` VALUES (520300, 520000, 2, '遵义市', '0', 100);
INSERT INTO `sys_area` VALUES (520301, 520300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (520302, 520300, 3, '红花岗区', '0', 100);
INSERT INTO `sys_area` VALUES (520303, 520300, 3, '汇川区', '0', 100);
INSERT INTO `sys_area` VALUES (520304, 520300, 3, '播州区', '0', 100);
INSERT INTO `sys_area` VALUES (520322, 520300, 3, '桐梓县', '0', 100);
INSERT INTO `sys_area` VALUES (520323, 520300, 3, '绥阳县', '0', 100);
INSERT INTO `sys_area` VALUES (520324, 520300, 3, '正安县', '0', 100);
INSERT INTO `sys_area` VALUES (520325, 520300, 3, '道真仡佬族苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (520326, 520300, 3, '务川仡佬族苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (520327, 520300, 3, '凤冈县', '0', 100);
INSERT INTO `sys_area` VALUES (520328, 520300, 3, '湄潭县', '0', 100);
INSERT INTO `sys_area` VALUES (520329, 520300, 3, '余庆县', '0', 100);
INSERT INTO `sys_area` VALUES (520330, 520300, 3, '习水县', '0', 100);
INSERT INTO `sys_area` VALUES (520381, 520300, 3, '赤水市', '0', 100);
INSERT INTO `sys_area` VALUES (520382, 520300, 3, '仁怀市', '0', 100);
INSERT INTO `sys_area` VALUES (520400, 520000, 2, '安顺市', '0', 100);
INSERT INTO `sys_area` VALUES (520401, 520400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (520402, 520400, 3, '西秀区', '0', 100);
INSERT INTO `sys_area` VALUES (520403, 520400, 3, '平坝区', '0', 100);
INSERT INTO `sys_area` VALUES (520422, 520400, 3, '普定县', '0', 100);
INSERT INTO `sys_area` VALUES (520423, 520400, 3, '镇宁布依族苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (520424, 520400, 3, '关岭布依族苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (520425, 520400, 3, '紫云苗族布依族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (520500, 520000, 2, '毕节市', '0', 100);
INSERT INTO `sys_area` VALUES (520501, 520500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (520502, 520500, 3, '七星关区', '0', 100);
INSERT INTO `sys_area` VALUES (520521, 520500, 3, '大方县', '0', 100);
INSERT INTO `sys_area` VALUES (520522, 520500, 3, '黔西县', '0', 100);
INSERT INTO `sys_area` VALUES (520523, 520500, 3, '金沙县', '0', 100);
INSERT INTO `sys_area` VALUES (520524, 520500, 3, '织金县', '0', 100);
INSERT INTO `sys_area` VALUES (520525, 520500, 3, '纳雍县', '0', 100);
INSERT INTO `sys_area` VALUES (520526, 520500, 3, '威宁彝族回族苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (520527, 520500, 3, '赫章县', '0', 100);
INSERT INTO `sys_area` VALUES (520600, 520000, 2, '铜仁市', '0', 100);
INSERT INTO `sys_area` VALUES (520601, 520600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (520602, 520600, 3, '碧江区', '0', 100);
INSERT INTO `sys_area` VALUES (520603, 520600, 3, '万山区', '0', 100);
INSERT INTO `sys_area` VALUES (520621, 520600, 3, '江口县', '0', 100);
INSERT INTO `sys_area` VALUES (520622, 520600, 3, '玉屏侗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (520623, 520600, 3, '石阡县', '0', 100);
INSERT INTO `sys_area` VALUES (520624, 520600, 3, '思南县', '0', 100);
INSERT INTO `sys_area` VALUES (520625, 520600, 3, '印江土家族苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (520626, 520600, 3, '德江县', '0', 100);
INSERT INTO `sys_area` VALUES (520627, 520600, 3, '沿河土家族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (520628, 520600, 3, '松桃苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (522300, 520000, 2, '黔西南布依族苗族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (522301, 522300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (522322, 522300, 3, '兴仁县', '0', 100);
INSERT INTO `sys_area` VALUES (522323, 522300, 3, '普安县', '0', 100);
INSERT INTO `sys_area` VALUES (522324, 522300, 3, '晴隆县', '0', 100);
INSERT INTO `sys_area` VALUES (522325, 522300, 3, '贞丰县', '0', 100);
INSERT INTO `sys_area` VALUES (522326, 522300, 3, '望谟县', '0', 100);
INSERT INTO `sys_area` VALUES (522327, 522300, 3, '册亨县', '0', 100);
INSERT INTO `sys_area` VALUES (522328, 522300, 3, '安龙县', '0', 100);
INSERT INTO `sys_area` VALUES (522600, 520000, 2, '黔东南苗族侗族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (522601, 522600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (522622, 522600, 3, '黄平县', '0', 100);
INSERT INTO `sys_area` VALUES (522623, 522600, 3, '施秉县', '0', 100);
INSERT INTO `sys_area` VALUES (522624, 522600, 3, '三穗县', '0', 100);
INSERT INTO `sys_area` VALUES (522625, 522600, 3, '镇远县', '0', 100);
INSERT INTO `sys_area` VALUES (522626, 522600, 3, '岑巩县', '0', 100);
INSERT INTO `sys_area` VALUES (522627, 522600, 3, '天柱县', '0', 100);
INSERT INTO `sys_area` VALUES (522628, 522600, 3, '锦屏县', '0', 100);
INSERT INTO `sys_area` VALUES (522629, 522600, 3, '剑河县', '0', 100);
INSERT INTO `sys_area` VALUES (522630, 522600, 3, '台江县', '0', 100);
INSERT INTO `sys_area` VALUES (522631, 522600, 3, '黎平县', '0', 100);
INSERT INTO `sys_area` VALUES (522632, 522600, 3, '榕江县', '0', 100);
INSERT INTO `sys_area` VALUES (522633, 522600, 3, '从江县', '0', 100);
INSERT INTO `sys_area` VALUES (522634, 522600, 3, '雷山县', '0', 100);
INSERT INTO `sys_area` VALUES (522635, 522600, 3, '麻江县', '0', 100);
INSERT INTO `sys_area` VALUES (522636, 522600, 3, '丹寨县', '0', 100);
INSERT INTO `sys_area` VALUES (522700, 520000, 2, '黔南布依族苗族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (522701, 522700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (522702, 522700, 3, '福泉市', '0', 100);
INSERT INTO `sys_area` VALUES (522722, 522700, 3, '荔波县', '0', 100);
INSERT INTO `sys_area` VALUES (522723, 522700, 3, '贵定县', '0', 100);
INSERT INTO `sys_area` VALUES (522725, 522700, 3, '瓮安县', '0', 100);
INSERT INTO `sys_area` VALUES (522726, 522700, 3, '独山县', '0', 100);
INSERT INTO `sys_area` VALUES (522727, 522700, 3, '平塘县', '0', 100);
INSERT INTO `sys_area` VALUES (522728, 522700, 3, '罗甸县', '0', 100);
INSERT INTO `sys_area` VALUES (522729, 522700, 3, '长顺县', '0', 100);
INSERT INTO `sys_area` VALUES (522730, 522700, 3, '龙里县', '0', 100);
INSERT INTO `sys_area` VALUES (522731, 522700, 3, '惠水县', '0', 100);
INSERT INTO `sys_area` VALUES (522732, 522700, 3, '三都水族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530000, 0, 1, '云南省', '0', 100);
INSERT INTO `sys_area` VALUES (530100, 530000, 2, '昆明市', '0', 100);
INSERT INTO `sys_area` VALUES (530101, 530100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (530102, 530100, 3, '五华区', '0', 100);
INSERT INTO `sys_area` VALUES (530103, 530100, 3, '盘龙区', '0', 100);
INSERT INTO `sys_area` VALUES (530111, 530100, 3, '官渡区', '0', 100);
INSERT INTO `sys_area` VALUES (530112, 530100, 3, '西山区', '0', 100);
INSERT INTO `sys_area` VALUES (530113, 530100, 3, '东川区', '0', 100);
INSERT INTO `sys_area` VALUES (530114, 530100, 3, '呈贡区', '0', 100);
INSERT INTO `sys_area` VALUES (530122, 530100, 3, '晋宁县', '0', 100);
INSERT INTO `sys_area` VALUES (530124, 530100, 3, '富民县', '0', 100);
INSERT INTO `sys_area` VALUES (530125, 530100, 3, '宜良县', '0', 100);
INSERT INTO `sys_area` VALUES (530126, 530100, 3, '石林彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530127, 530100, 3, '嵩明县', '0', 100);
INSERT INTO `sys_area` VALUES (530128, 530100, 3, '禄劝彝族苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530129, 530100, 3, '寻甸回族彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530181, 530100, 3, '安宁市', '0', 100);
INSERT INTO `sys_area` VALUES (530300, 530000, 2, '曲靖市', '0', 100);
INSERT INTO `sys_area` VALUES (530301, 530300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (530302, 530300, 3, '麒麟区', '0', 100);
INSERT INTO `sys_area` VALUES (530303, 530300, 3, '沾益区', '0', 100);
INSERT INTO `sys_area` VALUES (530321, 530300, 3, '马龙县', '0', 100);
INSERT INTO `sys_area` VALUES (530322, 530300, 3, '陆良县', '0', 100);
INSERT INTO `sys_area` VALUES (530323, 530300, 3, '师宗县', '0', 100);
INSERT INTO `sys_area` VALUES (530324, 530300, 3, '罗平县', '0', 100);
INSERT INTO `sys_area` VALUES (530325, 530300, 3, '富源县', '0', 100);
INSERT INTO `sys_area` VALUES (530326, 530300, 3, '会泽县', '0', 100);
INSERT INTO `sys_area` VALUES (530381, 530300, 3, '宣威市', '0', 100);
INSERT INTO `sys_area` VALUES (530400, 530000, 2, '玉溪市', '0', 100);
INSERT INTO `sys_area` VALUES (530401, 530400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (530402, 530400, 3, '红塔区', '0', 100);
INSERT INTO `sys_area` VALUES (530403, 530400, 3, '江川区', '0', 100);
INSERT INTO `sys_area` VALUES (530422, 530400, 3, '澄江县', '0', 100);
INSERT INTO `sys_area` VALUES (530423, 530400, 3, '通海县', '0', 100);
INSERT INTO `sys_area` VALUES (530424, 530400, 3, '华宁县', '0', 100);
INSERT INTO `sys_area` VALUES (530425, 530400, 3, '易门县', '0', 100);
INSERT INTO `sys_area` VALUES (530426, 530400, 3, '峨山彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530427, 530400, 3, '新平彝族傣族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530428, 530400, 3, '元江哈尼族彝族傣族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530500, 530000, 2, '保山市', '0', 100);
INSERT INTO `sys_area` VALUES (530501, 530500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (530502, 530500, 3, '隆阳区', '0', 100);
INSERT INTO `sys_area` VALUES (530521, 530500, 3, '施甸县', '0', 100);
INSERT INTO `sys_area` VALUES (530523, 530500, 3, '龙陵县', '0', 100);
INSERT INTO `sys_area` VALUES (530524, 530500, 3, '昌宁县', '0', 100);
INSERT INTO `sys_area` VALUES (530581, 530500, 3, '腾冲市', '0', 100);
INSERT INTO `sys_area` VALUES (530600, 530000, 2, '昭通市', '0', 100);
INSERT INTO `sys_area` VALUES (530601, 530600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (530602, 530600, 3, '昭阳区', '0', 100);
INSERT INTO `sys_area` VALUES (530621, 530600, 3, '鲁甸县', '0', 100);
INSERT INTO `sys_area` VALUES (530622, 530600, 3, '巧家县', '0', 100);
INSERT INTO `sys_area` VALUES (530623, 530600, 3, '盐津县', '0', 100);
INSERT INTO `sys_area` VALUES (530624, 530600, 3, '大关县', '0', 100);
INSERT INTO `sys_area` VALUES (530625, 530600, 3, '永善县', '0', 100);
INSERT INTO `sys_area` VALUES (530626, 530600, 3, '绥江县', '0', 100);
INSERT INTO `sys_area` VALUES (530627, 530600, 3, '镇雄县', '0', 100);
INSERT INTO `sys_area` VALUES (530628, 530600, 3, '彝良县', '0', 100);
INSERT INTO `sys_area` VALUES (530629, 530600, 3, '威信县', '0', 100);
INSERT INTO `sys_area` VALUES (530630, 530600, 3, '水富县', '0', 100);
INSERT INTO `sys_area` VALUES (530700, 530000, 2, '丽江市', '0', 100);
INSERT INTO `sys_area` VALUES (530701, 530700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (530702, 530700, 3, '古城区', '0', 100);
INSERT INTO `sys_area` VALUES (530721, 530700, 3, '玉龙纳西族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530722, 530700, 3, '永胜县', '0', 100);
INSERT INTO `sys_area` VALUES (530723, 530700, 3, '华坪县', '0', 100);
INSERT INTO `sys_area` VALUES (530724, 530700, 3, '宁蒗彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530800, 530000, 2, '普洱市', '0', 100);
INSERT INTO `sys_area` VALUES (530801, 530800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (530802, 530800, 3, '思茅区', '0', 100);
INSERT INTO `sys_area` VALUES (530821, 530800, 3, '宁洱哈尼族彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530822, 530800, 3, '墨江哈尼族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530823, 530800, 3, '景东彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530824, 530800, 3, '景谷傣族彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530825, 530800, 3, '镇沅彝族哈尼族拉祜族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530826, 530800, 3, '江城哈尼族彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530827, 530800, 3, '孟连傣族拉祜族佤族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530828, 530800, 3, '澜沧拉祜族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530829, 530800, 3, '西盟佤族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530900, 530000, 2, '临沧市', '0', 100);
INSERT INTO `sys_area` VALUES (530901, 530900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (530902, 530900, 3, '临翔区', '0', 100);
INSERT INTO `sys_area` VALUES (530921, 530900, 3, '凤庆县', '0', 100);
INSERT INTO `sys_area` VALUES (530922, 530900, 3, '云县', '0', 100);
INSERT INTO `sys_area` VALUES (530923, 530900, 3, '永德县', '0', 100);
INSERT INTO `sys_area` VALUES (530924, 530900, 3, '镇康县', '0', 100);
INSERT INTO `sys_area` VALUES (530925, 530900, 3, '双江拉祜族佤族布朗族傣族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530926, 530900, 3, '耿马傣族佤族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (530927, 530900, 3, '沧源佤族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (532300, 530000, 2, '楚雄彝族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (532301, 532300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (532322, 532300, 3, '双柏县', '0', 100);
INSERT INTO `sys_area` VALUES (532323, 532300, 3, '牟定县', '0', 100);
INSERT INTO `sys_area` VALUES (532324, 532300, 3, '南华县', '0', 100);
INSERT INTO `sys_area` VALUES (532325, 532300, 3, '姚安县', '0', 100);
INSERT INTO `sys_area` VALUES (532326, 532300, 3, '大姚县', '0', 100);
INSERT INTO `sys_area` VALUES (532327, 532300, 3, '永仁县', '0', 100);
INSERT INTO `sys_area` VALUES (532328, 532300, 3, '元谋县', '0', 100);
INSERT INTO `sys_area` VALUES (532329, 532300, 3, '武定县', '0', 100);
INSERT INTO `sys_area` VALUES (532331, 532300, 3, '禄丰县', '0', 100);
INSERT INTO `sys_area` VALUES (532500, 530000, 2, '红河哈尼族彝族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (532501, 532500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (532502, 532500, 3, '开远市', '0', 100);
INSERT INTO `sys_area` VALUES (532503, 532500, 3, '蒙自市', '0', 100);
INSERT INTO `sys_area` VALUES (532504, 532500, 3, '弥勒市', '0', 100);
INSERT INTO `sys_area` VALUES (532523, 532500, 3, '屏边苗族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (532524, 532500, 3, '建水县', '0', 100);
INSERT INTO `sys_area` VALUES (532525, 532500, 3, '石屏县', '0', 100);
INSERT INTO `sys_area` VALUES (532527, 532500, 3, '泸西县', '0', 100);
INSERT INTO `sys_area` VALUES (532528, 532500, 3, '元阳县', '0', 100);
INSERT INTO `sys_area` VALUES (532529, 532500, 3, '红河县', '0', 100);
INSERT INTO `sys_area` VALUES (532530, 532500, 3, '金平苗族瑶族傣族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (532531, 532500, 3, '绿春县', '0', 100);
INSERT INTO `sys_area` VALUES (532532, 532500, 3, '河口瑶族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (532600, 530000, 2, '文山壮族苗族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (532601, 532600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (532622, 532600, 3, '砚山县', '0', 100);
INSERT INTO `sys_area` VALUES (532623, 532600, 3, '西畴县', '0', 100);
INSERT INTO `sys_area` VALUES (532624, 532600, 3, '麻栗坡县', '0', 100);
INSERT INTO `sys_area` VALUES (532625, 532600, 3, '马关县', '0', 100);
INSERT INTO `sys_area` VALUES (532626, 532600, 3, '丘北县', '0', 100);
INSERT INTO `sys_area` VALUES (532627, 532600, 3, '广南县', '0', 100);
INSERT INTO `sys_area` VALUES (532628, 532600, 3, '富宁县', '0', 100);
INSERT INTO `sys_area` VALUES (532800, 530000, 2, '西双版纳傣族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (532801, 532800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (532822, 532800, 3, '勐海县', '0', 100);
INSERT INTO `sys_area` VALUES (532823, 532800, 3, '勐腊县', '0', 100);
INSERT INTO `sys_area` VALUES (532900, 530000, 2, '大理白族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (532901, 532900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (532922, 532900, 3, '漾濞彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (532923, 532900, 3, '祥云县', '0', 100);
INSERT INTO `sys_area` VALUES (532924, 532900, 3, '宾川县', '0', 100);
INSERT INTO `sys_area` VALUES (532925, 532900, 3, '弥渡县', '0', 100);
INSERT INTO `sys_area` VALUES (532926, 532900, 3, '南涧彝族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (532927, 532900, 3, '巍山彝族回族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (532928, 532900, 3, '永平县', '0', 100);
INSERT INTO `sys_area` VALUES (532929, 532900, 3, '云龙县', '0', 100);
INSERT INTO `sys_area` VALUES (532930, 532900, 3, '洱源县', '0', 100);
INSERT INTO `sys_area` VALUES (532931, 532900, 3, '剑川县', '0', 100);
INSERT INTO `sys_area` VALUES (532932, 532900, 3, '鹤庆县', '0', 100);
INSERT INTO `sys_area` VALUES (533100, 530000, 2, '德宏傣族景颇族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (533102, 533100, 3, '瑞丽市', '0', 100);
INSERT INTO `sys_area` VALUES (533103, 533100, 3, '芒市', '0', 100);
INSERT INTO `sys_area` VALUES (533122, 533100, 3, '梁河县', '0', 100);
INSERT INTO `sys_area` VALUES (533123, 533100, 3, '盈江县', '0', 100);
INSERT INTO `sys_area` VALUES (533124, 533100, 3, '陇川县', '0', 100);
INSERT INTO `sys_area` VALUES (533300, 530000, 2, '怒江傈僳族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (533301, 533300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (533323, 533300, 3, '福贡县', '0', 100);
INSERT INTO `sys_area` VALUES (533324, 533300, 3, '贡山独龙族怒族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (533325, 533300, 3, '兰坪白族普米族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (533400, 530000, 2, '迪庆藏族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (533401, 533400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (533422, 533400, 3, '德钦县', '0', 100);
INSERT INTO `sys_area` VALUES (533423, 533400, 3, '维西傈僳族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (540000, 0, 1, '西藏自治区', '0', 100);
INSERT INTO `sys_area` VALUES (540100, 540000, 2, '拉萨市', '0', 100);
INSERT INTO `sys_area` VALUES (540101, 540100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (540102, 540100, 3, '城关区', '0', 100);
INSERT INTO `sys_area` VALUES (540103, 540100, 3, '堆龙德庆区', '0', 100);
INSERT INTO `sys_area` VALUES (540121, 540100, 3, '林周县', '0', 100);
INSERT INTO `sys_area` VALUES (540122, 540100, 3, '当雄县', '0', 100);
INSERT INTO `sys_area` VALUES (540123, 540100, 3, '尼木县', '0', 100);
INSERT INTO `sys_area` VALUES (540124, 540100, 3, '曲水县', '0', 100);
INSERT INTO `sys_area` VALUES (540126, 540100, 3, '达孜县', '0', 100);
INSERT INTO `sys_area` VALUES (540127, 540100, 3, '墨竹工卡县', '0', 100);
INSERT INTO `sys_area` VALUES (540200, 540000, 2, '日喀则市', '0', 100);
INSERT INTO `sys_area` VALUES (540202, 540200, 3, '桑珠孜区', '0', 100);
INSERT INTO `sys_area` VALUES (540221, 540200, 3, '南木林县', '0', 100);
INSERT INTO `sys_area` VALUES (540222, 540200, 3, '江孜县', '0', 100);
INSERT INTO `sys_area` VALUES (540223, 540200, 3, '定日县', '0', 100);
INSERT INTO `sys_area` VALUES (540224, 540200, 3, '萨迦县', '0', 100);
INSERT INTO `sys_area` VALUES (540225, 540200, 3, '拉孜县', '0', 100);
INSERT INTO `sys_area` VALUES (540226, 540200, 3, '昂仁县', '0', 100);
INSERT INTO `sys_area` VALUES (540227, 540200, 3, '谢通门县', '0', 100);
INSERT INTO `sys_area` VALUES (540228, 540200, 3, '白朗县', '0', 100);
INSERT INTO `sys_area` VALUES (540229, 540200, 3, '仁布县', '0', 100);
INSERT INTO `sys_area` VALUES (540230, 540200, 3, '康马县', '0', 100);
INSERT INTO `sys_area` VALUES (540231, 540200, 3, '定结县', '0', 100);
INSERT INTO `sys_area` VALUES (540232, 540200, 3, '仲巴县', '0', 100);
INSERT INTO `sys_area` VALUES (540233, 540200, 3, '亚东县', '0', 100);
INSERT INTO `sys_area` VALUES (540234, 540200, 3, '吉隆县', '0', 100);
INSERT INTO `sys_area` VALUES (540235, 540200, 3, '聂拉木县', '0', 100);
INSERT INTO `sys_area` VALUES (540236, 540200, 3, '萨嘎县', '0', 100);
INSERT INTO `sys_area` VALUES (540237, 540200, 3, '岗巴县', '0', 100);
INSERT INTO `sys_area` VALUES (540300, 540000, 2, '昌都市', '0', 100);
INSERT INTO `sys_area` VALUES (540302, 540300, 3, '卡若区', '0', 100);
INSERT INTO `sys_area` VALUES (540321, 540300, 3, '江达县', '0', 100);
INSERT INTO `sys_area` VALUES (540322, 540300, 3, '贡觉县', '0', 100);
INSERT INTO `sys_area` VALUES (540323, 540300, 3, '类乌齐县', '0', 100);
INSERT INTO `sys_area` VALUES (540324, 540300, 3, '丁青县', '0', 100);
INSERT INTO `sys_area` VALUES (540325, 540300, 3, '察雅县', '0', 100);
INSERT INTO `sys_area` VALUES (540326, 540300, 3, '八宿县', '0', 100);
INSERT INTO `sys_area` VALUES (540327, 540300, 3, '左贡县', '0', 100);
INSERT INTO `sys_area` VALUES (540328, 540300, 3, '芒康县', '0', 100);
INSERT INTO `sys_area` VALUES (540329, 540300, 3, '洛隆县', '0', 100);
INSERT INTO `sys_area` VALUES (540330, 540300, 3, '边坝县', '0', 100);
INSERT INTO `sys_area` VALUES (540400, 540000, 2, '林芝市', '0', 100);
INSERT INTO `sys_area` VALUES (540402, 540400, 3, '巴宜区', '0', 100);
INSERT INTO `sys_area` VALUES (540421, 540400, 3, '工布江达县', '0', 100);
INSERT INTO `sys_area` VALUES (540422, 540400, 3, '米林县', '0', 100);
INSERT INTO `sys_area` VALUES (540423, 540400, 3, '墨脱县', '0', 100);
INSERT INTO `sys_area` VALUES (540424, 540400, 3, '波密县', '0', 100);
INSERT INTO `sys_area` VALUES (540425, 540400, 3, '察隅县', '0', 100);
INSERT INTO `sys_area` VALUES (540426, 540400, 3, '朗县', '0', 100);
INSERT INTO `sys_area` VALUES (540500, 540000, 2, '山南市', '0', 100);
INSERT INTO `sys_area` VALUES (540501, 540500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (540502, 540500, 3, '乃东区', '0', 100);
INSERT INTO `sys_area` VALUES (540521, 540500, 3, '扎囊县', '0', 100);
INSERT INTO `sys_area` VALUES (540522, 540500, 3, '贡嘎县', '0', 100);
INSERT INTO `sys_area` VALUES (540523, 540500, 3, '桑日县', '0', 100);
INSERT INTO `sys_area` VALUES (540524, 540500, 3, '琼结县', '0', 100);
INSERT INTO `sys_area` VALUES (540525, 540500, 3, '曲松县', '0', 100);
INSERT INTO `sys_area` VALUES (540526, 540500, 3, '措美县', '0', 100);
INSERT INTO `sys_area` VALUES (540527, 540500, 3, '洛扎县', '0', 100);
INSERT INTO `sys_area` VALUES (540528, 540500, 3, '加查县', '0', 100);
INSERT INTO `sys_area` VALUES (540529, 540500, 3, '隆子县', '0', 100);
INSERT INTO `sys_area` VALUES (540530, 540500, 3, '错那县', '0', 100);
INSERT INTO `sys_area` VALUES (540531, 540500, 3, '浪卡子县', '0', 100);
INSERT INTO `sys_area` VALUES (542400, 540000, 2, '那曲地区', '0', 100);
INSERT INTO `sys_area` VALUES (542421, 542400, 3, '那曲县', '0', 100);
INSERT INTO `sys_area` VALUES (542422, 542400, 3, '嘉黎县', '0', 100);
INSERT INTO `sys_area` VALUES (542423, 542400, 3, '比如县', '0', 100);
INSERT INTO `sys_area` VALUES (542424, 542400, 3, '聂荣县', '0', 100);
INSERT INTO `sys_area` VALUES (542425, 542400, 3, '安多县', '0', 100);
INSERT INTO `sys_area` VALUES (542426, 542400, 3, '申扎县', '0', 100);
INSERT INTO `sys_area` VALUES (542427, 542400, 3, '索县', '0', 100);
INSERT INTO `sys_area` VALUES (542428, 542400, 3, '班戈县', '0', 100);
INSERT INTO `sys_area` VALUES (542429, 542400, 3, '巴青县', '0', 100);
INSERT INTO `sys_area` VALUES (542430, 542400, 3, '尼玛县', '0', 100);
INSERT INTO `sys_area` VALUES (542431, 542400, 3, '双湖县', '0', 100);
INSERT INTO `sys_area` VALUES (542500, 540000, 2, '阿里地区', '0', 100);
INSERT INTO `sys_area` VALUES (542521, 542500, 3, '普兰县', '0', 100);
INSERT INTO `sys_area` VALUES (542522, 542500, 3, '札达县', '0', 100);
INSERT INTO `sys_area` VALUES (542523, 542500, 3, '噶尔县', '0', 100);
INSERT INTO `sys_area` VALUES (542524, 542500, 3, '日土县', '0', 100);
INSERT INTO `sys_area` VALUES (542525, 542500, 3, '革吉县', '0', 100);
INSERT INTO `sys_area` VALUES (542526, 542500, 3, '改则县', '0', 100);
INSERT INTO `sys_area` VALUES (542527, 542500, 3, '措勤县', '0', 100);
INSERT INTO `sys_area` VALUES (610000, 0, 1, '陕西省', '0', 100);
INSERT INTO `sys_area` VALUES (610100, 610000, 2, '西安市', '0', 100);
INSERT INTO `sys_area` VALUES (610101, 610100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (610102, 610100, 3, '新城区', '0', 100);
INSERT INTO `sys_area` VALUES (610103, 610100, 3, '碑林区', '0', 100);
INSERT INTO `sys_area` VALUES (610104, 610100, 3, '莲湖区', '0', 100);
INSERT INTO `sys_area` VALUES (610111, 610100, 3, '灞桥区', '0', 100);
INSERT INTO `sys_area` VALUES (610112, 610100, 3, '未央区', '0', 100);
INSERT INTO `sys_area` VALUES (610113, 610100, 3, '雁塔区', '0', 100);
INSERT INTO `sys_area` VALUES (610114, 610100, 3, '阎良区', '0', 100);
INSERT INTO `sys_area` VALUES (610115, 610100, 3, '临潼区', '0', 100);
INSERT INTO `sys_area` VALUES (610116, 610100, 3, '长安区', '0', 100);
INSERT INTO `sys_area` VALUES (610117, 610100, 3, '高陵区', '0', 100);
INSERT INTO `sys_area` VALUES (610122, 610100, 3, '蓝田县', '0', 100);
INSERT INTO `sys_area` VALUES (610124, 610100, 3, '周至县', '0', 100);
INSERT INTO `sys_area` VALUES (610125, 610100, 3, '户县', '0', 100);
INSERT INTO `sys_area` VALUES (610200, 610000, 2, '铜川市', '0', 100);
INSERT INTO `sys_area` VALUES (610201, 610200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (610202, 610200, 3, '王益区', '0', 100);
INSERT INTO `sys_area` VALUES (610203, 610200, 3, '印台区', '0', 100);
INSERT INTO `sys_area` VALUES (610204, 610200, 3, '耀州区', '0', 100);
INSERT INTO `sys_area` VALUES (610222, 610200, 3, '宜君县', '0', 100);
INSERT INTO `sys_area` VALUES (610300, 610000, 2, '宝鸡市', '0', 100);
INSERT INTO `sys_area` VALUES (610301, 610300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (610302, 610300, 3, '渭滨区', '0', 100);
INSERT INTO `sys_area` VALUES (610303, 610300, 3, '金台区', '0', 100);
INSERT INTO `sys_area` VALUES (610304, 610300, 3, '陈仓区', '0', 100);
INSERT INTO `sys_area` VALUES (610322, 610300, 3, '凤翔县', '0', 100);
INSERT INTO `sys_area` VALUES (610323, 610300, 3, '岐山县', '0', 100);
INSERT INTO `sys_area` VALUES (610324, 610300, 3, '扶风县', '0', 100);
INSERT INTO `sys_area` VALUES (610326, 610300, 3, '眉县', '0', 100);
INSERT INTO `sys_area` VALUES (610327, 610300, 3, '陇县', '0', 100);
INSERT INTO `sys_area` VALUES (610328, 610300, 3, '千阳县', '0', 100);
INSERT INTO `sys_area` VALUES (610329, 610300, 3, '麟游县', '0', 100);
INSERT INTO `sys_area` VALUES (610330, 610300, 3, '凤县', '0', 100);
INSERT INTO `sys_area` VALUES (610331, 610300, 3, '太白县', '0', 100);
INSERT INTO `sys_area` VALUES (610400, 610000, 2, '咸阳市', '0', 100);
INSERT INTO `sys_area` VALUES (610401, 610400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (610402, 610400, 3, '秦都区', '0', 100);
INSERT INTO `sys_area` VALUES (610403, 610400, 3, '杨陵区', '0', 100);
INSERT INTO `sys_area` VALUES (610404, 610400, 3, '渭城区', '0', 100);
INSERT INTO `sys_area` VALUES (610422, 610400, 3, '三原县', '0', 100);
INSERT INTO `sys_area` VALUES (610423, 610400, 3, '泾阳县', '0', 100);
INSERT INTO `sys_area` VALUES (610424, 610400, 3, '乾县', '0', 100);
INSERT INTO `sys_area` VALUES (610425, 610400, 3, '礼泉县', '0', 100);
INSERT INTO `sys_area` VALUES (610426, 610400, 3, '永寿县', '0', 100);
INSERT INTO `sys_area` VALUES (610427, 610400, 3, '彬县', '0', 100);
INSERT INTO `sys_area` VALUES (610428, 610400, 3, '长武县', '0', 100);
INSERT INTO `sys_area` VALUES (610429, 610400, 3, '旬邑县', '0', 100);
INSERT INTO `sys_area` VALUES (610430, 610400, 3, '淳化县', '0', 100);
INSERT INTO `sys_area` VALUES (610431, 610400, 3, '武功县', '0', 100);
INSERT INTO `sys_area` VALUES (610481, 610400, 3, '兴平市', '0', 100);
INSERT INTO `sys_area` VALUES (610500, 610000, 2, '渭南市', '0', 100);
INSERT INTO `sys_area` VALUES (610501, 610500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (610502, 610500, 3, '临渭区', '0', 100);
INSERT INTO `sys_area` VALUES (610503, 610500, 3, '华州区', '0', 100);
INSERT INTO `sys_area` VALUES (610522, 610500, 3, '潼关县', '0', 100);
INSERT INTO `sys_area` VALUES (610523, 610500, 3, '大荔县', '0', 100);
INSERT INTO `sys_area` VALUES (610524, 610500, 3, '合阳县', '0', 100);
INSERT INTO `sys_area` VALUES (610525, 610500, 3, '澄城县', '0', 100);
INSERT INTO `sys_area` VALUES (610526, 610500, 3, '蒲城县', '0', 100);
INSERT INTO `sys_area` VALUES (610527, 610500, 3, '白水县', '0', 100);
INSERT INTO `sys_area` VALUES (610528, 610500, 3, '富平县', '0', 100);
INSERT INTO `sys_area` VALUES (610581, 610500, 3, '韩城市', '0', 100);
INSERT INTO `sys_area` VALUES (610582, 610500, 3, '华阴市', '0', 100);
INSERT INTO `sys_area` VALUES (610600, 610000, 2, '延安市', '0', 100);
INSERT INTO `sys_area` VALUES (610601, 610600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (610602, 610600, 3, '宝塔区', '0', 100);
INSERT INTO `sys_area` VALUES (610603, 610600, 3, '安塞区', '0', 100);
INSERT INTO `sys_area` VALUES (610621, 610600, 3, '延长县', '0', 100);
INSERT INTO `sys_area` VALUES (610622, 610600, 3, '延川县', '0', 100);
INSERT INTO `sys_area` VALUES (610623, 610600, 3, '子长县', '0', 100);
INSERT INTO `sys_area` VALUES (610625, 610600, 3, '志丹县', '0', 100);
INSERT INTO `sys_area` VALUES (610626, 610600, 3, '吴起县', '0', 100);
INSERT INTO `sys_area` VALUES (610627, 610600, 3, '甘泉县', '0', 100);
INSERT INTO `sys_area` VALUES (610628, 610600, 3, '富县', '0', 100);
INSERT INTO `sys_area` VALUES (610629, 610600, 3, '洛川县', '0', 100);
INSERT INTO `sys_area` VALUES (610630, 610600, 3, '宜川县', '0', 100);
INSERT INTO `sys_area` VALUES (610631, 610600, 3, '黄龙县', '0', 100);
INSERT INTO `sys_area` VALUES (610632, 610600, 3, '黄陵县', '0', 100);
INSERT INTO `sys_area` VALUES (610700, 610000, 2, '汉中市', '0', 100);
INSERT INTO `sys_area` VALUES (610701, 610700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (610702, 610700, 3, '汉台区', '0', 100);
INSERT INTO `sys_area` VALUES (610721, 610700, 3, '南郑县', '0', 100);
INSERT INTO `sys_area` VALUES (610722, 610700, 3, '城固县', '0', 100);
INSERT INTO `sys_area` VALUES (610723, 610700, 3, '洋县', '0', 100);
INSERT INTO `sys_area` VALUES (610724, 610700, 3, '西乡县', '0', 100);
INSERT INTO `sys_area` VALUES (610725, 610700, 3, '勉县', '0', 100);
INSERT INTO `sys_area` VALUES (610726, 610700, 3, '宁强县', '0', 100);
INSERT INTO `sys_area` VALUES (610727, 610700, 3, '略阳县', '0', 100);
INSERT INTO `sys_area` VALUES (610728, 610700, 3, '镇巴县', '0', 100);
INSERT INTO `sys_area` VALUES (610729, 610700, 3, '留坝县', '0', 100);
INSERT INTO `sys_area` VALUES (610730, 610700, 3, '佛坪县', '0', 100);
INSERT INTO `sys_area` VALUES (610800, 610000, 2, '榆林市', '0', 100);
INSERT INTO `sys_area` VALUES (610801, 610800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (610802, 610800, 3, '榆阳区', '0', 100);
INSERT INTO `sys_area` VALUES (610803, 610800, 3, '横山区', '0', 100);
INSERT INTO `sys_area` VALUES (610821, 610800, 3, '神木县', '0', 100);
INSERT INTO `sys_area` VALUES (610822, 610800, 3, '府谷县', '0', 100);
INSERT INTO `sys_area` VALUES (610824, 610800, 3, '靖边县', '0', 100);
INSERT INTO `sys_area` VALUES (610825, 610800, 3, '定边县', '0', 100);
INSERT INTO `sys_area` VALUES (610826, 610800, 3, '绥德县', '0', 100);
INSERT INTO `sys_area` VALUES (610827, 610800, 3, '米脂县', '0', 100);
INSERT INTO `sys_area` VALUES (610828, 610800, 3, '佳县', '0', 100);
INSERT INTO `sys_area` VALUES (610829, 610800, 3, '吴堡县', '0', 100);
INSERT INTO `sys_area` VALUES (610830, 610800, 3, '清涧县', '0', 100);
INSERT INTO `sys_area` VALUES (610831, 610800, 3, '子洲县', '0', 100);
INSERT INTO `sys_area` VALUES (610900, 610000, 2, '安康市', '0', 100);
INSERT INTO `sys_area` VALUES (610901, 610900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (610902, 610900, 3, '汉滨区', '0', 100);
INSERT INTO `sys_area` VALUES (610921, 610900, 3, '汉阴县', '0', 100);
INSERT INTO `sys_area` VALUES (610922, 610900, 3, '石泉县', '0', 100);
INSERT INTO `sys_area` VALUES (610923, 610900, 3, '宁陕县', '0', 100);
INSERT INTO `sys_area` VALUES (610924, 610900, 3, '紫阳县', '0', 100);
INSERT INTO `sys_area` VALUES (610925, 610900, 3, '岚皋县', '0', 100);
INSERT INTO `sys_area` VALUES (610926, 610900, 3, '平利县', '0', 100);
INSERT INTO `sys_area` VALUES (610927, 610900, 3, '镇坪县', '0', 100);
INSERT INTO `sys_area` VALUES (610928, 610900, 3, '旬阳县', '0', 100);
INSERT INTO `sys_area` VALUES (610929, 610900, 3, '白河县', '0', 100);
INSERT INTO `sys_area` VALUES (611000, 610000, 2, '商洛市', '0', 100);
INSERT INTO `sys_area` VALUES (611001, 611000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (611002, 611000, 3, '商州区', '0', 100);
INSERT INTO `sys_area` VALUES (611021, 611000, 3, '洛南县', '0', 100);
INSERT INTO `sys_area` VALUES (611022, 611000, 3, '丹凤县', '0', 100);
INSERT INTO `sys_area` VALUES (611023, 611000, 3, '商南县', '0', 100);
INSERT INTO `sys_area` VALUES (611024, 611000, 3, '山阳县', '0', 100);
INSERT INTO `sys_area` VALUES (611025, 611000, 3, '镇安县', '0', 100);
INSERT INTO `sys_area` VALUES (611026, 611000, 3, '柞水县', '0', 100);
INSERT INTO `sys_area` VALUES (620000, 0, 1, '甘肃省', '0', 100);
INSERT INTO `sys_area` VALUES (620100, 620000, 2, '兰州市', '0', 100);
INSERT INTO `sys_area` VALUES (620101, 620100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (620102, 620100, 3, '城关区', '0', 100);
INSERT INTO `sys_area` VALUES (620103, 620100, 3, '七里河区', '0', 100);
INSERT INTO `sys_area` VALUES (620104, 620100, 3, '西固区', '0', 100);
INSERT INTO `sys_area` VALUES (620105, 620100, 3, '安宁区', '0', 100);
INSERT INTO `sys_area` VALUES (620111, 620100, 3, '红古区', '0', 100);
INSERT INTO `sys_area` VALUES (620121, 620100, 3, '永登县', '0', 100);
INSERT INTO `sys_area` VALUES (620122, 620100, 3, '皋兰县', '0', 100);
INSERT INTO `sys_area` VALUES (620123, 620100, 3, '榆中县', '0', 100);
INSERT INTO `sys_area` VALUES (620200, 620000, 2, '嘉峪关市', '0', 100);
INSERT INTO `sys_area` VALUES (620201, 620200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (620300, 620000, 2, '金昌市', '0', 100);
INSERT INTO `sys_area` VALUES (620301, 620300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (620302, 620300, 3, '金川区', '0', 100);
INSERT INTO `sys_area` VALUES (620321, 620300, 3, '永昌县', '0', 100);
INSERT INTO `sys_area` VALUES (620400, 620000, 2, '白银市', '0', 100);
INSERT INTO `sys_area` VALUES (620401, 620400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (620402, 620400, 3, '白银区', '0', 100);
INSERT INTO `sys_area` VALUES (620403, 620400, 3, '平川区', '0', 100);
INSERT INTO `sys_area` VALUES (620421, 620400, 3, '靖远县', '0', 100);
INSERT INTO `sys_area` VALUES (620422, 620400, 3, '会宁县', '0', 100);
INSERT INTO `sys_area` VALUES (620423, 620400, 3, '景泰县', '0', 100);
INSERT INTO `sys_area` VALUES (620500, 620000, 2, '天水市', '0', 100);
INSERT INTO `sys_area` VALUES (620501, 620500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (620502, 620500, 3, '秦州区', '0', 100);
INSERT INTO `sys_area` VALUES (620503, 620500, 3, '麦积区', '0', 100);
INSERT INTO `sys_area` VALUES (620521, 620500, 3, '清水县', '0', 100);
INSERT INTO `sys_area` VALUES (620522, 620500, 3, '秦安县', '0', 100);
INSERT INTO `sys_area` VALUES (620523, 620500, 3, '甘谷县', '0', 100);
INSERT INTO `sys_area` VALUES (620524, 620500, 3, '武山县', '0', 100);
INSERT INTO `sys_area` VALUES (620525, 620500, 3, '张家川回族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (620600, 620000, 2, '武威市', '0', 100);
INSERT INTO `sys_area` VALUES (620601, 620600, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (620602, 620600, 3, '凉州区', '0', 100);
INSERT INTO `sys_area` VALUES (620621, 620600, 3, '民勤县', '0', 100);
INSERT INTO `sys_area` VALUES (620622, 620600, 3, '古浪县', '0', 100);
INSERT INTO `sys_area` VALUES (620623, 620600, 3, '天祝藏族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (620700, 620000, 2, '张掖市', '0', 100);
INSERT INTO `sys_area` VALUES (620701, 620700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (620702, 620700, 3, '甘州区', '0', 100);
INSERT INTO `sys_area` VALUES (620721, 620700, 3, '肃南裕固族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (620722, 620700, 3, '民乐县', '0', 100);
INSERT INTO `sys_area` VALUES (620723, 620700, 3, '临泽县', '0', 100);
INSERT INTO `sys_area` VALUES (620724, 620700, 3, '高台县', '0', 100);
INSERT INTO `sys_area` VALUES (620725, 620700, 3, '山丹县', '0', 100);
INSERT INTO `sys_area` VALUES (620800, 620000, 2, '平凉市', '0', 100);
INSERT INTO `sys_area` VALUES (620801, 620800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (620802, 620800, 3, '崆峒区', '0', 100);
INSERT INTO `sys_area` VALUES (620821, 620800, 3, '泾川县', '0', 100);
INSERT INTO `sys_area` VALUES (620822, 620800, 3, '灵台县', '0', 100);
INSERT INTO `sys_area` VALUES (620823, 620800, 3, '崇信县', '0', 100);
INSERT INTO `sys_area` VALUES (620824, 620800, 3, '华亭县', '0', 100);
INSERT INTO `sys_area` VALUES (620825, 620800, 3, '庄浪县', '0', 100);
INSERT INTO `sys_area` VALUES (620826, 620800, 3, '静宁县', '0', 100);
INSERT INTO `sys_area` VALUES (620900, 620000, 2, '酒泉市', '0', 100);
INSERT INTO `sys_area` VALUES (620901, 620900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (620902, 620900, 3, '肃州区', '0', 100);
INSERT INTO `sys_area` VALUES (620921, 620900, 3, '金塔县', '0', 100);
INSERT INTO `sys_area` VALUES (620922, 620900, 3, '瓜州县', '0', 100);
INSERT INTO `sys_area` VALUES (620923, 620900, 3, '肃北蒙古族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (620924, 620900, 3, '阿克塞哈萨克族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (620981, 620900, 3, '玉门市', '0', 100);
INSERT INTO `sys_area` VALUES (620982, 620900, 3, '敦煌市', '0', 100);
INSERT INTO `sys_area` VALUES (621000, 620000, 2, '庆阳市', '0', 100);
INSERT INTO `sys_area` VALUES (621001, 621000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (621002, 621000, 3, '西峰区', '0', 100);
INSERT INTO `sys_area` VALUES (621021, 621000, 3, '庆城县', '0', 100);
INSERT INTO `sys_area` VALUES (621022, 621000, 3, '环县', '0', 100);
INSERT INTO `sys_area` VALUES (621023, 621000, 3, '华池县', '0', 100);
INSERT INTO `sys_area` VALUES (621024, 621000, 3, '合水县', '0', 100);
INSERT INTO `sys_area` VALUES (621025, 621000, 3, '正宁县', '0', 100);
INSERT INTO `sys_area` VALUES (621026, 621000, 3, '宁县', '0', 100);
INSERT INTO `sys_area` VALUES (621027, 621000, 3, '镇原县', '0', 100);
INSERT INTO `sys_area` VALUES (621100, 620000, 2, '定西市', '0', 100);
INSERT INTO `sys_area` VALUES (621101, 621100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (621102, 621100, 3, '安定区', '0', 100);
INSERT INTO `sys_area` VALUES (621121, 621100, 3, '通渭县', '0', 100);
INSERT INTO `sys_area` VALUES (621122, 621100, 3, '陇西县', '0', 100);
INSERT INTO `sys_area` VALUES (621123, 621100, 3, '渭源县', '0', 100);
INSERT INTO `sys_area` VALUES (621124, 621100, 3, '临洮县', '0', 100);
INSERT INTO `sys_area` VALUES (621125, 621100, 3, '漳县', '0', 100);
INSERT INTO `sys_area` VALUES (621126, 621100, 3, '岷县', '0', 100);
INSERT INTO `sys_area` VALUES (621200, 620000, 2, '陇南市', '0', 100);
INSERT INTO `sys_area` VALUES (621201, 621200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (621202, 621200, 3, '武都区', '0', 100);
INSERT INTO `sys_area` VALUES (621221, 621200, 3, '成县', '0', 100);
INSERT INTO `sys_area` VALUES (621222, 621200, 3, '文县', '0', 100);
INSERT INTO `sys_area` VALUES (621223, 621200, 3, '宕昌县', '0', 100);
INSERT INTO `sys_area` VALUES (621224, 621200, 3, '康县', '0', 100);
INSERT INTO `sys_area` VALUES (621225, 621200, 3, '西和县', '0', 100);
INSERT INTO `sys_area` VALUES (621226, 621200, 3, '礼县', '0', 100);
INSERT INTO `sys_area` VALUES (621227, 621200, 3, '徽县', '0', 100);
INSERT INTO `sys_area` VALUES (621228, 621200, 3, '两当县', '0', 100);
INSERT INTO `sys_area` VALUES (622900, 620000, 2, '临夏回族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (622901, 622900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (622921, 622900, 3, '临夏县', '0', 100);
INSERT INTO `sys_area` VALUES (622922, 622900, 3, '康乐县', '0', 100);
INSERT INTO `sys_area` VALUES (622923, 622900, 3, '永靖县', '0', 100);
INSERT INTO `sys_area` VALUES (622924, 622900, 3, '广河县', '0', 100);
INSERT INTO `sys_area` VALUES (622925, 622900, 3, '和政县', '0', 100);
INSERT INTO `sys_area` VALUES (622926, 622900, 3, '东乡族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (622927, 622900, 3, '积石山保安族东乡族撒拉族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (623000, 620000, 2, '甘南藏族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (623001, 623000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (623021, 623000, 3, '临潭县', '0', 100);
INSERT INTO `sys_area` VALUES (623022, 623000, 3, '卓尼县', '0', 100);
INSERT INTO `sys_area` VALUES (623023, 623000, 3, '舟曲县', '0', 100);
INSERT INTO `sys_area` VALUES (623024, 623000, 3, '迭部县', '0', 100);
INSERT INTO `sys_area` VALUES (623025, 623000, 3, '玛曲县', '0', 100);
INSERT INTO `sys_area` VALUES (623026, 623000, 3, '碌曲县', '0', 100);
INSERT INTO `sys_area` VALUES (623027, 623000, 3, '夏河县', '0', 100);
INSERT INTO `sys_area` VALUES (630000, 0, 1, '青海省', '0', 100);
INSERT INTO `sys_area` VALUES (630100, 630000, 2, '西宁市', '0', 100);
INSERT INTO `sys_area` VALUES (630101, 630100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (630102, 630100, 3, '城东区', '0', 100);
INSERT INTO `sys_area` VALUES (630103, 630100, 3, '城中区', '0', 100);
INSERT INTO `sys_area` VALUES (630104, 630100, 3, '城西区', '0', 100);
INSERT INTO `sys_area` VALUES (630105, 630100, 3, '城北区', '0', 100);
INSERT INTO `sys_area` VALUES (630121, 630100, 3, '大通回族土族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (630122, 630100, 3, '湟中县', '0', 100);
INSERT INTO `sys_area` VALUES (630123, 630100, 3, '湟源县', '0', 100);
INSERT INTO `sys_area` VALUES (630200, 630000, 2, '海东市', '0', 100);
INSERT INTO `sys_area` VALUES (630202, 630200, 3, '乐都区', '0', 100);
INSERT INTO `sys_area` VALUES (630203, 630200, 3, '平安区', '0', 100);
INSERT INTO `sys_area` VALUES (630222, 630200, 3, '民和回族土族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (630223, 630200, 3, '互助土族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (630224, 630200, 3, '化隆回族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (630225, 630200, 3, '循化撒拉族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (632200, 630000, 2, '海北藏族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (632221, 632200, 3, '门源回族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (632222, 632200, 3, '祁连县', '0', 100);
INSERT INTO `sys_area` VALUES (632223, 632200, 3, '海晏县', '0', 100);
INSERT INTO `sys_area` VALUES (632224, 632200, 3, '刚察县', '0', 100);
INSERT INTO `sys_area` VALUES (632300, 630000, 2, '黄南藏族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (632321, 632300, 3, '同仁县', '0', 100);
INSERT INTO `sys_area` VALUES (632322, 632300, 3, '尖扎县', '0', 100);
INSERT INTO `sys_area` VALUES (632323, 632300, 3, '泽库县', '0', 100);
INSERT INTO `sys_area` VALUES (632324, 632300, 3, '河南蒙古族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (632500, 630000, 2, '海南藏族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (632521, 632500, 3, '共和县', '0', 100);
INSERT INTO `sys_area` VALUES (632522, 632500, 3, '同德县', '0', 100);
INSERT INTO `sys_area` VALUES (632523, 632500, 3, '贵德县', '0', 100);
INSERT INTO `sys_area` VALUES (632524, 632500, 3, '兴海县', '0', 100);
INSERT INTO `sys_area` VALUES (632525, 632500, 3, '贵南县', '0', 100);
INSERT INTO `sys_area` VALUES (632600, 630000, 2, '果洛藏族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (632621, 632600, 3, '玛沁县', '0', 100);
INSERT INTO `sys_area` VALUES (632622, 632600, 3, '班玛县', '0', 100);
INSERT INTO `sys_area` VALUES (632623, 632600, 3, '甘德县', '0', 100);
INSERT INTO `sys_area` VALUES (632624, 632600, 3, '达日县', '0', 100);
INSERT INTO `sys_area` VALUES (632625, 632600, 3, '久治县', '0', 100);
INSERT INTO `sys_area` VALUES (632626, 632600, 3, '玛多县', '0', 100);
INSERT INTO `sys_area` VALUES (632700, 630000, 2, '玉树藏族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (632701, 632700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (632722, 632700, 3, '杂多县', '0', 100);
INSERT INTO `sys_area` VALUES (632723, 632700, 3, '称多县', '0', 100);
INSERT INTO `sys_area` VALUES (632724, 632700, 3, '治多县', '0', 100);
INSERT INTO `sys_area` VALUES (632725, 632700, 3, '囊谦县', '0', 100);
INSERT INTO `sys_area` VALUES (632726, 632700, 3, '曲麻莱县', '0', 100);
INSERT INTO `sys_area` VALUES (632800, 630000, 2, '海西蒙古族藏族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (632801, 632800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (632802, 632800, 3, '德令哈市', '0', 100);
INSERT INTO `sys_area` VALUES (632821, 632800, 3, '乌兰县', '0', 100);
INSERT INTO `sys_area` VALUES (632822, 632800, 3, '都兰县', '0', 100);
INSERT INTO `sys_area` VALUES (632823, 632800, 3, '天峻县', '0', 100);
INSERT INTO `sys_area` VALUES (640000, 0, 1, '宁夏回族自治区', '0', 100);
INSERT INTO `sys_area` VALUES (640100, 640000, 2, '银川市', '0', 100);
INSERT INTO `sys_area` VALUES (640101, 640100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (640104, 640100, 3, '兴庆区', '0', 100);
INSERT INTO `sys_area` VALUES (640105, 640100, 3, '西夏区', '0', 100);
INSERT INTO `sys_area` VALUES (640106, 640100, 3, '金凤区', '0', 100);
INSERT INTO `sys_area` VALUES (640121, 640100, 3, '永宁县', '0', 100);
INSERT INTO `sys_area` VALUES (640122, 640100, 3, '贺兰县', '0', 100);
INSERT INTO `sys_area` VALUES (640181, 640100, 3, '灵武市', '0', 100);
INSERT INTO `sys_area` VALUES (640200, 640000, 2, '石嘴山市', '0', 100);
INSERT INTO `sys_area` VALUES (640201, 640200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (640202, 640200, 3, '大武口区', '0', 100);
INSERT INTO `sys_area` VALUES (640205, 640200, 3, '惠农区', '0', 100);
INSERT INTO `sys_area` VALUES (640221, 640200, 3, '平罗县', '0', 100);
INSERT INTO `sys_area` VALUES (640300, 640000, 2, '吴忠市', '0', 100);
INSERT INTO `sys_area` VALUES (640301, 640300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (640302, 640300, 3, '利通区', '0', 100);
INSERT INTO `sys_area` VALUES (640303, 640300, 3, '红寺堡区', '0', 100);
INSERT INTO `sys_area` VALUES (640323, 640300, 3, '盐池县', '0', 100);
INSERT INTO `sys_area` VALUES (640324, 640300, 3, '同心县', '0', 100);
INSERT INTO `sys_area` VALUES (640381, 640300, 3, '青铜峡市', '0', 100);
INSERT INTO `sys_area` VALUES (640400, 640000, 2, '固原市', '0', 100);
INSERT INTO `sys_area` VALUES (640401, 640400, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (640402, 640400, 3, '原州区', '0', 100);
INSERT INTO `sys_area` VALUES (640422, 640400, 3, '西吉县', '0', 100);
INSERT INTO `sys_area` VALUES (640423, 640400, 3, '隆德县', '0', 100);
INSERT INTO `sys_area` VALUES (640424, 640400, 3, '泾源县', '0', 100);
INSERT INTO `sys_area` VALUES (640425, 640400, 3, '彭阳县', '0', 100);
INSERT INTO `sys_area` VALUES (640500, 640000, 2, '中卫市', '0', 100);
INSERT INTO `sys_area` VALUES (640501, 640500, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (640502, 640500, 3, '沙坡头区', '0', 100);
INSERT INTO `sys_area` VALUES (640521, 640500, 3, '中宁县', '0', 100);
INSERT INTO `sys_area` VALUES (640522, 640500, 3, '海原县', '0', 100);
INSERT INTO `sys_area` VALUES (650000, 0, 1, '新疆维吾尔自治区', '0', 100);
INSERT INTO `sys_area` VALUES (650100, 650000, 2, '乌鲁木齐市', '0', 100);
INSERT INTO `sys_area` VALUES (650101, 650100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (650102, 650100, 3, '天山区', '0', 100);
INSERT INTO `sys_area` VALUES (650103, 650100, 3, '沙依巴克区', '0', 100);
INSERT INTO `sys_area` VALUES (650104, 650100, 3, '新市区', '0', 100);
INSERT INTO `sys_area` VALUES (650105, 650100, 3, '水磨沟区', '0', 100);
INSERT INTO `sys_area` VALUES (650106, 650100, 3, '头屯河区', '0', 100);
INSERT INTO `sys_area` VALUES (650107, 650100, 3, '达坂城区', '0', 100);
INSERT INTO `sys_area` VALUES (650109, 650100, 3, '米东区', '0', 100);
INSERT INTO `sys_area` VALUES (650121, 650100, 3, '乌鲁木齐县', '0', 100);
INSERT INTO `sys_area` VALUES (650200, 650000, 2, '克拉玛依市', '0', 100);
INSERT INTO `sys_area` VALUES (650201, 650200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (650202, 650200, 3, '独山子区', '0', 100);
INSERT INTO `sys_area` VALUES (650203, 650200, 3, '克拉玛依区', '0', 100);
INSERT INTO `sys_area` VALUES (650204, 650200, 3, '白碱滩区', '0', 100);
INSERT INTO `sys_area` VALUES (650205, 650200, 3, '乌尔禾区', '0', 100);
INSERT INTO `sys_area` VALUES (650400, 650000, 2, '吐鲁番市', '0', 100);
INSERT INTO `sys_area` VALUES (650402, 650400, 3, '高昌区', '0', 100);
INSERT INTO `sys_area` VALUES (650421, 650400, 3, '鄯善县', '0', 100);
INSERT INTO `sys_area` VALUES (650422, 650400, 3, '托克逊县', '0', 100);
INSERT INTO `sys_area` VALUES (650500, 650000, 2, '哈密市', '0', 100);
INSERT INTO `sys_area` VALUES (650502, 650500, 3, '伊州区', '0', 100);
INSERT INTO `sys_area` VALUES (650521, 650500, 3, '巴里坤哈萨克自治县', '0', 100);
INSERT INTO `sys_area` VALUES (650522, 650500, 3, '伊吾县', '0', 100);
INSERT INTO `sys_area` VALUES (652300, 650000, 2, '昌吉回族自治州', '0', 100);
INSERT INTO `sys_area` VALUES (652301, 652300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (652302, 652300, 3, '阜康市', '0', 100);
INSERT INTO `sys_area` VALUES (652323, 652300, 3, '呼图壁县', '0', 100);
INSERT INTO `sys_area` VALUES (652324, 652300, 3, '玛纳斯县', '0', 100);
INSERT INTO `sys_area` VALUES (652325, 652300, 3, '奇台县', '0', 100);
INSERT INTO `sys_area` VALUES (652327, 652300, 3, '吉木萨尔县', '0', 100);
INSERT INTO `sys_area` VALUES (652328, 652300, 3, '木垒哈萨克自治县', '0', 100);
INSERT INTO `sys_area` VALUES (652700, 650000, 2, '博尔塔拉蒙古自治州', '0', 100);
INSERT INTO `sys_area` VALUES (652701, 652700, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (652702, 652700, 3, '阿拉山口市', '0', 100);
INSERT INTO `sys_area` VALUES (652722, 652700, 3, '精河县', '0', 100);
INSERT INTO `sys_area` VALUES (652723, 652700, 3, '温泉县', '0', 100);
INSERT INTO `sys_area` VALUES (652800, 650000, 2, '巴音郭楞蒙古自治州', '0', 100);
INSERT INTO `sys_area` VALUES (652801, 652800, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (652822, 652800, 3, '轮台县', '0', 100);
INSERT INTO `sys_area` VALUES (652823, 652800, 3, '尉犁县', '0', 100);
INSERT INTO `sys_area` VALUES (652824, 652800, 3, '若羌县', '0', 100);
INSERT INTO `sys_area` VALUES (652825, 652800, 3, '且末县', '0', 100);
INSERT INTO `sys_area` VALUES (652826, 652800, 3, '焉耆回族自治县', '0', 100);
INSERT INTO `sys_area` VALUES (652827, 652800, 3, '和静县', '0', 100);
INSERT INTO `sys_area` VALUES (652828, 652800, 3, '和硕县', '0', 100);
INSERT INTO `sys_area` VALUES (652829, 652800, 3, '博湖县', '0', 100);
INSERT INTO `sys_area` VALUES (652900, 650000, 2, '阿克苏地区', '0', 100);
INSERT INTO `sys_area` VALUES (652901, 652900, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (652922, 652900, 3, '温宿县', '0', 100);
INSERT INTO `sys_area` VALUES (652923, 652900, 3, '库车县', '0', 100);
INSERT INTO `sys_area` VALUES (652924, 652900, 3, '沙雅县', '0', 100);
INSERT INTO `sys_area` VALUES (652925, 652900, 3, '新和县', '0', 100);
INSERT INTO `sys_area` VALUES (652926, 652900, 3, '拜城县', '0', 100);
INSERT INTO `sys_area` VALUES (652927, 652900, 3, '乌什县', '0', 100);
INSERT INTO `sys_area` VALUES (652928, 652900, 3, '阿瓦提县', '0', 100);
INSERT INTO `sys_area` VALUES (652929, 652900, 3, '柯坪县', '0', 100);
INSERT INTO `sys_area` VALUES (653000, 650000, 2, '克孜勒苏柯尔克孜自治州', '0', 100);
INSERT INTO `sys_area` VALUES (653001, 653000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (653022, 653000, 3, '阿克陶县', '0', 100);
INSERT INTO `sys_area` VALUES (653023, 653000, 3, '阿合奇县', '0', 100);
INSERT INTO `sys_area` VALUES (653024, 653000, 3, '乌恰县', '0', 100);
INSERT INTO `sys_area` VALUES (653100, 650000, 2, '喀什地区', '0', 100);
INSERT INTO `sys_area` VALUES (653101, 653100, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (653121, 653100, 3, '疏附县', '0', 100);
INSERT INTO `sys_area` VALUES (653122, 653100, 3, '疏勒县', '0', 100);
INSERT INTO `sys_area` VALUES (653123, 653100, 3, '英吉沙县', '0', 100);
INSERT INTO `sys_area` VALUES (653124, 653100, 3, '泽普县', '0', 100);
INSERT INTO `sys_area` VALUES (653125, 653100, 3, '莎车县', '0', 100);
INSERT INTO `sys_area` VALUES (653126, 653100, 3, '叶城县', '0', 100);
INSERT INTO `sys_area` VALUES (653127, 653100, 3, '麦盖提县', '0', 100);
INSERT INTO `sys_area` VALUES (653128, 653100, 3, '岳普湖县', '0', 100);
INSERT INTO `sys_area` VALUES (653129, 653100, 3, '伽师县', '0', 100);
INSERT INTO `sys_area` VALUES (653130, 653100, 3, '巴楚县', '0', 100);
INSERT INTO `sys_area` VALUES (653131, 653100, 3, '塔什库尔干塔吉克自治县', '0', 100);
INSERT INTO `sys_area` VALUES (653200, 650000, 2, '和田地区', '0', 100);
INSERT INTO `sys_area` VALUES (653201, 653200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (653221, 653200, 3, '和田县', '0', 100);
INSERT INTO `sys_area` VALUES (653222, 653200, 3, '墨玉县', '0', 100);
INSERT INTO `sys_area` VALUES (653223, 653200, 3, '皮山县', '0', 100);
INSERT INTO `sys_area` VALUES (653224, 653200, 3, '洛浦县', '0', 100);
INSERT INTO `sys_area` VALUES (653225, 653200, 3, '策勒县', '0', 100);
INSERT INTO `sys_area` VALUES (653226, 653200, 3, '于田县', '0', 100);
INSERT INTO `sys_area` VALUES (653227, 653200, 3, '民丰县', '0', 100);
INSERT INTO `sys_area` VALUES (654000, 650000, 2, '伊犁哈萨克自治州', '0', 100);
INSERT INTO `sys_area` VALUES (654002, 654000, 3, '伊宁市', '0', 100);
INSERT INTO `sys_area` VALUES (654003, 654000, 3, '奎屯市', '0', 100);
INSERT INTO `sys_area` VALUES (654004, 654000, 3, '霍尔果斯市', '0', 100);
INSERT INTO `sys_area` VALUES (654021, 654000, 3, '伊宁县', '0', 100);
INSERT INTO `sys_area` VALUES (654022, 654000, 3, '察布查尔锡伯自治县', '0', 100);
INSERT INTO `sys_area` VALUES (654023, 654000, 3, '霍城县', '0', 100);
INSERT INTO `sys_area` VALUES (654024, 654000, 3, '巩留县', '0', 100);
INSERT INTO `sys_area` VALUES (654025, 654000, 3, '新源县', '0', 100);
INSERT INTO `sys_area` VALUES (654026, 654000, 3, '昭苏县', '0', 100);
INSERT INTO `sys_area` VALUES (654027, 654000, 3, '特克斯县', '0', 100);
INSERT INTO `sys_area` VALUES (654028, 654000, 3, '尼勒克县', '0', 100);
INSERT INTO `sys_area` VALUES (654200, 650000, 2, '塔城地区', '0', 100);
INSERT INTO `sys_area` VALUES (654201, 654200, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (654202, 654200, 3, '乌苏市', '0', 100);
INSERT INTO `sys_area` VALUES (654221, 654200, 3, '额敏县', '0', 100);
INSERT INTO `sys_area` VALUES (654223, 654200, 3, '沙湾县', '0', 100);
INSERT INTO `sys_area` VALUES (654224, 654200, 3, '托里县', '0', 100);
INSERT INTO `sys_area` VALUES (654225, 654200, 3, '裕民县', '0', 100);
INSERT INTO `sys_area` VALUES (654226, 654200, 3, '和布克赛尔蒙古自治县', '0', 100);
INSERT INTO `sys_area` VALUES (654300, 650000, 2, '阿勒泰地区', '0', 100);
INSERT INTO `sys_area` VALUES (654301, 654300, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (654321, 654300, 3, '布尔津县', '0', 100);
INSERT INTO `sys_area` VALUES (654322, 654300, 3, '富蕴县', '0', 100);
INSERT INTO `sys_area` VALUES (654323, 654300, 3, '福海县', '0', 100);
INSERT INTO `sys_area` VALUES (654324, 654300, 3, '哈巴河县', '0', 100);
INSERT INTO `sys_area` VALUES (654325, 654300, 3, '青河县', '0', 100);
INSERT INTO `sys_area` VALUES (654326, 654300, 3, '吉木乃县', '0', 100);
INSERT INTO `sys_area` VALUES (659000, 650000, 2, '自治区直辖县级行政区划', '0', 100);
INSERT INTO `sys_area` VALUES (659001, 659000, 3, '市辖区', '0', 100);
INSERT INTO `sys_area` VALUES (659002, 659000, 3, '阿拉尔市', '0', 100);
INSERT INTO `sys_area` VALUES (659003, 659000, 3, '图木舒克市', '0', 100);
INSERT INTO `sys_area` VALUES (659004, 659000, 3, '五家渠市', '0', 100);
INSERT INTO `sys_area` VALUES (659006, 659000, 3, '铁门关市', '0', 100);
INSERT INTO `sys_area` VALUES (710000, 0, 1, '台湾省', '0', 100);
INSERT INTO `sys_area` VALUES (810000, 0, 1, '香港特别行政区', '0', 100);
INSERT INTO `sys_area` VALUES (820000, 0, 1, '澳门特别行政区', '0', 100);
INSERT INTO `sys_area` VALUES (920000, 500200, 3, '静海县', '301600', 100);
INSERT INTO `sys_area` VALUES (920001, 330100, 3, '富阳市', '311400', 100);
INSERT INTO `sys_area` VALUES (920002, 410100, 3, '郑东新区', '0', 100);
INSERT INTO `sys_area` VALUES (920003, 440100, 3, '萝岗区', '510100', 100);
INSERT INTO `sys_area` VALUES (920004, 441900, 3, '东城街道', '0', 100);
INSERT INTO `sys_area` VALUES (920005, 441900, 3, '南城街道', '0', 100);
INSERT INTO `sys_area` VALUES (920006, 441900, 3, '万江街道', '0', 100);
INSERT INTO `sys_area` VALUES (920007, 441900, 3, '莞城街道', '0', 100);
INSERT INTO `sys_area` VALUES (920008, 441900, 3, '石碣镇', '0', 100);
INSERT INTO `sys_area` VALUES (920009, 441900, 3, '石龙镇', '0', 100);
INSERT INTO `sys_area` VALUES (920010, 441900, 3, '茶山镇', '0', 100);
INSERT INTO `sys_area` VALUES (920011, 441900, 3, '石排镇', '0', 100);
INSERT INTO `sys_area` VALUES (920012, 441900, 3, '企石镇', '0', 0);
INSERT INTO `sys_area` VALUES (920013, 441900, 3, '横沥镇', '0', 0);
INSERT INTO `sys_area` VALUES (920014, 441900, 3, '桥头镇', '0', 0);
INSERT INTO `sys_area` VALUES (920015, 441900, 3, '谢岗镇', '0', 0);
INSERT INTO `sys_area` VALUES (920016, 441900, 3, '东坑镇', '0', 0);
INSERT INTO `sys_area` VALUES (920017, 441900, 3, '常平镇', '0', 0);
INSERT INTO `sys_area` VALUES (920018, 441900, 3, '寮步镇', '0', 0);
INSERT INTO `sys_area` VALUES (920019, 441900, 3, '樟木头镇', '0', 0);
INSERT INTO `sys_area` VALUES (920020, 441900, 3, '大朗镇', '0', 0);
INSERT INTO `sys_area` VALUES (920021, 441900, 3, '黄江镇', '0', 0);
INSERT INTO `sys_area` VALUES (920022, 441900, 3, '清溪镇', '0', 0);
INSERT INTO `sys_area` VALUES (920023, 441900, 3, '塘厦镇', '0', 0);
INSERT INTO `sys_area` VALUES (920024, 441900, 3, '凤岗镇', '0', 0);
INSERT INTO `sys_area` VALUES (920025, 441900, 3, '大岭山镇', '0', 0);
INSERT INTO `sys_area` VALUES (920026, 441900, 3, '长安镇', '0', 0);
INSERT INTO `sys_area` VALUES (920027, 441900, 3, '虎门镇', '0', 0);
INSERT INTO `sys_area` VALUES (920028, 441900, 3, '厚街镇', '0', 0);
INSERT INTO `sys_area` VALUES (920029, 441900, 3, '沙田镇', '0', 0);
INSERT INTO `sys_area` VALUES (920030, 441900, 3, '道滘镇', '0', 0);
INSERT INTO `sys_area` VALUES (920031, 441900, 3, '洪梅镇', '0', 0);
INSERT INTO `sys_area` VALUES (920032, 441900, 3, '麻涌镇', '0', 0);
INSERT INTO `sys_area` VALUES (920033, 441900, 3, '望牛墩镇', '0', 0);
INSERT INTO `sys_area` VALUES (920034, 441900, 3, '中堂镇', '0', 0);
INSERT INTO `sys_area` VALUES (920035, 441900, 3, '高埗镇', '0', 0);
INSERT INTO `sys_area` VALUES (920036, 441900, 3, '松山湖管委会', '0', 0);
INSERT INTO `sys_area` VALUES (920037, 441900, 3, '虎门港管委会', '0', 0);
INSERT INTO `sys_area` VALUES (920038, 441900, 3, '东莞生态园', '0', 0);
INSERT INTO `sys_area` VALUES (920039, 442000, 3, '中山市', '0', 0);
INSERT INTO `sys_area` VALUES (920040, 460300, 3, '三沙市', '0', 0);
INSERT INTO `sys_area` VALUES (920041, 460400, 3, '儋州市', '0', 0);
INSERT INTO `sys_area` VALUES (920042, 810000, 2, '香港特别行政区', '0', 0);
INSERT INTO `sys_area` VALUES (920043, 920042, 3, '中西区', '0', 0);
INSERT INTO `sys_area` VALUES (920044, 920042, 3, '东区', '0', 0);
INSERT INTO `sys_area` VALUES (920045, 920042, 3, '九龙城区', '0', 0);
INSERT INTO `sys_area` VALUES (920046, 920042, 3, '观塘区', '0', 0);
INSERT INTO `sys_area` VALUES (920047, 920042, 3, '南区', '0', 0);
INSERT INTO `sys_area` VALUES (920048, 920042, 3, '深水埗区', '0', 0);
INSERT INTO `sys_area` VALUES (920049, 920042, 3, '湾仔区', '0', 0);
INSERT INTO `sys_area` VALUES (920050, 920042, 3, '黄大仙区', '0', 0);
INSERT INTO `sys_area` VALUES (920051, 920042, 3, '油尖旺区', '0', 0);
INSERT INTO `sys_area` VALUES (920052, 920042, 3, '离岛区', '0', 0);
INSERT INTO `sys_area` VALUES (920053, 920042, 3, '葵青区', '0', 0);
INSERT INTO `sys_area` VALUES (920054, 920042, 3, '北区', '0', 0);
INSERT INTO `sys_area` VALUES (920055, 920042, 3, '西贡区', '0', 0);
INSERT INTO `sys_area` VALUES (920056, 920042, 3, '沙田区', '0', 0);
INSERT INTO `sys_area` VALUES (920057, 920042, 3, '屯门区', '0', 0);
INSERT INTO `sys_area` VALUES (920058, 920042, 3, '大埔区', '0', 0);
INSERT INTO `sys_area` VALUES (920059, 920042, 3, '荃湾区', '0', 0);
INSERT INTO `sys_area` VALUES (920060, 920042, 3, '元朗区', '0', 0);
INSERT INTO `sys_area` VALUES (920061, 820000, 2, '澳门特别行政区', '0', 0);
INSERT INTO `sys_area` VALUES (920062, 920061, 3, '澳门半岛', '0', 0);
INSERT INTO `sys_area` VALUES (920063, 920061, 3, '凼仔', '0', 0);
INSERT INTO `sys_area` VALUES (920064, 920061, 3, '路凼城', '0', 0);
INSERT INTO `sys_area` VALUES (920065, 920061, 3, '路环', '0', 0);
INSERT INTO `sys_area` VALUES (920066, 710000, 2, '台北市', '0', 0);
INSERT INTO `sys_area` VALUES (920067, 710000, 2, '高雄市', '0', 0);
INSERT INTO `sys_area` VALUES (920068, 710000, 2, '台南市', '0', 0);
INSERT INTO `sys_area` VALUES (920069, 710000, 2, '台中市', '0', 0);
INSERT INTO `sys_area` VALUES (920070, 710000, 2, '南投县', '0', 0);
INSERT INTO `sys_area` VALUES (920071, 710000, 2, '基隆市', '0', 0);
INSERT INTO `sys_area` VALUES (920072, 710000, 2, '新竹市', '0', 0);
INSERT INTO `sys_area` VALUES (920073, 710000, 2, '嘉义市', '0', 0);
INSERT INTO `sys_area` VALUES (920074, 710000, 2, '新北市', '0', 0);
INSERT INTO `sys_area` VALUES (920075, 710000, 2, '宜兰县', '0', 0);
INSERT INTO `sys_area` VALUES (920076, 710000, 2, '新竹县', '0', 0);
INSERT INTO `sys_area` VALUES (920077, 710000, 2, '桃园市', '0', 0);
INSERT INTO `sys_area` VALUES (920078, 710000, 2, '苗栗县', '0', 0);
INSERT INTO `sys_area` VALUES (920079, 710000, 2, '彰化县', '0', 0);
INSERT INTO `sys_area` VALUES (920080, 710000, 2, '嘉义县', '0', 0);
INSERT INTO `sys_area` VALUES (920081, 710000, 2, '云林县', '0', 0);
INSERT INTO `sys_area` VALUES (920082, 710000, 2, '屏东县', '0', 0);
INSERT INTO `sys_area` VALUES (920083, 710000, 2, '台东县', '0', 0);
INSERT INTO `sys_area` VALUES (920084, 710000, 2, '花莲县', '0', 0);
INSERT INTO `sys_area` VALUES (920085, 710000, 2, '澎湖县', '0', 0);
INSERT INTO `sys_area` VALUES (920086, 920066, 3, '台北市', '0', 0);
INSERT INTO `sys_area` VALUES (920087, 920067, 3, '高雄市', '0', 0);
INSERT INTO `sys_area` VALUES (920088, 920068, 3, '台南市', '0', 0);
INSERT INTO `sys_area` VALUES (920089, 920069, 3, '台中市', '0', 0);
INSERT INTO `sys_area` VALUES (920090, 920070, 3, '南投县', '0', 0);
INSERT INTO `sys_area` VALUES (920091, 920071, 3, '基隆市', '0', 0);
INSERT INTO `sys_area` VALUES (920092, 920072, 3, '新竹市', '0', 0);
INSERT INTO `sys_area` VALUES (920093, 920073, 3, '嘉义市', '0', 0);
INSERT INTO `sys_area` VALUES (920094, 920074, 3, '新北市', '0', 0);
INSERT INTO `sys_area` VALUES (920095, 920075, 3, '宜兰县', '0', 0);
INSERT INTO `sys_area` VALUES (920096, 920076, 3, '新竹县', '0', 0);
INSERT INTO `sys_area` VALUES (920097, 920077, 3, '桃园市', '0', 0);
INSERT INTO `sys_area` VALUES (920098, 920078, 3, '苗栗县', '0', 0);
INSERT INTO `sys_area` VALUES (920099, 920079, 3, '彰化县', '0', 0);
INSERT INTO `sys_area` VALUES (920100, 920080, 3, '嘉义县', '0', 0);
INSERT INTO `sys_area` VALUES (920101, 920081, 3, '云林县', '0', 0);
INSERT INTO `sys_area` VALUES (920102, 920082, 3, '屏东县', '0', 0);
INSERT INTO `sys_area` VALUES (920103, 920083, 3, '台东县', '0', 0);
INSERT INTO `sys_area` VALUES (920104, 920084, 3, '花莲县', '0', 0);
INSERT INTO `sys_area` VALUES (920105, 920085, 3, ' 澎湖县', '0', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_article
-- ----------------------------
DROP TABLE IF EXISTS `sys_article`;
CREATE TABLE `sys_article` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `article_classify_id` int(8) NOT NULL DEFAULT '0' COMMENT '文章分类id',
  `article_title` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文章标题',
  `cover` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文章封面',
  `article_content` longtext COLLATE utf8mb4_bin COMMENT '文章内容',
  `status` int(1) DEFAULT NULL COMMENT '状态',
  `sort` int(8) DEFAULT NULL COMMENT '排序',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='文章列表';

-- ----------------------------
-- Records of sys_article
-- ----------------------------
BEGIN;
INSERT INTO `sys_article` VALUES (1, 0, '关于我们', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/article/20230413/e3f0e180-ef0a-4e28-a217-122ae68e3f6b.png', '<p><br></p><p style=\"text-indent: 35px; text-align: start;\">北京信隆优创科技有限公司，是国内专业的电子商务系统软件供应商，致力于为中小企业提供高质量的电商解决方案。</p><p style=\"text-indent: 35px; text-align: start;\">公司秉承着“让软件变得更简单” 的使命，用心做产品。为客户提供专业、成熟、稳定的电商系统与服务。</p><p style=\"text-indent: 35px; text-align: start;\">公司一贯以“合理的价格”、“高质量的产品”、“良好的信誉服务”为基本准则，本公司期待成为您忠实的朋友和合作伙伴，共创互惠双赢未来！</p><p><br></p>', 1, 0, 1691199180, 1681377874);
INSERT INTO `sys_article` VALUES (4, 0, '客户服务', NULL, '<p><img src=\"https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20230805/6c9aa6b7-1fe0-43af-abe6-f33276f99b8e.JPG\" alt=\"IMG_2951.JPG\" data-href=\"null\" style=\"\"/></p>', 1, 0, 1691201761, 1683453863);
INSERT INTO `sys_article` VALUES (7, 0, '积分规则', 'https://yashangxuan.oss-cn-beijing.aliyuncs.com/article/20230613/5d628966-b3ba-45fa-b0ab-2d9c170255c0.jpg', '<h2>积分获取：</h2><p><br></p><ol><li style=\"line-height: 2.5;\"><span style=\"font-size: 16px;\">注册赠送</span><span style=\"color: rgb(225, 60, 57); font-size: 16px;\">5积分，</span></li><li style=\"line-height: 2.5;\"><span style=\"font-size: 16px;\">每邀请1人成功注册，赠送</span><span style=\"color: rgb(225, 60, 57); font-size: 16px;\">5积分，</span></li><li style=\"line-height: 2.5;\"><span style=\"color: rgb(0, 0, 0); font-size: 16px;\">每日签到赠送</span><span style=\"color: rgb(225, 60, 57); font-size: 16px;\">2积分</span></li></ol><p><br></p>', 1, 0, 1691506624, 1686623649);
INSERT INTO `sys_article` VALUES (8, 0, '如何申请推广员', NULL, '<p style=\"text-align: left;\">暂时不开放推广员</p>', 1, 0, 1691506649, 1686749192);
COMMIT;

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `file_name` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件名称',
  `file_type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件类型',
  `file_path` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件路径',
  `url` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件访问地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='系统上传文件表\n';

-- ----------------------------
-- Records of sys_file
-- ----------------------------
BEGIN;
INSERT INTO `sys_file` VALUES (1, 'b6351e12e6506717.jpg', 'jpg', 'goods/20231010/8c920db2-703e-4910-a50f-4ba3fbb9ad78.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/8c920db2-703e-4910-a50f-4ba3fbb9ad78.jpg');
INSERT INTO `sys_file` VALUES (2, '68314e5a7a8594d0.jpg', 'jpg', 'editor/20231010/49383bb4-850a-4216-86d5-39d634df5eb1.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/49383bb4-850a-4216-86d5-39d634df5eb1.jpg');
INSERT INTO `sys_file` VALUES (3, 'afdc9c4e107f2d7d.jpg', 'jpg', 'editor/20231010/6bac2d21-c347-4604-b7b2-8dc0fa01a15c.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/6bac2d21-c347-4604-b7b2-8dc0fa01a15c.jpg');
INSERT INTO `sys_file` VALUES (4, 'e701fc2c7a649bfe.jpg', 'jpg', 'editor/20231010/02d61757-8e9e-41ab-b960-11d77078a5cc.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/02d61757-8e9e-41ab-b960-11d77078a5cc.jpg');
INSERT INTO `sys_file` VALUES (5, '68314e5a7a8594d0.jpg', 'jpg', 'editor/20231010/e8112a99-754a-4a85-847e-b4940aa862cf.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/e8112a99-754a-4a85-847e-b4940aa862cf.jpg');
INSERT INTO `sys_file` VALUES (6, 'afdc9c4e107f2d7d.jpg', 'jpg', 'editor/20231010/1b5c9e32-a3e4-458e-b4af-107dec25fb19.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/1b5c9e32-a3e4-458e-b4af-107dec25fb19.jpg');
INSERT INTO `sys_file` VALUES (7, '0a96837c34a1eed9.jpg', 'jpg', 'goods/20231010/3b6bb48c-d849-4aaf-bdaa-f64dda5a6ee2.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/3b6bb48c-d849-4aaf-bdaa-f64dda5a6ee2.jpg');
INSERT INTO `sys_file` VALUES (8, '1.jpeg', 'jpeg', 'editor/20231010/27c73266-355c-4a8b-b6ff-aacf781b98ea.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/27c73266-355c-4a8b-b6ff-aacf781b98ea.jpeg');
INSERT INTO `sys_file` VALUES (9, '2.jpeg', 'jpeg', 'editor/20231010/83d8010b-7971-446c-bfc5-303a0a3d7922.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/83d8010b-7971-446c-bfc5-303a0a3d7922.jpeg');
INSERT INTO `sys_file` VALUES (10, '3.jpeg', 'jpeg', 'editor/20231010/fce0944f-6a9f-4732-8c99-8e302dea16b0.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/fce0944f-6a9f-4732-8c99-8e302dea16b0.jpeg');
INSERT INTO `sys_file` VALUES (11, '26b01cc077765280.jpg', 'jpg', 'goods/20231010/e9d30c61-845f-462b-b0db-0d9de6f20688.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/e9d30c61-845f-462b-b0db-0d9de6f20688.jpg');
INSERT INTO `sys_file` VALUES (12, '11.jpeg', 'jpeg', 'editor/20231010/251db661-965b-4578-aa7c-c980b2a8d4fe.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/251db661-965b-4578-aa7c-c980b2a8d4fe.jpeg');
INSERT INTO `sys_file` VALUES (13, '22.jpeg', 'jpeg', 'editor/20231010/0748e005-a97e-4e1c-be01-dce918260c20.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/0748e005-a97e-4e1c-be01-dce918260c20.jpeg');
INSERT INTO `sys_file` VALUES (14, '33.jpeg', 'jpeg', 'editor/20231010/ab01e26f-833a-49c4-9585-54473fc26b24.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/ab01e26f-833a-49c4-9585-54473fc26b24.jpeg');
INSERT INTO `sys_file` VALUES (15, '139f44a2bc471fec.jpg', 'jpg', 'goods/20231010/82b0a345-81f7-4684-8734-2a30254ec2e1.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/82b0a345-81f7-4684-8734-2a30254ec2e1.jpg');
INSERT INTO `sys_file` VALUES (16, '111.jpeg', 'jpeg', 'editor/20231010/5a0e15f9-86ef-484b-85c1-d72f60c9ba4d.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/5a0e15f9-86ef-484b-85c1-d72f60c9ba4d.jpeg');
INSERT INTO `sys_file` VALUES (17, '222.jpeg', 'jpeg', 'editor/20231010/106bb7b5-9cdf-475c-94f3-18ebfa17fe0c.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/106bb7b5-9cdf-475c-94f3-18ebfa17fe0c.jpeg');
INSERT INTO `sys_file` VALUES (18, '333.jpeg', 'jpeg', 'editor/20231010/d0c2d210-fdc9-4b9a-a166-7e56ec651fd1.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/d0c2d210-fdc9-4b9a-a166-7e56ec651fd1.jpeg');
INSERT INTO `sys_file` VALUES (19, '444.jpeg', 'jpeg', 'editor/20231010/9a0ca398-919d-44f4-b366-e6cee40ff2cc.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/9a0ca398-919d-44f4-b366-e6cee40ff2cc.jpeg');
INSERT INTO `sys_file` VALUES (20, '555.jpeg', 'jpeg', 'editor/20231010/7bb90938-fd97-446d-9c8b-edebd50b1b70.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/7bb90938-fd97-446d-9c8b-edebd50b1b70.jpeg');
INSERT INTO `sys_file` VALUES (21, '666.jpeg', 'jpeg', 'editor/20231010/cfb29c9e-2685-4173-9ec9-5fbff8822695.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/cfb29c9e-2685-4173-9ec9-5fbff8822695.jpeg');
INSERT INTO `sys_file` VALUES (22, '777.jpeg', 'jpeg', 'editor/20231010/0ba3efce-5e8a-4b53-89c1-0ff06e63cfd5.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/0ba3efce-5e8a-4b53-89c1-0ff06e63cfd5.jpeg');
INSERT INTO `sys_file` VALUES (23, '222.jpeg', 'jpeg', 'editor/20231010/6fff38ec-053a-4433-ad1e-275910eb5c1d.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/6fff38ec-053a-4433-ad1e-275910eb5c1d.jpeg');
INSERT INTO `sys_file` VALUES (24, '333.jpeg', 'jpeg', 'editor/20231010/d06f955d-8d33-4298-8bcf-e8215666a4fd.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/d06f955d-8d33-4298-8bcf-e8215666a4fd.jpeg');
INSERT INTO `sys_file` VALUES (25, '444.jpeg', 'jpeg', 'editor/20231010/1827405d-50ad-4255-9b81-74ceb2133ff5.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/1827405d-50ad-4255-9b81-74ceb2133ff5.jpeg');
INSERT INTO `sys_file` VALUES (26, '555.jpeg', 'jpeg', 'editor/20231010/7c9d9842-348a-4f60-83d3-35264a8549a6.jpeg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/7c9d9842-348a-4f60-83d3-35264a8549a6.jpeg');
INSERT INTO `sys_file` VALUES (27, '0a96837c34a1eed9.jpg', 'jpg', 'blind/20231010/3547f915-67df-472a-b615-f77fae8697a8.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/3547f915-67df-472a-b615-f77fae8697a8.jpg');
INSERT INTO `sys_file` VALUES (28, '背景@2x.png', 'png', 'blind/20231010/25417139-1193-4422-b0ef-784fe36c88f9.png', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/25417139-1193-4422-b0ef-784fe36c88f9.png');
INSERT INTO `sys_file` VALUES (29, '97edce481d497491.jpg', 'jpg', 'goods/20231010/50f451a8-fdac-492e-9503-2fb0c73adcd3.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/50f451a8-fdac-492e-9503-2fb0c73adcd3.jpg');
INSERT INTO `sys_file` VALUES (30, 'beec9b5943cfa570.jpg', 'jpg', 'goods/20231010/b41e54bb-869a-47c6-ba66-35a3b1bed690.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/b41e54bb-869a-47c6-ba66-35a3b1bed690.jpg');
INSERT INTO `sys_file` VALUES (31, '2ac2e192b8123570.jpg', 'jpg', 'goods/20231010/65bf7c07-53eb-4cec-898e-f0ce8568f9b9.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/65bf7c07-53eb-4cec-898e-f0ce8568f9b9.jpg');
INSERT INTO `sys_file` VALUES (32, '97edce481d497491.jpg', 'jpg', 'goods/20231010/61b82566-75ec-44b5-b170-6d4eea604438.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/goods/20231010/61b82566-75ec-44b5-b170-6d4eea604438.jpg');
INSERT INTO `sys_file` VALUES (33, '5cccda6644a5f9b7.jpg', 'jpg', 'editor/20231010/fcff9604-134c-4494-8756-7c714980b1c4.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/fcff9604-134c-4494-8756-7c714980b1c4.jpg');
INSERT INTO `sys_file` VALUES (34, '172cdd76d74fec14.jpg', 'jpg', 'editor/20231010/3c582b16-0550-403e-8f49-a551765c0070.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/3c582b16-0550-403e-8f49-a551765c0070.jpg');
INSERT INTO `sys_file` VALUES (35, '689b77cfb889e977.jpg', 'jpg', 'editor/20231010/25bff038-289f-4b03-b193-8ac64d0b16c4.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/25bff038-289f-4b03-b193-8ac64d0b16c4.jpg');
INSERT INTO `sys_file` VALUES (36, '731caaf084392dfb.jpg', 'jpg', 'editor/20231010/609123c8-b518-43d5-9c70-df9ca415bd1e.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/609123c8-b518-43d5-9c70-df9ca415bd1e.jpg');
INSERT INTO `sys_file` VALUES (37, '172cdd76d74fec14.jpg', 'jpg', 'editor/20231010/41dd825e-f2d9-4e39-a4d9-fda44cc52e80.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/41dd825e-f2d9-4e39-a4d9-fda44cc52e80.jpg');
INSERT INTO `sys_file` VALUES (38, '731caaf084392dfb.jpg', 'jpg', 'editor/20231010/1d74d70c-de0f-4790-9769-dc27e1c81417.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/1d74d70c-de0f-4790-9769-dc27e1c81417.jpg');
INSERT INTO `sys_file` VALUES (39, '5cccda6644a5f9b7.jpg', 'jpg', 'editor/20231010/11fb18d9-bbe4-4346-90ce-6bf855bdb7c6.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/11fb18d9-bbe4-4346-90ce-6bf855bdb7c6.jpg');
INSERT INTO `sys_file` VALUES (40, '689b77cfb889e977.jpg', 'jpg', 'editor/20231010/eb44bffd-87a2-4258-bf4f-ae77a39cd5ec.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/editor/20231010/eb44bffd-87a2-4258-bf4f-ae77a39cd5ec.jpg');
INSERT INTO `sys_file` VALUES (41, '0a96837c34a1eed9.jpg', 'jpg', 'blind/20231010/eed5aef1-3166-4f71-961d-c3a759794d79.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/eed5aef1-3166-4f71-961d-c3a759794d79.jpg');
INSERT INTO `sys_file` VALUES (42, '背景@2x.png', 'png', 'blind/20231010/9ab4b8a0-f4ca-4075-a092-db03f16fd7cf.png', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/9ab4b8a0-f4ca-4075-a092-db03f16fd7cf.png');
INSERT INTO `sys_file` VALUES (43, '背景@2x.png', 'png', 'blind/20231010/798d8b35-6b7a-410f-8fd1-38a745af1840.png', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/798d8b35-6b7a-410f-8fd1-38a745af1840.png');
INSERT INTO `sys_file` VALUES (44, '139f44a2bc471fec.jpg', 'jpg', 'blind/20231010/c3abcc6b-f74a-47d6-831c-af979784d747.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/c3abcc6b-f74a-47d6-831c-af979784d747.jpg');
INSERT INTO `sys_file` VALUES (45, '26b01cc077765280.jpg', 'jpg', 'blind/20231010/4d39ce46-4eae-42ee-a87f-c2fc3f8c5f20.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/4d39ce46-4eae-42ee-a87f-c2fc3f8c5f20.jpg');
INSERT INTO `sys_file` VALUES (46, '背景@2x.png', 'png', 'blind/20231010/a7849aff-93dd-4f35-b1d1-adc2662c610a.png', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/a7849aff-93dd-4f35-b1d1-adc2662c610a.png');
INSERT INTO `sys_file` VALUES (47, '背景@2x.png', 'png', 'blind/20231010/730270bd-f95a-406b-9d0c-f441d5ecf026.png', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/730270bd-f95a-406b-9d0c-f441d5ecf026.png');
INSERT INTO `sys_file` VALUES (48, '26d3f49d1d6211dc.jpg', 'jpg', 'blind/20231010/82fee902-7506-4ab6-aed4-51c704d77703.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/82fee902-7506-4ab6-aed4-51c704d77703.jpg');
INSERT INTO `sys_file` VALUES (49, 'b6351e12e6506717.jpg', 'jpg', 'blind/20231010/90fd6d81-6d1d-4217-9db3-97c2cbe50d1e.jpg', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/90fd6d81-6d1d-4217-9db3-97c2cbe50d1e.jpg');
INSERT INTO `sys_file` VALUES (50, '背景@2x.png', 'png', 'blind/20231010/16823d4e-27ca-4b8b-bba1-2abb1d1404ae.png', 'https://xinlong-shop.oss-cn-beijing.aliyuncs.com/blind/20231010/16823d4e-27ca-4b8b-bba1-2abb1d1404ae.png');
COMMIT;

-- ----------------------------
-- Table structure for sys_home_site
-- ----------------------------
DROP TABLE IF EXISTS `sys_home_site`;
CREATE TABLE `sys_home_site` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '位置名称',
  `site_code` varchar(128) COLLATE utf8mb4_bin NOT NULL COMMENT '位置编码',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='首页位置';

-- ----------------------------
-- Records of sys_home_site
-- ----------------------------
BEGIN;
INSERT INTO `sys_home_site` VALUES (2, '热销专区', 'hot', 1696769550);
INSERT INTO `sys_home_site` VALUES (3, '一番赏', 'yifanshang', 1696840461);
INSERT INTO `sys_home_site` VALUES (4, '新品专区', 'new', 1696840499);
COMMIT;

-- ----------------------------
-- Table structure for sys_home_site_blind
-- ----------------------------
DROP TABLE IF EXISTS `sys_home_site_blind`;
CREATE TABLE `sys_home_site_blind` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `site_id` int(8) DEFAULT NULL COMMENT '位置ID',
  `site_code` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '位置code',
  `blind_goods_id` int(8) DEFAULT NULL COMMENT '盲盒商品id',
  `sort` int(8) DEFAULT NULL COMMENT '排序',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='首页显示的盲盒商品';

-- ----------------------------
-- Records of sys_home_site_blind
-- ----------------------------
BEGIN;
INSERT INTO `sys_home_site_blind` VALUES (1, 2, 'hot', 3, 0, 1696922699);
INSERT INTO `sys_home_site_blind` VALUES (2, 2, 'hot', 2, 0, 1696922699);
INSERT INTO `sys_home_site_blind` VALUES (3, 3, 'yifanshang', 1, 0, 1696922711);
INSERT INTO `sys_home_site_blind` VALUES (4, 3, 'yifanshang', 4, 0, 1696922711);
INSERT INTO `sys_home_site_blind` VALUES (5, 3, 'yifanshang', 5, 0, 1696922711);
INSERT INTO `sys_home_site_blind` VALUES (6, 4, 'new', 2, 0, 1696922719);
INSERT INTO `sys_home_site_blind` VALUES (7, 4, 'new', 3, 0, 1696922719);
INSERT INTO `sys_home_site_blind` VALUES (8, 4, 'new', 4, 0, 1696922719);
INSERT INTO `sys_home_site_blind` VALUES (9, 4, 'new', 5, 0, 1696922719);
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '菜单名称',
  `permission` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '权限url正则',
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
  `pid` bigint(20) DEFAULT '0' COMMENT '父id',
  `sort` int(10) DEFAULT '0' COMMENT '排序',
  `identity` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '唯一标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` VALUES (1, '系统管理', '/sys.*', '系统管理', 0, 1, 'setting');
INSERT INTO `sys_menu` VALUES (2, '开发管理', '', '开发管理', 0, 99, 'development');
INSERT INTO `sys_menu` VALUES (3, '菜单管理', '/dev/menu.*', '菜单管理', 1, 1, 'system_menu');
INSERT INTO `sys_menu` VALUES (4, '站点设置', '/sys/setting.*', '站点设置', 1, 4, 'site_setting');
INSERT INTO `sys_menu` VALUES (5, '角色权限管理', '/sys/role.*', '角色权限管理', 1, 2, 'system_role');
INSERT INTO `sys_menu` VALUES (6, '个人设置', '/sys/admin/setting.*', '个人设置', 1, 100, 'admin_setting');
INSERT INTO `sys_menu` VALUES (7, '会员管理', '', '', 0, 1, 'member-p');
INSERT INTO `sys_menu` VALUES (8, '会员列表', '/member/member.*', '', 7, 0, 'member_list');
INSERT INTO `sys_menu` VALUES (9, '管理员管理', '/sys/admin.*', '', 1, 3, 'system_admin');
INSERT INTO `sys_menu` VALUES (10, 'Dashboard', '', '工作台', 0, 0, 'dashboard');
INSERT INTO `sys_menu` VALUES (11, '工作台', '/dashboard/workplace.*', '', 10, 0, 'dashboard_workplace');
INSERT INTO `sys_menu` VALUES (12, '会员等级', '/member/member/grade.*', '', 7, 2, 'member_grade');
INSERT INTO `sys_menu` VALUES (16, '商品管理', '', '', 0, 2, 'goods');
INSERT INTO `sys_menu` VALUES (17, '商品列表', '/goods/goods.*', '', 16, 0, 'goods_list');
INSERT INTO `sys_menu` VALUES (18, '商品分类', '/goods/classify.*', '', 16, 1, 'goods_classify');
INSERT INTO `sys_menu` VALUES (20, '商品品牌', '/goods/brand.*', '', 16, 2, 'goods_brand');
INSERT INTO `sys_menu` VALUES (21, '促销活动', 'promotion', '促销活动', 0, 5, 'promotion');
INSERT INTO `sys_menu` VALUES (22, '抢购活动', '/promotion/rush-purchase.*', '抢购活动', 21, 0, 'rush_purchase');
INSERT INTO `sys_menu` VALUES (23, '提现管理', '/member/member/withdrawal.*', '提现', 7, 3, 'member_withdrawal');
INSERT INTO `sys_menu` VALUES (24, '站点管理', '/site.*', '', 0, 6, 'site');
INSERT INTO `sys_menu` VALUES (25, '文章管理', '/site/article.*', '', 24, 99, 'article_list');
INSERT INTO `sys_menu` VALUES (26, '签名管理', '/member/membe-signr.*', '', 7, 1, 'member_sign');
INSERT INTO `sys_menu` VALUES (27, '抢购订单', '/promotion/rush-purchase-order.*', '', 21, 2, 'rush_purchase_order');
INSERT INTO `sys_menu` VALUES (28, '会员收益', '/member/activity-income.*', '会员收益', 21, 3, 'member_income');
INSERT INTO `sys_menu` VALUES (29, '上架订单', '/promotion/put-sale-order.*', '上架订单', 21, 4, 'put_sale_order_list');
INSERT INTO `sys_menu` VALUES (30, '购物车', '/promotion/rush-purchase/cart', '', 21, 5, 'rush_purchase_cart');
INSERT INTO `sys_menu` VALUES (31, '盲盒管理', '/blind_box', '', 0, 4, 'blind_box');
INSERT INTO `sys_menu` VALUES (32, '盲盒列表', '/blind-box/blind-box.*', '', 31, 0, 'blind_box_list');
INSERT INTO `sys_menu` VALUES (33, '广告位管理', '/site/promotion.*', '', 24, 1, 'adv_promotion');
INSERT INTO `sys_menu` VALUES (34, '广告管理', '/site/adv.*', '', 24, 2, 'adv_list');
INSERT INTO `sys_menu` VALUES (35, '盲盒订单', '/blind-box/blind-box-order.*', '', 31, 2, 'blind_box_order');
INSERT INTO `sys_menu` VALUES (37, '订单', '/order.*', '订单', 36, 5, 'order');
INSERT INTO `sys_menu` VALUES (38, '订单管理', '/order', '订单管理', 0, 5, 'order');
INSERT INTO `sys_menu` VALUES (39, '订单列表', '/order/order.*', '订单列表', 38, 1, 'order_list');
INSERT INTO `sys_menu` VALUES (40, '物流管理', '/site/logistics.*', '', 24, 4, 'logistics');
INSERT INTO `sys_menu` VALUES (41, '会员余额记录', '/member/balance-detail.*', '', 7, 5, 'member_balance');
INSERT INTO `sys_menu` VALUES (42, '首页管理', '/site/home-site.*', '', 24, 0, 'home_site');
COMMIT;

-- ----------------------------
-- Table structure for sys_page_statistics
-- ----------------------------
DROP TABLE IF EXISTS `sys_page_statistics`;
CREATE TABLE `sys_page_statistics` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `member_id` int(8) DEFAULT '0' COMMENT '会员id',
  `page_id` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '页面id',
  `rush_purchase_id` int(8) DEFAULT NULL COMMENT '活动id',
  `visit_time` bigint(20) DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='页面统计';

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `role_code` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `permissions` text COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'SUPER_ADMIN', '超级管理员，拥有所有权限', 'dashboard,dashboard_workplace,setting,system_menu,system_role,system_admin,site_setting,admin_setting,member-p,member_list,member_grade,member_withdrawal,goods,goods_list,goods_classify,goods_brand,promotion,rush_purchase,site,site_article,development');
INSERT INTO `sys_role` VALUES (2, '管理员', 'ADMIN', '管理员', 'dashboard_workplace,system_menu,system_role,system_admin,site_setting,admin_setting,member_list,member_sign,member_grade,member_withdrawal,goods_list,goods_classify,goods_brand,rush_purchase,rush_purchase_order,member_income,put_sale_order_list,article_list,rush_purchase_cart,adv_promotion,adv_list,blind_box_list,blind_box_order,order_list,logistics,member_balance,home_site');
INSERT INTO `sys_role` VALUES (3, '开发人员', 'DEV', '开发人员', 'development,system_menu,dashboard,dashboard_workplace');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_admin
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_admin`;
CREATE TABLE `sys_role_admin` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `role_id` int(20) DEFAULT NULL,
  `admin_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of sys_role_admin
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_admin` VALUES (1, 1, 1);
INSERT INTO `sys_role_admin` VALUES (2, 2, 2);
INSERT INTO `sys_role_admin` VALUES (3, 3, 3);
COMMIT;

-- ----------------------------
-- Table structure for sys_setting
-- ----------------------------
DROP TABLE IF EXISTS `sys_setting`;
CREATE TABLE `sys_setting` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `setting_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '配置名称',
  `settings` text COLLATE utf8mb4_bin COMMENT '配置内容JSON格式',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='系统配置表';

-- ----------------------------
-- Records of sys_setting
-- ----------------------------
BEGIN;
INSERT INTO `sys_setting` VALUES (1, 'site', '{\"siteName\":\"信隆优创\",\"logo\":\"https://xiyuange.oss-cn-beijing.aliyuncs.com/setting/20230707/d6d544f0-110a-4555-9923-3d033a5faaae.png\",\"smallLogo\":\"https://xiyuange.oss-cn-beijing.aliyuncs.com/setting/20230707/5dad3de1-c57b-44d0-9a60-16d71711d08f.png\",\"closeText\":\"网站维护中，暂时无法访问！本网站正在进行系统维护和技术升级，网站暂时无法访问，敬请谅解！\",\"systemOpen\":true,\"isShow\":false}');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
