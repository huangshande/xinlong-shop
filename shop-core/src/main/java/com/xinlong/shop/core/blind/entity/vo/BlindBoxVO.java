package com.xinlong.shop.core.blind.entity.vo;

import com.xinlong.shop.core.blind.entity.BlindBox;
import com.xinlong.shop.core.blind.entity.BlindBoxGoods;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 10:36 2023/5/24
 */
public class BlindBoxVO extends BlindBox {


    /**
     * 商品列表
     */
    private List<BlindBoxGoods> goodsList;



    public List<BlindBoxGoods> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<BlindBoxGoods> goodsList) {
        this.goodsList = goodsList;
    }
}
