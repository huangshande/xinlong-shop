package com.xinlong.shop.core.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinlong.shop.core.goods.entity.GoodsQueryParam;
import com.xinlong.shop.core.goods.entity.dto.GoodsDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 商品DTO Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2022-12-04
 */
@Mapper
public interface GoodsDTOMapper extends BaseMapper<GoodsDTO> {

    IPage<GoodsDTO> findGoodsPage(IPage<GoodsDTO> page, @Param("goodsQueryParam") GoodsQueryParam goodsQueryParam);

    GoodsDTO findGoodsById(@Param("goodsId") Integer id);
}
