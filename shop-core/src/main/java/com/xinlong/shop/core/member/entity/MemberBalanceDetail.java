package com.xinlong.shop.core.member.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 用户余额明细
 * </p>
 *
 * @author Sylow
 * @since 2023-04-06
 */
@TableName("s_member_balance_detail")
public class MemberBalanceDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 来源 类型1的来源就是上架订单号
     */
    private String source;

    /**
     * 类型0=系统默认 1等于服务费分成 参考
     */
    private Integer type;

    /**
     * 金额
     */
    private BigDecimal price;

    /**
     * 备注
     */
    private String memo;

    /**
     * 操作时间
     */
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public Integer getType() {
        return type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "MemberBalanceDetail{" +
            "id=" + id +
            ", memberId=" + memberId +
            ", type=" + type +
            ", price=" + price +
            ", memo=" + memo +
            ", createTime=" + createTime +
        "}";
    }
}
