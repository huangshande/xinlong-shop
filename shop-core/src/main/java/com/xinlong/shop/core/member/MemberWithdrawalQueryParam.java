package com.xinlong.shop.core.member;

/**
 * @Author Sylow
 * @Description 会员提现查询参数
 * @Date: Created in 10:48 2023/4/11
 */
public class MemberWithdrawalQueryParam {

    private String mobile;

    private Integer status;

    private Long startTime;

    private Long endTime;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }
}
