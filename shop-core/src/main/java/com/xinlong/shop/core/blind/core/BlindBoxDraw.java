package com.xinlong.shop.core.blind.core;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @Author Sylow
 * @Description 盲盒核心算法
 * @Date: Created in 17:52 2023/6/5
 */
@Component
public class BlindBoxDraw {

    /**
     * 先循环构建概率占比，假如随机数为0.2646，添加进入后进行排序，排序的下标就是所要得到的概率的下标
     * 例如：0.2646添加进去后排序为：0.2646，0.3，0.5，0.7，1.0, 那么0.2546就是在0.3这个区间内，所以返回0,即为第一个概率
     * @param probEntityList
     * @return
     */
    public ProbEntity draw(List<ProbEntity> probEntityList) {
        //最终加入随机值后排序的集合
        List<Double> sortRateList = new ArrayList<>();

        // 计算概率总和
        Integer sumRate = 0;
        for (ProbEntity probEntity : probEntityList) {
            sumRate += probEntity.getRate();
        }

        if (sumRate != 0) {
            // 概率所占比例
            double rate = 0D;
            for (ProbEntity probEntity : probEntityList) {
                rate += probEntity.getRate();
                // 构建一个比例区段组成的集合(避免概率和不为1)
                sortRateList.add(rate / sumRate);
            }

            // 随机生成一个随机数，并排序
            ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
            //生成0-1之间一个随机数
            double random = threadLocalRandom.nextDouble(0, 1);
            sortRateList.add(random);
            Collections.sort(sortRateList);
            // 得到下标
            int result = sortRateList.indexOf(random);
            // 返回下标
            return probEntityList.get(result);
        }

        return null;
    }

    /**
     *  先循环构建概率占比，假如随机数为0.2646，添加进入后进行排序，排序的下标就是所要得到的概率的下标
     *  例如：0.2646添加进去后排序为：0.2646，0.3，0.5，0.7，1.0, 那么0.2546就是在0.3这个区间内，所以返回0,即为第一个概率
     * @param probList
     * @return
     */
//    public static int draw(List<Integer> probList) {
//        //最终加入随机值后排序的集合
//        List<Double> sortRateList = new ArrayList<>();
//
//        // 计算概率总和
//        Integer sumRate = 0;
//        for (Integer prob : probList) {
//            sumRate += prob;
//        }
//
//        if (sumRate != 0) {
//            // 概率所占比例
//            double rate = 0D;
//            for (Integer prob : probList) {
//                rate += prob;
//                // 构建一个比例区段组成的集合(避免概率和不为1)
//                sortRateList.add(rate / sumRate);
//            }
//
//            // 随机生成一个随机数，并排序
//            ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
//            //生成0-1之间一个随机数
//            double random = threadLocalRandom.nextDouble(0, 1);
//            sortRateList.add(random);
//            Collections.sort(sortRateList);
//            // 得到下标
//            int result = sortRateList.indexOf(random);
//            // 返回下标
//            return result;
//        }
//
//        return -1;
//    }

//    public static void main(String[] args) {
//
//        List<BlindBoxGoods> list = new ArrayList<>();
//        list.add(new BlindBoxGoods(1, 1, 30));
//        list.add(new BlindBoxGoods(2, 2, 15));
//        list.add(new BlindBoxGoods(3, 3, 10));
//        list.add(new BlindBoxGoods(4, 4, 15));
//        list.add(new BlindBoxGoods(5, 5, 30));
////        list.add(new BlindBoxGoods(5, 5, 4));
////        list.add(new BlindBoxGoods(6, 6, 1));
//
//        //将商品概率添加到集合中,用于下面抽奖
//        List<Integer> probList = new ArrayList<>(list.size());
//        for (BlindBoxGoods blind : list) {
//            probList.add(blind.getRealRate());
//        }
//        //开盒核心方法
//        Integer one = 0;
//        Integer two = 0;
//        Integer three = 0;
//        Integer four = 0;
//        Integer five = 0;
//        Integer error = 0;
//        for(int i=0;i<10000;i++){
//            int drawResult = draw(probList);
//            switch (drawResult) {
//                case 0:
//                    //System.out.println("抽中了1");
//                    one++;
//                    break;
//                case 1:
//                    //System.out.println("抽中了2");
//                    two++;
//                    break;
//                case 2:
//                    //System.out.println("抽中了3");
//                    three++;
//                    break;
//                case 3:
//                    //System.out.println("抽中了4");
//                    four++;
//                    break;
//                case 4:
//                    //System.out.println("抽中了5");
//                    five++;
//                    break;
//                default:
//                    //System.out.println("异常");
//                    error++;
//                    break;
//            }
//        }
//        System.out.println("抽中了1的次数："+one);
//        System.out.println("抽中了2的次数："+two);
//        System.out.println("抽中了3的次数："+three);
//        System.out.println("抽中了4的次数："+four);
//        System.out.println("抽中了5的次数："+five);
//        System.out.println("异常次数："+error);
//
//    }


//
//
//    public static class BlindBoxGoods {
//        public BlindBoxGoods(Integer id, Integer goodsId, Integer realRate) {
//            this.id = id;
//            this.goodsId = goodsId;
//            this.realRate = realRate;
//        }
//        private Integer id;
//        private Integer goodsId;
//        private Integer realRate;
//
//        public Integer getId() {
//            return id;
//        }
//
//        public void setId(Integer id) {
//            this.id = id;
//        }
//
//        public Integer getGoodsId() {
//            return goodsId;
//        }
//
//        public void setGoodsId(Integer goodsId) {
//            this.goodsId = goodsId;
//        }
//
//        public Integer getRealRate() {
//            return realRate;
//        }
//
//        public void setRealRate(Integer realRate) {
//            this.realRate = realRate;
//        }
//    }

}
