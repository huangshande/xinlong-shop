package com.xinlong.shop.core.blind.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.core.blind.entity.BlindBox;
import com.xinlong.shop.core.blind.entity.vo.BlindBoxVO;
import com.xinlong.shop.core.blind.mapper.BlindBoxMapper;
import com.xinlong.shop.core.blind.mapstruct.BlindBoxStruct;
import com.xinlong.shop.core.blind.service.IBlindBoxGoodsService;
import com.xinlong.shop.core.blind.service.IBlindBoxService;
import com.xinlong.shop.framework.exception.ServiceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 盲盒 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-15
 */
@Service
public class BlindBoxServiceImpl extends ServiceImpl<BlindBoxMapper, BlindBox> implements IBlindBoxService {

    private final BlindBoxStruct blindBoxStruct;
    private final IBlindBoxGoodsService blindBoxGoodsService;

    public BlindBoxServiceImpl(BlindBoxStruct blindBoxStruct, IBlindBoxGoodsService blindBoxGoodsService) {
        this.blindBoxStruct = blindBoxStruct;
        this.blindBoxGoodsService = blindBoxGoodsService;
    }

    @Override
    public void update(BlindBox blindBox, Integer id) {
        UpdateWrapper<BlindBox> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(blindBox, updateWrapper);
    }
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        boolean delResult = this.blindBoxGoodsService.deleteByBlindBoxId(id);
        if (!delResult) {
            throw new ServiceException("系统异常，删除失败");
        }
        UpdateWrapper<BlindBox> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveBlindBox(BlindBoxVO blindBoxVO) {
        BlindBox blindBox = blindBoxStruct.toBlindBox(blindBoxVO);
        blindBox.setCreateTime(DateUtil.currentSeconds());
        boolean result = this.save(blindBox);
        if (result) {
            boolean saveBlindBoxGoods = blindBoxGoodsService.batchSaveBlindBoxGoods(blindBoxVO.getGoodsList(), blindBox.getId());
            if (!saveBlindBoxGoods) {
                throw new ServiceException("系统异常，保存失败");
            }
        } else {
            throw new ServiceException("系统异常，保存失败");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateBlindBox(BlindBoxVO blindBoxVO, Integer id) {
        // 先删除明细  再新增
        boolean delResult = this.blindBoxGoodsService.deleteByBlindBoxId(id);
        if (!delResult) {
            throw new ServiceException("系统异常，删除盲盒商品失败");
        }
        BlindBox blindBox = blindBoxStruct.toBlindBox(blindBoxVO);
        blindBox.setId(id);
        boolean updateResult = this.updateById(blindBox);
        if (updateResult) {
            boolean saveBlindBoxGoods = blindBoxGoodsService.batchSaveBlindBoxGoods(blindBoxVO.getGoodsList(), blindBox.getId());
            if (!saveBlindBoxGoods) {
                throw new ServiceException("系统异常，保存失败");
            }
        } else {
            throw new ServiceException("系统异常，保存失败");
        }
    }

    @Override
    public void updateStatus(Integer id, Integer status) {
        BlindBox blindBox = new BlindBox();
        blindBox.setId(id);
        blindBox.setStatus(status);
        boolean updateResult = this.updateById(blindBox);
        if (!updateResult) {
            throw new ServiceException("系统异常，更新失败");
        }
    }

    @Override
    public List<BlindBox> findAllBlindBox(String blindBoxName) {
        QueryWrapper<BlindBox> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(blindBoxName != null, "blind_box_name", blindBoxName);
        return this.list(queryWrapper);
    }

}
