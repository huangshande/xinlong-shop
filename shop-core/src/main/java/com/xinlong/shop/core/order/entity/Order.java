package com.xinlong.shop.core.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author Sylow
 * @since 2023-07-04
 */
@ApiModel(value="Order对象", description="订单表")
@TableName("s_order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    public Order(){}

    public Order(String orderSn, Integer orderType, Integer memberId, String memberName, BigDecimal orderPrice, BigDecimal goodsPrice, BigDecimal shippingPrice, BigDecimal discountPrice, String discountRule, Integer goodsNum, Integer shipAreaId, String shipAreaName, String shipFullAddress, String shipName, String shipMobile, Long createTime) {
        this.orderSn = orderSn;
        this.orderType = orderType;
        this.memberId = memberId;
        this.memberName = memberName;
        this.orderPrice = orderPrice;
        this.goodsPrice = goodsPrice;
        this.shippingPrice = shippingPrice;
        this.discountPrice = discountPrice;
        this.discountRule = discountRule;
        this.goodsNum = goodsNum;
        this.shipAreaId = shipAreaId;
        this.shipAreaName = shipAreaName;
        this.shipFullAddress = shipFullAddress;
        this.shipName = shipName;
        this.shipMobile = shipMobile;
        this.createTime = createTime;
    }

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    /**
     * 订单类型
     */
    @ApiModelProperty(value = "订单类型")
    private Integer orderType;

    /**
     * 会员id
     */
    @ApiModelProperty(value = "会员id")
    private Integer memberId;

    /**
     * 会员名称
     */
    @ApiModelProperty(value = "会员名称")
    private String memberName;

    /**
     * 订单状态
     */
    @ApiModelProperty(value = "订单状态")
    private Integer orderStatus;

    /**
     * 付款状态
     */
    @ApiModelProperty(value = "付款状态")
    private Integer payStatus;

    /**
     * 支付时间
     */
    @ApiModelProperty(value = "支付时间")
    private Long paymentTime;

    /**
     * 支付单单号
     */
    @ApiModelProperty(value = "支付单单号")
    private String paymentSn;

    /**
     * 订单总金额
     */
    @ApiModelProperty(value = "订单总金额")
    private BigDecimal orderPrice;

    /**
     * 商品总金额
     */
    @ApiModelProperty(value = "商品总金额")
    private BigDecimal goodsPrice;

    /**
     * 配送金额
     */
    @ApiModelProperty(value = "配送金额")
    private BigDecimal shippingPrice;

    /**
     * 优惠金额
     */
    @ApiModelProperty(value = "优惠金额")
    private BigDecimal discountPrice;

    /**
     * 优惠规则
     */
    @ApiModelProperty(value = "优惠规则")
    private String discountRule;

    /**
     * 商品总数
     */
    @ApiModelProperty(value = "商品总数")
    private Integer goodsNum;

    @ApiModelProperty(value = "配送区域id")
    private Integer shipAreaId;

    @ApiModelProperty(value = "配送区域名称")
    private String shipAreaName;

//    /**
//     * 配送省份id
//     */
//    @ApiModelProperty(value = "配送省份id")
//    private Integer shipProvinceId;
//
//    /**
//     * 配送省名
//     */
//    @ApiModelProperty(value = "配送省名")
//    private String shipProvinceName;
//
//    /**
//     * 配送市区id
//     */
//    @ApiModelProperty
//    private Integer shipCityId;
//
//    /**
//     * 配送市区名称
//     */
//    @ApiModelProperty(value = "配送市区名称")
//    private String shipCityName;
//
//    /**
//     * 配送县区id
//     */
//    @ApiModelProperty(value = "配送县区id")
//    private Integer shipCountyId;
//
//    /**
//     * 配送县区名称
//     */
//    @ApiModelProperty(value = "配送县区名称")
//    private String shipCountyName;

    /**
     * 配送详细地址
     */
    @ApiModelProperty(value = "配送详细地址")
    private String shipFullAddress;

    /**
     * 收货人姓名
     */
    @ApiModelProperty(value = "收货人姓名")
    private String shipName;

    /**
     * 收货人手机号
     */
    @ApiModelProperty(value = "收货人手机号")
    private String shipMobile;

    /**
     * 发货时间
     */
    @ApiModelProperty(value = "发货时间")
    private Long shipTime;

    /**
     * 物流单号
     */
    @ApiModelProperty(value = "物流单号")
    private String logisticsSn;

    /**
     * 物流公司id
     */
    @ApiModelProperty(value = "物流公司id")
    private Integer logisticsId;

    /**
     * 物流公司名称
     */
    @ApiModelProperty(value = "物流公司名称")
    private String logisticsName;

    /**
     * 订单备注
     */
    @ApiModelProperty(value = "订单备注")
    private String remark;

    /**
     * 订单取消原因
     */
    @ApiModelProperty(value = "订单取消原因")
    private String cancelReason;

    /**
     * 订单创建时间
     */
    @ApiModelProperty(value = "订单创建时间")
    private Long createTime;

    /**
     * 完成时间
     */
    @ApiModelProperty(value = "完成时间")
    private Long completeTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }
    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }
    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }
    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }
    public Long getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Long paymentTime) {
        this.paymentTime = paymentTime;
    }
    public String getPaymentSn() {
        return paymentSn;
    }

    public void setPaymentSn(String paymentSn) {
        this.paymentSn = paymentSn;
    }
    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }
    public BigDecimal getShippingPrice() {
        return shippingPrice;
    }

    public void setShippingPrice(BigDecimal shippingPrice) {
        this.shippingPrice = shippingPrice;
    }
    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }
    public String getDiscountRule() {
        return discountRule;
    }

    public void setDiscountRule(String discountRule) {
        this.discountRule = discountRule;
    }
    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public Integer getShipAreaId() {
        return shipAreaId;
    }

    public void setShipAreaId(Integer shipAreaId) {
        this.shipAreaId = shipAreaId;
    }

    public String getShipAreaName() {
        return shipAreaName;
    }

    public void setShipAreaName(String shipAreaName) {
        this.shipAreaName = shipAreaName;
    }

    public String getShipFullAddress() {
        return shipFullAddress;
    }

    public void setShipFullAddress(String shipFullAddress) {
        this.shipFullAddress = shipFullAddress;
    }
    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }
    public String getShipMobile() {
        return shipMobile;
    }

    public void setShipMobile(String shipMobile) {
        this.shipMobile = shipMobile;
    }
    public Long getShipTime() {
        return shipTime;
    }

    public void setShipTime(Long shipTime) {
        this.shipTime = shipTime;
    }
    public String getLogisticsSn() {
        return logisticsSn;
    }

    public void setLogisticsSn(String logisticsSn) {
        this.logisticsSn = logisticsSn;
    }
    public Integer getLogisticsId() {
        return logisticsId;
    }

    public void setLogisticsId(Integer logisticsId) {
        this.logisticsId = logisticsId;
    }
    public String getLogisticsName() {
        return logisticsName;
    }

    public void setLogisticsName(String logisticsName) {
        this.logisticsName = logisticsName;
    }
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
    public Long getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Long completeTime) {
        this.completeTime = completeTime;
    }

    @Override
    public String toString() {
        return "Order{" +
            "id=" + id +
            ", orderSn=" + orderSn +
            ", orderType=" + orderType +
            ", memberId=" + memberId +
            ", memberName=" + memberName +
            ", orderStatus=" + orderStatus +
            ", payStatus=" + payStatus +
            ", paymentTime=" + paymentTime +
            ", paymentSn=" + paymentSn +
            ", orderPrice=" + orderPrice +
            ", goodsPrice=" + goodsPrice +
            ", shippingPrice=" + shippingPrice +
            ", discountPrice=" + discountPrice +
            ", discountRule=" + discountRule +
            ", goodsNum=" + goodsNum +
            ", shipAreaId=" + shipAreaId +
            ", shipAreaName=" + shipAreaName +
            ", shipFullAddress=" + shipFullAddress +
            ", shipName=" + shipName +
            ", shipMobile=" + shipMobile +
            ", shipTime=" + shipTime +
            ", logisticsSn=" + logisticsSn +
            ", logisticsId=" + logisticsId +
            ", logisticsName=" + logisticsName +
            ", remark=" + remark +
            ", cancelReason=" + cancelReason +
            ", createTime=" + createTime +
            ", completeTime=" + completeTime +
        "}";
    }
}
