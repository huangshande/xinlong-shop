package com.xinlong.shop.core.member.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 会员收益表
 * </p>
 *
 * @author Sylow
 * @since 2023-03-30
 */
@TableName("s_member_activity_income")
public class MemberActivityIncome implements Serializable {

    private static final long serialVersionUID = 1L;

    public MemberActivityIncome(){}

    public MemberActivityIncome(Integer memberId, BigDecimal buyPrice, BigDecimal sellPrice, BigDecimal servicePrice, BigDecimal profit, Integer rushPurchaseDetailId, String activityId){
        this.memberId = memberId;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
        this.servicePrice = servicePrice;
        this.profit = profit;
        this.rushPurchaseDetailId = rushPurchaseDetailId;
        this.activityId = activityId;
    }

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 买入价格
     */
    private BigDecimal buyPrice;

    /**
     * 卖出价格
     */
    private BigDecimal sellPrice;

    /**
     * 服务费
     */
    private BigDecimal servicePrice;

    /**
     * 利润=卖出价格减去买入和服务费
     */
    private BigDecimal profit;

    /**
     * 活动明细id
     */
    private Integer rushPurchaseDetailId;


    /**
     *  活动期数
     */
    private String activityId;

    /**
     * 创建时间戳
     */
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }
    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }
    public BigDecimal getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(BigDecimal servicePrice) {
        this.servicePrice = servicePrice;
    }
    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }
    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "MemberActivityIncome{" +
            "id=" + id +
            ", memberId=" + memberId +
            ", buyPrice=" + buyPrice +
            ", sellPrice=" + sellPrice +
            ", servicePrice=" + servicePrice +
            ", profit=" + profit +
            ", activityId=" + activityId +
            ", createTime=" + createTime +
        "}";
    }
}
