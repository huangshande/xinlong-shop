package com.xinlong.shop.core.member.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 会员等级
 * </p>
 *
 * @author Sylow
 * @since 2022-11-07
 */
@TableName("s_member_grade")
public class MemberGrade implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 等级名称
     */
    private String gradeName;

    /**
     * 等级标识
     */
    private String gradeId;

    /**
     * 禁用状态0=正常，1禁用，2删除
     */
    private Integer disable;

    /**
     * 排序
     */
    private Integer sort;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }
    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "MemberGrade{" +
                "id=" + id +
                ", gradeName=" + gradeName +
                ", gradeId=" + gradeId +
                ", disable=" + disable +
                ", sort=" + sort +
                "}";
    }
}
