package com.xinlong.shop.core.member.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 用户积分明细
 * </p>
 *
 * @author Sylow
 * @since 2023-07-22
 */
@TableName("s_member_points_detail")
public class MemberPointsDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 来源
     */
    private String source;

    /**
     * 0上架商品1系统赠送
     */
    private Integer type;

    /**
     * 积分
     */
    private BigDecimal points;

    /**
     * 备注
     */
    private String memo;

    /**
     * 操作时间
     */
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "MemberPointsDetail{" +
            "id=" + id +
            ", memberId=" + memberId +
            ", source=" + source +
            ", type=" + type +
            ", points=" + points +
            ", memo=" + memo +
            ", createTime=" + createTime +
        "}";
    }
}
