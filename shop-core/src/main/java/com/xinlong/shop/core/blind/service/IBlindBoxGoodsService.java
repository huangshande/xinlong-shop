package com.xinlong.shop.core.blind.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.blind.entity.BlindBoxGoods;

import java.util.List;

/**
 * <p>
 * 盲盒关联商品表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-15
 */
public interface IBlindBoxGoodsService extends IService<BlindBoxGoods> {

    void update(BlindBoxGoods blindBoxGoods, Integer id);

    void delete(Integer id);

    /**
     * 批量保存盲盒商品
     * @param blindBoxGoodsList
     * @param blindBoxId
     * @return
     */
    boolean batchSaveBlindBoxGoods(List<BlindBoxGoods> blindBoxGoodsList, Integer blindBoxId);

    /**
     * 根据盲盒id查询盲盒商品明细
     * @param blindBoxId
     * @return
     */
    List<BlindBoxGoods> findByBlindBoxId(Integer blindBoxId);

    /**
     * 根据盲盒id删除盲盒商品明细
     * @param blindBoxId
     * @return
     */
    boolean deleteByBlindBoxId(Integer blindBoxId);

    /**
     * 根据盲盒id和商品概率标签查询盲盒商品列表
     * @param blindBoxId
     * @param tag
     * @return
     */
    List<BlindBoxGoods> findByBlindBoxIdAndTag(Integer blindBoxId, String tag);

    /**
     * 库存减少
     * @param blindBoxId
     * @param goodsId
     * @param stock
     * @return
     */
    boolean reduceStock(Integer blindBoxId, Integer goodsId, Integer stock);

}
