package com.xinlong.shop.core.goods.service;

import com.xinlong.shop.core.goods.entity.GoodsClassify;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.goods.entity.vo.BuyerGoodsClassifyVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
public interface IGoodsClassifyService extends IService<GoodsClassify> {

    void add(GoodsClassify goodsClassify);

    void update(GoodsClassify goodsClassify, Integer id);

    void delete(Integer id);

    List<GoodsClassify> findByPid(Integer pid);

    void updateShow(Integer id, Integer isShow);

    /**
     * 查询所有的分类，包含父子关系
     * @return
     */
    List<BuyerGoodsClassifyVO> findListChildren();

}
