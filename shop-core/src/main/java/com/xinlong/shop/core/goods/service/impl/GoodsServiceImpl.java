package com.xinlong.shop.core.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinlong.shop.core.goods.entity.Goods;
import com.xinlong.shop.core.goods.entity.GoodsQueryParam;
import com.xinlong.shop.core.goods.entity.dto.GoodsDTO;
import com.xinlong.shop.core.goods.mapper.GoodsDTOMapper;
import com.xinlong.shop.core.goods.mapper.GoodsMapper;
import com.xinlong.shop.core.goods.service.IGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import java.util.List;

/**
 * <p>
 * 商品 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {

    private final GoodsMapper goodsMapper;

    private final GoodsDTOMapper goodsDTOMapper;

    public GoodsServiceImpl(GoodsMapper goodsMapper, GoodsDTOMapper goodsDTOMapper) {
        this.goodsMapper = goodsMapper;
        this.goodsDTOMapper = goodsDTOMapper;
    }

    @Override
    public void update(Goods goods, Integer id) {
        UpdateWrapper<Goods> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(goods, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<Goods> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public IPage<GoodsDTO> page(IPage<GoodsDTO> page, GoodsQueryParam goodsQueryParam) {

        return this.goodsDTOMapper.findGoodsPage(page, goodsQueryParam);
    }

    @Override
    public void updateStatus(Integer id, Integer status) {
        UpdateWrapper<Goods> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        Goods goods = new Goods();
        goods.setGoodsStatus(status);
        this.update(goods, updateWrapper);
    }

    @Override
    public GoodsDTO findById(Integer id) {
        return this.goodsDTOMapper.findGoodsById(id);
    }

    @Override
    public List<Goods> findAllGoods(String goodsName, Integer goodsType) {
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("goods_name", goodsName);
        queryWrapper.eq(goodsType != null, "goods_type", goodsType);
        queryWrapper.eq("goods_status", 1);
        return this.list(queryWrapper);
    }

}
