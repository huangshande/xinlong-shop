package com.xinlong.shop.core.goods.entity;

/**
 * @Author Sylow
 * @Description 商品类型枚举
 * @Date: Created in 18:22 2023/6/11
 */
public enum GoodsTypeEnum {

    // 普通商品、抢购商品、盲盒商品
    NORMAL(0), RUSH_PURCHASE(1), BLIND_BOX(2);

    private int code;
    private GoodsTypeEnum(int code) {
        this.code = code;
    }

    public int getCode(){
        return code;
    }
}
