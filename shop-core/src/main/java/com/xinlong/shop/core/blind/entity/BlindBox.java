package com.xinlong.shop.core.blind.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 盲盒
 * </p>
 *
 * @author Sylow
 * @since 2023-05-15
 */
@TableName(value = "s_blind_box", autoResultMap = true)
public class BlindBox implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 盲盒名称
     */
    @NotBlank(message = "盲盒名称不能为空")
    private String blindBoxName;

    /**
     * 背景图
     */
    @NotBlank(message = "背景图不能为空")
    private String backImg;

    /**
     * 盲盒标题图
     */
    @NotBlank(message = "盲盒标题图不能为空")
    private String blindBoxImg;

    /**
     * 单价 仅仅做盲盒列表展示用，实际购买价格以价格规则为准
     */
    @NotNull(message = "价格不能为空")
    private BigDecimal price;

    /**
     * 价格规则
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<Map> priceRules;

    /**
     * 产出规则  规则图片名称暂时写死，概率可编辑
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<Map> produceRules;

    /**
     * 显示规则
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<Map> showProduceRules;


    /**
     * 状态
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getBlindBoxName() {
        return blindBoxName;
    }

    public void setBlindBoxName(String blindBoxName) {
        this.blindBoxName = blindBoxName;
    }
    public String getBackImg() {
        return backImg;
    }

    public void setBackImg(String backImg) {
        this.backImg = backImg;
    }
    public String getBlindBoxImg() {
        return blindBoxImg;
    }

    public void setBlindBoxImg(String blindBoxImg) {
        this.blindBoxImg = blindBoxImg;
    }
    public BigDecimal getPrice() {
        return price;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<Map> getPriceRules() {
        return priceRules;
    }

    public void setPriceRules(List<Map> priceRules) {
        this.priceRules = priceRules;
    }

    public List<Map> getShowProduceRules() {
        return showProduceRules;
    }

    public void setShowProduceRules(List<Map> showProduceRules) {
        this.showProduceRules = showProduceRules;
    }

    public List<Map> getProduceRules() {
        return produceRules;
    }

    public void setProduceRules(List<Map> produceRules) {
        this.produceRules = produceRules;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }



    @Override
    public String toString() {
        return "BlindBox{" +
            "id=" + id +
            ", blindBoxName=" + blindBoxName +
            ", backImg=" + backImg +
            ", blindBoxImg=" + blindBoxImg +
            ", price=" + price +
            ", priceRules=" + priceRules +
            ", produceRules=" + produceRules +
            ", showProduceRules=" + showProduceRules +
            ", status=" + status +
            ", createTime=" + createTime +
        "}";
    }
}
