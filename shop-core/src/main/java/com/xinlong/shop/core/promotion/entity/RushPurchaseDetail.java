package com.xinlong.shop.core.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 抢购活动明细表
 * </p>
 *
 * @author Sylow
 * @since 2023-02-17
 */
@TableName("s_rush_purchase_detail")
public class RushPurchaseDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主健
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 活动id
     */
    private Integer rushPurchaseId;

    /**
     * 活动场次id
     */
    private String activityId;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 卖家会员id
     */
    private Integer sellerId;

    /**
     * 卖家会员名称
     */
    private String sellerName;

    /**
     * 卖家手机
     */
    private String sellerMobile;

    /**
     * 售价
     */
    private BigDecimal price;

    /**
     * 原价
     */
    private BigDecimal originalPrice;

    /**
     * 可获得积分
     */
    private BigDecimal points;

    /**
     * 买家会员id
     */
    private Integer buyerId;

    /**
     * 买家会员名称
     */
    private String buyerName;

    /**
     * 买家手机号
     */
    private String buyerMobile;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 支付凭证
     */
    private String payProof;

    /**
     * 是否提货
     */
    private Integer isPickUp;

    /**
     * 预订会员id
     */
    private Integer bookingMemberId;

    /**
     * 购买时间
     */
    private Long buyTime;

    /**
     * 创建时间
     */
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getRushPurchaseId() {
        return rushPurchaseId;
    }

    public void setRushPurchaseId(Integer rushPurchaseId) {
        this.rushPurchaseId = rushPurchaseId;
    }
    public Integer getGoodsId() {
        return goodsId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }
    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }
    public String getSellerMobile() {
        return sellerMobile;
    }

    public void setSellerMobile(String sellerMobile) {
        this.sellerMobile = sellerMobile;
    }
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public Integer getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Integer buyerId) {
        this.buyerId = buyerId;
    }
    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }
    public String getBuyerMobile() {
        return buyerMobile;
    }

    public void setBuyerMobile(String buyerMobile) {
        this.buyerMobile = buyerMobile;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPayProof() {
        return payProof;
    }

    public void setPayProof(String payProof) {
        this.payProof = payProof;
    }

    public Integer getIsPickUp() {
        return isPickUp;
    }

    public void setIsPickUp(Integer isPickUp) {
        this.isPickUp = isPickUp;
    }

    public Integer getBookingMemberId() {
        return bookingMemberId;
    }

    public void setBookingMemberId(Integer bookingMemberId) {
        this.bookingMemberId = bookingMemberId;
    }

    public Long getBuyTime() {
        return buyTime;
    }

    public void setBuyTime(Long buyTime) {
        this.buyTime = buyTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "RushPurchaseDetail{" +
            "id=" + id +
            ", rushPurchaseId=" + rushPurchaseId +
            ", activityId=" + activityId +
            ", orderSn=" + orderSn +
            ", goodsId=" + goodsId +
            ", goodsName=" + goodsName +
            ", sellerId=" + sellerId +
            ", sellerName=" + sellerName +
            ", sellerMobile=" + sellerMobile +
            ", price=" + price +
            ", originalPrice=" + originalPrice +
            ", points=" + points +
            ", buyerId=" + buyerId +
            ", buyerName=" + buyerName +
            ", buyerMobile=" + buyerMobile +
            ", status=" + status +
            ", payProof=" + payProof +
            ", isPickUp=" + isPickUp +
            ", bookingMemberId=" + bookingMemberId +
            ", buyTime=" + buyTime +
            ", createTime=" + createTime +

        "}";
    }
}
