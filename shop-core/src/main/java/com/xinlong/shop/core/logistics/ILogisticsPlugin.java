package com.xinlong.shop.core.logistics;

import com.xinlong.shop.core.logistics.entity.LogisticsEnum;
import com.xinlong.shop.core.logistics.entity.LogisticsTraces;
import com.xinlong.shop.core.site.entity.Logistics;

/**
 * @Author Sylow
 * @Description 物流查询插件接口
 * @Date: Created in 21:47 2023/7/27
 */
public interface ILogisticsPlugin {

    LogisticsEnum pluginName();

    /**
     * 即时查询物流轨迹信息
     * @param logistics 物流公司对象
     * @param logisticsSn 物流单号
     * @param mobile    手机号
     * @return
     */
    LogisticsTraces instantQuery(Logistics logistics, String logisticsSn, String mobile);
}
