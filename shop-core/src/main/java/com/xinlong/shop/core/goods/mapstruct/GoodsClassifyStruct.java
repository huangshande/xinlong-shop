package com.xinlong.shop.core.goods.mapstruct;

import com.xinlong.shop.core.goods.entity.GoodsClassify;
import com.xinlong.shop.core.goods.entity.vo.BuyerGoodsClassifyVO;
import com.xinlong.shop.core.goods.entity.vo.GoodsClassifyVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 17:13 2022/11/10
 */
@Mapper(componentModel = "spring")
public interface GoodsClassifyStruct {
    GoodsClassifyStruct INSTANCE = Mappers.getMapper(GoodsClassifyStruct.class);

    List<GoodsClassifyVO> toGoodsClassifyVO(List<GoodsClassify> goodsClassifys);

    BuyerGoodsClassifyVO toBuyerGoodsClassifyVO(GoodsClassify goodsClassify);
}
