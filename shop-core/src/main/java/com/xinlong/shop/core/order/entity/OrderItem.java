package com.xinlong.shop.core.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 订单子项表
 * </p>
 *
 * @author Sylow
 * @since 2023-07-04
 */
@TableName("s_order_item")
public class OrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    public OrderItem(){}

    public OrderItem(Integer orderId, String orderSn, Integer goodsId, String goodsName, String goodsImg, Integer goodsNum, BigDecimal goodsOriginalPrice, BigDecimal goodsPrice, BigDecimal goodsDiscountPrice, String discountRule, Integer status, Long createTime) {
        this.orderId = orderId;
        this.orderSn = orderSn;
        this.goodsId = goodsId;
        this.goodsName = goodsName;
        this.goodsImg = goodsImg;
        this.goodsNum = goodsNum;
        this.goodsOriginalPrice = goodsOriginalPrice;
        this.goodsPrice = goodsPrice;
        this.goodsDiscountPrice = goodsDiscountPrice;
        this.discountRule = discountRule;
        this.status = status;
        this.createTime = createTime;
    }

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 订单ID
     */
    private Integer orderId;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 商品ID
     */
    private Integer goodsId;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    /**
     * 商品主图
     */
    @ApiModelProperty(value = "商品主图")
    private String goodsImg;

    /**
     * 商品数量
     */
    @ApiModelProperty(value = "商品数量")
    private Integer goodsNum;

    /**
     * 商品原价
     */
    @ApiModelProperty(value = "商品原价")
    private BigDecimal goodsOriginalPrice;

    /**
     * 商品成交价
     */
    @ApiModelProperty(value = "商品成交价")
    private BigDecimal goodsPrice;

    /**
     * 商品优惠金额
     */
    @ApiModelProperty(value = "商品优惠金额")
    private BigDecimal goodsDiscountPrice;

    /**
     * 优惠规则
     */
    private String discountRule;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }
    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }
    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }
    public BigDecimal getGoodsOriginalPrice() {
        return goodsOriginalPrice;
    }

    public void setGoodsOriginalPrice(BigDecimal goodsOriginalPrice) {
        this.goodsOriginalPrice = goodsOriginalPrice;
    }
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }
    public BigDecimal getGoodsDiscountPrice() {
        return goodsDiscountPrice;
    }

    public void setGoodsDiscountPrice(BigDecimal goodsDiscountPrice) {
        this.goodsDiscountPrice = goodsDiscountPrice;
    }
    public String getDiscountRule() {
        return discountRule;
    }

    public void setDiscountRule(String discountRule) {
        this.discountRule = discountRule;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
            "id=" + id +
            ", orderId=" + orderId +
            ", orderSn=" + orderSn +
            ", goodsId=" + goodsId +
            ", goodsName=" + goodsName +
            ", goodsImg=" + goodsImg +
            ", goodsNum=" + goodsNum +
            ", goodsOriginalPrice=" + goodsOriginalPrice +
            ", goodsPrice=" + goodsPrice +
            ", goodsDiscountPrice=" + goodsDiscountPrice +
            ", discountRule=" + discountRule +
            ", status=" + status +
            ", createTime=" + createTime +
        "}";
    }
}
