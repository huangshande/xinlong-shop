package com.xinlong.shop.core.member.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.core.member.entity.MemberBalanceDetail;
import com.xinlong.shop.core.member.entity.vo.MemberBalanceDetailVO;
import com.xinlong.shop.core.member.mapper.MemberBalanceDetailMapper;
import com.xinlong.shop.core.member.service.IMemberBalanceDetailService;
import com.xinlong.shop.core.member.service.IMemberService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * <p>
 * 用户余额明细 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-04-06
 */
@Service
public class MemberBalanceDetailServiceImpl extends ServiceImpl<MemberBalanceDetailMapper, MemberBalanceDetail> implements IMemberBalanceDetailService {

    private final IMemberService memberService;

    public MemberBalanceDetailServiceImpl(IMemberService memberService) {
        this.memberService = memberService;
    }

    @Override
    public void update(MemberBalanceDetail memberBalanceDetail, Integer id) {
        UpdateWrapper<MemberBalanceDetail> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(memberBalanceDetail, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<MemberBalanceDetail> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(Integer memberId,String source, Integer type, BigDecimal price, String memo) {
        MemberBalanceDetail memberBalanceDetail = new MemberBalanceDetail();
        memberBalanceDetail.setMemberId(memberId);
        memberBalanceDetail.setSource(source);
        memberBalanceDetail.setType(type);
        memberBalanceDetail.setPrice(price);
        memberBalanceDetail.setMemo(memo);
        memberBalanceDetail.setCreateTime(DateUtil.currentSeconds());
        this.save(memberBalanceDetail);
        // 会员余额新增
        this.memberService.addBalance(memberId, price);
    }

    @Override
    public MemberBalanceDetail findBySource(String source) {
        QueryWrapper<MemberBalanceDetail> query = new QueryWrapper<>();
        query.eq("source", source);

        return this.getOne(query);
    }

    @Override
    public IPage<MemberBalanceDetail> page(IPage<MemberBalanceDetail> page, Integer memberId) {
        QueryWrapper<MemberBalanceDetail> query = new QueryWrapper<>();
        query.eq("member_id", memberId);
        query.orderByDesc("create_time");
        return this.page(page, query);
    }

    @Override
    public BigDecimal findBalanceTotalByMemberId(Integer memberId) {
        QueryWrapper<MemberBalanceDetail> query = new QueryWrapper<>();
        query.select("SUM(price) as price");
        query.eq("member_id", memberId);
        // 用MemberBalanceDetail顶替一下 没有专门的dto
        MemberBalanceDetail memberBalanceDetail = this.getOne(query);
        if (memberBalanceDetail == null) {
            return BigDecimal.valueOf(0);
        } else {
            return memberBalanceDetail.getPrice();
        }
    }

    @Override
    public IPage<MemberBalanceDetailVO> pageByVo(IPage<MemberBalanceDetailVO> page, String mobile) {
        QueryWrapper<MemberBalanceDetailVO> query = new QueryWrapper<>();
        query.like(mobile != null, "m.mobile", mobile);
        query.orderByDesc("mbd.create_time");
        return this.baseMapper.selectPageByVo(page, query);
    }

}
