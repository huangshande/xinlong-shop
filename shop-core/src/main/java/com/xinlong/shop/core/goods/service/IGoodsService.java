package com.xinlong.shop.core.goods.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.goods.entity.Goods;
import com.xinlong.shop.core.goods.entity.GoodsQueryParam;
import com.xinlong.shop.core.goods.entity.dto.GoodsDTO;

import java.util.List;

/**
 * <p>
 * 商品 服务类
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
public interface IGoodsService extends IService<Goods> {

    void update(Goods goods, Integer id);

    void delete(Integer id);

    IPage<GoodsDTO> page(IPage<GoodsDTO> page, GoodsQueryParam goodsQueryParam);

    /**
     * 更新商品状态
     * @param id
     * @param status 状态
     */
    void updateStatus(Integer id, Integer status);

    GoodsDTO findById(Integer id);

    List<Goods> findAllGoods(String goodsName, Integer goodsType);
}
