package com.xinlong.shop.core.blind.mapstruct;

import com.xinlong.shop.core.blind.entity.dto.BlindBoxOrderDTO;
import com.xinlong.shop.core.blind.entity.vo.BlindBoxOrderVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 15:13 2023/6/29
 */
@Mapper(componentModel = "spring")
public interface BlindBoxOrderVOStruct {

    BlindBoxOrderVOStruct INSTANCE = Mappers.getMapper(BlindBoxOrderVOStruct.class);

    List<BlindBoxOrderVO> toBlindBoxOrderVOList(List<BlindBoxOrderDTO> blindBoxOrderDTOList);

}
