package com.xinlong.shop.core.blind.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 盲盒关联商品表
 * </p>
 *
 * @author Sylow
 * @since 2023-05-24
 */
@TableName("s_blind_box_goods")
public class BlindBoxGoods implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 盲盒id
     */
    private Integer blindBoxId;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品标签
     */
    private String tag;

    /**
     * 商品概率 整数计算权重
     */
    private Integer odds;

    /**
     * 商品图片
     */
    private String goodsImg;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品价格
     */
    private BigDecimal goodsPrice;

    /**
     * 库存量
     */
    private Integer stock;

    /**
     * 创建时间
     */
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getBlindBoxId() {
        return blindBoxId;
    }

    public void setBlindBoxId(Integer blindBoxId) {
        this.blindBoxId = blindBoxId;
    }
    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
    public Integer getOdds() {
        return odds;
    }

    public void setOdds(Integer odds) {
        this.odds = odds;
    }
    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }
    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "BlindBoxGoods{" +
            "id=" + id +
            ", blindBoxId=" + blindBoxId +
            ", goodsId=" + goodsId +
            ", tag=" + tag +
            ", odds=" + odds +
            ", goodsImg=" + goodsImg +
            ", goodsName=" + goodsName +
            ", goodsPrice=" + goodsPrice +
            ", stock=" + stock +
            ", createTime=" + createTime +
        "}";
    }
}
