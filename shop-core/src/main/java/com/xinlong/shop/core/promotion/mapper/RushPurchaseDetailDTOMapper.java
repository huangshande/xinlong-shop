package com.xinlong.shop.core.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinlong.shop.core.promotion.entity.dto.RushPurchaseDetailDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author Sylow
 * @Description 活动商品DTO
 * @Date: Created in 12:45 2023/3/8
 */
@Mapper
public interface RushPurchaseDetailDTOMapper extends BaseMapper<RushPurchaseDetailDTO> {

    RushPurchaseDetailDTO findById(@Param("RushPurchaseDetailId") Integer rushPurchaseDetailId);

    IPage<RushPurchaseDetailDTO> findByPage(IPage<RushPurchaseDetailDTO> page, @Param("RushPurchaseId") Integer rushPurchaseId, @Param("MinPrice") Integer min, @Param("MaxPrice") Integer max);

    IPage<RushPurchaseDetailDTO> findPageByBuyer(IPage<RushPurchaseDetailDTO> page, @Param("BuyerId") Integer buyerId, @Param("status") Integer status);

    IPage<RushPurchaseDetailDTO> findPageBySeller(IPage<RushPurchaseDetailDTO> page, @Param("SellerId") Integer sellerId, @Param("status") Integer status);

}
