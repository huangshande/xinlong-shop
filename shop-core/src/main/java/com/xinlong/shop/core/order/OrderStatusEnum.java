package com.xinlong.shop.core.order;

/**
 * 订单状态枚举
 */
public enum OrderStatusEnum {
    // 0:待付款 1:已付款 2:已发货 3:已收货 4:已取消 5:已完成
    CREATE(0),PAID_OFF(1), SHIPPED(2), ROG(3), CANCEL(4), COMPLETE(5);

    private int code;
    private OrderStatusEnum(int code) {
        this.code = code;
    }

    public int getCode(){
        return code;
    }
}
