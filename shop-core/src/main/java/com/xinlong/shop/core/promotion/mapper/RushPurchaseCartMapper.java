package com.xinlong.shop.core.promotion.mapper;

import com.xinlong.shop.core.promotion.entity.RushPurchaseCart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 抢购购物车 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-05-05
 */
@Mapper
public interface RushPurchaseCartMapper extends BaseMapper<RushPurchaseCart> {

}
