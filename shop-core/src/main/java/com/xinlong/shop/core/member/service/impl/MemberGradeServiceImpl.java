package com.xinlong.shop.core.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xinlong.shop.core.member.MemberGradeEnum;
import com.xinlong.shop.core.member.entity.MemberGrade;
import com.xinlong.shop.core.member.mapper.MemberGradeMapper;
import com.xinlong.shop.core.member.service.IMemberGradeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import java.util.List;

/**
 * <p>
 * 会员等级 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@Service
public class MemberGradeServiceImpl extends ServiceImpl<MemberGradeMapper, MemberGrade> implements IMemberGradeService {

    @Override
    public void update(MemberGrade memberGrade, Integer id) {
        UpdateWrapper<MemberGrade> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(memberGrade, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<MemberGrade> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public List<MemberGrade> list() {
        QueryWrapper<MemberGrade> query = new QueryWrapper<>();
        query.orderByAsc("sort");
        query.eq("disable", MemberGradeEnum.NORMAL.getCode());
        return this.list(query);
    }

}
