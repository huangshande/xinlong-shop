package com.xinlong.shop.core.order;

/**
 * 订单类型 枚举类
 */
public enum OrderTypeEnum {

    ORDER(1),BLIND_BOX(2);

    private int code;
    private OrderTypeEnum(int code) {
        this.code = code;
    }

    public int getCode(){
        return code;
    }

}
