package com.xinlong.shop.core.member.entity.vo;

import com.xinlong.shop.framework.core.entity.Member;

import java.io.Serializable;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 03:07 2023/2/4
 */
public class MemberLoginVO implements Serializable {

    private Member member;

    private String token;

    private String refreshToken;

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
