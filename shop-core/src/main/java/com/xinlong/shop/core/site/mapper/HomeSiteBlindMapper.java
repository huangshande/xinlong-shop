package com.xinlong.shop.core.site.mapper;

import com.xinlong.shop.core.site.entity.HomeSiteBlind;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.core.site.entity.dto.HomeBlindBoxDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 首页显示的盲盒商品 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-10-08
 */
@Mapper
public interface HomeSiteBlindMapper extends BaseMapper<HomeSiteBlind> {


    @Select("SELECT b.*,s.* FROM sys_home_site_blind s " +
            "LEFT JOIN s_blind_box b ON s.blind_goods_id=b.id " +
            "WHERE s.site_code='${SiteCode}' ORDER BY sort")
    List<HomeBlindBoxDTO> findHomeBlindBoxDTO(@Param("SiteCode") String siteCode);

}
