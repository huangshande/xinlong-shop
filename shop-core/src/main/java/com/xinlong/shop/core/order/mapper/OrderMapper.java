package com.xinlong.shop.core.order.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.xinlong.shop.core.order.OrderQueryParam;
import com.xinlong.shop.core.order.entity.Order;
import com.xinlong.shop.core.order.entity.vo.OrderVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-07-04
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    @Select("SELECT * FROM s_order "+
            "${ew.customSqlSegment}")
    IPage<Order> page(IPage<Order> page, @Param(Constants.WRAPPER) Wrapper queryWrapper);

    /**
     * 分页查询VO 数据包含订单项
     * @param page
     * @param orderQueryParam
     * @return
     */
    IPage<OrderVO> pageOrderVo(IPage<OrderVO> page, @Param("orderQueryParam") OrderQueryParam orderQueryParam);
}
