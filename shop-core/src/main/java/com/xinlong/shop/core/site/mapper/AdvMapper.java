package com.xinlong.shop.core.site.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.xinlong.shop.core.site.entity.Adv;
import com.xinlong.shop.core.site.entity.dto.AdvListDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 广告表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
@Mapper
public interface AdvMapper extends BaseMapper<Adv> {


    @Select("SELECT a.*,ap.promotion_name FROM sys_adv a LEFT JOIN sys_adv_promotion ap ON a.promotion_id=ap.id ${ew.customSqlSegment}")
    IPage<AdvListDTO> pageByPromotionId(IPage<AdvListDTO> page, @Param(Constants.WRAPPER) Wrapper queryWrapper);

}
