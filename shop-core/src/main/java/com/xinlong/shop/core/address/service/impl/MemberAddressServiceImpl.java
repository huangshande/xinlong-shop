package com.xinlong.shop.core.address.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.core.address.entity.MemberAddress;
import com.xinlong.shop.core.address.mapper.MemberAddressMapper;
import com.xinlong.shop.core.address.service.IMemberAddressService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 会员收货地址 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-03-17
 */
@Service
public class MemberAddressServiceImpl extends ServiceImpl<MemberAddressMapper, MemberAddress> implements IMemberAddressService {

    @Override
    public void update(MemberAddress memberAddress, Integer id) {
        // 如果此次标记为默认  就先全部改为非默认
        if (memberAddress.getIsDef() == 1) {
            UpdateWrapper<MemberAddress> defUpdateWrapper = new UpdateWrapper<>();
            defUpdateWrapper.set("is_def", 0);
            defUpdateWrapper.eq("member_id", memberAddress.getMemberId());
            this.update(defUpdateWrapper);
        }
        UpdateWrapper<MemberAddress> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(memberAddress, updateWrapper);
    }
    @Override
    public void delete(Integer id, Integer memberId) {
        UpdateWrapper<MemberAddress> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        deleteWrapper.eq("member_id", memberId);
        this.remove(deleteWrapper);
    }

    @Override
    public List<MemberAddress> listByMemberId(Integer memberId) {
        QueryWrapper<MemberAddress> query = new QueryWrapper<>();
        query.eq("member_id", memberId);
        query.orderByDesc("is_def");    //默认的 排第一个
        return this.list(query);
    }

    @Override
    public boolean saveAddress(MemberAddress memberAddress) {
        // 如果此次标记为默认  就先全部改为非默认
        if (memberAddress.getIsDef() == 1) {
            UpdateWrapper<MemberAddress> defUpdateWrapper = new UpdateWrapper<>();
            defUpdateWrapper.set("is_def", 0);
            defUpdateWrapper.eq("member_id", memberAddress.getMemberId());
            this.update(defUpdateWrapper);
        }
        return this.save(memberAddress);
    }

    @Override
    public long findAddressNum(Integer memberId) {
        QueryWrapper<MemberAddress> query = new QueryWrapper<>();
        query.eq("member_id", memberId);
        return this.count(query);
    }

    @Override
    public MemberAddress getDefByMemberId(Integer memberId) {
        QueryWrapper<MemberAddress> query = new QueryWrapper<>();
        query.eq("member_id", memberId);
        List<MemberAddress> list = this.list(query);
        if (list.size() > 1) {
            for(MemberAddress address : list) {
                if (address.getIsDef() == 1) {
                    return address;
                }
            }
            return list.get(0);
        } else if (list.size() == 1) {
            return list.get(0);
        } else {
            return null;
        }
    }

}
