package com.xinlong.shop.core.blind.entity.dto;

import com.xinlong.shop.core.blind.entity.BlindBoxOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Author Sylow
 * @Description 盲盒订单DTO 一般用于后台展示
 * @Date: Created in 10:07 2023/6/27
 */
@ApiModel(description = "盲盒订单DTO")
public class BlindBoxOrderDTO extends BlindBoxOrder {

    @ApiModelProperty(value = "盲盒名称")
    private String blindBoxName;
    @ApiModelProperty(value = "会员名称")
    private String memberName;
    @ApiModelProperty(value = "会员手机号")
    private String memberMobile;
    @ApiModelProperty(value = "会员姓名（昵称）")
    private String nickname;

    public String getBlindBoxName() {
        return blindBoxName;
    }

    public void setBlindBoxName(String blindBoxName) {
        this.blindBoxName = blindBoxName;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
