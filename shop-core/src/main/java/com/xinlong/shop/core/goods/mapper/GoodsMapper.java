package com.xinlong.shop.core.goods.mapper;

import com.xinlong.shop.core.goods.entity.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

/**
 * <p>
 * 商品 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@Mapper
public interface GoodsMapper extends BaseMapper<Goods> {


}
