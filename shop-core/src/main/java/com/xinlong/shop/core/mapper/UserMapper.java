package com.xinlong.shop.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 21:50 2022/5/15
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
