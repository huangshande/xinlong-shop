package com.xinlong.shop.core.site.mapper;

import com.xinlong.shop.core.site.entity.AdvPromotion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 广告位 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
@Mapper
public interface AdvPromotionMapper extends BaseMapper<AdvPromotion> {

}
