package com.xinlong.shop.core.site.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xinlong.shop.core.site.entity.Logistics;
import com.xinlong.shop.core.site.mapper.LogisticsMapper;
import com.xinlong.shop.core.site.service.ILogisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import java.util.List;

/**
 * <p>
 * 物流公司 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
@Service
public class LogisticsServiceImpl extends ServiceImpl<LogisticsMapper, Logistics> implements ILogisticsService {

    @Override
    public void update(Logistics logistics, Integer id) {
        UpdateWrapper<Logistics> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(logistics, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<Logistics> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public void updateStatus(Integer id, Integer status) {
        UpdateWrapper<Logistics> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        Logistics logistics = new Logistics();
        logistics.setStatus(status);
        this.update(logistics, updateWrapper);
    }

    @Override
    public List<Logistics> listAll() {
        QueryWrapper<Logistics> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", 1);
        queryWrapper.orderByAsc("sort");
        List<Logistics> logisticsList = this.list(queryWrapper);

        return logisticsList;
    }

}
