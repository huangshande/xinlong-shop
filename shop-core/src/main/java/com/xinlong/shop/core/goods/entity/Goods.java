package com.xinlong.shop.core.goods.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 商品
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@TableName(value = "s_goods", autoResultMap = true)
public class Goods implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品分类id
     */
    @NotNull(message = "商品分类不能为空")
    private Integer goodsClassifyId;

    /**
     * 商品品牌id
     */
    private Integer goodsBrandId;

    /**
     * 商品类型
     * @see com.xinlong.shop.core.goods.entity.GoodsTypeEnum
     */
    @NotNull(message = "商品类型不能为空")
    private Integer goodsType;

    /**
     * 商品名称
     */
    @NotBlank(message = "商品名称不能为空")
    private String goodsName;

    /**
     * 商品编号
     */
    @NotBlank(message = "商品编号不能为空")
    @Pattern(regexp = "^[a-zA-Z0-9_-]{4,20}$", message = "商品必须是4到20位（字母，数字，下划线，减号）")
    private String goodsSn;

    /**
     * 商品简介
     */
    private String goodsDesc;

    /**
     * 商品销售价格
     */
    private BigDecimal goodsPrice;

    /**
     * 商品成本价格
     */
    private BigDecimal goodsCostPrice;

    /**
     * 商品成本价格
     */
    private BigDecimal goodsMarketPrice;

    /**
     * 商品货号
     */
    @NotBlank(message = "商品货号不能为空")
    @Pattern(regexp = "^[a-zA-Z0-9_-]{4,20}$", message = "商品编号必须是4到20位（字母，数字，下划线，减号）")
    private String goodsSkuSn;

    /**
     * 商品状态
     */
    @NotNull(message = "商品状态不能为空")
    private Integer goodsStatus;

    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> goodsImgs;

    /**
     * 商品详情
     */
    private String goodsIntro;

    /**
     * 商品新旧等级
     */
    private Integer goodsGrade;

    /**
     * 商品标签
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> goodsTags;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 禁用状态
     */
    private Integer disable;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsClassifyId() {
        return goodsClassifyId;
    }

    public void setGoodsClassifyId(Integer goodsClassifyId) {
        this.goodsClassifyId = goodsClassifyId;
    }

    public Integer getGoodsBrandId() {
        return goodsBrandId;
    }

    public void setGoodsBrandId(Integer goodsBrandId) {
        this.goodsBrandId = goodsBrandId;
    }

    public Integer getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(Integer goodsType) {
        this.goodsType = goodsType;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public String getGoodsSn() {
        return goodsSn;
    }

    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn;
    }
    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }
    public BigDecimal getGoodsCostPrice() {
        return goodsCostPrice;
    }

    public void setGoodsCostPrice(BigDecimal goodsCostPrice) {
        this.goodsCostPrice = goodsCostPrice;
    }
    public BigDecimal getGoodsMarketPrice() {
        return goodsMarketPrice;
    }

    public void setGoodsMarketPrice(BigDecimal goodsMarketPrice) {
        this.goodsMarketPrice = goodsMarketPrice;
    }
    public String getGoodsSkuSn() {
        return goodsSkuSn;
    }

    public void setGoodsSkuSn(String goodsSkuSn) {
        this.goodsSkuSn = goodsSkuSn;
    }
    public Integer getGoodsStatus() {
        return goodsStatus;
    }

    public void setGoodsStatus(Integer goodsStatus) {
        this.goodsStatus = goodsStatus;
    }
    public String getGoodsIntro() {
        return goodsIntro;
    }

    public void setGoodsIntro(String goodsIntro) {
        this.goodsIntro = goodsIntro;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public List<String> getGoodsImgs() {
        return goodsImgs;
    }

    public void setGoodsImgs(List<String> goodsImgs) {
        this.goodsImgs = goodsImgs;
    }

    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }

    public Integer getGoodsGrade() {
        return goodsGrade;
    }

    public void setGoodsGrade(Integer goodsGrade) {
        this.goodsGrade = goodsGrade;
    }

    public List<String> getGoodsTags() {
        return goodsTags;
    }

    public void setGoodsTags(List<String> goodsTags) {
        this.goodsTags = goodsTags;
    }

    @Override
    public String toString() {
        return "Goods{" +
            "id=" + id +
            ", goodsClassifyId=" + goodsClassifyId +
            ", goodsBrandId=" + goodsBrandId +
            ", goodsType=" + goodsType +
            ", goodsName=" + goodsName +
            ", goodsSn=" + goodsSn +
            ", goodsDesc=" + goodsDesc +
            ", goodsPrice=" + goodsPrice +
            ", goodsCostPrice=" + goodsCostPrice +
            ", goodsMarketPrice=" + goodsMarketPrice +
            ", goodsSkuSn=" + goodsSkuSn +
            ", goodsStatus=" + goodsStatus +
            ", goodsImgs=" + goodsImgs +
            ", goodsIntro=" + goodsIntro +
            ", goodsGrade=" + goodsGrade +
            ", goodsTags=" + goodsTags +
            ", createTime=" + createTime +
            ", disable=" + disable +
        "}";
    }
}
