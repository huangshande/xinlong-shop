package com.xinlong.shop.core.member.mapper;

import com.xinlong.shop.core.member.entity.MemberWithdrawal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 用户提现记录 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-04-06
 */
@Mapper
public interface MemberWithdrawalMapper extends BaseMapper<MemberWithdrawal> {

}
