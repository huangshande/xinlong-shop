package com.xinlong.shop.core.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.member.entity.dto.MemberDTO;
import com.xinlong.shop.core.member.entity.dto.MemberListDTO;
import com.xinlong.shop.core.member.entity.dto.MemberSearchDTO;
import com.xinlong.shop.core.member.entity.vo.MemberLoginVO;
import com.xinlong.shop.framework.core.entity.Member;

import java.math.BigDecimal;

/**
 * <p>
 * 会员 服务类
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
public interface IMemberService extends IService<Member> {

    /**
     * 更新用户信息 后台管理用
     * @param memberDTO
     * @param id
     */
    void update(MemberDTO memberDTO, Integer id);

    void delete(Integer id);

    MemberDTO save(MemberDTO memberDTO);

    Member findByMobile(String mobile);

    Member findByMemberName(String memberName);

    //Member findByToken(String token);

    /**
     * 寻找会员 会查询用户名和手机号
     * @param param 用户名或手机号
     * @return
     */
    Member findMember(String param);

    /**
     * 获取会员信息DTO，这里复用MemberListDTO因为所需要的字段差不多
     * @param id
     * @return
     */
    MemberListDTO findMemberDTOById(Integer id);

    IPage page(MemberSearchDTO memberSearchDTO, IPage page);

    /**
     *
     * @param memberName
     * @param password
     * @param isVerify 是否需要校验密码
     * @return
     */
    MemberLoginVO login(String memberName, String password, boolean isVerify);

    /**
     * 生成刷新token
     * @param memberName
     * @param refreshToken 是否是refreshToken
     * @return
     */
    String generateRefreshToken(String memberName, boolean refreshToken);

    /**
     * 发送短信验证码 并记录缓存中
     * @param phone
     * @param servicePrefix 业务前缀
     */
    boolean sendSmsCode(String phone, String servicePrefix);

    /**
     * 验证短信验证码 并清除缓存
     * @param phone
     * @param servicePrefix
     * @param code
     * @return
     */
    boolean verifySmsCode(String phone, String servicePrefix, String code);

    /**
     * 用户端注册
     * @param phone
     * @param password
     */
    void register(String phone, String password, String inviteMemberNo);

    Member registerByOpenId(String openId, String inviteMemberNo);


    /**
     * 受邀人分页列表
     * @param memberId 上级会员id
     * @param page
     * @return
     */
    IPage<Member> inviteePage(Integer memberId, IPage<Member> page);

    /**
     * 通过受邀人会员id 获取邀请人的会员信息
     * @param memberId 受邀人会员id
     * @return
     */
    Member getInviteeMemberByMemberId(Integer memberId);

    /**
     * 增加余额
     * @param memberId
     * @param price
     */
    void addBalance(Integer memberId, BigDecimal price);

    /**
     * 扣减余额
     * @param memberId
     * @param price
     */
    void subBalance(Integer memberId, BigDecimal price);

    /**
     * 增加积分
     * @param memberId
     * @param points
     * @param type  0上架商品获得 1系统操作
     * @param source
     * @param memo
     */
    void addPoints(Integer memberId, BigDecimal points, Integer type, String source, String memo);

    /**
     * 扣减积分
     * @param memberId
     * @param points
     * @param type  1系统操作
     * @param source
     * @param memo
     */
    void subPoints(Integer memberId, BigDecimal points, Integer type, String source, String memo);

    void updatePassword(Integer memberId, String password);

    /**
     * 修改会员状态
     * @param memberId
     * @param status
     */
    void updateStatus(Integer memberId, Integer status);

    void updateBalance(Integer memberId, BigDecimal price);

    Member findByOpenId(String openId);
}
