package com.xinlong.shop.core.logistics.plugins.kuaidi100;

import com.google.gson.Gson;
import com.kuaidi100.sdk.api.QueryTrack;
import com.kuaidi100.sdk.core.IBaseClient;
import com.kuaidi100.sdk.pojo.HttpResult;
import com.kuaidi100.sdk.request.QueryTrackParam;
import com.kuaidi100.sdk.request.QueryTrackReq;
import com.kuaidi100.sdk.response.QueryTrackData;
import com.kuaidi100.sdk.response.QueryTrackResp;
import com.xinlong.shop.core.logistics.ILogisticsPlugin;
import com.xinlong.shop.core.logistics.entity.LogisticsEnum;
import com.xinlong.shop.core.logistics.entity.LogisticsTraces;
import com.xinlong.shop.core.site.entity.Logistics;
import com.xinlong.shop.core.util.Kuaidi100SignUtils;
import com.xinlong.shop.framework.cache.ICache;
import com.xinlong.shop.framework.exception.ServiceException;
import com.xinlong.shop.framework.service.ISysSettingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description 快递100物流查询插件实现类
 * @Date: Created in 21:49 2023/7/27
 */
@Component
public class Kuaidi100PluginImpl implements ILogisticsPlugin {

    private static final Logger logger = LoggerFactory.getLogger(Kuaidi100PluginImpl.class);

    private final ISysSettingService sysSettingService;
    private final ICache cache;

    public Kuaidi100PluginImpl(ISysSettingService sysSettingService, ICache cache) {
        this.sysSettingService = sysSettingService;
        this.cache = cache;
    }

    @Override
    public LogisticsEnum pluginName() {
        return LogisticsEnum.KUAIDI100;
    }

    @Override
    public LogisticsTraces instantQuery(Logistics logistics, String logisticsSn, String mobile) {
        try {
            // 从缓存中获取
            LogisticsTraces logisticsTraces = (LogisticsTraces) cache.get(logisticsSn);
            if (logisticsTraces != null) {
                logger.debug("物流查询插件：快递100，从缓存中获取");
                return logisticsTraces;
            }

            Map<String, Object> setting = sysSettingService.getSetting();
            String key = setting.get("kuaidi100Key").toString();
            String customer = setting.get("kuaidi100Customer").toString();

            //logger.debug("物流查询插件：快递100");
            QueryTrackReq queryTrackReq = new QueryTrackReq();
            QueryTrackParam queryTrackParam = new QueryTrackParam();
            queryTrackParam.setCom(logistics.getCode());
            queryTrackParam.setNum(logisticsSn);
            queryTrackParam.setPhone(mobile);
            String param = new Gson().toJson(queryTrackParam);

            queryTrackReq.setParam(param);
            queryTrackReq.setCustomer(customer);
            queryTrackReq.setSign(Kuaidi100SignUtils.querySign(param, key, customer));

            IBaseClient baseClient = new QueryTrack();
            //System.out.println(baseClient.execute(queryTrackReq));
            HttpResult httpResult = baseClient.execute(queryTrackReq);
            QueryTrackResp queryTrackResp = new Gson().fromJson(httpResult.getBody(), QueryTrackResp.class);

            List<Map> traces = new ArrayList<>();
            if ("ok".equals(queryTrackResp.getMessage())) {
                for (QueryTrackData queryTrackData : queryTrackResp.getData()) {
                    Map map = new HashMap<String, String>();
                    map.put("time", queryTrackData.getTime());
                    map.put("content", queryTrackData.getContext());
                    traces.add(map);
                }
            }

            logisticsTraces = new LogisticsTraces(logistics.getLogisticsName(), logisticsSn, traces);
            // 缓存一小时
            cache.put(logisticsSn, logisticsTraces, 60 * 60);
            return logisticsTraces;
        }catch (Exception e) {
            logger.error("快递100物流查询异常", e);
            throw new ServiceException("快递100物流查询异常", e);
        }
    }

}
