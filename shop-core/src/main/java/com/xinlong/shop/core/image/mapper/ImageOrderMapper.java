package com.xinlong.shop.core.image.mapper;

import com.xinlong.shop.core.image.entity.ImageOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 图片下载订单 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-08-04
 */
@Mapper
public interface ImageOrderMapper extends BaseMapper<ImageOrder> {

}
