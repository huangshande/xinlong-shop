package com.xinlong.shop.core.address.mapper;

import com.xinlong.shop.core.address.entity.SysArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 地区表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-03-17
 */
@Mapper
public interface SysAreaMapper extends BaseMapper<SysArea> {

}
