package com.xinlong.shop.core.member.service;

import com.xinlong.shop.core.member.entity.MemberPayment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员收款方式 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-03-15
 */
public interface IMemberPaymentService extends IService<MemberPayment> {

    void update(MemberPayment memberPayment, Integer id);

    void delete(Integer id);

    MemberPayment findByMemberId(Integer memberId);
}
