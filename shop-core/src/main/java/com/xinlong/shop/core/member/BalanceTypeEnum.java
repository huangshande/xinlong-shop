package com.xinlong.shop.core.member;

/**
 * @Author Sylow
 * @Description 会员余额操作类型
 * @Date: Created in 14:48 2023/04/06
 */
public enum BalanceTypeEnum {
    DEFAULT(0, "系统默认"),
    SERVICE_CHARGE(1, "服务费分成");

    private Integer code;
    private String desc;

    BalanceTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
