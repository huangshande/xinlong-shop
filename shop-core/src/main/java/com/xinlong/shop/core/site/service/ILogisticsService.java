package com.xinlong.shop.core.site.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.site.entity.Logistics;

import java.util.List;

/**
 * <p>
 * 物流公司 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
public interface ILogisticsService extends IService<Logistics> {

    void update(Logistics logistics, Integer id);

    void delete(Integer id);

    void updateStatus(Integer id, Integer status);

    /**
     * 获取全部正在使用中的物流公司
     * @return
     */
    List<Logistics> listAll();
}
