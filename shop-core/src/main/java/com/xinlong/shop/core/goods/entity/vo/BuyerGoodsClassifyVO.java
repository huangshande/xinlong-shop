package com.xinlong.shop.core.goods.entity.vo;

import com.xinlong.shop.core.goods.entity.GoodsClassify;

import java.io.Serializable;
import java.util.List;

/**
 * @Author Sylow
 * @Description 买家端使用的商品分类VO对象
 * @Date: Created in 16:33 2022/12/2
 */
public class BuyerGoodsClassifyVO extends GoodsClassify implements Serializable {

    private List<BuyerGoodsClassifyVO> children;

    public List<BuyerGoodsClassifyVO> getChildren() {
        return children;
    }

    public void setChildren(List<BuyerGoodsClassifyVO> children) {
        this.children = children;
    }
}
