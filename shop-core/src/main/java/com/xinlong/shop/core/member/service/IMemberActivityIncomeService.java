package com.xinlong.shop.core.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.member.entity.MemberActivityIncome;

import java.math.BigDecimal;

/**
 * <p>
 * 会员收益表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-03-30
 */
public interface IMemberActivityIncomeService extends IService<MemberActivityIncome> {

    void update(MemberActivityIncome memberActivityIncome, Integer id);

    void delete(Integer id);

    /**
     * 新增收益明细 上架时新增
     * @param memberId
     * @param buyPrice
     * @param sellPrice
     * @param servicePrice
     * @param profit
     * @param rushPurchaseDetailId
     * @param activityId
     */
    void addActivityIncome(Integer memberId, BigDecimal buyPrice, BigDecimal sellPrice,
                           BigDecimal servicePrice, BigDecimal profit,
                           Integer rushPurchaseDetailId, String activityId);


    IPage<MemberActivityIncome> page(IPage<MemberActivityIncome> page, Integer memberId);

    BigDecimal getTotal(Integer memberId);

    BigDecimal getTotalByTime(Integer memberId, Long startTime, Long endTime);

    /**
     * 根据活动详细id查询
     * @param rushPurchaseDetailId
     * @return
     */
    MemberActivityIncome findByRushPurchaseDetailId(Integer rushPurchaseDetailId);
}
