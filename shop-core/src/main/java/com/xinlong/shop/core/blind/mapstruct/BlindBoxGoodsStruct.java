package com.xinlong.shop.core.blind.mapstruct;

import com.xinlong.shop.core.blind.entity.BlindBoxGoods;
import com.xinlong.shop.core.blind.entity.BlindBoxOrderItem;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 14:10 2023/6/27
 */
@Mapper(componentModel = "spring")
public interface BlindBoxGoodsStruct {

    BlindBoxGoodsStruct INSTANCE = Mappers.getMapper(BlindBoxGoodsStruct.class);

    BlindBoxOrderItem toBlindBoxOrderItem(BlindBoxGoods blindBoxGoods);
}
