package com.xinlong.shop.core.member.mapper;

import com.xinlong.shop.core.member.entity.MemberPointsDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 用户积分明细 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-07-22
 */
@Mapper
public interface MemberPointsDetailMapper extends BaseMapper<MemberPointsDetail> {

}
