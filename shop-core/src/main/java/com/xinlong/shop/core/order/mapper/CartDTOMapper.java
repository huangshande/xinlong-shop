package com.xinlong.shop.core.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.core.order.entity.dto.CartDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 购物车表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-04-21
 */
@Mapper
public interface CartDTOMapper extends BaseMapper<CartDTO> {

    List<CartDTO> findCartList(@Param("memberId") Integer memberId);


}
