package com.xinlong.shop.core.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.member.entity.MemberPointsDetail;

import java.math.BigDecimal;

/**
 * <p>
 * 用户积分明细 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-07-22
 */
public interface IMemberPointsDetailService extends IService<MemberPointsDetail> {

    void update(MemberPointsDetail memberPointsDetail, Integer id);

    void delete(Integer id);

    /**
     * 新增操作记录
     * @param memberId
     * @param source 来源 上架商品的话 订单号，系统操作无
     * @param type 类型
     * @param points 积分
     * @param memo 备注
     */
    void add(Integer memberId, String source, Integer type, BigDecimal points, String memo);
}
