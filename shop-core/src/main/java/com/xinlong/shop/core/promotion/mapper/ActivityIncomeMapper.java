package com.xinlong.shop.core.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.core.member.entity.MemberActivityIncome;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * 会员收益表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-03-30
 */
@Mapper
public interface ActivityIncomeMapper extends BaseMapper<MemberActivityIncome> {

    BigDecimal getTotal(@Param("MemberId") Integer memberId);
    BigDecimal getTotalByTime(@Param("memberId") Integer memberId, @Param("startTime") Long startTime, @Param("endTime") Long endTime);

}
