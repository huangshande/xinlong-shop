package com.xinlong.shop.core.promotion.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.promotion.entity.PutSaleOrder;
import com.xinlong.shop.core.promotion.entity.RushPurchaseDetail;
import com.xinlong.shop.core.promotion.entity.RushPurchaseDetailQueryParam;
import com.xinlong.shop.core.promotion.entity.dto.RushPurchaseDetailDTO;
import com.xinlong.shop.framework.core.entity.Member;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 抢购活动明细表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-02-17
 */
public interface IRushPurchaseDetailService extends IService<RushPurchaseDetail> {

    void update(RushPurchaseDetail rushPurchaseDetail, Integer id);

    void delete(Integer id);

    /**
     * 根据抢购活动id 查询明细
     * @param rushPurchaseId
     * @return
     */
    List<RushPurchaseDetail> listByRushPurchaseId(Integer rushPurchaseId);

    /**
     * 查询活动商品明细 外联商品数据
     * @param rushPurchaseDetailId
     * @return
     */
    RushPurchaseDetailDTO findDTOById(Integer rushPurchaseDetailId);

    /**
     * 根据抢购活动id 获取分页DTO数据 一般前台使用
     * @param page
     * @param rushPurchaseId
     * @param min 最小价格
     * @param max 最大价格
     * @return
     */
    IPage<RushPurchaseDetailDTO> page(IPage<RushPurchaseDetailDTO> page, Integer rushPurchaseId, Integer min, Integer max);

    /**
     * 根据查询参数对象获取分页数据 一般后台使用
     * @param page
     * @param rushPurchaseDetailQueryParam
     * @return
     */
    IPage<RushPurchaseDetail> findPageByParam(IPage<RushPurchaseDetail> page, RushPurchaseDetailQueryParam rushPurchaseDetailQueryParam);

    /**
     * 抢购
     * @param rushPurchaseDetailId
     * @param member
     * @return 成功返回success 其余返回失败消息
     */
    String buy(Integer rushPurchaseDetailId, Member member);

    /**
     * 获取买家订单列表
     * @param page
     * @param memberId
     * @param status
     * @return
     */
    IPage<RushPurchaseDetailDTO> findBuyerOrder(IPage<RushPurchaseDetailDTO> page, Integer memberId, Integer status);

    /**
     * 上传凭证
     * @param id
     * @param buyerId
     * @param imgUrl
     */
    void uploadProof(Integer id, Integer buyerId, String imgUrl);

    /**
     * 获取卖家订单列表
     * @param page
     * @param memberId
     * @param status
     * @return
     */
    IPage<RushPurchaseDetailDTO> findSellerOrder(IPage<RushPurchaseDetailDTO> page, Integer memberId, Integer status);

    /**
     * 确认收款(给管理端使用)
     * @param id
     */
    void confirmReceipt(Integer id);

    /**
     * 确认提货
     * @param id
     * @param buyerId
     */
    void pickUp(Integer id, Integer buyerId);

    /**
     * 上架操作，生成新的活动
     * @param id 活动明细id
     * @param putSaleOrder 上架订单详情 包含服务费和新价格
     * @return 上架下一场活动明细的id
     */
    Integer putSale(Integer id, PutSaleOrder putSaleOrder);

    /**
     * 通过活动规则id和商品id以及买家会员id
     * @param activityId
     * @param goodsId
     * @return
     */
    RushPurchaseDetail find(String activityId, Integer goodsId);

    void cancelOrder(Integer id);

    /**
     * 通过会员id获取会员预期收益
     * @param memberId
     * @return
     */
    BigDecimal getExpectIncome(Integer memberId);

    /**
     * 通过状态汇总各个状态数量
     * @param status
     */
    long countByStatus(Integer status);

    /**
     * 根据活动id 获取该活动交易量总额
     * @param rushPurchaseId
     * @return
     */
    BigDecimal getTotleByRushPurchaseId(Integer rushPurchaseId);

    /**
     * 根据会员id和活动id获取该会员购买该活动的次数
     * @param memberId
     * @param rushPurchaseId
     * @return
     */
    int getBuyerOrderCount(Integer memberId, Integer rushPurchaseId);

    /**
     * 根据会员id和活动规则id获取该会员在该活动购买的未上架订单
     * @param memberId
     * @param activityId
     * @return
     */
    List<RushPurchaseDetail> findBuyerOrder(Integer memberId, String activityId);


    void setBookingMember(Integer id, Integer memberId);
}
