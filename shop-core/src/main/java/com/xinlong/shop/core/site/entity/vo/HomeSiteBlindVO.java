package com.xinlong.shop.core.site.entity.vo;

import com.xinlong.shop.core.blind.entity.BlindBox;
import com.xinlong.shop.core.site.entity.HomeSite;
import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel(value = "首页位置展示盲盒VO")
public class HomeSiteBlindVO extends HomeSite {

    List<BlindBox> blindBoxList;

    public List<BlindBox> getBlindBoxList() {
        return blindBoxList;
    }

    public void setBlindBoxList(List<BlindBox> blindBoxList) {
        this.blindBoxList = blindBoxList;
    }
}
