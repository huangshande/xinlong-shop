package com.xinlong.shop.framework.cache;

import cn.hutool.core.date.DateUtil;
import com.xinlong.shop.framework.cache.model.CacheModel;
import com.xinlong.shop.framework.util.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Component;
import org.springframework.cache.CacheManager;


/**
 * @Author Sylow
 * @Description
 * @Date: Created in 19:36 2022/5/20
 */
@Component
public class EhCacheImpl implements ICache {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private Cache cache;

    public void init(){
        this.cache = SpringUtils.getBean(CacheManager.class).getCache("xinlong-shop");
    }




    /**
     * 添加缓存数据
     *
     * @param key   键
     * @param value 值
     */
    @Override
    public void put(String key, Object value) {
//        try {
//            if (cache == null) {
//                this.init();
//            }
//            // 0表示没有过期时间
//            CacheModel cacheModel = new CacheModel(value, 0L);
//            cache.put(key, cacheModel);
//        } catch (Exception e) {
//            logger.error("添加缓存失败：{}", e.getMessage());
//        }
        this.put(key, value, 0);
    }

    @Override
    public void put(String key, Object value, int expirationTime) {
        try {
            Long exp = 0L;
            // 0表示没有过期时间
            if(expirationTime != 0) {
                // 过期时间 当前时间戳加上有效期
                exp = DateUtil.currentSeconds() + expirationTime;
            }
            CacheModel cacheModel = new CacheModel(value, exp);
            cache.put(key, cacheModel);
            logger.debug("put缓存{},值{},过期时间{}", key, value, expirationTime);
        } catch (Exception e) {
            logger.error("添加缓存失败：{}", e.getMessage());
        }
    }

    @Override
    public void modify(String key, Object value) {
        try {
            Cache.ValueWrapper obj = cache.get(key);
            if (obj != null) {
                CacheModel cacheModel = (CacheModel) obj.get();
                cacheModel = new CacheModel(value, cacheModel.getExpirationTime());
                cache.put(key, cacheModel);
                logger.debug("modify缓存{},值{},过期时间{}", key, value, cacheModel.getExpirationTime());
            } else {
                throw new RuntimeException("修改缓存失败，缓存不存在");
            }
        } catch (Exception e) {
            logger.error("修改缓存失败：{}", e.getMessage());
        }
    }

    /**
     * 获取缓存数据
     *
     * @param key 键
     * @return 缓存数据
     */
    @Override
    public Object get(String key) {
        try {
            if (cache == null) {
                this.init();
            }
            Cache.ValueWrapper obj = cache.get(key);
            if (obj != null) {
                CacheModel cacheModel = (CacheModel) obj.get();
                logger.debug("{}缓存，过期时间{}", key, cacheModel.getExpirationTime());

                // 等于0 是永不过期 直接返回
                if(cacheModel.getExpirationTime() == 0) {
                    return cacheModel.getValue();
                }
                // 当前时间大于过期时间
                if (DateUtil.currentSeconds() > cacheModel.getExpirationTime()) {
                    // 过期了删除
                    this.delete(key);
                    return null;
                } else {
                    return cacheModel.getValue();
                }

            }
            return null;
        } catch (Exception e) {
            logger.error("获取缓存数据失败：", e);
            return null;
        }
    }

    /**
     * 删除缓存数据
     *
     * @param key 键
     */
    @Override
    public void delete(String key) {
        try {
            if (cache == null) {
                this.init();
            }
            cache.evict(key);
        } catch (Exception e) {
            logger.error("删除缓存数据失败：", e);
        }
    }

}
