package com.xinlong.shop.framework.uploader;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.xinlong.shop.framework.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.Date;

/**
 * @Author Sylow
 * 先做成单一的，以后再考虑兼容多个oss的情况
 * @Description 阿里云oss
 * @Date: Created in 12:07 2022/11/18
 */
@Component
public class AliyunOSSUploader {

    private final static Logger logger = LoggerFactory.getLogger(AliyunOSSUploader.class);

    private final AliyunOSSConfig aliyunOSSConfig;

    public AliyunOSSUploader(AliyunOSSConfig aliyunOSSConfig) {
        this.aliyunOSSConfig = aliyunOSSConfig;
    }

    public String upload(InputStream input, String path, String fileType) {


        String endpoint = aliyunOSSConfig.getEndpoint();
        String accessKeyId = aliyunOSSConfig.getAccessKeyId();
        String accessKeySecret = aliyunOSSConfig.getAccessKeySecret();
        String bucketName = aliyunOSSConfig.getBucketName();
        String objectName = path;

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        String url = "";
        try {
            // 创建上传Object的Metadata
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentType(FileUtils.contentType(fileType));
            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, input, meta);

            // 如果需要上传时设置存储类型和访问权限，请参考以下示例代码。
//             ObjectMetadata metadata = new ObjectMetadata();
//             metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, FileUtils.contentType(fileType));
             //metadata.setObjectAcl(CannedAccessControlList.Private);
             //putObjectRequest.setMetadata(metadata);

            // 上传字符串。
            ossClient.putObject(putObjectRequest);
            // 设置URL过期时间, 图片公开访问 没关系
            Date expiration = new Date(System.currentTimeMillis() + 60 * 1000);
            url = ossClient.generatePresignedUrl(bucketName, objectName, expiration).toString();
            url = url.substring(0, url.indexOf("?"));
            url = url.replaceAll("-internal","");
        } catch (OSSException oe) {
            logger.error("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            logger.error("Error Message:" + oe.getErrorMessage());
            logger.error("Error Code:" + oe.getErrorCode());
            logger.error("Request ID:" + oe.getRequestId());
            logger.error("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            logger.error("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            logger.error("Error Message:" + ce.getMessage());
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }

        return url;
    }


}
