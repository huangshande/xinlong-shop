//package com.xinlong.shop.framework.tencent;
//
//import com.tencentcloudapi.aiart.v20221229.AiartClient;
//import com.tencentcloudapi.aiart.v20221229.models.ImageToImageRequest;
//import com.tencentcloudapi.aiart.v20221229.models.ImageToImageResponse;
//import com.tencentcloudapi.common.Credential;
//import com.tencentcloudapi.common.exception.TencentCloudSDKException;
//import com.tencentcloudapi.common.profile.ClientProfile;
//import com.tencentcloudapi.common.profile.HttpProfile;
//import com.xinlong.shop.framework.exception.ServiceException;
//import com.xinlong.shop.framework.service.ISysSettingService;
//import org.springframework.stereotype.Component;
//
//import java.util.Map;
//
///**
// * @Author Sylow
// * @Description
// * @Date: Created in 11:19 2023/8/3
// */
//@Component
//public class ImageToImage {
//    private final ISysSettingService sysSettingService;
//
//    public ImageToImage(ISysSettingService sysSettingService) {
//        this.sysSettingService = sysSettingService;
//    }
//
//    public String generate(String url) {
//        try{
//            // 实例化一个认证对象，入参需要传入腾讯云账户 SecretId 和 SecretKey，此处还需注意密钥对的保密
//            // 代码泄露可能会导致 SecretId 和 SecretKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议采用更安全的方式来使用密钥，请参见：https://cloud.tencent.com/document/product/1278/85305
//            // 密钥可前往官网控制台 https://console.cloud.tencent.com/cam/capi 进行获取
//            Map<String, Object> settings = sysSettingService.getSetting();
//            String secretId = settings.get("tencentSecretId").toString();
//            String secretKey = settings.get("tencentSecretKey").toString();
//            Credential cred = new Credential(secretId, secretKey);
//            // 实例化一个http选项，可选的，没有特殊需求可以跳过
//            HttpProfile httpProfile = new HttpProfile();
//            httpProfile.setEndpoint("aiart.tencentcloudapi.com");
//            // 实例化一个client选项，可选的，没有特殊需求可以跳过
//            ClientProfile clientProfile = new ClientProfile();
//            clientProfile.setHttpProfile(httpProfile);
//            // 实例化要请求产品的client对象,clientProfile是可选的
//            AiartClient client = new AiartClient(cred, "ap-shanghai", clientProfile);
//            // 实例化一个请求对象,每个接口都会对应一个request对象
//            ImageToImageRequest req = new ImageToImageRequest();
//            req.setInputUrl(url);
//            req.setLogoAdd(0L);
//            req.setStrength(0.4F);
//            req.setRspImgType("url");
//            // 返回的resp是一个ImageToImageResponse的实例，与请求对象对应
//            ImageToImageResponse resp = client.ImageToImage(req);
//            return resp.getResultImage();
//        } catch (TencentCloudSDKException e) {
//            throw new ServiceException("图生图发生错误: + " + e.getMessage(), e);
//        }
//    }
//}
