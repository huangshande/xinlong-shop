//package com.xinlong.shop.framework.rabbitmq;
//
//import org.springframework.amqp.core.*;
//import org.springframework.amqp.support.converter.MessageConverter;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @Author Sylow
// * @Description RabbitMq 配置类
// * @Date: Created in 21:20 2023/8/17
// */
//@Configuration
//public class RabbitMqConfig {
//    // 交换机名称
//    public static final String ITEM_TOPIC_EXCHANGE = "xinlong_item_topic_exchange";
//    // 队列名称
//    public static final String ITEM_QUEUE = "xinlong_item_queue";
//
//    // 声明交换机
//    @Bean("itemTopicExchange")
//    public Exchange topicExchange(){
//        /**
//         * ExchangeBuilder
//         * - topicExchange(ITEM_TOPIC_EXCHANGE) 设置 topic 模式的交换机名称
//         * - durable(true) 设置持久化
//         * - build() 构建返回 Exchange 对象
//         */
//        return ExchangeBuilder.topicExchange(ITEM_TOPIC_EXCHANGE).durable(true).build();
//    }
//
//    // 声明队列
//    @Bean("itemQueue")
//    public Queue itemQueue(){
//        /**
//         * QueueBuilder
//         * - .durable(ITEM_QUEUE) 设置队列名称以及持久化
//         * - .build() 构建返回 Queue 对象
//         */
//        return QueueBuilder.durable(ITEM_QUEUE).build();
//    }
//
//    // 绑定队列与交换机
//    @Bean
//    public Binding itemQueueExchange(@Qualifier("itemQueue") Queue queue,
//                                     @Qualifier("itemTopicExchange") Exchange exchange){
//        /**
//         * BindingBuilder
//         * - .bind(queue) 绑定队列
//         * - .to(exchange) 绑定交换机
//         * - with("xinlong.#") 设置 Routing Key
//         * - .noargs() 设置无参数
//         */
//        return BindingBuilder.bind(queue).to(exchange).with("xinlong.image.#").noargs();
//    }
//
//    @Bean
//    public MessageConverter messageConverter() {
//        return new Gson2JsonMessageConverter();
//    }
//}
