package com.xinlong.shop.framework.core.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author Sylow
 * @Description 菜单
 * @Date: Created in 21:43 2022/5/31
 */
@TableName("sys_menu")
public class SysMenu implements Serializable {

    private Integer id;

    @NotBlank(message = "菜单名称不能为空")
    private String menuName;

    @NotBlank(message = "权限标识不能为空")
    private String identity;

    /**
     * 权限url正则
     */
    @NotBlank(message = "权限不能为空")
    private String permission;

    /**
     * 菜单描述
     */
    private String description;

    private Integer pid;

    @NotNull(message = "排序不能为空")
    private Integer sort;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }
}
