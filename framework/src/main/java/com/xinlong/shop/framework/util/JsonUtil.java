package com.xinlong.shop.framework.util;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;


/**
 * JSON 相关操作
 * 方便以后更换不同的框架
 * @author Sylow
 * 需要GJSON
 * 2015-07-14
 */
public class JsonUtil {

	private final static Logger logger = LoggerFactory.getLogger(JsonUtil.class);

	public static String objectToJson(Object object){

		String jsonStr = "";
		try {
			Gson gson = new GsonBuilder().disableHtmlEscaping().create();

			jsonStr  = gson.toJson(object);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return jsonStr;
	}

	public static Map<String, Object> jsonToMap(String json) {
		Gson gson = new Gson();
		return gson.fromJson(json, Map.class);
	}

	public static void main(String[] args) {
		String json = "{\"name\":\"sylow\",\"age\":18}";
		Map<String, Object> map = jsonToMap(json);
		System.out.println(map.get("name"));
	}


}
