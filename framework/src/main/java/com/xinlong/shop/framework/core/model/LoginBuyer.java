package com.xinlong.shop.framework.core.model;

import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.core.entity.SysRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 22:19 2022/12/25
 */
public class LoginBuyer implements UserDetails {

    private Member member;

    private List<SysRole> authoritys;

    public LoginBuyer (Member member) {
        this.member = member;

        // 默认拥有买家权限
        SysRole role = new SysRole();
        role.setRoleCode("BUYER");
        this.authoritys = new ArrayList<SysRole>();
        this.authoritys.add(role);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authoritys;
    }

    @Override
    public String getPassword() {
        return this.member.getPassword();
    }

    @Override
    public String getUsername() {
        // 用会员编号当做用户名 用来生成token
        return this.member.getMemberNo();
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public List<SysRole> getAuthoritys() {
        return authoritys;
    }

    public void setAuthoritys(List<SysRole> authoritys) {
        this.authoritys = authoritys;
    }
}
