package com.xinlong.shop.framework.service;

import com.xinlong.shop.framework.core.entity.SysFile;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * <p>
 * 系统上传文件表
 服务类
 * </p>
 *
 * @author Sylow
 * @since 2022-11-18
 */
public interface ISysFileService extends IService<SysFile> {

    void update(SysFile sysFile, Integer id);

    void delete(Integer id);

    /**
     * 上传文件
     * @param file 文件对象
     * @param scene 业务场景
     * @return
     */
    SysFile upload(MultipartFile file, String scene) throws IOException;
}
