package com.xinlong.shop.framework.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.framework.core.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 21:45 2022/5/31
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    List<SysRole> findAllRoles();

    List<SysRole> findRolesByAdminId(@Param("adminId") Integer adminId);

    List<SysRole> findRolesByPermission(@Param("permission") String permission);

}
