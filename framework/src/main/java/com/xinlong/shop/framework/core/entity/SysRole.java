package com.xinlong.shop.framework.core.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import org.springframework.security.core.GrantedAuthority;

import javax.validation.constraints.NotBlank;

/**
 * @Author Sylow
 * @Description 权限
 * @Date: Created in 21:35 2022/5/31
 */
@TableName("sys_role")
public class SysRole implements GrantedAuthority {

    private Integer id;

    @NotBlank(message = "角色名称不能为空")
    private String roleName;

    private String description;

    @NotBlank(message = "角色代码不能为空")
    private String roleCode;

    /**
     * 权限
     */
    private String permissions;

    @Override
    public String getAuthority() {
        return "ROLE_" + this.roleCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }
}
