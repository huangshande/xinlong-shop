package com.xinlong.shop.framework.weixin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 17:53 2023/1/3
 */
@Component
public class WeiXinPayConfig {

    @Value("${xinlong.weixin.pay.mchid}")
    private String mchid;

    @Value("${xinlong.weixin.pay.key}")
    private String key;

    @Value("${xinlong.weixin.pay.serial-number}")
    private String serialNumber;

    @Value("${xinlong.weixin.pay.api-key}")
    private String apiKey;

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
