package com.xinlong.shop.framework.core.model;

import com.xinlong.shop.framework.core.entity.SysAdmin;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 19:02 2022/7/20
 */
public class AdminInfoDTO extends SysAdmin {


    private  List<String> permissions;

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }
}
