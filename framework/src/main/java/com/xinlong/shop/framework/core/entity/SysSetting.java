package com.xinlong.shop.framework.core.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;

import javax.validation.constraints.NotBlank;
import java.util.Map;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 15:55 2023/6/19
 */
@TableName(value = "sys_setting", autoResultMap = true)
public class SysSetting {

    private Integer id;

    @NotBlank(message = "配置名称不能为空")
    private String settingName;

    @TableField(typeHandler = JacksonTypeHandler.class)
    private Map<String, Object> settings;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSettingName() {
        return settingName;
    }

    public void setSettingName(String settingName) {
        this.settingName = settingName;
    }

    public Map<String, Object> getSettings() {
        return settings;
    }

    public void setSettings(Map<String, Object> settings) {
        this.settings = settings;
    }
}
