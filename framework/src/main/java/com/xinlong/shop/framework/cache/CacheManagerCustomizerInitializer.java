package com.xinlong.shop.framework.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.cache.CacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author Sylow
 * @Description 自定义
 * @Date: Created in 20:36 2022/5/20
 */
@Configuration
@EnableCaching
public class CacheManagerCustomizerInitializer {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Bean
    public CacheManagerCustomizer<EhCacheCacheManager> ehcacheManagerCustomizer() {
        return cacheManager -> {
            //自定义设置
            logger.debug("ehcache cacheManager customizer.");
        };
    }

    // 为以后更换redis做注备
//    @Bean
//    public CacheManagerCustomizer<RedisCacheManager> redisManagerCustomizer() {
//        return cacheManager -> {
//            // 设置默认的过期时间
//            cacheManager.setDefaultExpiration(1800);
//            System.out.println("redis cacheManager customizer.");
//        };
//    }
//
//    /**
//     * 自己定义一个 RedisTemplate,改变默认的jdk序列化方式，转为json，方便之后进行key的删除，并取消警告信息
//      */
//    @Bean
//    public RedisTemplate redisTemplate(RedisConnectionFactory factory) {
//        RedisTemplate template = new RedisTemplate();
//        template.setConnectionFactory(factory);
//        // Json序列化配置
//        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
//        ObjectMapper om = new ObjectMapper();
//        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
//        jackson2JsonRedisSerializer.setObjectMapper(om);
//        // String 的序列化
//        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
//        // key采用String的序列化方式
//        template.setKeySerializer(stringRedisSerializer);
//        // hash的key也采用String的序列化方式
//        template.setHashKeySerializer(stringRedisSerializer);
//        // value序列化方式采用jackson
//        template.setValueSerializer(jackson2JsonRedisSerializer);
//        // hash的value序列化方式采用jackson
//        template.setHashValueSerializer(jackson2JsonRedisSerializer);
//        template.afterPropertiesSet();
//        return template;
//    }

}