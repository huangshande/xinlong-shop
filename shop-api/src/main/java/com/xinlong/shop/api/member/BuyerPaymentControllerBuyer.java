package com.xinlong.shop.api.member;

import com.xinlong.shop.api.common.BuyerBaseController;
import com.xinlong.shop.core.member.entity.MemberPayment;
import com.xinlong.shop.core.member.service.IMemberPaymentService;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import com.xinlong.shop.framework.util.StringUtil;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 会员收款方式 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-03-15
 */
@RestController
@RequestMapping("/buyer/member/payment")
public class BuyerPaymentControllerBuyer extends BuyerBaseController {

    private final IMemberPaymentService memberPaymentService;

    public BuyerPaymentControllerBuyer(IMemberPaymentService memberPaymentService, IMemberService memberService, TokenUtil tokenUtil) {
        super(memberService, tokenUtil);
        this.memberPaymentService = memberPaymentService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public R find(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        if (StringUtil.notEmpty(token)) {
            Member member = getMember(token);
            Integer memberId = member.getId();
            MemberPayment payment = this.memberPaymentService.findByMemberId(memberId);
            return R.success("操作成功", payment);
        }
        return R.error("操作失败");
    }

    /**
     * 根据会员id查询  一般查询卖家收款信息时使用
     * @param memberId
     * @return
     */
    @RequestMapping(value = "/{member_id}", method = RequestMethod.GET)
    public R find(@PathVariable("member_id") Integer memberId){
        MemberPayment payment = this.memberPaymentService.findByMemberId(memberId);
        return R.success("操作成功", payment);
    }

    /**
     * 更新银行卡信息
     * @param memberPayment
     * @return
     */
    @RequestMapping(value = "/bank", method = RequestMethod.POST)
    public R bank(@Valid @RequestBody MemberPayment memberPayment, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        if (StringUtil.notEmpty(token)) {
            Member member = getMember(token);
            Integer memberId = member.getId();
            memberPayment.setMemberId(memberId);
            MemberPayment payment = memberPaymentService.findByMemberId(memberPayment.getMemberId());
            this.memberPaymentService.update(memberPayment, payment.getId());
            return R.success("操作成功",memberPayment);
        }
        return R.error("操作失败");
    }

    /**
     * 更新支付宝信息
     * @param imgUrl 二维码图片
     * @return
     */
    @RequestMapping(value = "/alipay", method = RequestMethod.POST)
    public R alipay(String imgUrl, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        if (StringUtil.notEmpty(token)) {
            Member member = getMember(token);
            Integer memberId = member.getId();
            MemberPayment payment = memberPaymentService.findByMemberId(memberId);
            payment.setAlipay(imgUrl);
            this.memberPaymentService.update(payment, payment.getId());
            return R.success("操作成功");
        }
        return R.error("操作失败");
    }

    /**
     * 更新支付宝信息
     * @param imgUrl 二维码图片
     * @return
     */
    @RequestMapping(value = "/wxpay", method = RequestMethod.POST)
    public R wx(String imgUrl, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        if (StringUtil.notEmpty(token)) {
            Member member = getMember(token);
            Integer memberId = member.getId();
            MemberPayment payment = memberPaymentService.findByMemberId(memberId);
            payment.setWxpay(imgUrl);
            this.memberPaymentService.update(payment, payment.getId());
            return R.success("操作成功");
        }

        return R.error("操作失败");
    }


}
