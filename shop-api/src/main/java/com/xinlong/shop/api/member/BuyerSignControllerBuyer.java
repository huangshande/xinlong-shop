package com.xinlong.shop.api.member;

import com.xinlong.shop.api.common.BuyerBaseController;
import com.xinlong.shop.core.member.entity.MemberSign;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.core.member.service.IMemberSignService;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 会员签名记录表 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-04-14
 */
@RestController
@RequestMapping("/buyer/member/sign")
public class BuyerSignControllerBuyer extends BuyerBaseController {

    private final IMemberSignService memberSignService;

    public BuyerSignControllerBuyer(IMemberSignService memberSignService, IMemberService memberService, TokenUtil tokenUtil) {
        super(memberService, tokenUtil);
        this.memberSignService = memberSignService;
    }

    @RequestMapping(value = "/is-sign", method = RequestMethod.GET)
    public R isSign(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        Member member = getMember(token);
        MemberSign sign = this.memberSignService.findByMemberId(member.getId());
        if (sign == null) {
            return R.success(false);
        } else {
            // 暂时只要签名了 都通过
            return R.success(true);
        }

    }

    // 保存签名
    @RequestMapping(value = "", method = RequestMethod.POST)
    public R sign(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token, String signImg){
        Member member = getMember(token);
        this.memberSignService.addSign(member.getId(), signImg);
        return R.success();
    }


}
