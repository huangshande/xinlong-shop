package com.xinlong.shop.api.order;

import com.xinlong.shop.api.common.BuyerBaseController;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.core.order.entity.Cart;
import com.xinlong.shop.core.order.entity.dto.CartDTO;
import com.xinlong.shop.core.order.service.ICartService;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 购物车表 会员前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-04-21
 */
@RestController
@RequestMapping("/buyer/order/cart")
public class BuyerCartController extends BuyerBaseController {

    private final ICartService cartService;

    public BuyerCartController(IMemberService memberService, TokenUtil tokenUtil, ICartService cartService) {
        super(memberService, tokenUtil);
        this.cartService = cartService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public R list(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        Member member = getMember(token);
        List<CartDTO> list = this.cartService.findListByMemberId(member.getId());
        return R.success("操作成功", list);
    }


    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody Cart cart , @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        Member member = getMember(token);
        cart.setMemberId(member.getId());
        this.cartService.addCart(cart);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/num", method = RequestMethod.PUT)
    public R updateNum(Integer id, Integer num, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        Member member = getMember(token);
        this.cartService.updateNum(id, member.getId(), num);
        return R.success("操作成功");
    }


}
