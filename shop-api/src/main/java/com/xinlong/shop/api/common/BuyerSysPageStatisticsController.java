package com.xinlong.shop.api.common;

import cn.hutool.core.date.DateUtil;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.SysPageStatistics;
import com.xinlong.shop.framework.service.ISysPageStatisticsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 20:59 2023/5/10
 */
@RestController
@RequestMapping("/buyer/page-statistics")
public class BuyerSysPageStatisticsController {

    private final ISysPageStatisticsService sysPageStatisticsService;

    public BuyerSysPageStatisticsController(ISysPageStatisticsService sysPageStatisticsService) {
        this.sysPageStatisticsService = sysPageStatisticsService;
    }

    @RequestMapping(value = "/rush-purchase/visit", method = RequestMethod.POST)
    public R visitRushPurchase(Integer memberId, Integer rushPurchaseId) {
        int hour = DateUtil.thisHour(true);
        int minute = DateUtil.thisMinute();
        // 统计活动开始前的访问数
        if (hour < 11) {
            if (hour == 10 && minute > 40) {
                return R.success("不记录");
            }
            SysPageStatistics sysPageStatistics = new SysPageStatistics();
            sysPageStatistics.setRushPurchaseId(rushPurchaseId);
            sysPageStatistics.setMemberId(memberId);
            sysPageStatistics.setVisitTime(DateUtil.currentSeconds());
            sysPageStatistics.setPageId("rush_purchase");   // 活动页面
            sysPageStatisticsService.save(sysPageStatistics);
            return R.success("操作成功");
        } else {
            return R.success("不记录");
        }


    }
}
