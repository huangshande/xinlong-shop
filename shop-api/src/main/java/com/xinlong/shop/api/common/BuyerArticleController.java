package com.xinlong.shop.api.common;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.site.entity.Article;
import com.xinlong.shop.core.site.service.IArticleService;
import com.xinlong.shop.framework.common.R;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 文章列表 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-04-14
 */
@RestController
@RequestMapping("/buyer/article")
public class BuyerArticleController {

    private final IArticleService articleService;

    public BuyerArticleController(IArticleService articleService) {
        this.articleService = articleService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<Article> page = new Page<>(current, size);
        page = this.articleService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public R get(@PathVariable("id") Integer id){
        Article article = this.articleService.getById(id);
        return R.success("操作成功",article);
    }

}
