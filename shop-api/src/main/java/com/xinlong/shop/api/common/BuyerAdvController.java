package com.xinlong.shop.api.common;

import com.xinlong.shop.core.site.entity.Adv;
import com.xinlong.shop.core.site.service.IAdvService;
import com.xinlong.shop.framework.common.R;
import io.swagger.annotations.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author Sylow
 * @Description 买家广告轮播图API
 * @Date: Created in 10:54 2023/5/31
 */
@Validated      // get请求验证参数时需要
@RestController
@Api(tags = "买家广告轮播图API")
@RequestMapping("/buyer/adv")
public class BuyerAdvController {

    private final IAdvService advService;

    public BuyerAdvController(IAdvService advService) {
        this.advService = advService;
    }

    @ApiOperation(value = "获取广告列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "promotionCode", value = "广告位编码", required = true, paramType = "query"),
    })
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = Adv.class))
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = "application/json")
    public R list(@NotNull(message = "广告位编码不能为空") String promotionCode){
        List<Adv> advList = this.advService.listByAdvPromotionCode(promotionCode);
        return R.success("操作成功", advList);
    }



}
