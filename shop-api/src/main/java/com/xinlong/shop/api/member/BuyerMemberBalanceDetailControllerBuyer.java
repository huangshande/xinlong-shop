package com.xinlong.shop.api.member;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.api.common.BuyerBaseController;
import com.xinlong.shop.core.member.entity.MemberBalanceDetail;
import com.xinlong.shop.core.member.service.IMemberBalanceDetailService;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Sylow
 * @Description 会员余额明细
 * @Date: Created in 00:18 2023/04/07
 */
@RestController
@RequestMapping("/buyer/member/balance")
public class BuyerMemberBalanceDetailControllerBuyer extends BuyerBaseController {

    private final IMemberBalanceDetailService memberBalanceDetailService;

    public BuyerMemberBalanceDetailControllerBuyer(IMemberService memberService, TokenUtil tokenUtil, IMemberBalanceDetailService memberBalanceDetailService) {
        super(memberService, tokenUtil);
        this.memberBalanceDetailService = memberBalanceDetailService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        IPage<MemberBalanceDetail> page = new Page<>(current, size);
        Member member = getMember(token);
        page = this.memberBalanceDetailService.page(page, member.getId());
        return R.success("操作成功", page);
    }


}
