package com.xinlong.shop.api.common;

import com.xinlong.shop.core.goods.entity.vo.BuyerGoodsClassifyVO;
import com.xinlong.shop.core.goods.service.IGoodsClassifyService;
import com.xinlong.shop.framework.common.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 16:26 2022/12/2
 */
@RestController
@RequestMapping("/buyer/classify")
public class BuyerGoodsClassifyController {

    private final IGoodsClassifyService goodsClassifyService;

    public BuyerGoodsClassifyController(IGoodsClassifyService goodsClassifyService) {
        this.goodsClassifyService = goodsClassifyService;
    }

    @GetMapping(value = "/children")
    public R children() {
        List<BuyerGoodsClassifyVO> list = this.goodsClassifyService.findListChildren();
        return R.success(list);
    }
}
