package com.xinlong.shop.api.promotion;

import cn.hutool.core.date.DateUtil;
import com.xinlong.shop.api.common.BuyerBaseController;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.core.promotion.entity.RushPurchase;
import com.xinlong.shop.core.promotion.entity.vo.RushPurchasePrivilegeVO;
import com.xinlong.shop.core.promotion.service.IRushPurchaseDetailService;
import com.xinlong.shop.core.promotion.service.IRushPurchaseService;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 抢购活动
 * </p>
 *
 * @author Sylow
 * @since 2023-03-06
 */
@RestController
@RequestMapping("/buyer/promotion/rush-purchase")
public class BuyerRushPurchaseController extends BuyerBaseController {

    private final IRushPurchaseService rushPurchaseService;
    private final IRushPurchaseDetailService rushPurchaseDetailService;

    public BuyerRushPurchaseController(IRushPurchaseService rushPurchaseService, IMemberService memberService, TokenUtil tokenUtil, IRushPurchaseDetailService rushPurchaseDetailService) {
        super(memberService, tokenUtil);
        this.rushPurchaseService = rushPurchaseService;
        this.rushPurchaseDetailService = rushPurchaseDetailService;
    }

    /**
     * 获取当天活动场次
     */
    @RequestMapping(value = "/today", method = RequestMethod.GET)
    public R today() {
        Date date = new Date();
        // 获取到今天开始时间和结束时间
        Long startTime = DateUtil.beginOfDay(date).getTime() / 1000;
        Long endTime = DateUtil.endOfDay(date).getTime() / 1000;
        List<RushPurchase> list = this.rushPurchaseService.findByTime(startTime, endTime);
        return R.success("操作成功", list);
    }

    /**
     * 获取抢购预告
     */
    @RequestMapping(value = "/next", method = RequestMethod.GET)
    public R next() {
        Date date = new Date();
        // 今天的结束时间 就是预告的开始时间
        Long startTime = DateUtil.endOfDay(date).getTime() / 1000;
        // 预告结束时间30天之后
        Long endTime = startTime + 60 * 60 * 24 * 30;
        List<RushPurchase> list = this.rushPurchaseService.findByTime(startTime, endTime);
        return R.success("操作成功", list);
    }

    /**
     * 特权信息
     * @return
     */
    @RequestMapping(value = "/privilege-info", method = RequestMethod.GET)
    public R getPrivilegeInfo(Integer id, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        RushPurchasePrivilegeVO rushPurchasePrivilegeVO = new RushPurchasePrivilegeVO();
        if (id == 0) {
            return R.error("id不能为空");
        }
        Member member = getMember(token);

        Integer gradeId = member.getGradeId();
        /**
         * 暂时用id 判断 以后优化, 提前入场 除了普通会员都有
         */
        if (gradeId.intValue() != 1) {
            // 提前入场30秒
            int orderNum = this.rushPurchaseDetailService.getBuyerOrderCount(member.getId(), id);
            if (orderNum > 0) {
                rushPurchasePrivilegeVO.setEarlyEntryTime(0);
            } else {
                rushPurchasePrivilegeVO.setEarlyEntryTime(30);
            }

            // 拥有购物车特权
            if (gradeId.intValue() == 5) {
                rushPurchasePrivilegeVO.setIsHaveCart(1);
            }

            if (gradeId.intValue() == 6) {
                int hour = DateUtil.thisHour(true);
                // 9点之后有购物车特权
                if (hour  > 8) {
                    rushPurchasePrivilegeVO.setIsHaveCart(1);
                }
            }
        }

        return R.success("操作成功", rushPurchasePrivilegeVO);
    }

}
