package com.xinlong.shop.swagger;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 21:36 2022/5/17
 */
@Configuration
@EnableOpenApi // 开始swagger
@EnableKnife4j // 开启knife4j
public class SwaggerConfig {

    @Bean
    public Docket blindConfig(){
        return getDocket("盲盒模块", "com.xinlong.shop.api.blind");
    }

    @Bean
    public Docket goodsConfig(){
        return getDocket("商品模块", "com.xinlong.shop.api.goods");
    }

    @Bean
    public Docket memberConfig(){
        return getDocket("会员模块", "com.xinlong.shop.api.member");
    }

    @Bean
    public Docket orderConfig(){
        return getDocket("订单模块", "com.xinlong.shop.api.order");
    }
    @Bean
    public Docket promotionConfig(){
        return getDocket("活动模块", "com.xinlong.shop.api.promotion");
    }

    @Bean
    public Docket siteConfig(){
        return getDocket("站点模块", "com.xinlong.shop.api.site");
    }

    @Bean
    public Docket commonConfig(){
        return getDocket("公共模块", "com.xinlong.shop.api.common");
    }

    @Bean
    public Docket buyerConfig(){
        return new Docket(DocumentationType.OAS_30)
                .groupName("移动端API")
                .apiInfo(webApiInfo())
                .enable(true)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.xinlong.shop.api"))
                // 路径配置
                .paths(PathSelectors.ant("/buyer/**"))
                .build();
    }

    private Docket getDocket(String name, String packName) {
        return new Docket(DocumentationType.OAS_30)
                .groupName(name)
                .apiInfo(webApiInfo())
                .enable(true)
                .select()
                .apis(RequestHandlerSelectors.basePackage(packName))
                // 路径配置  过滤掉buyer下的接口
                .paths(PathSelectors.regex("^(?!/buyer).*$"))
                .build();
    }

    private ApiInfo webApiInfo(){

        return new ApiInfoBuilder()
                .title("信隆优创-电商系统API文档")
                .description("本文档描述了电商系统接口定义")
                .version("1.0")
                .contact(new Contact("Sylow", "https://gitee.com/Sylow", "777770@sina.com"))
                .build();
    }

}
