package com.xinlong.shop;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import springfox.documentation.oas.annotations.EnableOpenApi;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 23:55 2022/5/13
 */
@EnableOpenApi
@SpringBootApplication(exclude = {JacksonAutoConfiguration.class})  //解除jackson依赖
@MapperScan(basePackages = {"com.xinlong.shop.**.mapper"})
@EnableCaching()
public class ShopApiApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(ShopApiApplication.class);
        application.run(args);
    }

}
