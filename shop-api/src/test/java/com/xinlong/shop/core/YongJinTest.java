package com.xinlong.shop.core;

import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.extension.toolkit.SqlRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description 上线前佣金错误 批量处理
 * @Date: Created in 14:53 2023/5/8
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class YongJinTest {


    @Test
    public void yongJinTest() {
        List<Map<String, Object>> maps = SqlRunner.db().selectList("select ps.*,m.nickname,m.mobile from s_put_sale_order_copy1 ps LEFT JOIN s_member m on ps.member_id = m.id where ps.`status` = 1;");

        for(Map order : maps) {

            BigDecimal oldServicePrice = (BigDecimal) order.get("service_price");
            BigDecimal originalPrice = (BigDecimal) order.get("original_price");
            BigDecimal servicePrice = getServicePrice(originalPrice, 2.5);
            servicePrice = servicePrice.setScale(2, BigDecimal.ROUND_HALF_UP);

            if (servicePrice.subtract(oldServicePrice).compareTo(BigDecimal.ZERO) != 1) {
                System.out.println("退款");
                // 退款

            } else {
                System.out.println("会员补款");

            }
            String orderSn = order.get("order_sn").toString();

            System.out.println(order.get("order_sn"));
            System.out.println(order.get("nickname"));
            System.out.println("原服务费：" + oldServicePrice);
            System.out.println("新服务费：" + servicePrice);
            System.out.println("差价：" + servicePrice.subtract(oldServicePrice));
            BigDecimal newPrice = NumberUtil.mul(originalPrice, 1.04);
            newPrice = newPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
            SqlRunner.db().update("update s_put_sale_order set service_price = " + servicePrice + ", new_price="+ newPrice +" where order_sn = '" + orderSn+"'");


            String sql = "select* from s_member_balance_detail where source = '" + orderSn + "'";
            Map<String, Object> balance = SqlRunner.db().selectOne(sql);
            if (balance == null) {
                System.out.println("没有佣金");
                continue;
            }
            // 佣金
            BigDecimal money = (BigDecimal) balance.get("price");
            System.out.println("旧佣金：" + money);
            BigDecimal newMoney = NumberUtil.div(NumberUtil.mul(servicePrice, 20),100);
            newMoney = newMoney.setScale(2, BigDecimal.ROUND_HALF_UP);
            System.out.println("新佣金：" + newMoney);
            BigDecimal moneyDifference = newMoney.subtract(money);
            System.out.println("佣金差价：" + moneyDifference);
            SqlRunner.db().update("update s_member_balance_detail set price = " + newMoney + " where source = '" + orderSn + "'");


            String setSql = "";
            if (moneyDifference.compareTo(BigDecimal.ZERO) == 1) {
                setSql = "balance = balance + " + moneyDifference;
            } else {
                setSql = "balance = balance - " + moneyDifference.abs();
            }


            SqlRunner.db().update("update s_member set " + setSql + " where id = " + balance.get("member_id"));

        }
    }

    /**
     * 获取服务费金额
     * @param price 商品价格
     * @param putSalePrice 服务费百分比
     * @return
     */
    private BigDecimal getServicePrice(BigDecimal price, Double putSalePrice){

        // 配置中是整数 需要除以100
        Double divisor = 100D;
        putSalePrice = NumberUtil.div(putSalePrice, divisor, 3);

        // 服务费=新价格*百分比
        BigDecimal servicePrice = NumberUtil.mul(price, putSalePrice);

        return servicePrice;
    }

}
