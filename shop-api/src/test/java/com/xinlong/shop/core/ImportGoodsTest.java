package com.xinlong.shop.core;

import cn.hutool.http.HttpUtil;
import com.baomidou.mybatisplus.extension.toolkit.SqlRunner;
import com.xinlong.shop.core.goods.entity.Goods;
import com.xinlong.shop.core.goods.service.IGoodsService;
import com.xinlong.shop.framework.core.entity.SysFile;
import com.xinlong.shop.framework.service.ISysFileService;
import org.apache.http.entity.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description 导入老系统商品测试用例
 * @Date: Created in 16:06 2023/4/19
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ImportGoodsTest {

    @Resource
    private IGoodsService goodsService;

    @Resource
    private ISysFileService sysFileService;

    @Test
    public void importTest() {

        List<Map<String, Object>> maps = SqlRunner.db().selectList("select g.*,i.url from tea_goods g left join tea_images i on g.image_id = i.id");
        List<Goods> goodsList = new ArrayList<>();
        for(Map map : maps) {
            try {
                //System.out.println(map.get("name"));
                String goodsName = map.get("name").toString();
                String goodsSn = map.get("bn").toString();
                String goodsDesc = map.get("brief").toString();
                BigDecimal goodsPrice = (BigDecimal) map.get("price");
                String imgUrl = map.get("url").toString();
                String goodsIntro = map.get("intro").toString();
                Long createTime = Long.parseLong(map.get("utime").toString());
                String goodsSkuSn = "";
                if (goodsName.startsWith("C")) {
                    goodsSkuSn = goodsName.substring(0, 7);
                } else {
                    goodsSkuSn = goodsSn;
                }

                File imgFile = HttpUtil.downloadFileFromUrl(imgUrl, "/Users/Sylow/Downloads/goodsImgTemp/" + goodsSn + ".jpg");

                FileInputStream fileInputStream = new FileInputStream(imgFile);
                MultipartFile multipartFile = new MockMultipartFile(imgFile.getName(), imgFile.getName(),
                        ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStream);
                SysFile sysFile = sysFileService.upload(multipartFile, "goods");

                Goods goods = new Goods();
                goods.setGoodsName(goodsName);
                goods.setGoodsSn(goodsSn);
                goods.setGoodsDesc(goodsDesc);
                goods.setGoodsPrice(goodsPrice);
                goods.setGoodsSkuSn(goodsSkuSn);
                goods.setGoodsIntro(goodsIntro);
                goods.setCreateTime(createTime);
                goods.setGoodsImgs(new ArrayList<String>() {{
                    add(sysFile.getUrl());
                }});
                // 俩数据定死
                goods.setGoodsClassifyId(704);
                // 老系统id 先暂时存放在品牌id里
                Integer oldId = Integer.parseInt(map.get("id").toString());
                goods.setGoodsBrandId(oldId);

                goods.setGoodsStatus(1);

                goodsList.add(goods);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

        goodsService.saveBatch(goodsList);
    }


}
