package com.xinlong.shop.core;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.toolkit.SqlRunner;
import com.xinlong.shop.core.goods.entity.Goods;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.core.promotion.entity.RushPurchase;
import com.xinlong.shop.core.promotion.entity.RushPurchaseDetail;
import com.xinlong.shop.core.promotion.service.IRushPurchaseDetailService;
import com.xinlong.shop.core.promotion.service.IRushPurchaseService;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.util.StringUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description 导入老系统抢购测试用例
 * @Date: Created in 17:14 2023/4/26
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ImportRushPurchaseTest {
    private static final Logger logger = LoggerFactory.getLogger(ImportRushPurchaseTest.class);

    @Resource
    private IRushPurchaseDetailService rushPurchaseDetailService;

    @Resource
    private IRushPurchaseService rushPurchaseService;

    @Resource
    private IMemberService memberService;

    @Test
    public void importTest() {
        String ruleId = "2023061901";
        // 活动数据
        Map<String, Object> ruleMap = SqlRunner.db().selectOne("select * from tea_promotion where id = " + ruleId);

        RushPurchase rushPurchase = new RushPurchase();

        rushPurchase.setActivityId(ruleId);
        rushPurchase.setActivityName(ruleMap.get("name").toString());
        rushPurchase.setStartTime(Long.parseLong(ruleMap.get("stime").toString()));
        rushPurchase.setEndTime(Long.parseLong(ruleMap.get("etime").toString()));
        rushPurchase.setCreateTime(DateUtil.currentSeconds());

        rushPurchaseService.save(rushPurchase);

        System.out.println(rushPurchase.getId());

        // 老会员数据
        List<Map<String, Object>> oldMemberMaps = SqlRunner.db().selectList("select * from tea_user");

        // 老活动详情数据
        List<Map<String, Object>> detailList = SqlRunner.db().selectList("select * from tea_group_goods where rule_id = " + ruleId);

        List<RushPurchaseDetail> rpdList = new ArrayList<>();

        for(Map map : detailList) {

            Integer oldUserId = Integer.parseInt(map.get("user_id").toString());
            Integer oldGoodsId = Integer.parseInt(map.get("goods_id").toString());
            BigDecimal price = new BigDecimal(map.get("price").toString());
            BigDecimal oldPrice = new BigDecimal(map.get("old_price").toString());

            Member member = getMember(oldMemberMaps, oldUserId);
            Goods goods = getGoods(oldGoodsId);

            RushPurchaseDetail rpd = new RushPurchaseDetail();
            rpd.setRushPurchaseId(rushPurchase.getId());
            rpd.setActivityId(ruleId);
            rpd.setGoodsId(goods.getId());
            rpd.setGoodsName(goods.getGoodsName());
            rpd.setSellerId(member.getId());
            rpd.setSellerName(member.getNickname());
            rpd.setSellerMobile(member.getMobile());
            rpd.setCreateTime(DateUtil.currentSeconds());
            rpd.setPrice(price);
            rpd.setOriginalPrice(oldPrice);

            rpd.setOrderSn("P" + rpd.getActivityId() + StringUtil.random(10000) + rpd.getGoodsId());
            rpdList.add(rpd);
            //rpd.set
            //rushPurchaseDetailService.save(rushPurchase.getId(), Integer.parseInt(map.get("goods_id").toString()));
        }

        rushPurchaseDetailService.saveBatch(rpdList);
    }

    /**
     * 通过老系统id 和数据 得到会员手机号 得到会员
     * @param maps
     * @param memberId
     * @return
     */
    private Member getMember(List<Map<String, Object>> maps, Integer memberId) {
        //List<Map<String, Object>> maps = SqlRunner.db().selectList("select * from tea_user");
        for(Map map : maps) {
            if (memberId.equals(Integer.parseInt(map.get("id").toString()))) {
                String mobile = map.get("mobile").toString();
                Member member = memberService.findByMobile(mobile);
                return member;
            }
        }
        return null;
    }

    /**
     * 通过老系统商品id 得到商品信息
     * @param goodsId
     * @return
     */
    private Goods getGoods(Integer goodsId) {
        // 导入老数据时  老系统商品id是临时存放在品牌id里的
        Map<String, Object> goodsMap = SqlRunner.db().selectOne("select * from s_goods where goods_brand_id = " + goodsId);
        Goods goods = new Goods();
        goods.setGoodsName(goodsMap.get("goods_name").toString());
        goods.setId(Integer.parseInt(goodsMap.get("id").toString()));
        return goods;
    }

}
