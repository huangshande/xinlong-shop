package com.xinlong.shop.core;

import com.xinlong.shop.core.logistics.ILogisticsPlugin;
import com.xinlong.shop.core.logistics.LogisticsPluginFactory;
import com.xinlong.shop.core.logistics.entity.LogisticsTraces;
import com.xinlong.shop.core.site.entity.Logistics;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 00:32 2023/7/28
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class LogisticsQueryTest {

    @Autowired
    private LogisticsPluginFactory logisticsPluginFactory;

    @Test
    public void queryTest() {
        ILogisticsPlugin logisticsPlugin = logisticsPluginFactory.currentPlugin();
        Logistics logistics = new Logistics();
        logistics.setLogisticsName("申通快递");
        logistics.setCode("shentong");
        LogisticsTraces logisticsTraces = logisticsPlugin.instantQuery(logistics, "773222490431850", "15911100004");
        //System.out.println(logisticsTraces);
        for (Map map : logisticsTraces.getTraces()) {
            String time = (String) map.get("time");
            String content = (String) map.get("content");
            System.out.println(time);
            System.out.println(content);
        }
    }
}
