package com.xinlong.shop.generator.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 22:46 2022/5/17
 */
public class GeneratorVO {


    /**
     * 自动生成表名集合
     */
    @NotNull(message = "表名不能为空")
    @Size(min = 1, message = "表名集合至少包含一个元素")
    private List<String> tables;
    /**
     * 作者
     */
    @NotBlank(message = "作者必填")
    private String author;

    @NotBlank(message = "包名必填")
    private String packageConfig;

    @NotBlank(message = "模块名必填")
    private String module;

    private Map<String,List> fields;

    /**
     * 逻辑删除字段名
     */
    // 暂时不用
//    @NotBlank(message = "逻辑删除字段名必填")
//    private String deleteColumnName;

    public String getPackageConfig() {
        return packageConfig;
    }

    public void setPackageConfig(String packageConfig) {
        this.packageConfig = packageConfig;
    }

    public Map<String, List> getFields() {
        return fields;
    }

    public void setFields(Map<String, List> fields) {
        this.fields = fields;
    }

    /**
     * 乐观锁字段名
     */
//    private String versionColumnName;


    public List<String> getTables() {
        return tables;
    }

    public void setTables(List<String> tables) {
        this.tables = tables;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }
}
