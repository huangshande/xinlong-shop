package com.xinlong.shop.generator;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 01:00 2022/6/14
 */
@ComponentScan(basePackages = "com.xinlong.shop")
@SpringBootApplication(exclude = {JacksonAutoConfiguration.class})  //解除jackson依赖
@MapperScan(basePackages = "com.xinlong.shop.**.mapper")
@EnableCaching()
public class GeneratorApplication {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(GeneratorApplication.class);
        // 允许重复注册
        application.setAllowBeanDefinitionOverriding(true);
        application.run(args);
    }

}
