package com.xinlong.shop.generator.entity;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 17:25 2022/6/12
 */
public class FieldInfo {

    private String fieldName;

    private String fieldComment;

    private String dataType;

    // 是否允许查询字段
    private boolean queryField;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldComment() {
        return fieldComment;
    }

    public void setFieldComment(String fieldComment) {
        this.fieldComment = fieldComment;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public boolean isQueryField() {
        return queryField;
    }

    public void setQueryField(boolean queryField) {
        this.queryField = queryField;
    }
}
