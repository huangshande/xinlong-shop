package com.xinlong.shop.generator.service.impl;

import com.xinlong.shop.generator.config.GeneratorConfig;
import com.xinlong.shop.generator.entity.FieldInfo;
import com.xinlong.shop.generator.entity.TableInfo;
import com.xinlong.shop.generator.mapper.FieldMapper;
import com.xinlong.shop.generator.mapper.TableMapper;
import com.xinlong.shop.generator.model.GeneratorVO;
import com.xinlong.shop.generator.service.IGeneratorService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 22:30 2022/6/12
 */
@Component
public class GeneratorServiceImpl implements IGeneratorService {

    private final GeneratorConfig generatorConfig;

    private final TableMapper tableMapper;

    private final FieldMapper fieldMapper;

    public GeneratorServiceImpl(GeneratorConfig generatorConfig, TableMapper tableMapper, FieldMapper fieldMapper) {
        this.generatorConfig = generatorConfig;
        this.tableMapper = tableMapper;
        this.fieldMapper = fieldMapper;
    }

    @Override
    public Map findFieldsByTableName(String[] tableNames) {

        Map<String,List> fieldInfo = new HashMap<>();
        for(String tableName : tableNames) {
            List<FieldInfo> list = fieldMapper.findFields(tableName);
            fieldInfo.put(tableName, list);
        }

        return fieldInfo;
    }

    @Override
    public List<TableInfo> findTablesByTableName(String tableName) {
        List<TableInfo> list = tableMapper.findTables(tableName);
        return list;
    }

    @Override
    public String execute(GeneratorVO generatorVO) {
        generatorConfig.execute(generatorVO);
        return null;
    }
}
