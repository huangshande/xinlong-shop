package ${package.Controller};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
<#--<#if restControllerStyle>-->
<#--import org.springframework.web.bind.annotation.RestController;-->
<#--<#else>-->
<#--import org.springframework.stereotype.Controller;-->
<#--</#if>-->
import com.xinlong.shop.framework.common.R;
import ${package.Entity}.${entity};
import ${package.Service}.${table.serviceName};
import javax.validation.Valid;
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>

    <#-- 声明 -->
    <#assign serviceName = table.serviceName?substring(1)?uncap_first />
    private final ${table.serviceName} ${serviceName};

    public ${table.controllerName}(${table.serviceName} ${serviceName}) {
        this.${serviceName} = ${serviceName};
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<${entity}> page = new Page<>(current, size);
        page = this.${serviceName}.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody ${entity} ${entity?uncap_first} ){
        this.${serviceName}.save(${entity?uncap_first});
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody ${entity} ${entity?uncap_first}, @PathVariable("id") Integer id){
        this.${serviceName}.update(${entity?uncap_first}, id);
        return R.success("操作成功",${entity?uncap_first});
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.${serviceName}.delete(id);
        return R.success("删除成功");
    }

}
</#if>
